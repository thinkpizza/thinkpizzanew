package com.waycreon.waycreon.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.waycreon.thinkpizza.R;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class Utils {

    public static AlertDialog mAlertDialog;
    public static TranslateAnimation top;
    public static TranslateAnimation bottom;

    // public static void SetFont(TextView textView, Activity activity) {
    // Typeface font = Typeface.createFromAsset(activity.getAssets(),
    // "lucida.ttf");
    // textView.setTypeface(font);
    // }
    //
    // public static void SetFont(Button button, Activity activity) {
    // Typeface font = Typeface.createFromAsset(activity.getAssets(),
    // "lucida.ttf");
    // button.setTypeface(font);
    // }
    //
    // public static void SetFont(EditText editText, Activity activity) {
    // Typeface font = Typeface.createFromAsset(activity.getAssets(),
    // "lucida.ttf");
    // editText.setTypeface(font);
    // }

    // public static ProgressDialog SetProgressBar(ProgressDialog mDialog,
    // Activity activity) {
    // mDialog = ProgressDialog.show(activity, "", "");
    // mDialog.setContentView(R.layout.progress);
    // mDialog.setCancelable(false);
    // mDialog.setIndeterminate(false);
    // mDialog.show();
    // return mDialog;
    // }

    // public static ProgressDialog SetProgressBar1(ProgressDialog mDialog,
    // Activity activity) {
    // // mDialog=ProgressDialog.show(activity, "","");
    // mDialog.setContentView(R.layout.progress);
    // mDialog.setCancelable(true);
    // mDialog.setIndeterminate(false);
    // mDialog.show();
    // return mDialog;
    // }

    // public static void CopyStream(InputStream is, OutputStream os) {
    // final int buffer_size = 1024;
    // try {
    // byte[] bytes = new byte[buffer_size];
    // for (;;) {
    // int count = is.read(bytes, 0, buffer_size);
    // if (count == -1)
    // break;
    // os.write(bytes, 0, count);
    // }
    // } catch (Exception ex) {
    // }
    // }

    public static void SetDiolog(Activity activity, String message) {
        mAlertDialog = new AlertDialog.Builder(activity).create();
        mAlertDialog.setMessage(message);
        // mAlertDialog.setContentView(R.layout.fragment_order);
        mAlertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mAlertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mAlertDialog.show();
    }

    public final static Pattern EMAIL_ADDRESS_PATTERN = Pattern
            .compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static boolean isConnectingToInternet(Context mContext) {
        boolean isInternet = false;
        try {
            ConnectivityManager connect = null;
            connect = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);


                if ((resultMobile != null && resultMobile.isConnected())
                        || (resultWifi != null && resultWifi.isConnected())) {
                    isInternet = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isInternet;
    }

    public static void TopAnimation() {
        // System.out.println("Inside startAnimation()");
        top = new TranslateAnimation(0, 0, 300, 0);
        top.setDuration(500);
        top.setRepeatCount(0);
        top.setInterpolator(new AccelerateInterpolator());
        top.setFillAfter(true);

    }

    public static void BottomAnimation() {
        // System.out.println("Inside startAnimation()");
        bottom = new TranslateAnimation(0, 0, 0, 300);
        bottom.setDuration(500);
        bottom.setRepeatCount(0);
        bottom.setInterpolator(new AccelerateInterpolator());
        bottom.setFillAfter(false);
    }

    public static void HideLayout(final RelativeLayout mLayout) {
        Utils.bottom.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayout.setVisibility(View.GONE);
            }
        });
    }

    public static void HideLayout(final LinearLayout mLayout) {
        Utils.bottom.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayout.setVisibility(View.GONE);
            }
        });
    }

    public static void ShowSoftKeyBoard(Context mContext, AutoCompleteTextView mAutoCompleteTextView) {
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
// only will trigger it if no physical keyboard is open
        inputMethodManager.showSoftInput(mAutoCompleteTextView, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void ShowSoftKeyBoard(Context mContext, EditText mEditText) {
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
// only will trigger it if no physical keyboard is open
        inputMethodManager.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void HideSoftKeyBoard(Context mContext, AutoCompleteTextView mAutoCompleteTextView) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//txtName is a reference of an EditText Field
        imm.hideSoftInputFromWindow(mAutoCompleteTextView.getWindowToken(), 0);
//    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public static String RemoveLastComma(String tmp) {
        System.out.println("Selected String(B): " + tmp);
        int endIndex = tmp.lastIndexOf(", ");
        if (endIndex != -1) {
            tmp = tmp.substring(0, endIndex);
        }
        System.out.println("Selected String(A): " + tmp);
        return tmp;
    }

    public static String RemoveLastComma_ID(String tmp) {
        System.out.println("Selected ID(B): " + tmp);
        int endIndex = tmp.lastIndexOf(",");
        if (endIndex != -1) {
            tmp = tmp.substring(0, endIndex);
        }
        System.out.println("Selected Id(A): " + tmp);
        return tmp;
    }


/*    public static void setListViewHeightBasedOnChildren(ListView mListViewOrderedItems) {
        ListAdapter listAdapter = mListViewOrderedItems.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListViewOrderedItems.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        Log.e("setListViewHeightBasedOnChildren()", "List Size: " + listAdapter.getCount());
        *//*for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, mListViewOrderedItems);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                System.out.println("Height of view:" + view.getLayoutParams().height);
            }
            System.out.println("measured Height of view(B):" + view.getMeasuredHeight());
            System.out.println("Height of view(B):" + view.getLayoutParams().height);
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += view.getMeasuredHeight();
            System.out.println("Height of view(A):" + view.getLayoutParams().height);
            System.out.println("totalHeight(in loop):" +totalHeight);
        }*//*
        if (mListViewOrderedItems.getCount() == 0) {
            System.out.println("in if");
            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
            System.out.println("Height of view:" + view.getLayoutParams().height);
        } else {
            System.out.println("in else");
            totalHeight = view.getMeasuredHeight() * mListViewOrderedItems.getCount();
            System.out.println("measured Height of view(B):" + view.getMeasuredHeight());
            System.out.println("Height of view(B):" + view.getLayoutParams().height);
        }
        Log.e("setListViewHeightBasedOnChildren()", "Total Hieght (out of loop): " + totalHeight);
        ViewGroup.LayoutParams params = mListViewOrderedItems.getLayoutParams();
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        params.height = totalHeight + (mListViewOrderedItems.getDividerHeight() * (listAdapter.getCount() - 1));
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        mListViewOrderedItems.setLayoutParams(params);
        mListViewOrderedItems.requestLayout();
    }*/

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    // my method to convert uri to path of image
    public static String getRealPathFromURI(Uri contentURI, Context mContext) {

        String result;

        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null,
                null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static Bitmap getImageBitmap(String path) throws IOException {
        // Allocate files and objects outside of timingoops

        Bitmap bmp = null;
        try {
            File file = new File(path);
            RandomAccessFile in = new RandomAccessFile(file, "rws");
            final FileChannel channel = in.getChannel();
            final int fileSize = (int) channel.size();
            final byte[] testBytes = new byte[fileSize];
            final ByteBuffer buff = ByteBuffer.allocate(testBytes.length);
            final byte[] buffArray = buff.array();
            // @SuppressWarnings("unused")
            // final int buffBase = buff.arrayOffset();

            // Read from channel into buffer, and batch read from buffer to byte
            // array;
            long time1 = System.currentTimeMillis();
            channel.position(0);
            channel.read(buff);
            buff.flip();
            buff.get(testBytes, 0, testBytes.length);

            bmp = Bitmap_process(buffArray);
            long time2 = System.currentTimeMillis();
            System.out.println("Time taken to load: " + (time2 - time1) + " ms");
            freeMemory();
        } catch (BufferUnderflowException e) {

        }
        return bmp;
    }

    public static Bitmap Bitmap_process(byte[] buffArray) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inDither = false; // Disable Dithering mode
        options.inPurgeable = true; // Tell to gc that whether it needs free
        // memory, the Bitmap can be cleared
        options.inInputShareable = true; // Which kind of reference will be used
        // to recover the Bitmap data after
        // being clear, when it will be used
        // in the future
        options.inTempStorage = new byte[32 * 1024]; // Allocate some temporal
        // memory for decoding

        options.inSampleSize = 1;

        Bitmap imageBitmap = BitmapFactory.decodeByteArray(buffArray, 0,
                buffArray.length, options);
        return imageBitmap;
    }

    public static File getFileUri() {
        File mFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "thinkpizza");
        if (!mFile.exists()) {
            if (!mFile.mkdir()) {
                Log.d("Utils.java", "getFilePath() Failed to create the directory.");
                return null;
            }
        }
        File file = new File(mFile.getPath() + File.separator + "THINKPIZZA_" + (System.currentTimeMillis()) + ".png");
        return file;
    }

    public static void freeMemory() {
        Log.e("called", "freeMemory() called");
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public static void showMyToast(Activity mActivity, String msg, int time) {
        LayoutInflater inflater = mActivity.getLayoutInflater();
        View mView1 = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) mActivity.findViewById(R.id.custom_toast_layout_id));
        Toast mToast = new Toast(mActivity);
        mToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        mToast.setDuration(time);
        mToast.setView(mView1);
        TextView mTextViewMessage;
        mTextViewMessage = (TextView) mView1.findViewById(R.id.custom_toast_message);
        mTextViewMessage.setText(msg);
        mToast.show();
    }

    public static void setShowHidePassword(EditText mEditText, ImageView mImageView) {
        System.out.println("Utils.setShowHidePassword() clicked: isPasswordVisible: " + mEditText.getInputType());
        if (mEditText.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)) {

            mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mImageView.setImageResource(R.drawable.e_gray);

        } else {
            mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mImageView.setImageResource(R.drawable.e_red);
        }
        mEditText.setSelection(mEditText.getText().length());
    }

    public static void setDefaultImage(EditText mEditText, ImageView mImageView) {

        mEditText.setText("");
        mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mImageView.setImageResource(R.drawable.e_gray);
    }

    public static void showSoftKeyBoard(Context mContext, EditText mEditText) {
        Log.e("showSoftKeyBoard()", "showSoftKeyBoard() method called");
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    /* public static void hideSoftKeyBoard(Activity mActivity*//*, EditText mEditText*//*) {
        Log.e("hideSoftKeyBoard()", "hideSoftKeyBoard() method called");
//        ((InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
    */
    public static void hide_keyboard(Activity activity) {
        Log.e("hide_keyboard()", "hide_keyboard() method called");
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showInternetDialog(final Context mContext) {

        Log.e("ShowInternetDialog()", "ShowInternetDialog() method called.");

        final Dialog mDialogNoInternet = new Dialog(mContext);
        mDialogNoInternet.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogNoInternet.setContentView(R.layout.dialog_no_internet);

        mDialogNoInternet.findViewById(R.id.dialog_confirm_text_gotosetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDialogNoInternet.dismiss();
//                new Intent(android.provider.Settings.ACTION_SETTINGS);
                mContext.startActivity(new Intent(Settings.ACTION_SETTINGS));

            }
        });
        mDialogNoInternet.findViewById(R.id.dialog_confirm_text_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDialogNoInternet.dismiss();
            }
        });

        mDialogNoInternet.show();
    }

    public static boolean isTimeInBetween(Time open, Time close) {

        Time curreTime = new Time(Calendar.getInstance().getTime().getTime());

        System.out.println("Current Date: " + curreTime.toString());//18:30
        System.out.println("open Time: " + open.toString());//9.45
        System.out.println("close Time: " + close.toString());//18.45
        if (curreTime.getHours() > open.getHours() && curreTime.getHours() < close.getHours()) {

            System.out.println("Yes Current time is within range.");
            return true;

        } else if ((curreTime.getHours() == open.getHours()) && (curreTime.getMinutes() >= open.getHours())
                && curreTime.getHours() < close.getHours()) {

            System.out.println("Before hour equal Yes Current time is within range.");
            return true;

        } else if (curreTime.getHours() >= open.getHours()
                && ((curreTime.getHours() == close.getHours()) && (curreTime.getMinutes() <= close.getMinutes()))
                ) {

            System.out.println("After hour equal Yes Current time is not within range.");
            return true;

        } else {

            System.out.println("No Current time is not within range.");
            return false;
        }
    }


    public static double CalculationByDistance(double initialLat, double initialLong, double finalLat, double finalLong) {

//        double latDiff = finalLat - initialLat;
//        double longDiff = finalLong - initialLong;
//        double earthRadius = 6371; //In Km if you want the distance in km
//
//        double distance = 2 * earthRadius * Math.asin(Math.sqrt(Math.pow(Math.sin(latDiff / 2.0), 2) +
//                Math.cos(initialLat) * Math.cos(finalLat) * Math.pow(Math.sin(longDiff / 2), 2)));
        float[] dist = new float[2];
        Location.distanceBetween(initialLat, initialLong, finalLat, finalLong, dist);
        System.out.println("CalculationByDistance() Distance in Meters: " + dist[0]);
        System.out.println("CalculationByDistance() Distance in KM: " + dist[0] * 0.001);
        return dist[0] * 0.001;

    }

    public static double getDoubleFormat(Double price) {
        System.out.println("getDoubleFormat: " + Double.parseDouble(new DecimalFormat("##.##").format(price)));
        return Double.parseDouble(new DecimalFormat("##.##").format(price));
    }


    public static void changeLayoutHeight(FragmentActivity activity, View mRelativeLayoutMainAddressLayout) {

        // int h = mRelativeLayoutMainAddressLayout.getHeight();
        int h = mRelativeLayoutMainAddressLayout.getHeight();
        int w = mRelativeLayoutMainAddressLayout.getWidth();
        int change_height = (int) activity.getResources().getDimension(R.dimen.mainheight_address_layout);
        FrameLayout.LayoutParams layoutParams;

        System.out.println("change_height : " + change_height);
        System.out.println("mRelativeLayoutMainAddressLayout Width: " + w);
        System.out.println("mRelativeLayoutMainAddressLayout Height: " + h);

//        System.out.println("Heightof mListViewFavoriteAddresses: " + mListViewFavoriteAddresses.getHeight());
//        System.out.println("Heightof mDialogFavorite: " + mDialogFavorite.getWindow().getDecorView().getHeight());

        if (h <= change_height) {
            System.out.println("h <= change_height ");
            layoutParams = new FrameLayout.LayoutParams(w, FrameLayout.LayoutParams.WRAP_CONTENT);
            mRelativeLayoutMainAddressLayout.setLayoutParams(layoutParams);
            System.out.println("Heightof mRelativeLayoutMainAddressLayout: " + mRelativeLayoutMainAddressLayout.getHeight());
        } else {
            System.out.println("h <= change_height  Else ");
            layoutParams = new FrameLayout.LayoutParams(w, change_height);
            mRelativeLayoutMainAddressLayout.setLayoutParams(layoutParams);
            System.out.println("Heightof mRelativeLayoutMainAddressLayout: " + mRelativeLayoutMainAddressLayout.getHeight());
        }
//        System.out.println("Heightof mDialogFavorite width: " + mDialogFavorite.getWindow().getDecorView().getWidth());
//        System.out.println("Heightof mDialogFavorite height: " + mDialogFavorite.getWindow().getDecorView().getHeight());

    }


    public static String getOpenCloseTime(String open_days,int openclose) {
        String days[] = open_days.split(",");
        Date d = new Date();
        CharSequence s = DateFormat.format("MMMM d, yyyy ", d.getTime());
        System.out.println("getOpenTime() Day: " + DateFormat.format("EEEE", new Date()));

        switch (DateFormat.format("EEEE", new Date()).toString()) {
            case "Monday":
                System.out.println("Monday: " + days[0]);
                System.out.println("Time: " + days[0].split(" - ")[openclose]);
                return days[0].split(" - ")[openclose];
//            break;
            case "Tuesday":
                System.out.println("Tuesday: " + days[1]);
                System.out.println("Time: " + days[1].split(" - ")[openclose]);
                return days[1].split(" - ")[openclose];
//            break;
            case "Wednesday":
                System.out.println("Wednesday: " + days[2]);
                System.out.println("Time: " + days[2].split(" - ")[openclose]);
                return days[2].split(" - ")[openclose];
//            break;
            case "Thursday":
                System.out.println("Thurday: " + days[3]);
                System.out.println("Time: " + days[3].split(" - ")[openclose]);
                return days[3].split(" - ")[openclose];
//            break;
            case "Friday":
                System.out.println("Friday: " + days[4]);
                System.out.println("Time: " + days[4].split(" - ")[openclose]);
                return days[4].split(" - ")[openclose];
//            break;
            case "Saturday":
                System.out.println("Saturday: " + days[5]);
                System.out.println("Time: " + days[5].split(" - ")[openclose]);
                return days[5].split(" - ")[openclose];
//            break;
            case "Sunday":
                System.out.println("Sunday: " + days[6]);
                System.out.println("Time: " + days[6].split(" - ")[openclose]);
                return days[6].split(" - ")[openclose];
//            break;
        }
        return "";
    }

}