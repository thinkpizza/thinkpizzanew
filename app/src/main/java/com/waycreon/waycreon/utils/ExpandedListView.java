package com.waycreon.waycreon.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ListView;

/**
 * Created by abcd on 9/15/2015. pradip
 */
public class ExpandedListView extends ListView {

    private android.view.ViewGroup.LayoutParams params;
    private int old_count = 0;

    public ExpandedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if (getCount() != old_count) {
            params = getLayoutParams();
            old_count = getCount();
            int totalHeight = 0;
            Log.e("ExpandListView.onDraw()", "totalHeight(B): " + totalHeight);
//            Log.e("ExpandListView.onDraw()", "Measuredheight: " + getMeasuredHeight());
//            Log.e("ExpandListView.onDraw()", "Count: " + getCount());
           /* for (int i = 0; i < getCount(); i++) {
//                Log.e("ExpandListView.onDraw()", "Measuredheight:(inloop): " + getMeasuredHeight());
//                this.measure(0, 0);
                Log.e("ExpandListView.onDraw()", "i: " + i);
                totalHeight += getMeasuredHeight();
                Log.e("ExpandListView.onDraw()", "totalHeight: " + totalHeight);
            }
*/
            Log.e("ExpandListView.onDraw()", "getMeasuredHeight: " + getMeasuredHeight());
//            totalHeight += getMeasuredHeight();
            totalHeight = getMeasuredHeight();
            Log.e("ExpandListView.onDraw()", "totalHeight: " + totalHeight);

            totalHeight = totalHeight * getCount();

            params = getLayoutParams();
            params.height = totalHeight + (getDividerHeight() * (getCount() - 1));
            setLayoutParams(params);
            Log.e("ExpandListView.onDraw()", "params.height: " + params.height);
        }

        super.onDraw(canvas);
    }
}
