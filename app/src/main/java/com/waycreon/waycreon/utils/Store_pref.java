package com.waycreon.waycreon.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.User;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Store_pref {

    Gson gson = new Gson();

    SharedPreferences pref_master;
    SharedPreferences.Editor editor_pref_master;
    SharedPreferences pref_remember;
    SharedPreferences.Editor editor_remember;
    Context c;

    public Store_pref(Context con) {
        c = con;
        pref_master = con.getSharedPreferences("Live_Streming_App_pref", 0);
        pref_remember = con.getSharedPreferences("REMEMBER_ME", 0);

    }

    public void open_editor() {
        editor_pref_master = pref_master.edit();
    }


    public void setUser(User user) {
        // TODO Auto-generated method stub
        open_editor();
        String ss = gson.toJson(user);
        editor_pref_master.putString("user", ss);
        editor_pref_master.commit();
    }

    public User getUser() {
        // TODO Auto-generated method stub
        User new_User = gson.fromJson(pref_master.getString("user", ""),
                User.class);
        return new_User;

    }


    public void setUserId(String user_id) {
        // TODO Auto-generated method stub
        open_editor();
        editor_pref_master.putString("user_id", user_id);
        editor_pref_master.commit();
    }

    public String getUserId() {
        // TODO Auto-generated method stub
        return pref_master.getString("user_id", "");
    }

    public void set_isGcmEnabled(String GcmStatus) {
        // TODO Auto-generated method stub
        open_editor();
        editor_pref_master.putString("isGcmEnabled", GcmStatus);
        editor_pref_master.commit();
    }

    public String get_isGcmEnabled() {
        // TODO Auto-generated method stub
        return pref_master.getString("isGcmEnabled", "true");
    }

    public void removeAll() {

        open_editor();
        editor_pref_master.clear();
        editor_pref_master.commit();
    }

    public void open_editor_remember() {
        editor_remember = pref_remember.edit();
    }

    public void setRememberMe(String emailid) {
        open_editor_remember();
        editor_remember.putString("RememberMe", emailid);
        editor_remember.commit();
        System.out.println("Email ID RememberME: " + pref_remember.getString("RememberMe", ""));
    }

    public String getRememberMe() {
        return pref_remember.getString("RememberMe", "");
    }

    public boolean isLogin() {
        if (getUserId().length() > 0) {
            return true;
        } else
            return false;
    }

    public void setAdditionsID(String additionsIDs, String from) {
        open_editor();
        editor_pref_master.putString("selected_additions", additionsIDs);
        editor_pref_master.commit();
        Log.e("setAdditionsID()", "Additions sotred in prefrences.");
        System.out.println("Additions are: " + getAdditionsIDs());
    }

    public String getAdditionsIDs() {
        Log.e("getAdditionsIDs().Store_Pref.java", "Selected Additions: " + pref_master.getString("selected_additions", ""));
        return pref_master.getString("selected_additions", "");
    }

    public void removeAdditionsIDS() {
        open_editor();
        editor_pref_master.putString("selected_additions", "");
        editor_pref_master.commit();
        Log.e("removeAdditionsIDS()", "Additions removed from prefrences.");
        System.out.println("Additions are: " + getAdditionsIDs());
    }

    public void setCrustID(String additionsIDs, String from) {
        open_editor();
        editor_pref_master.putString("selected_crust", additionsIDs);
        editor_pref_master.commit();
        Log.e("setCrustID()", "Crust sotred in prefrences.");
        System.out.println("Crust ID is: " + getCrustID());
    }

    public String getCrustID() {
        Log.e("getCrustIDs().Store_Pref.java", "Selected Crust ID: " + pref_master.getString("selected_crust", ""));
        return pref_master.getString("selected_crust", "");
    }

    public void removeCrustID() {
        open_editor();
        editor_pref_master.putString("selected_crust", "");
        editor_pref_master.commit();
        Log.e("removeCrustID()", "Crust removed from prefrences.");
        System.out.println("Crust is: " + getCrustID());
    }


    public void setToppingsIDs(String additionsIDs, String from) {
        open_editor();
        editor_pref_master.putString("selected_toppings", additionsIDs);
        editor_pref_master.commit();
        Log.e("setToppingsIDs()", "Pizza Toppings sotred in prefrences.");
        System.out.println("Toppings ID is: " + getToppingsIDs());
    }

    public String getToppingsIDs() {
        Log.e("getToppingsIDs().Store_Pref.java", "Selected Pizza Toppings ID: " + pref_master.getString("selected_toppings", ""));
        return pref_master.getString("selected_toppings", "");
    }

    public void removeToppingsIDs() {
        open_editor();
        editor_pref_master.putString("selected_toppings", "");
        editor_pref_master.commit();
        Log.e("removeToppingsIDs()", "Toppings removed from prefrences.");
        System.out.println("Pizza Toppings is: " + getToppingsIDs());
    }


    public void setOrderedData(ArrayList<OrderedItemsData> mOrderedItemsDatas, String from) {
        open_editor();

        String jsonOrder = gson.toJson(mOrderedItemsDatas);

        editor_pref_master.putString("ordered_data", jsonOrder);
        editor_pref_master.commit();
        Log.e("setOrderedData()", "Order Data is sotred in prefrences.");
        System.out.println("Ordered Data: " + jsonOrder);
    }

    public ArrayList<OrderedItemsData> getOrderedData() {

        String jsonOrder = pref_master.getString("ordered_data", null);
        Type type = new TypeToken<ArrayList<OrderedItemsData>>() {
        }.getType();
        ArrayList<OrderedItemsData> arrayListOrderedData = gson.fromJson(jsonOrder, type);

        Log.e("getOrderedData().Store_Pref.java", "Ordered Data: " + jsonOrder);
        return arrayListOrderedData;
    }

    public void removeOrderedData() {
        open_editor();
        editor_pref_master.putString("ordered_data", "");
        editor_pref_master.commit();
        Log.e("removeOrderedData()", "Ordered Data is  removed from prefrences.");

    }


}
