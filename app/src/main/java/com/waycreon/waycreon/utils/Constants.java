package com.waycreon.waycreon.utils;

/**
 * Created by Waycreon on 8/5/2015.
 */
public class Constants {

    private static String HOST = "http://incredibleapps.net/thinkpizza/action.php?do=";

    public static String REGISTER = HOST + "register_user";
    public static String LOGIN = HOST + "login_user";
    public static String GET_PIZARIA_LOC = HOST + "get_pizzaria";
    public static String GET_MENU = HOST + "get_menutype";
    public static String GET_PIZZAS = HOST + "get_menus";
    public static String GET_PIZZA_CRUST = HOST + "get_pizzacrust";
    public static String GET_ADDITIONS = HOST + "get_additions";
    public static String GET_FAVORITES = HOST + "get_favourites";
    public static String SET_FAVORITES = HOST + "set_favourites";
    public static String GET_ORDERES = HOST + "get_orders";
    public static String SET_ADDRESS = HOST + "set_address";
    public static String SET_ORDERES = HOST + "set_orders";
    public static String GET_ADDRESS = HOST + "get_addresses";
    public static String GET_FAVORITE_ADDRESS = HOST + "get_fav_addresses";
    public static String GET_HISTORY_ADDRESS = HOST + "get_addresses";
    public static String UPDATE_FAVORITE_ADDRESS = HOST + "set_fav_address";
    public static String UPDATE_USER_PROFILE = HOST + "update_user_profile";
    public static String CHANGE_PASSWORD = HOST + "change_password";
    public static String DELETE_FAVORITE_PIZZA = HOST + "delete_favourite";
    public static String GET_ORDERS_NEW = HOST + "get_orders_new";
    public static String GET_FAVORITES_NEW = HOST + "get_favourites_new";
    public static String DELETE_FAVORITE_ORDER = HOST + "delete_favourite_new";
    public static String GET_ORDER_DATA = HOST + "get_order_data";
    public static String DELETE_USER = HOST + "delete_user";
    public static String ORDER_STATUS = HOST + "get_orders_status";
    public static String GET_ORDER_MENU_ITEM = HOST + "get_orders_menu";
    public static String SET_RECOMMEND_PIZZERIA = HOST + "set_recommend_pizzeria";
    public static String PIZZERIA_RATINGS = HOST + "update_pizzaria_ratings";
    public static String FORGOT_PASSWORD = HOST + "forgot_password_confirm_email";
    public static String RESET_PASSWORD = HOST + "reset_password";
    public static String GET_STORED_ADDRESS= HOST + "get_storeded_addresses";
//    &id=149";


    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String OUT_JSON = "/json";
    /*
    NEW API OF PLACES API AFTER CREATING THE WAYCREON ACCOUNT IN GOOGLE.
     */
    //FROM OLD PROJECT "thinkpizza"
//    public static final String GOOGLE_PLACES_API = "AIzaSyD_FgRqwfG_bnCSLyRqzFYVUP-urOD3Q6g";
//    BROWER KEY FROM NEW PROJECT "THINKPIZZANEW"
    public static final String GOOGLE_PLACES_API = "AIzaSyBy44duSPsZYpogQEJ2W6Dn_94kYmlHGRE";

    public static final int GALLERY_PHOTO_REQUEST_CODE = 100;
    public static final int GALLERY_PHOTO_REQUEST_CODE_KITKAT = 300;
    public static final int CAMERA_PHOTO_REQUEST_CODE = 200;

    /*
        below static is added to change the name on screen name
        when user click on History short cut from map screen
     */
    public static boolean isHistoryServiceCalled = false;
    /*
        below static is added to cancel order while order is added
        from History or Favorites and if user presses the backpress then will be asked
        user to cancel order.
     */
    public static boolean isOrderHistoryFavorite = false;

    /*
    Below given static is added to check if the pizza in the order screen is came from
     the the OrderStatus screen or from pizza menu.
     */
    public static boolean isOrderStatusShown = false;


}
