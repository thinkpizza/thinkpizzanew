package com.waycreon.waycreon.utils;

import com.waycreon.thinkpizza.datamodels.Address;

public class Tags {
    public static final double MINIMUM_ORDER_QUANTITY = 0.25;
    public static final double MAXIMUM_ORDER_QUANTITY = 10.5;
    public static final int MINIMUM_ADDITIONS_QUANTITY = 5;

    public static double CURRENT_LONGITUDE = 0;
    public static double CURRENT_LATITUDE = 0;

    public static double SELECTED_LONGITUDE = 0;
    public static double SELECTED_LATITUDE = 0;

    public static int BUTTON_SELECTED = 2;

    //    magics.... api
//    static final String GOOGLE_API = "AIzaSyCP9KIRV5LxJQNa2VaLdmNtu_fT6ZA9VQg";
//    pradip... api
//    public static final String GOOGLE_PLACES_API = "AIzaSyDfMyUzdb903cfYXPQzOIa0tlBKSoqJKZU";
//    public static final String API_KEY = "AIzaSyA_109z_dZaCrcAFpwJ8itTpfZde5edkXo";


    public static int ADDRESS_NUMBER = 1;
    public static Address LAST_SELECTED_DELIVERY_ADDRESS = null; // still unused

//    public static boolean isMultipleRestroOrderSelected = false;
}
