package com.waycreon.thinkpizza;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.Response.Register;
import com.waycreon.thinkpizza.datamodels.User;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Utils;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends ActionBarActivity {

    CheckBox mCheckBoxRememberMe;
    EditText email_et;
    EditText password_et;
    ImageView show_password_iv;
    ProgressDialog Dialog;
    LinearLayout mLinearLayoutSignup;

    Gson mGson;
    User mUser;

    String email;
    String password;

    LinearLayout mLinearLayoutResetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        init();
        mLinearLayoutResetPassword = (LinearLayout) findViewById(R.id.login_layout_reset_password);
        mLinearLayoutResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPasswordDialog();
            }
        });

    }

    public void init() {

        Dialog = new ProgressDialog(this);
        Dialog.setMessage("Loading...");
        Dialog.setCancelable(false);

        mUser = new User();
        mGson = new Gson();

        mCheckBoxRememberMe = (CheckBox) findViewById(R.id.login_checkbox_remember_me);
        email_et = (EditText) findViewById(R.id.et_email);
        password_et = (EditText) findViewById(R.id.et_password);
        show_password_iv = (ImageView) findViewById(R.id.iv_showpassword);
        mLinearLayoutSignup = (LinearLayout) findViewById(R.id.login_layout_signup);

        Intent i = getIntent();
        Log.e("LoginActivity.java", "Email Received:" + i.getStringExtra("email"));
        if (i.hasExtra("email") && i.getStringExtra("email").length() > 0) {
            email_et.setText("" + i.getStringExtra("email"));
            password_et.requestFocus();
            Log.e("LoginActivity.java", "Email Received:" + i.getStringExtra("email"));
        }

        mLinearLayoutSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(i);
                finish();
            }
        });
        show_password_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked: isPasswordVisible: " + password_et.getInputType());
                if (password_et.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)) {
                    password_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    System.out.println(" go in if: isPasswordVisible: " + password_et.getInputType());
                    show_password_iv.setImageResource(R.drawable.e_gray);
                } else {
                    password_et.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    System.out.println("Gone in else: isPasswordVisible: " + password_et.getInputType());
                    show_password_iv.setImageResource(R.drawable.e_red);
                }
                password_et.setSelection(password_et.getText().length());
            }
        });
    }

//    public void SignUp_Login(View v) {
//
//        Intent i = new Intent(LoginActivity.this, SignupActivity.class);
//        startActivity(i);
//        finish();
//    }

    public void Login_Login(View v) {

        email = getText(email_et);
        password = getText(password_et);

        if (!isValidEmail(email)) {
            ShowToast("Enter valid email...");
            email_et.requestFocus();
            Utils.showSoftKeyBoard(LoginActivity.this, email_et);
        } else if (password.length() < 4) {
            ShowToast("Enter proper password...");
            password_et.requestFocus();
            Utils.showSoftKeyBoard(LoginActivity.this, password_et);
        } else {
            mUser.setEmail(email);
            mUser.setPassword(password);

            Log.e("IsRemember Checked ? ", "IsChecked: " + mCheckBoxRememberMe.isChecked());
            if (mCheckBoxRememberMe.isChecked()) {
                Store_pref mStore_pref = new Store_pref(LoginActivity.this);
                mStore_pref.setRememberMe(email_et.getText().toString());
            } else {
                Store_pref mStore_pref = new Store_pref(LoginActivity.this);
                mStore_pref.setRememberMe("");
            }
            callLogin();
        }
    }

    public void callLogin() {
        RequestQueue queue = Volley.newRequestQueue(this);
        Dialog.show();
        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.LOGIN,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String userdata = mGson.toJson(mUser);
                Log.e("Login.java", "User Gson: " + userdata);
                params.put("data", userdata);
                return params;
            }
        };
        queue.add(myReq);
    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Dialog.dismiss();

//                ShowToast(response + "");
                Log.e("result", "" + response);
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {

                    Store_pref mStore_pref = new Store_pref(getApplicationContext());
                    mStore_pref.setUser(register.getUser());
                    mStore_pref.setUserId(register.getUser().getId());

                    Intent i = new Intent(LoginActivity.this, NavigationActivity.class);
                    i.putExtra("email", mUser.getEmail());
                    startActivity(i);
                    finish();
                } else if (register.getResponseCode() == 3) {
                    showAccountActivationDialog();
                }
//                ShowToast(register.getResponseMsg());
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Dialog.dismiss();
                ShowToast(error.getMessage() + "");
            }
        };
    }

    android.app.Dialog mDialogAccountActivation;

    void showAccountActivationDialog() {

        mDialogAccountActivation = new Dialog(LoginActivity.this);
        mDialogAccountActivation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogAccountActivation.setContentView(R.layout.dialog_email_confirm);

        EditText mEditTextCpfno;
        EditText mEditTextEmail;
        final EditText mEditTextCode;

//        mEditTextCpfno = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_cpfno);
        mEditTextEmail = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_email);
        mEditTextCode = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_code);
//        Button mButtonActivateAccount mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_code);

        mEditTextEmail.setText(mUser.getEmail());
//        mEditTextCpfno.setText(mUser.getCpf_no());

        mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_button_activateaccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEditTextCode.getText().toString().length() > 0 && mEditTextCode.getText().toString().length() == 7) {

                    ShowToast("Please enter six digit activation code sent to your mail.");
                } else {
                    mUser.setCode(mEditTextCode.getText().toString());
                    callLogin();
                }

            }
        });

        mDialogAccountActivation.show();
    }

    public String getText(EditText et) {

        if (et != null) {
            return et.getText().toString().trim();
        } else {
            return "";
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void ShowToast(String s) {

        if (s.length() > 0) {
            Toast.makeText(LoginActivity.this, s, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Store_pref mStore_pref;
        mStore_pref = new Store_pref(this);
        if (!mStore_pref.isLogin()) {
            Intent main = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(main);
            finish();
        }
    }


    void showForgotPasswordDialog() {
        final Dialog mDialogForgotpassword;
        final EditText mEditTextEmail;

        mDialogForgotpassword = new Dialog(LoginActivity.this);
        mDialogForgotpassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogForgotpassword.setContentView(R.layout.dialog_forgot_password_new);

        mEditTextEmail = (EditText) mDialogForgotpassword.findViewById(R.id.dialog_forgotpassword_edt_email);

        mDialogForgotpassword.findViewById(R.id.dialog_forgotpassword_button_resetpassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mEditTextEmail.getText().length() > 0) {

                    mDialogForgotpassword.dismiss();
                    Utils.hide_keyboard(LoginActivity.this);
                    new ForgotPasswordService("" + mEditTextEmail.getText()).execute();
                } else {
                    Toast.makeText(LoginActivity.this, "Please enter email address.", Toast.LENGTH_SHORT).show();
                    mEditTextEmail.requestFocus();
                    Utils.showSoftKeyBoard(getApplicationContext(), mEditTextEmail);
                }
            }
        });
        mDialogForgotpassword.show();
        mEditTextEmail.requestFocus();
    }


    public class ForgotPasswordService extends AsyncTask<Void, Void, Void> {

        ProgressDialog mProgressDialog;
        String response = "";
        String url = "";
        String forgotemail = "";

        public ForgotPasswordService(String forgot_email) {
            forgotemail = forgot_email;
            url = Constants.FORGOT_PASSWORD + "&email=" + forgot_email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Varifying Email...");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpClient mHttpClient = new DefaultHttpClient();
                HttpGet mHttpGet = new HttpGet(url);

                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
                System.out.println("ForgotPasswordService URL : " + url);
                System.out.println("ForgotPasswordService Response: " + response);

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            try {
                JSONObject mJsonObject = new JSONObject(response);
                if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {

                    showResetPasswordDialog(forgotemail);

                } else if (mJsonObject.getString("responseCode").equalsIgnoreCase("0")) {

                    showForgotPasswordDialog();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    Dialog mDialogChangePassword;

    void showResetPasswordDialog(final String forgotemail) {
        mDialogChangePassword = new Dialog(LoginActivity.this);
        mDialogChangePassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogChangePassword.setContentView(R.layout.dialog_change_password);

        final TextView heading;
        final EditText mEditTextNewPassword;
        final EditText mEditTextConfirmNewPassword;
        final Button mButtonSavePassword;

        heading = (TextView) mDialogChangePassword.findViewById(R.id.layout_change_password_button_save);
        heading.setText("RESET PASSWORD");

        mDialogChangePassword.findViewById(R.id.dialog_change_password_layout_current_password).setVisibility(View.GONE);
        mEditTextNewPassword = (EditText) mDialogChangePassword.findViewById(R.id.layout_change_password_edit_new_password_first);
        mEditTextConfirmNewPassword = (EditText) mDialogChangePassword.findViewById(R.id.layout_change_password_edit_new_confirm_password);

        mButtonSavePassword = (Button) mDialogChangePassword.findViewById(R.id.layout_change_password_button_save);

        mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_password_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setShowHidePassword(mEditTextNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_password_first));
            }
        });

        mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_confirm_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setShowHidePassword(mEditTextConfirmNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_confirm_password));
            }
        });


        mButtonSavePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditTextNewPassword.getText().toString().length() < 4) {

                    Toast.makeText(LoginActivity.this, "New Password must be more then 4 characters.", Toast.LENGTH_SHORT).show();
                    mEditTextNewPassword.requestFocus();
                    mEditTextNewPassword.setSelection(0, mEditTextNewPassword.length());
                    Utils.showSoftKeyBoard(LoginActivity.this, mEditTextNewPassword);

                } else if (mEditTextConfirmNewPassword.getText().toString().length() < 4) {

                    Toast.makeText(LoginActivity.this, "Confirm New Password must be more then 4 characters.", Toast.LENGTH_SHORT).show();
                    mEditTextConfirmNewPassword.requestFocus();
                    mEditTextConfirmNewPassword.setSelection(0, mEditTextConfirmNewPassword.length());
                    Utils.showSoftKeyBoard(LoginActivity.this, mEditTextConfirmNewPassword);

                } else {
                    String newpd = mEditTextNewPassword.getText().toString();
                    String confirmpd = mEditTextConfirmNewPassword.getText().toString();
                    if (!newpd.equalsIgnoreCase(confirmpd)) {
                        Toast.makeText(LoginActivity.this, "Password does not match, please re-enter.", Toast.LENGTH_SHORT).show();
                        mEditTextNewPassword.requestFocus();
                        mEditTextNewPassword.setSelection(0, mEditTextNewPassword.length());
                        Utils.showSoftKeyBoard(LoginActivity.this, mEditTextNewPassword);
                    } else {
                        Utils.hide_keyboard(LoginActivity.this);
                        new ChangePasswordService(forgotemail, mEditTextConfirmNewPassword.getText().toString()).execute();
                    }
                }
            }
        });

        mDialogChangePassword.show();
    }


    public class ChangePasswordService extends AsyncTask<Void, Void, Void> {

        String response = "";
        String url = "";

        ChangePasswordService(String forgotemail, String changepassword) {

            try {
                url = Constants.RESET_PASSWORD + "&email=" + forgotemail
                        + "&new_password=" + URLEncoder.encode(changepassword, "UTF-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Dialog = new ProgressDialog(LoginActivity.this);
            Dialog.setMessage("Please wait...");
            Dialog.setCancelable(false);
            Dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                HttpClient mHttpClient = new DefaultHttpClient();
                System.out.println("ChangePasswordService Url: " + url);
                HttpGet mHttpGet = new HttpGet(url);
                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("ChangePasswordService().DoinBackground()", "Could not reach to server.");
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Register register = null;
            Dialog.dismiss();
            Log.e("ChangePasswordService:", "ChangePasswordService Response: " + response);
            Toast.makeText(LoginActivity.this, response + "", Toast.LENGTH_SHORT).show();

            try {
                JSONObject mJsonObject = new JSONObject(response);
                if (mJsonObject.getJSONArray("user").length() > 0) {

                    Store_pref mStore_pref = new Store_pref(getApplicationContext());

                    Log.e("result: ", "" + response);
                    response = response.replace("[", "");
                    response = response.replace("]", "");
                    Log.i("result: ", "" + response);
                    register = mGson.fromJson(response, Register.class);
                    if (register.getResponseCode() == 1) {

                        mStore_pref.setUser(register.getUser());
                        mStore_pref.setUserId(register.getUser().getId());

                    }
                    Toast.makeText(LoginActivity.this, "Password changed successfully.", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(LoginActivity.this, "Password could not be changed, please try again later.", Toast.LENGTH_SHORT).show();
                }
                TextView heading = (TextView) mDialogChangePassword.findViewById(R.id.layout_change_password_button_save);
                heading.setText(getString(R.string.enter_new_password));
                mDialogChangePassword.dismiss();
            } catch (JSONException ej) {
                ej.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }
    }


}


