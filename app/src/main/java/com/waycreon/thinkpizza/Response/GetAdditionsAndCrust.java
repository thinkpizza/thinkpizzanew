package com.waycreon.thinkpizza.Response;

import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.thinkpizza.datamodels.Menu;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waycreon on 8/7/2015.
 */
public class GetAdditionsAndCrust {
    String responseMsg;
    int responseCode;
    ArrayList<PizzaCrustData> pizzacrust = new ArrayList<>();
    ArrayList<AdditionsListData> extra_ingredients = new ArrayList<>();


    public ArrayList<PizzaCrustData> getPizzacrust() {
        return pizzacrust;
    }

    public void setPizzacrust(ArrayList<PizzaCrustData> pizzacrust) {
        this.pizzacrust = pizzacrust;
    }

    public ArrayList<AdditionsListData> getExtra_ingredients() {
        return extra_ingredients;
    }

    public void setExtra_ingredients(ArrayList<AdditionsListData> extra_ingredients) {
        this.extra_ingredients = extra_ingredients;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
