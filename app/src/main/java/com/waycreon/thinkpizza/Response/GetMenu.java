package com.waycreon.thinkpizza.Response;

import com.waycreon.thinkpizza.datamodels.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waycreon on 8/7/2015.
 */
public class GetMenu {
    String responseMsg;
    int responseCode;
    List<Menu> menutype = new ArrayList<>();


    public List<Menu> getMenutype() {
        return menutype;
    }

    public void setMenutype(List<Menu> menutype) {
        this.menutype = menutype;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
