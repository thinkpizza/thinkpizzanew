package com.waycreon.thinkpizza.Response;

import com.waycreon.thinkpizza.datamodels.PizzaRestro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waycreon on 8/7/2015.
 */
public class GetPizzaria {
    String responseMsg;
    int responseCode;
    List<PizzaRestro> pizzaria = new ArrayList<>();


    public List<PizzaRestro> getPizzaria() {
        return pizzaria;
    }

    public void setPizzaria(List<PizzaRestro> pizzaria) {
        this.pizzaria = pizzaria;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
