package com.waycreon.thinkpizza.Response;

import com.waycreon.thinkpizza.datamodels.User;

/**
 * Created by Waycreon on 8/5/2015.
 */
public class Register {
    String responseMsg = "";
    int responseCode;
    String user_id = "";
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
