package com.waycreon.thinkpizza.Response;

import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Waycreon on 8/7/2015.
 */
public class GetPizzalist {
    String responseMsg;
    int responseCode;
    PizzaRestro pizzaria;

    List<PizzaListData> menus = new ArrayList<>();

    public PizzaRestro getPizzaria() {
        return pizzaria;
    }

    public void setPizzaria(PizzaRestro pizzaria) {
        this.pizzaria = pizzaria;
    }

    public List<PizzaListData> getMenus() {
        return menus;
    }

    public void setMenus(List<PizzaListData> menus) {
        this.menus = menus;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
