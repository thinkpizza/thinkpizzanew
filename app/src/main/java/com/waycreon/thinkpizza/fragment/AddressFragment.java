package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.adapter.AddressAdapter;
import com.waycreon.thinkpizza.datamodels.Address;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import java.util.ArrayList;


public class AddressFragment extends Fragment implements View.OnClickListener {

    LinearLayout mLinearLayoutCurrentLocation;
    RadioButton mRadioButtonCurrentLocation;
    TextView mTextViewCurrentLocation_Address;

    RelativeLayout mRelativeLayoutManual;
    RadioButton mRadioButtonManual;
    //    TextView mTextViewPostalAddress;
    EditText mEditTextTagName;
    TextView mTextViewPinCode;
    EditText mEditTextStreetName;
    EditText mEditTextAddressline1;
    EditText mEditTextAddressline2;
    EditText mEditTextCity;
    EditText mEditTextState;
    Button mButtonSaveManualAddress;
    ImageView mImageVieweManualAddressServer;

    /*
  Favorite Dialog
   */

    RelativeLayout mRelativeLayoutFavorite;
    RadioButton mRadioButtonFavourite;
    TextView mTextViewFavorite_Address;
    Dialog mDialogFavorite;
    ArrayList<Address> mAddressesFavorites;
    AddressAdapter mAddressAdapterFavorite;
    ListView mListViewFavorite;

    /*
    History Dialog
     */

    RelativeLayout mRelativeLayoutHistory;
    RadioButton mRadioButtonHostory;
    TextView mTextViewHistory_Address;
    Dialog mDialogHistory;
    ArrayList<com.waycreon.thinkpizza.datamodels.Address> mAddressesHistory;
    AddressAdapter mAddressAdapterHistory;
    ListView mListViewHistory;

    TextView mTextViewDialogHeading;
    ImageView mImageViewDialogImage;

    RadioButton mCurrentlyCheckedRB;

    String Streetname = "";

    Address mAddressCurrentLocationAddressReceived;
    Address mAddressPostalAddressReceived;
    Address mAddressFavoriteAddressReceived;
    Address mAddressHistoryAddressReceived;

    NavigationActivity mNavigationActivity;

    View mView;
    Store_pref mStore_pref;
    String Userid = "";
    Gson mGson;
    ProgressDialog mProgressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_address, container, false);

        mLinearLayoutCurrentLocation = (LinearLayout) mView.findViewById(R.id.f_address_layout_current_address);
        mRadioButtonCurrentLocation = (RadioButton) mView.findViewById(R.id.f_address_radio_currentlocatoin);
        mTextViewCurrentLocation_Address = (TextView) mView.findViewById(R.id.f_address_text_current_location_address);

        mNavigationActivity = (NavigationActivity) getActivity();

        mRelativeLayoutManual = (RelativeLayout) mView.findViewById(R.id.f_address_layout_manual);
        mRadioButtonManual = (RadioButton) mView.findViewById(R.id.f_address_radio_manual);
//        mTextViewPostalAddress = (TextView) mView.findViewById(R.id.f_address_text_postal_address);
        mEditTextTagName = (EditText) mView.findViewById(R.id.f_address_edittext_address_tagname);
        mTextViewPinCode = (TextView) mView.findViewById(R.id.f_address_edittext_address_pincode);
        mEditTextStreetName = (EditText) mView.findViewById(R.id.f_address_text_streetname);
        mEditTextAddressline1 = (EditText) mView.findViewById(R.id.f_address_edittext_address_line1);
        mEditTextAddressline2 = (EditText) mView.findViewById(R.id.f_address_edittext_address_line2);
        mButtonSaveManualAddress = (Button) mView.findViewById(R.id.frag_address_button_save_manual);
        mImageVieweManualAddressServer = (ImageView) mView.findViewById(R.id.frag_address_image_manualaddress_server);

        mTextViewPinCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNavigationActivity.mapFragment.setAutocompleteTextAdapter();
                mNavigationActivity.mLinearLayoutScreenNameLogo.setVisibility(View.INVISIBLE);
                mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.VISIBLE);
                mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
                mNavigationActivity.mAutoCompleteTextViewSearch.requestFocus();
                mNavigationActivity.mAutoCompleteTextViewSearch.setText("");
                Utils.ShowSoftKeyBoard(getActivity(), mNavigationActivity.mAutoCompleteTextViewSearch);

            }
        });
        mButtonSaveManualAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditTextStreetName.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter the street name.", Toast.LENGTH_SHORT).show();
                    mEditTextStreetName.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextStreetName);
                } else if (mEditTextAddressline1.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter the street number.", Toast.LENGTH_SHORT).show();
                    mEditTextAddressline1.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextAddressline1);
                } else if (mEditTextAddressline2.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter the appartment number.", Toast.LENGTH_SHORT).show();
                    mEditTextAddressline2.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextAddressline2);
                } else if (mAddressPostalAddressReceived != null) {
                    mAddressPostalAddressReceived.setTag_name(mEditTextTagName.getText().toString());
//                mAddressPostalAddressReceived.setZip_code(mTextViewPinCode.getText().toString());
                    mAddressPostalAddressReceived.setStreet_no(mEditTextAddressline1.getText().toString());
                    mAddressPostalAddressReceived.setApt_no(mEditTextAddressline2.getText().toString());
                    mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressPostalAddressReceived);
                    mNavigationActivity.onBackPressed();
                }
            }
        });

        mImageVieweManualAddressServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hide_keyboard(getActivity());
                initStoredAddressDialog();
            }
        });

        mRelativeLayoutFavorite = (RelativeLayout) mView.findViewById(R.id.f_address_layout_favourite);
        mRadioButtonFavourite = (RadioButton) mView.findViewById(R.id.f_address_radio_favourite);
        mTextViewFavorite_Address = (TextView) mView.findViewById(R.id.f_address_text_favourite_address);

        mRelativeLayoutHistory = (RelativeLayout) mView.findViewById(R.id.f_address_layout_history);
        mRadioButtonHostory = (RadioButton) mView.findViewById(R.id.f_address_radio_history);
        mTextViewHistory_Address = (TextView) mView.findViewById(R.id.f_address_text_history_address);


        mTextViewFavorite_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAddressesFavorites != null && mAddressesFavorites.size() > 0) {
                    mDialogFavorite.show();
                } else {
                    initFavoriteDialog();
                }

            }
        });

        mTextViewHistory_Address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mAddressesFavorites != null && mAddressesHistory.size() > 0) {
                    mDialogHistory.show();
                } else {
                    initHistoryDialog();
                }

            }
        });

        mRadioButtonCurrentLocation.setOnClickListener(this);
        mRadioButtonFavourite.setOnClickListener(this);
        mRadioButtonHostory.setOnClickListener(this);
        mRadioButtonManual.setOnClickListener(this);

        mRadioButtonCurrentLocation.setChecked(true);
        mCurrentlyCheckedRB = mRadioButtonCurrentLocation;
        mLinearLayoutCurrentLocation.setVisibility(View.VISIBLE);

        if (Tags.ADDRESS_NUMBER == 1) {
            Log.e(" onCreateView()-->", "getFullAddress_CurrentLocation() method called from on create address fragment.");
            mNavigationActivity.mapFragment.getFullAddress_CurrentLocation();
        }
        mStore_pref = new Store_pref(getActivity());
        if (mStore_pref.getUserId().length() > 0) {
            Userid = mStore_pref.getUserId();
        }
        mProgressDialog = new ProgressDialog(getActivity());
        return mView;

    }

    @Override
    public void onClick(View view) {

        System.out.println("Current Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
        if (mCurrentlyCheckedRB == null) {
            System.out.println("in null condition");
            Log.e("AddressFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
            mCurrentlyCheckedRB = (RadioButton) view;
            mCurrentlyCheckedRB.setChecked(true);
        }
        if (mCurrentlyCheckedRB == view) {
            System.out.println("in if current radio condition");
            Log.e("AddressFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
            return;
        } else {
            System.out.println("in else of current radio condition");
            Log.e("AddressFragment: ", "Radiobutton Previous Selected: " + mCurrentlyCheckedRB.getText());
            mCurrentlyCheckedRB.setChecked(false);
            ((RadioButton) view).setChecked(true);
            mCurrentlyCheckedRB = (RadioButton) view;
            Log.e("AddressFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());

            switch (mCurrentlyCheckedRB.getId()) {
                case R.id.f_address_radio_currentlocatoin:
                    mLinearLayoutCurrentLocation.setVisibility(View.VISIBLE);
                    mRelativeLayoutManual.setVisibility(View.GONE);
                    mRelativeLayoutFavorite.setVisibility(View.GONE);
                    mRelativeLayoutHistory.setVisibility(View.GONE);

                    Tags.ADDRESS_NUMBER = 1;

                    if (mAddressCurrentLocationAddressReceived != null) {
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressCurrentLocationAddressReceived);
                        setCurrentLocationAddress(mAddressCurrentLocationAddressReceived);
                    } else {
                        Log.e(" onCreateView()-->", "getFullAddress_CurrentLocation() method called from on create address fragment radio click.");
                        mNavigationActivity.mapFragment.getFullAddress_CurrentLocation();
                    }
                    Utils.hide_keyboard(getActivity());
                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
                    mNavigationActivity.onBackPressed();
                    break;
                case R.id.f_address_radio_manual:

//                    mNavigationActivity.mapFragment.setAutocompleteTextAdapter();
//                    mNavigationActivity.mLinearLayoutScreenNameLogo.setVisibility(View.INVISIBLE);
//                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.VISIBLE);
//                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
//                    mNavigationActivity.mAutoCompleteTextViewSearch.requestFocus();
//                    mNavigationActivity.mAutoCompleteTextViewSearch.setText("");
//                    Utils.ShowSoftKeyBoard(getActivity(), mNavigationActivity.mAutoCompleteTextViewSearch);

                    mNavigationActivity.showAutocompleteTextview();
                    mNavigationActivity.mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

                    mLinearLayoutCurrentLocation.setVisibility(View.GONE);
                    mRelativeLayoutManual.setVisibility(View.VISIBLE);
                    mRelativeLayoutFavorite.setVisibility(View.GONE);
                    mRelativeLayoutHistory.setVisibility(View.GONE);

                    Tags.ADDRESS_NUMBER = 2;

                    if (mAddressPostalAddressReceived != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressPostalAddressReceived);

                    break;
                case R.id.f_address_radio_favourite:
                    mLinearLayoutCurrentLocation.setVisibility(View.GONE);
                    mRelativeLayoutManual.setVisibility(View.GONE);
                    mRelativeLayoutFavorite.setVisibility(View.VISIBLE);
                    mRelativeLayoutHistory.setVisibility(View.GONE);

                    Tags.ADDRESS_NUMBER = 3;

                    if (mAddressFavoriteAddressReceived != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressFavoriteAddressReceived);

                    Utils.hide_keyboard(getActivity());
                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);

                    initFavoriteDialog();

                    break;
                case R.id.f_address_radio_history:
                    mLinearLayoutCurrentLocation.setVisibility(View.GONE);
                    mRelativeLayoutManual.setVisibility(View.GONE);
                    mRelativeLayoutFavorite.setVisibility(View.GONE);
                    mRelativeLayoutHistory.setVisibility(View.VISIBLE);

                    Tags.ADDRESS_NUMBER = 4;

                    if (mAddressHistoryAddressReceived != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressHistoryAddressReceived);

                    Utils.hide_keyboard(getActivity());
                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);

                    initHistoryDialog();

                    break;
            }
        }
    }


    //    public void setCurrentLocationAddress(String add) {
    public void setCurrentLocationAddress(Address mAddressreceived) {

        Tags.LAST_SELECTED_DELIVERY_ADDRESS = mAddressreceived;

//        mTextViewCurrentLocation_Address = (TextView) mView.findViewById(R.id.f_address_text_current_location_address);

        mAddressCurrentLocationAddressReceived = mAddressreceived;
//        CurrentAddressReceived = add;
//        System.out.println("Ad:" + add);
//        mTextViewCurrentLocation_Address.setText(CurrentAddressReceived);

        mAddressreceived.printAddress();
        mTextViewCurrentLocation_Address.setText(mAddressCurrentLocationAddressReceived.getOnlyAddressLines());

        mRadioButtonCurrentLocation.setChecked(true);
        mRadioButtonManual.setChecked(false);
        mRadioButtonFavourite.setChecked(false);
        mRadioButtonHostory.setChecked(false);

        mCurrentlyCheckedRB = mRadioButtonCurrentLocation;

        mLinearLayoutCurrentLocation.setVisibility(View.VISIBLE);
        mRelativeLayoutManual.setVisibility(View.GONE);
        mRelativeLayoutFavorite.setVisibility(View.GONE);
        mRelativeLayoutHistory.setVisibility(View.GONE);
    }

    //    public void setPostalAddress(String streatname, String add) {
    public void setPostalAddress(String streatname, Address mAddressReceived, int maxAddresslines) {

        Tags.LAST_SELECTED_DELIVERY_ADDRESS = mAddressReceived;

//        PostalAddressReceived = add;
        mAddressPostalAddressReceived = mAddressReceived;


        /*Streetname = streatname;
//        Addressline2 = add;
//        System.out.println("Postal Address: " + add);
        mAddressReceived.printAddress();
        if (maxAddresslines == 1) {
            Log.e("AddressFragment.setPostalAddress()", "max address line: 1");
            mTextViewStreetName.setText(streatname);
            mEditTextAddressline2.setText(mAddressReceived.getTag_name() + "\n" +mAddressReceived.getAddressline1() +
                    ",\n" + mAddressReceived.getAddressline2());
        } else if (maxAddresslines == 2) {
            Log.e("AddressFragment.setPostalAddress()", "max address line: 2");
            mTextViewStreetName.setText(streatname);
            mEditTextAddressline2.setText(mAddressReceived.getTag_name()+"\n"+
                    mAddressReceived.getAddressline2() +",\n" + mAddressReceived.getAddressline3());
        } else {
            Log.e("AddressFragment.setPostalAddress()", "max address line else part");
            mTextViewStreetName.setText(streatname);
            mEditTextAddressline2.setText(mAddressReceived.getTag_name()+"\n"+mAddressReceived.getAddressline2() +
                    ",\n" + mAddressReceived.getAddressline3());
        }*/


        mEditTextTagName.setText(mAddressReceived.getTag_name());
        mTextViewPinCode.setText(mAddressReceived.getZip_code());
        Streetname = streatname;
        if (maxAddresslines == 1) {
            Log.e("AddressFragment.setPostalAddress()", "max address line: 1");
            mEditTextStreetName.setText(streatname);
            mEditTextAddressline1.setText(mAddressReceived.getStreet_no());

        } else if (maxAddresslines == 2) {
            Log.e("AddressFragment.setPostalAddress()", "max address line: 2");
            mEditTextStreetName.setText(streatname);
            mEditTextAddressline1.setText(mAddressReceived.getStreet_no());
            mEditTextAddressline2.setText(mAddressReceived.getApt_no());
        } else {
            Log.e("AddressFragment.setPostalAddress()", "max address line else part");
            mEditTextStreetName.setText(streatname);
            mEditTextAddressline1.setText(mAddressReceived.getStreet_no());
            mEditTextAddressline2.setText(mAddressReceived.getApt_no());
        }

        mEditTextAddressline1.requestFocus();
        mRadioButtonCurrentLocation.setChecked(false);
        mRadioButtonManual.setChecked(true);
        mRadioButtonFavourite.setChecked(false);
        mRadioButtonHostory.setChecked(false);

        mCurrentlyCheckedRB = mRadioButtonManual;

        mLinearLayoutCurrentLocation.setVisibility(View.GONE);
        mRelativeLayoutManual.setVisibility(View.VISIBLE);
        mRelativeLayoutFavorite.setVisibility(View.GONE);
        mRelativeLayoutHistory.setVisibility(View.GONE);

    }

    //    public void setFavoriteAddress(String add) {
    public void setFavoriteAddress(Address mAddressReceived) {

        Tags.LAST_SELECTED_DELIVERY_ADDRESS = mAddressReceived;
        mAddressFavoriteAddressReceived = mAddressReceived;
        mAddressReceived.printAddress();
        mTextViewFavorite_Address.setText(mAddressReceived.getOnlyAddressLines());

        mRadioButtonCurrentLocation.setChecked(false);
        mRadioButtonManual.setChecked(false);
        mRadioButtonFavourite.setChecked(true);
        mRadioButtonHostory.setChecked(false);

        mCurrentlyCheckedRB = mRadioButtonFavourite;

        mLinearLayoutCurrentLocation.setVisibility(View.GONE);
        mRelativeLayoutManual.setVisibility(View.GONE);
        mRelativeLayoutFavorite.setVisibility(View.VISIBLE);
        mRelativeLayoutHistory.setVisibility(View.GONE);
    }

    //    public void setHistoryAddress(String add) {
    public void setHistoryAddress(Address mAddressReceived) {

        Tags.LAST_SELECTED_DELIVERY_ADDRESS = mAddressReceived;
        mAddressHistoryAddressReceived = mAddressReceived;
//        HistoryAddressReceived = add;
//        System.out.println("History Address: " + add);
        mAddressReceived.printAddress();
        mTextViewHistory_Address.setText(mAddressReceived.getOnlyAddressLines());

        mRadioButtonCurrentLocation.setChecked(false);
        mRadioButtonManual.setChecked(false);
        mRadioButtonFavourite.setChecked(false);
        mRadioButtonHostory.setChecked(true);

        mCurrentlyCheckedRB = mRadioButtonHostory;

        mLinearLayoutCurrentLocation.setVisibility(View.GONE);
        mRelativeLayoutManual.setVisibility(View.GONE);
        mRelativeLayoutFavorite.setVisibility(View.GONE);
        mRelativeLayoutHistory.setVisibility(View.VISIBLE);
    }

    public void CallFavoriteAddressService() {
        mProgressDialog.setMessage("Loading Address");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mGson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.GET_FAVORITE_ADDRESS + "&user_id=" + Userid,
                createSuccessFavoriteAddress(),
                createFailureFavoriteAddress());
        queue.add(myReq);

    }

    private Response.Listener<String> createSuccessFavoriteAddress() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();

                System.out.println("createSuccessFavorite().MapFragment: Favorite Address Response: " + response);
                com.waycreon.thinkpizza.datamodels.Address mAddress = mGson.fromJson(response, com.waycreon.thinkpizza.datamodels.Address.class);

                if (mAddress.getResponseCode() == 1) {
                    mAddressesFavorites = mAddress.getAddresses();

                    if (mAddressesFavorites.size() > 0) {
                        mAddressAdapterFavorite = new AddressAdapter(getActivity(), mAddressesFavorites);
                        mListViewFavorite.setAdapter(mAddressAdapterFavorite);
                        mDialogFavorite.show();
                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite_address);
                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
                    }
                } else {
                    Toast.makeText(getActivity(), mAddress.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("createSuccessFavorite().MapFragment", "Response: " + mAddress.getResponseMsg());
                }
            }
        };
    }

    private Response.ErrorListener createFailureFavoriteAddress() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.dismiss();
                Log.e("createFailureFavorite().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), error.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void CallHistoryAddressService() {
        mProgressDialog.setMessage("Loading Address");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mGson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.GET_HISTORY_ADDRESS + "&user_id=" + Userid,
                createSuccessHistory(),
                createFailureHistory());
        queue.add(myReq);

    }

    private Response.Listener<String> createSuccessHistory() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();

                System.out.println("createSuccessHistory().MapFragment: History Address Response: " + response);
                com.waycreon.thinkpizza.datamodels.Address mAddress = mGson.fromJson(response, com.waycreon.thinkpizza.datamodels.Address.class);

                if (mAddress.getResponseCode() == 1) {
                    mAddressesHistory = mAddress.getAddresses();

                    if (mAddressesHistory.size() > 0) {
                        mAddressAdapterHistory = new AddressAdapter(getActivity(), mAddressesHistory);
                        mListViewHistory.setAdapter(mAddressAdapterHistory);
                        mDialogHistory.show();
                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_history_address);
                        mImageViewDialogImage.setImageResource(R.drawable.history_pizza_60);
                    }
                } else {
                    Toast.makeText(getActivity(), mAddress.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("createSuccessHistory().MapFragment", "Response: " + mAddress.getResponseMsg());
                }
            }
        };
    }

    private Response.ErrorListener createFailureHistory() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.dismiss();
                Log.e("createFailureHistory().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), error.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void initFavoriteDialog() {
        mDialogFavorite = new Dialog(getActivity());
        mDialogFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavorite.setContentView(R.layout.dialog_list_address);

        mListViewFavorite = (ListView) mDialogFavorite.findViewById(R.id.address_list_list_pizzas);
        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.address_list_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.address_list_image_dialog_title);

        CallFavoriteAddressService();
        mListViewFavorite.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mDialogFavorite.dismiss();
                Tags.ADDRESS_NUMBER = 3;
                com.waycreon.thinkpizza.datamodels.Address mAddress = (com.waycreon.thinkpizza.datamodels.Address) mListViewFavorite.getItemAtPosition(i);
                String add = mAddress.getTag_name();
//                        add += "\n" + mAddress.getStreet_name();
//                        add += "\n" + mAddress.getBlock_number();
                add += "\n" + mAddress.getStreet_name();
                add += "\n" + mAddress.getStreet_no();
                add += "\n" + mAddress.getApt_no();
                add += "\n" + mAddress.getCity() + " - " + mAddress.getZip_code();
                add += "\n" + mAddress.getState();
//                        mNavigationActivity.mAddressFragment.setFavoriteAddress(add);
                if (mNavigationActivity.mArrayListOrderedItem.size() > 0) {
                    double lat = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLat());
                    double lng = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLng());

                    double radius = Utils.CalculationByDistance(lat, lng,
                            Double.parseDouble(mAddress.getLat()), Double.parseDouble(mAddress.getLng()));
                    System.out.println("FavoriteClick Pizzeria Radius: " + mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius());
                    System.out.println("FavoriteClick Distance in KM: " + radius);
                    if (radius > 0 && radius <= mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius()) {
                        setFavoriteAddress(mAddress);
                        if (mAddress != null)
                            mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                        mNavigationActivity.onBackPressed();
                    } else {
                        Utils.showMyToast(getActivity(), "Address you selected is not valid for selected pizzeria.", Toast.LENGTH_SHORT);
                    }

                } else {
                    setFavoriteAddress(mAddress);
                    if (mAddress != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                    mNavigationActivity.mapFragment.setMapLocationFromFavoriteHistoryAddress(mAddress);
                    mNavigationActivity.onBackPressed();
                }


            }
        });
    }

    private void initHistoryDialog() {

        mDialogHistory = new Dialog(getActivity());
        mDialogHistory.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogHistory.setContentView(R.layout.dialog_list_address);

        mListViewHistory = (ListView) mDialogHistory.findViewById(R.id.address_list_list_pizzas);
        mTextViewDialogHeading = (TextView) mDialogHistory.findViewById(R.id.address_list_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogHistory.findViewById(R.id.address_list_image_dialog_title);

        CallHistoryAddressService();
        mListViewHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mDialogHistory.dismiss();
                Tags.ADDRESS_NUMBER = 4;
                com.waycreon.thinkpizza.datamodels.Address mAddress = (com.waycreon.thinkpizza.datamodels.Address) mListViewHistory.getItemAtPosition(i);
                String add = mAddress.getTag_name();
//                        add += "\n" + mAddress.getStreet_name();
//                        add += "\n" + mAddress.getBlock_number();
                add += "\n" + mAddress.getStreet_name();
                add += "\n" + mAddress.getStreet_no();
                add += "\n" + mAddress.getApt_no();
                add += "\n" + mAddress.getCity() + " - " + mAddress.getZip_code();
                add += "\n" + mAddress.getState();
//                        mNavigationActivity.mAddressFragment.setHistoryAddress(add);

                if (mNavigationActivity.mArrayListOrderedItem.size() > 0) {
                    double lat = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLat());
                    double lng = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLng());

                    double radius = Utils.CalculationByDistance(lat, lng,
                            Double.parseDouble(mAddress.getLat()), Double.parseDouble(mAddress.getLng()));
                    System.out.println("HistoryClick Pizzeria Radius: " + mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius());
                    System.out.println("HistoryClick Distance in KM: " + radius);
                    if (radius > 0 && radius <= mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius()) {
                        setHistoryAddress(mAddress);
                        if (mAddress != null)
                            mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                        mNavigationActivity.onBackPressed();
                    } else {
                        Utils.showMyToast(getActivity(), "Address you selected is not valid for selected pizzeria.", Toast.LENGTH_SHORT);
                    }

                } else {
                    setHistoryAddress(mAddress);
                    if (mAddress != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                    mNavigationActivity.mapFragment.setMapLocationFromFavoriteHistoryAddress(mAddress);
                    mNavigationActivity.onBackPressed();
                }

            }
        });
    }


    private void initStoredAddressDialog() {

        mDialogHistory = new Dialog(getActivity());
        mDialogHistory.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogHistory.setContentView(R.layout.dialog_list_address);

        mListViewHistory = (ListView) mDialogHistory.findViewById(R.id.address_list_list_pizzas);
        mTextViewDialogHeading = (TextView) mDialogHistory.findViewById(R.id.address_list_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogHistory.findViewById(R.id.address_list_image_dialog_title);

        CallStoredAddressService();
        mListViewHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mDialogHistory.dismiss();
                Tags.ADDRESS_NUMBER = 2;
                com.waycreon.thinkpizza.datamodels.Address mAddress = (com.waycreon.thinkpizza.datamodels.Address) mListViewHistory.getItemAtPosition(i);
                String add = mAddress.getTag_name();
//                        add += "\n" + mAddress.getStreet_name();
//                        add += "\n" + mAddress.getBlock_number();
                add += "\n" + mAddress.getStreet_name();
                add += "\n" + mAddress.getStreet_no();
                add += "\n" + mAddress.getApt_no();
                add += "\n" + mAddress.getCity() + " - " + mAddress.getZip_code();
                add += "\n" + mAddress.getState();
//                        mNavigationActivity.mAddressFragment.setHistoryAddress(add);

                if (mNavigationActivity.mArrayListOrderedItem.size() > 0) {
                    double lat = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLat());
                    double lng = Double.parseDouble(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getLng());

                    double radius = Utils.CalculationByDistance(lat, lng,
                            Double.parseDouble(mAddress.getLat()), Double.parseDouble(mAddress.getLng()));
                    System.out.println("StoredAddressClick Pizzeria Radius: " + mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius());
                    System.out.println("StoredAddressClick Distance in KM: " + radius);

                    if (radius > 0 && radius <= mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getRadius()) {

                        mEditTextTagName.setText(mAddress.getTag_name());
                        mTextViewPinCode.setText(mAddress.getZip_code());
                        mEditTextStreetName.setText(mAddress.getStreet_name());
                        mEditTextAddressline1.setText(mAddress.getStreet_no());
                        mEditTextAddressline2.setText(mAddress.getApt_no());

                        if (mAddress != null)
                            mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                        mNavigationActivity.onBackPressed();
                    } else {
                        Utils.showMyToast(getActivity(), "Address you selected is not valid for selected pizzeria.", Toast.LENGTH_SHORT);
                    }

                } else {

                    mEditTextTagName.setText(mAddress.getTag_name());
                    mTextViewPinCode.setText(mAddress.getZip_code());
                    mEditTextStreetName.setText(mAddress.getStreet_name());
                    mEditTextAddressline1.setText(mAddress.getStreet_no());
                    mEditTextAddressline2.setText(mAddress.getApt_no());

                    if (mAddress != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                    mNavigationActivity.mapFragment.setMapLocationFromFavoriteHistoryAddress(mAddress);
                    mNavigationActivity.onBackPressed();
                }

            }
        });
    }

    public void CallStoredAddressService() {
        mProgressDialog.setMessage("Loading Address");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mGson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.GET_STORED_ADDRESS + "&user_id=" + Userid,
                createSuccessStoredAddress(),
                createFailureStoredAddress());
        queue.add(myReq);
        System.out.println("StoredAddresses URL:" + Constants.GET_STORED_ADDRESS + "&user_id=" + Userid);

    }

    private Response.Listener<String> createSuccessStoredAddress() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();

                System.out.println("createSuccessStoredAddress().MapFragment: Favorite Address Response: " + response);
                com.waycreon.thinkpizza.datamodels.Address mAddress = mGson.fromJson(response, com.waycreon.thinkpizza.datamodels.Address.class);

                if (mAddress.getResponseCode() == 1) {

                    mDialogHistory.show();

                    mAddressesHistory = mAddress.getAddresses();

                    if (mAddressesHistory.size() > 0) {
                        mAddressAdapterHistory = new AddressAdapter(getActivity(), mAddressesHistory);
                        mListViewHistory.setAdapter(mAddressAdapterHistory);

                        mTextViewDialogHeading.setText("Saved Address");
                        mImageViewDialogImage.setImageResource(R.drawable.address_icon_white_60);
                    }
                } else {
                    Toast.makeText(getActivity(), mAddress.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("createSuccessStoredAddress().MapFragment", "Response: " + mAddress.getResponseMsg());
                }
            }
        };
    }

    private Response.ErrorListener createFailureStoredAddress() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.dismiss();
                Log.e("createFailureStoredAddress().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), error.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        };
    }

}
