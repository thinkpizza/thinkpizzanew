package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.adapter.OrderStatusListAdapter;
import com.waycreon.thinkpizza.datamodels.Items;
import com.waycreon.thinkpizza.datamodels.OrderedItemsDataNew;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class OrderStatusFragment extends Fragment {

    Store_pref mStore_pref;
    String Userid = "";

    ProgressDialog mProgressDialog;
    Gson mGson;

    ImageView mImageViewRefreshService;
    RelativeLayout mLayoutNo_OrderStatusList;
    LinearLayout mLayoutOrderStatus;
    List<OrderedItemsDataNew> mOrderedItemsDatasOrderStatus;
    public OrderStatusListAdapter mOrderStatusListAdapter;
    ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_order_status, container, false);
        mLayoutNo_OrderStatusList = (RelativeLayout) mView.findViewById(R.id.frag_order_status_layout_no_order_status);
        mLayoutOrderStatus = (LinearLayout) mView.findViewById(R.id.frag_order_status_layout_orderstatuslist);
        mListView = (ListView) mView.findViewById(R.id.frag_order_status_layout_order_status);
        mImageViewRefreshService = (ImageView) mView.findViewById(R.id.frag_order_status_layout_no_order_status_image_refresh);
        mImageViewRefreshService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callOrderStatusService();
            }
        });

        mStore_pref = new Store_pref(getActivity());
        mGson = new Gson();


        return mView;
    }

    public void callOrderStatusService() {
        mLayoutOrderStatus.setVisibility(View.GONE);
        mLayoutNo_OrderStatusList.setVisibility(View.GONE);
        new OrderStatusService().execute();
    }

    private class OrderStatusService extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading Orders...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(Constants.ORDER_STATUS + "&user_id=" + mStore_pref.getUser().getId());
//            HttpGet mHttpGet = new HttpGet(Constants.ORDER_STATUS + "&user_id=87" );
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("OrderStatusService().DoinBackground()", "Could not reach to server.");
            }
            System.out.println("URL:" + mHttpGet.getURI());
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of OrderStatusService:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
//                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

                        mLayoutOrderStatus.setVisibility(View.VISIBLE);
                        mLayoutNo_OrderStatusList.setVisibility(View.GONE);

                        JSONArray mJsonArray = mJsonObject.getJSONArray("orders");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasOrderStatus = new ArrayList<>();

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            OrderedItemsDataNew mOrderedItemsData = new OrderedItemsDataNew();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setOrder_id(temp.getString("order_id"));
                            mOrderedItemsData.setOrder_date(temp.getString("order_date"));
                            mOrderedItemsData.setOrder_status(temp.getString("status"));
                            mOrderedItemsData.setName(temp.getString("name"));
                            mOrderedItemsData.setImg_url(temp.getString("img_url"));

                            com.waycreon.thinkpizza.datamodels.Address mAddress = new com.waycreon.thinkpizza.datamodels.Address();
                            mAddress.setId(temp.getString("id"));
                            mAddress.setTag_name(temp.getString("tag_name"));
                            mAddress.setLat(temp.getString("lat"));
                            mAddress.setLng(temp.getString("lng"));
                            mAddress.setStreet_name(temp.getString("street_name"));
                            mAddress.setStreet_no(temp.getString("street_no"));
                            mAddress.setApt_no(temp.getString("apt_no"));
                            mAddress.setCity(temp.getString("city"));
                            mAddress.setZip_code(temp.getString("zip_code"));
                            mAddress.setState(temp.getString("state"));

                            mOrderedItemsData.setDeliveryAddress(mAddress);

                            JSONArray mJsonArrayOrderedItems = temp.getJSONArray("ordereditems");
                            ArrayList<Items> mItemses = new ArrayList<>();
                            for (int j = 0; j < mJsonArrayOrderedItems.length(); j++) {
                                Items mItems = new Items();

                                mItems.setRes_name(mOrderedItemsData.getName());
                                mItems.setMenu_id(mJsonArrayOrderedItems.getJSONObject(j).getString("menu_id"));
                                mItems.setName(mJsonArrayOrderedItems.getJSONObject(j).getString("name"));
                                mItems.setQty(mJsonArrayOrderedItems.getJSONObject(j).getString("qty"));
                                mItems.setTotal(mJsonArrayOrderedItems.getJSONObject(j).getString("total"));
                                mItems.setPid(mJsonArrayOrderedItems.getJSONObject(j).getString("pid"));
                                mItemses.add(mItems);
                            }
                            mOrderedItemsData.setItems(mItemses);
                            mItemses = null;
                            System.out.println("Size of Items" + mJsonArrayOrderedItems.length());
                            System.out.println("OrderedItemsDataNew: Size of Items" + mOrderedItemsData.getItems().size());
                            mOrderedItemsDatasOrderStatus.add(mOrderedItemsData);
//                            mOrderedItemsData.printAllData();
                        }
                        //LOOP ENDS

                        mOrderStatusListAdapter = new OrderStatusListAdapter(getActivity(), mOrderedItemsDatasOrderStatus);
                        mListView.setAdapter(mOrderStatusListAdapter);
                        mOrderStatusListAdapter.notifyDataSetChanged();


                    } else {
                        Log.e("OrderStatusService().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                        mLayoutOrderStatus.setVisibility(View.GONE);
                        mLayoutNo_OrderStatusList.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    mLayoutOrderStatus.setVisibility(View.GONE);
                    mLayoutNo_OrderStatusList.setVisibility(View.VISIBLE);
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            Log.e("onHiddenChanged().OrderStatusFragment.java", "Onhidden called");
            if (Constants.isOrderStatusShown) {
                System.out.println("isOrderStatusShown: " + Constants.isOrderStatusShown);
                Constants.isOrderStatusShown = false;
            }
        }
    }
}
