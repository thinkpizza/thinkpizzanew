package com.waycreon.thinkpizza.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.Response.GetPizzaria;
import com.waycreon.thinkpizza.adapter.AddressAdapter;
import com.waycreon.thinkpizza.adapter.FavoritePizzaAdapter;
import com.waycreon.thinkpizza.adapter.RestaurentListAdapter;
import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.thinkpizza.datamodels.User;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.GPSTracker;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class MapFragment extends Fragment implements GoogleMap.OnMapClickListener, View.OnClickListener {
    Gson mGson;
    User mUser;
    double latitude = 21.1305697;
    double longitude = 72.7903613;

    Boolean isManual = false;
    List<PizzaRestro> mPizzaRestros = new ArrayList<>();
    Button manual_btn;
    ImageView mImageViewFavorite;
    ImageView mImageViewHistory;
    ImageView mImageViewFavoriteAddress;

    MapView mMapView;
    private GoogleMap googleMap;
    GPSTracker mGpsTracker;

    NavigationActivity mNavigationActivity;

    public RelativeLayout mRelativeLayoutMapMode;
    LinearLayout mLinearLayoutNoPizzaria;
    public RelativeLayout mRelativeLayoutListMode;
    ListView mListViewRestro;
    RestaurentListAdapter adapter;
    public boolean isModeList = false;
    public boolean isModeMap = true;

    com.waycreon.thinkpizza.datamodels.Address mAddressCurrentLocationSelected;
    com.waycreon.thinkpizza.datamodels.Address mAddressPostalAddressSelected;

    Store_pref mStore_pref;
    String Userid = "";

    /*
    Pizza Favorite Dialog
     */

    public Dialog mDialogPizzaFavorite;
    List<OrderedItemsData> mOrderedItemsDatasFavorite;
    public FavoritePizzaAdapter mAdapterFavoritePizza;
    ListView mListViewFavorite;
    TextView mTextViewTagNameFavorite;
    TextView mTextViewFullAddressFavorite;
    LinearLayout mLinearLayoutFavoriteAddress;

    /*
    Pizza History Dialog
     */

    public Dialog mDialogPizzaHistory;
    List<OrderedItemsData> mOrderedItemsDatasHistory;
    FavoritePizzaAdapter mAdapterHistoryPizza;
    ListView mListViewHistory;
    TextView mTextViewTagNameHistory;
    TextView mTextViewFullAddressHistory;
    LinearLayout mLinearLayoutHistoryAddress;

    TextView mTextViewDialogHeading;
    ImageView mImageViewDialogImage;
    ProgressDialog mProgressDialog;

    public boolean isRestaroAvailable = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_map, container,
                false);
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mRelativeLayoutMapMode = (RelativeLayout) v.findViewById(R.id.layout_map_mode);
        mLinearLayoutNoPizzaria = (LinearLayout) v.findViewById(R.id.frag_map_layout_no_restaurant);
        mRelativeLayoutListMode = (RelativeLayout) v.findViewById(R.id.layout_list_mode);
        mListViewRestro = (ListView) v.findViewById(R.id.listview_pizzarestro);
        manual_btn = (Button) v.findViewById(R.id.manual_btn);
        mImageViewFavorite = (ImageView) v.findViewById(R.id.frag_map_image_favourite_address);
        mImageViewHistory = (ImageView) v.findViewById(R.id.frag_map_image_history_address);
        mImageViewFavoriteAddress = (ImageView) v.findViewById(R.id.frag_map_image_favourite_address_only);

        manual_btn.setOnClickListener(this);
        mImageViewFavorite.setOnClickListener(this);
        mImageViewHistory.setOnClickListener(this);
        mImageViewFavoriteAddress.setOnClickListener(this);

        mStore_pref = new Store_pref(getActivity());
        if (mStore_pref.getUserId().length() > 0) {
            Userid = mStore_pref.getUserId();
        }

        mMapView.onCreate(savedInstanceState);

        mGpsTracker = new GPSTracker(getActivity());
        mUser = new User();
        mGson = new Gson();
        mMapView.onResume();// needed to get the map to display immediately


        mNavigationActivity = (NavigationActivity) getActivity();
        mListViewRestro.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                mNavigationActivity.showFragment(
                        mNavigationActivity.mFoodMenuFragment, true);
                mNavigationActivity.mFoodMenuFragment.SetRetaurent(mPizzaRestros.get(position));
                System.out.println("Size of Backstacklist: " + mNavigationActivity.backstacklist.size());
                mNavigationActivity.backstacklist.add(mNavigationActivity.mRestaurentFragment);
                Log.e("BackstackList", "Restaurent Fragment is added. from map fragment");
                mNavigationActivity.showFragments();
            }
        });

        initmap();
//        new CEPBrazipService().execute("01427001");
        return v;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.manual_btn:

                isManual = !isManual;
                try {
                    googleMap.clear();
                } catch (Exception e) {
                }
                if (isManual) {

                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View mView1 = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) getActivity().findViewById(R.id.custom_toast_layout_id));
                    Toast mToast = new Toast(getActivity());
                    mToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    mToast.setDuration(Toast.LENGTH_LONG);
                    mToast.setView(mView1);
                    TextView mTextViewMessage;
                    mTextViewMessage = (TextView) mView1.findViewById(R.id.custom_toast_message);
                    mTextViewMessage.setText(getString(R.string.custom_dialog_message_map));
                    mToast.show();
                    manual_btn.setText("Go to Current Location");

                } else {
                    manual_btn.setText("Another Location ?");
                    initmap();
                }
//                Tags.ADDRESS_Number = 1;
                getFullAddress_CurrentLocation();
                break;
            case R.id.frag_map_image_favourite_address:

                /*
                Old Favorite Code
                 */
               /*
                mImageViewFavorite.setEnabled(false);
                mDialogPizzaFavorite = new Dialog(getActivity());
                mDialogPizzaFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialogPizzaFavorite.setContentView(R.layout.dialog_list_pizzas);

                mLinearLayoutFavoriteAddress = (LinearLayout) mDialogPizzaFavorite.findViewById(R.id.dialog_list_pizzas_address_layout_selectedaddress);
                mTextViewTagNameFavorite = (TextView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_text_address_tagname);
                mTextViewFullAddressFavorite = (TextView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_text_address_full_address);

                mListViewFavorite = (ListView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_list_pizzas);
                mTextViewDialogHeading = (TextView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_text_dialog_title);
                mImageViewDialogImage = (ImageView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_image_dialog_title);
                mImageViewClose = (ImageView) mDialogPizzaFavorite.findViewById(R.id.address_list_pizzas_image_dialog_close);
                mImageViewClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialogPizzaFavorite.dismiss();

                    }
                });

//                CallFavoriteAddressService();
                new FavoriteSerivce().execute();
                */

                Constants.isHistoryServiceCalled = false;
                mNavigationActivity.showFragment(mNavigationActivity.mFavoriteFragment, false);
                mNavigationActivity.mFavoriteFragment.callFavoriteService();
                mNavigationActivity.backstacklist.clear();
                mNavigationActivity.backstacklist = new ArrayList<>();
                mNavigationActivity.backstacklist.add(mNavigationActivity.mapFragment);
                mNavigationActivity.backstacklist.add(mNavigationActivity.mFavoriteFragment);
                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(0).setFlag(false);
                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(2).setFlag(true);
//                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(2).setListItemName("Favorites");
                mNavigationActivity.mNavigationListAdapter.notifyDataSetChanged();
                Utils.hide_keyboard(getActivity());
                break;
            case R.id.frag_map_image_history_address:

                /*
                Old Favorite Code
                 */
                /*
                mImageViewHistory.setEnabled(false);
                mDialogPizzaHistory = new Dialog(getActivity());
                mDialogPizzaHistory.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialogPizzaHistory.setContentView(R.layout.dialog_list_pizzas);

                mLinearLayoutHistoryAddress = (LinearLayout) mDialogPizzaHistory.findViewById(R.id.dialog_list_pizzas_address_layout_selectedaddress);
                mTextViewTagNameHistory = (TextView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_text_address_tagname);
                mTextViewFullAddressHistory = (TextView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_text_address_full_address);

                mListViewHistory = (ListView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_list_pizzas);
                mTextViewDialogHeading = (TextView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_text_dialog_title);
                mImageViewDialogImage = (ImageView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_image_dialog_title);
                mImageViewClose = (ImageView) mDialogPizzaHistory.findViewById(R.id.address_list_pizzas_image_dialog_close);

                mImageViewClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialogPizzaHistory.dismiss();
                    }
                });

//                CallHistoryAddressService();
                new HistorySerivce().execute();
                */
                Constants.isHistoryServiceCalled = true;
                mNavigationActivity.showFragment(mNavigationActivity.mFavoriteFragment, false);
                mNavigationActivity.mFavoriteFragment.callHistoryService();
                mNavigationActivity.backstacklist.clear();
                mNavigationActivity.backstacklist = new ArrayList<>();
                mNavigationActivity.backstacklist.add(mNavigationActivity.mapFragment);
                mNavigationActivity.backstacklist.add(mNavigationActivity.mFavoriteFragment);
                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(0).setFlag(false);
                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(2).setFlag(true);
//                mNavigationActivity.mNavigationListAdapter.navDrawerItems.get(2).setListItemName("History Orders");
                mNavigationActivity.mNavigationListAdapter.notifyDataSetChanged();
                Utils.hide_keyboard(getActivity());
                break;

            case R.id.frag_map_image_favourite_address_only:
                initFavoriteAddressesDialog();
                Utils.hide_keyboard(getActivity());
                break;

           /* case R.id.frag_map_image_mylocation:

                if (mylocation != null) {
                    System.out.println("mylocation is not null");
                    LatLng target = new LatLng(mylocation.getLatitude(), mylocation.getLongitude());
                    CameraPosition position = googleMap.getCameraPosition();

                    CameraPosition.Builder builder = new CameraPosition.Builder();
                    builder.zoom(18);
                    builder.target(target);
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
                    googleMap.setMyLocationEnabled(false);

                }
                break;*/
        }


    }

    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions marker = new MarkerOptions().position(latLng).title("" + "Selected Position...");

        // Changing marker icon
        if (isManual) {
            try {
                googleMap.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }

            latitude = latLng.latitude;
            longitude = latLng.longitude;
            Tags.SELECTED_LATITUDE = latitude;
            Tags.SELECTED_LONGITUDE = longitude;
            initmap();
//            Tags.ADDRESS_Number = 1;
            getFullAddress_CurrentLocation();
        }
        Log.d("onMapClick()", "onMapClick() is clicked.");
        Utils.hide_keyboard(getActivity());
        mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
        mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
        mNavigationActivity.ShowHideIcons();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initmap();
    }

    @Override
    public void onResume() {
        super.onResume();

        mMapView.onResume();
//        if (mGpsTracker != null) {
//            initmap();
//        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            System.out.println("MapFrag.java onHiddenChanged()");
            if (mRelativeLayoutListMode.isShown()) {
                System.out.println("MapFrag.java onHiddenChanged(): Map Fragment is visible.");
                System.out.println("MapFrag.java onHiddenChanged(): Map Fragment is visible. and Restaurant List mode is shown");
                mNavigationActivity.mImageViewModeList.setVisibility(View.INVISIBLE);
                mNavigationActivity.mImageViewModeMap.setVisibility(View.VISIBLE);

            }

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void initmap() {
        if (mGpsTracker.canGetLocation()) {

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            googleMap = mMapView.getMap();
            googleMap.setOnMapClickListener(this);
            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    Log.d("onCameraChange()", "googleMap.setOnCameraChangeListener() is clicked.");
                    Utils.hide_keyboard(getActivity());
                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
                    mNavigationActivity.ShowHideIcons();
                }
            });

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    System.out.println("Marker Clicked from google map.");


                    if (marker.getSnippet() != null) {
//                    if (mPizzaRestros != null) {
                        for (int i = 0; i < mPizzaRestros.size(); i++) {
                            System.out.println("gone in loop");
                            if (mPizzaRestros.get(i).isOpen()) {
                                if (marker.getTitle().contains(mPizzaRestros.get(i).getName())) {
                                    mNavigationActivity.showFragment(
                                            mNavigationActivity.mFoodMenuFragment, true);
                                    mNavigationActivity.mFoodMenuFragment.SetRetaurent(mPizzaRestros.get(i));
                                    mNavigationActivity.backstacklist.add(mNavigationActivity.mFoodMenuFragment);
                                    mNavigationActivity.showFragments();
                                    Log.e("BackstackList", "FoodMenu Fragment is added.(from Map Marker click event)");
                                }
                            }// restaurant is open

                        }
                    }
                    Utils.hide_keyboard(getActivity());
                    mNavigationActivity.mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                    mNavigationActivity.mImageViewSearch.setVisibility(View.VISIBLE);
                    return false;
                }
            });

            // latitude and longitude
            if (!isManual) {

                latitude = mGpsTracker.getLatitude();
                longitude = mGpsTracker.getLongitude();

//                setting longitude and latitude to tags file
                Tags.CURRENT_LONGITUDE = mGpsTracker.getLongitude();
                Tags.CURRENT_LATITUDE = mGpsTracker.getLatitude();
            }
            getFullAddress_CurrentLocation();
            new getAreaName_CL_With_Pizzeria().execute();
//            CallRestaurantService();
            // create marker

            Marker marker;
            MarkerOptions markerOptions;
            if (isManual) {

                markerOptions = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title("Selected Location...");
            } else {

                markerOptions = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title("You are hear...");

                // Changing marker icon
            }
//            marker.icon(BitmapDescriptorFactory
//                    .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
            markerOptions.icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.location));

            // adding marker
            marker = googleMap.addMarker(markerOptions);
            marker.showInfoWindow();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } else {
            Toast.makeText(getActivity(), "Please enable gps...", Toast.LENGTH_SHORT).show();
            mGpsTracker.showSettingsAlert();
        }
    }

    public void CallRestaurantService(final String Area, final String Pin) {
        isRestaroAvailable = false;

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.GET_PIZARIA_LOC,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", "" + latitude);
                params.put("lng", "" + longitude);
                params.put("areaname", "" + Area);
                params.put("pincode", "" + Pin);

                return params;
            }
        };
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("MapFragment: Restarurant Response: " + response);
                GetPizzaria mGetPizzaria = mGson.fromJson(response, GetPizzaria.class);

                if (mGetPizzaria.getResponseCode() == 1) {
                    mPizzaRestros = mGetPizzaria.getPizzaria();

                    if (mPizzaRestros.size() > 0) {

                        isRestaroAvailable = true;
                        mPizzaRestros = mGetPizzaria.getPizzaria();

                        adapter = new RestaurentListAdapter(getActivity(), mPizzaRestros);
                        mListViewRestro.setAdapter(adapter);

                        mNavigationActivity.mRestaurentFragment.setRestaurantData(mPizzaRestros);

                        for (PizzaRestro p : mPizzaRestros) {
                            /*
                            New code for open close restaurant depand on Time of Day Start
                             */
                            p.setOpen_time(Utils.getOpenCloseTime(p.getOpen_days(),0));
                            p.setClose_time(Utils.getOpenCloseTime(p.getOpen_days(),1));
                            /*
                            New code for open close restaurant depand on Time of Day Ends
                             */
                            String open_time[] = p.getOpen_time().split(":");
                            String close_time[] = p.getClose_time().split(":");

                            Time open = new Time(Integer.parseInt(open_time[0]), Integer.parseInt(open_time[1]), 0);
                            Time close = new Time(Integer.parseInt(close_time[0]), Integer.parseInt(close_time[1]), 0);

                            if (Utils.isTimeInBetween(open, close)) {
                                Log.i("bec_delivery_charge", "We found a Branch nearest to you.");
                                addRestaurantMarker(p.getLat(), p.getLng(),
                                        p.getName() + " is " + p.getDistance() + " Km away from current location", "Open");
                                p.setIsOpen(true);
                            } else {
                                Toast.makeText(getActivity(), p.getName() + " is closed.", Toast.LENGTH_LONG).show();
                                addRestaurantMarker(p.getLat(), p.getLng(),
                                        p.getName() + " " + p.getDistance() + " Km away from current location", "Close");
                                p.setIsOpen(false);
                            }


                        }
                        if (isModeList) {
                            Log.e("createMyReqSuccessListener().MapFragment", "Mode list selected");
                            mNavigationActivity.mImageViewModeList.setVisibility(View.INVISIBLE);
                            mNavigationActivity.mImageViewModeMap.setVisibility(View.VISIBLE);
                        } else if (isModeMap) {
                            Log.e("createMyReqSuccessListener().MapFragment", "Mode Map selected");
                            mNavigationActivity.mImageViewModeList.setVisibility(View.VISIBLE);
                            mNavigationActivity.mImageViewModeMap.setVisibility(View.INVISIBLE);
                        }
                        adapter.notifyDataSetChanged();

                    }
                } else {
                    Toast.makeText(getActivity(), mGetPizzaria.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("createMyReqSuccessListener().MapFragment", "Restaurant Response: " + mGetPizzaria.getResponseMsg());
                }
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Pizzerias: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void addRestaurantMarker(String lat, String lon, String details, String open_close) {

        MarkerOptions marker = new MarkerOptions()
                .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)))
                .title("" + details)
                .snippet("Pizzeria: " + open_close);

        // Changing marker icon
//        marker.icon(BitmapDescriptorFactory
//                .defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        if (open_close.equalsIgnoreCase("open"))
            marker.icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.res_open_icon));
        else
            marker.icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.res_close_icon));

        // adding marker
        googleMap.addMarker(marker);
    }


/*
        code for the pin code starts here
-----------------------------------------------------------------
 */

    /*
        Autocomplete Address Methods starts
     */

    ArrayList<String> GoogleAddressesresultList;
    /*
        Arraylist for storing the result of search addresses
        This is set in GLOBAL because When i implemented the CallBrazilAddress() service, null pointer error
        was coming in its adapter to avoid it
         and if address not found at that time what i am doing is that i pass the address to
         autocomplete() method and after 100 ms showing the list of search addresses
     */


    public static ArrayList<String> autocomplete(String input) {

        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(Constants.PLACES_API_BASE + Constants.TYPE_AUTOCOMPLETE + Constants.OUT_JSON);
            sb.append("?key=" + Constants.GOOGLE_PLACES_API);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("Mapfragment: URL: " + url);

            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("autocomplete().Mapfragment", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("autocomplete().Mapfragment", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("autocomplete().Mapfragment", "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
//        private ArrayList<String> GoogleAddressesresultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return GoogleAddressesresultList.size();
        }

        @Override
        public String getItem(int index) {
            return GoogleAddressesresultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    System.out.println("in FilterResults:");
                    if (constraint != null) {
                        System.out.println("in FilterResults not null");
                        // Retrieve the autocomplete results.
                        GoogleAddressesresultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = GoogleAddressesresultList;
                        filterResults.count = GoogleAddressesresultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    System.out.println("in publishResults not null");
                    if (results != null && results.count > 0) {
                        System.out.println("in publishResults not null ");
                        notifyDataSetChanged();
                    } else {
                        System.out.println("in publishResults is null");
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public void setAutocompleteTextAdapter() {
        System.out.println("setAutocompleteTextAdapter() is called");
        mNavigationActivity.mAutoCompleteTextViewSearch.setAdapter(
                new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
    }

    public void setAutocompleteTextAdapterForMyOption() {
        System.out.println("setAutocompleteTextAdapterForMyOption() is called");
        mNavigationActivity.myOptionsFragment.mEditTextZipCode.setAdapter(
                new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
    }
    /*
                Autocomplete Address Methods Ends
        -----------------------------------------------------------------
     */

    public class getAreaName_CL_With_Pizzeria extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Finding Location...");
            mProgressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                mAddressCurrentLocationSelected = new com.waycreon.thinkpizza.datamodels.Address();
                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocation(latitude, longitude, 1);
//                Tags.ADDRESS_NUMBER = 1;
            } catch (Exception e) {
                e.printStackTrace();
//                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();

            if (add != null && add.size() > 0) {

                Log.e("getAreaName_CL_With_Pizzeria()", "-----------------------getAreaName_CL_With_Pizzeria--------------");
                System.out.println("Max addressline index: " + add.get(0).getMaxAddressLineIndex());
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());

                /*
                Tags.ADDRESS_NUMBER = 1;

                if (Tags.ADDRESS_NUMBER == 1) {
                    for (int i = 0; i < add.get(0).getMaxAddressLineIndex(); i++) {
                        System.out.println("Current Address Line (" + i + "): " + add.get(0).getAddressLine(i));
//                    SelectedCurrentAddress += add.get(0).getAddressLine(i) + "\n";
                    }
                    mAddressCurrentLocationSelected.setTag_name("Current Location");
//                    mAddressCurrentLocationSelected.setAddressline1(add.get(0).getAddressLine(0));
//                    mAddressCurrentLocationSelected.setAddressline2(add.get(0).getAddressLine(1));
//                    mAddressCurrentLocationSelected.setAddressline3(add.get(0).getAddressLine(2) + ", " + add.get(0).getAddressLine(3));
                    mAddressCurrentLocationSelected.setStreet_name(add.get(0).getAddressLine(0));

                    if (add.get(0).getMaxAddressLineIndex() == 2) {
                        mAddressCurrentLocationSelected.setApt_no(add.get(0).getAddressLine(1) +
                                ", " + add.get(0).getAddressLine(2));
                    } else if (add.get(0).getMaxAddressLineIndex() == 3) {
                        mAddressCurrentLocationSelected.setApt_no(add.get(0).getAddressLine(1) +
                                ",\n" + add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));
                    }


                    mAddressCurrentLocationSelected.setZip_code(add.get(0).getPostalCode());
                    if (add.get(0).getSubAdminArea() == null)
                        mAddressCurrentLocationSelected.setCity(add.get(0).getLocality());
                    else
                        mAddressCurrentLocationSelected.setCity(add.get(0).getSubAdminArea());
                    mAddressCurrentLocationSelected.setState(add.get(0).getAdminArea());

                    if (mNavigationActivity.mAddressFragment != null) {
//                    mNavigationActivity.mAddressFragment.setCurrentLocationAddress(SelectedCurrentAddress);
                        mNavigationActivity.mAddressFragment.setCurrentLocationAddress(mAddressCurrentLocationSelected);
                        if (mAddressCurrentLocationSelected != null)
                            mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressCurrentLocationSelected);

                    }
                }
                */

                CallRestaurantService(add.get(0).getAddressLine(0) + "," + add.get(0).getAddressLine(1), add.get(0).getPostalCode());

            } else {
//                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void getFullAddress_CurrentLocation() {

        if (getActivity() != null)
            new LoadAddress().execute();
    }

    public class LoadAddress extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Finding Location...");
            mProgressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
//        SelectedCurrentAddress = "";
                mAddressCurrentLocationSelected = new com.waycreon.thinkpizza.datamodels.Address();
                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocation(latitude, longitude, 1);
                Tags.ADDRESS_NUMBER = 1;
            } catch (Exception e) {
                e.printStackTrace();
                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();

            if (add != null && add.size() > 0) {
                Log.e("LoadAddress()", "-----------------------LoadAddressService--------------");
                System.out.println("Max addressline index: " + add.get(0).getMaxAddressLineIndex());
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());


                Tags.ADDRESS_NUMBER = 1;

                if (Tags.ADDRESS_NUMBER == 1) {
                    /*for (int i = 0; i < add.get(0).getMaxAddressLineIndex(); i++) {
                        System.out.println("Current Address Line (" + i + "): " + add.get(0).getAddressLine(i));
//                    SelectedCurrentAddress += add.get(0).getAddressLine(i) + "\n";
                    }
                    mAddressCurrentLocationSelected.setTag_name("Current Location");
//                    mAddressCurrentLocationSelected.setAddressline1(add.get(0).getAddressLine(0));
//                    mAddressCurrentLocationSelected.setAddressline2(add.get(0).getAddressLine(1));
//                    mAddressCurrentLocationSelected.setAddressline3(add.get(0).getAddressLine(2) + ", " + add.get(0).getAddressLine(3));
                    mAddressCurrentLocationSelected.setLat("" + add.get(0).getLatitude());
                    mAddressCurrentLocationSelected.setLng("" + add.get(0).getLongitude());
                    mAddressCurrentLocationSelected.setStreet_name(add.get(0).getAddressLine(0));

                    if (add.get(0).getMaxAddressLineIndex() == 2) {
                        mAddressCurrentLocationSelected.setApt_no(add.get(0).getAddressLine(1) +
                                ", " + add.get(0).getAddressLine(2));
                    } else if (add.get(0).getMaxAddressLineIndex() == 3) {
                        mAddressCurrentLocationSelected.setApt_no(add.get(0).getAddressLine(1) +
                                ",\n" + add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));
                    }


                    mAddressCurrentLocationSelected.setZip_code(add.get(0).getPostalCode());
                    if (add.get(0).getSubAdminArea() == null)
                        mAddressCurrentLocationSelected.setCity(add.get(0).getLocality());
                    else
                        mAddressCurrentLocationSelected.setCity(add.get(0).getSubAdminArea());
                    mAddressCurrentLocationSelected.setState(add.get(0).getAdminArea());

                    if (mNavigationActivity.mAddressFragment != null) {
//                    mNavigationActivity.mAddressFragment.setCurrentLocationAddress(SelectedCurrentAddress);
                        mNavigationActivity.mAddressFragment.setCurrentLocationAddress(mAddressCurrentLocationSelected);
                        if (mAddressCurrentLocationSelected != null)
                            mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressCurrentLocationSelected);

                    }*/


                    mAddressCurrentLocationSelected.setTag_name("Current Location");
                    if (add.get(0).getThoroughfare() != null)
                        mAddressCurrentLocationSelected.setStreet_name(add.get(0).getThoroughfare());
                    else if (add.get(0).getFeatureName() != null)
                        mAddressCurrentLocationSelected.setStreet_name(add.get(0).getFeatureName());
                    else
                        mAddressCurrentLocationSelected.setStreet_name(add.get(0).getAddressLine(0));

                    if (add.get(0).getSubThoroughfare() != null)
                        mAddressCurrentLocationSelected.setStreet_no(add.get(0).getSubThoroughfare());

                    if (add.get(0).getSubAdminArea() != null)
                        mAddressCurrentLocationSelected.setCity(add.get(0).getSubAdminArea());
                    else if (add.get(0).getLocality() != null)
                        mAddressCurrentLocationSelected.setCity(add.get(0).getLocality());

                    if (add.get(0).getAdminArea() != null)
                        mAddressCurrentLocationSelected.setState(add.get(0).getAdminArea());

                    if (add.get(0).getPostalCode() != null)
                        mAddressCurrentLocationSelected.setZip_code(add.get(0).getPostalCode());
//                    else
//                        mAutoCompleteTextViewPincode.setText(zipcode);

                    mAddressCurrentLocationSelected.setLat("" + add.get(0).getLatitude());
                    mAddressCurrentLocationSelected.setLng("" + add.get(0).getLongitude());

                    mNavigationActivity.mAddressFragment.setCurrentLocationAddress(mAddressCurrentLocationSelected);

                    if (mAddressCurrentLocationSelected != null)
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressCurrentLocationSelected);


                }
            } else {
                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public class LoadLocationFromCEP extends AsyncTask<String, Void, String> {

        String response = "";
        List<Address> add;
        String brazil_address_received = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Finding CEP Location...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (params.length == 0)
                return "";
            try {
                System.out.println("------------------getLocationPoints()----------------");
                System.out.println("Address Received: " + params[0]);
                brazil_address_received = params[0];
                System.out.println("Address Received Length: " + params[0].length());
                mAddressPostalAddressSelected = new com.waycreon.thinkpizza.datamodels.Address();
                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocationName(params[0], 1);
                Tags.ADDRESS_NUMBER = 2;
            } catch (Exception e) {
                e.printStackTrace();
                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            String address[] = brazil_address_received.split("#");
            if (add != null && add.size() > 0) {
                Log.e("LoadLocationFromCEP()", "-----------------------LoadLocationFromCEP--------------");
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());

                Log.e("getLocationPoint()", "Full Address: " + add.get(0));
                System.out.println("getLocationPoint() Longitude" + add.get(0).getLongitude());
                System.out.println("getLocationPoint() Latitude" + add.get(0).getLatitude());

                latitude = add.get(0).getLatitude();
                longitude = add.get(0).getLongitude();

                if (Tags.ADDRESS_NUMBER == 2) {
                    Log.e("getLocationPoints().MapFragment", "Address line 1: " + add.get(0).getAddressLine(0));
                    /*if (add.get(0).getAddressLine(3).length() > 0) {
                        Log.e("getLocationPoints().MapFragment", "Address line 2" + add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                    } else {*/
                    Log.e("getLocationPoints().MapFragment", "Address line 2 & 3: " + add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));

                    mAddressPostalAddressSelected.setTag_name("Manual Address (By CEP)");
                    if (add.get(0).getThoroughfare() != null)
                        mAddressPostalAddressSelected.setStreet_name(add.get(0).getThoroughfare());
                    else if (add.get(0).getFeatureName() != null)
                        mAddressPostalAddressSelected.setStreet_name(add.get(0).getFeatureName());
                    else
                        mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                    if (add.get(0).getSubThoroughfare() != null)
                        mAddressPostalAddressSelected.setStreet_no(add.get(0).getSubThoroughfare());

                    if (add.get(0).getSubAdminArea() != null)
                        mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                    else if (add.get(0).getLocality() != null)
                        mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                    else {
                        /*
                        this else part is added because some time city name is not coming in G places
                         */
                        mAddressPostalAddressSelected.setCity(address[0]);
                    }

                    if (add.get(0).getAdminArea() != null)
                        mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                    if (add.get(0).getPostalCode() != null)
                        mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());
                    else {
                        mAddressPostalAddressSelected.setZip_code(brazil_CEP_search);
                    }
//                    else
//                        mAutoCompleteTextViewPincode.setText(zipcode);

                    mAddressPostalAddressSelected.setLat("" + add.get(0).getLatitude());
                    mAddressPostalAddressSelected.setLng("" + add.get(0).getLongitude());

                        /*
                                To set the Zipcode address on Address screen
                         */
                    mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), mAddressPostalAddressSelected, 3);

                    /*
                                To set the Zipcode address on Basket screen
                         */
                    if (mAddressPostalAddressSelected != null) {
                        Log.e("MapFragment.getLocationPoints()", "Delivery Address has been set to Basket Fragment");
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressPostalAddressSelected);
                    }

                    /*}*/

                }

                googleMap.clear();
                System.out.println("Map Cleared to go on Postal address.");
//                if (mGpsTracker.canGetLocation()) {

                try {
                    MapsInitializer.initialize(getActivity().getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                    CallRestaurantService();
//                    new getAreaName_CL_With_Pizzeria().execute();
                CallRestaurantService(add.get(0).getAddressLine(0) + "," + add.get(0).getAddressLine(1), add.get(0).getPostalCode());

                googleMap = mMapView.getMap();
                // create marker
                MarkerOptions marker;
                marker = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title("Selected location...");

                // Changing marker icon
//                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

                // adding marker
                googleMap.addMarker(marker);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude)).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//                } else {
//                    Toast.makeText(getActivity(), "Pizzerias: " + "Please enable gps...", Toast.LENGTH_SHORT).show();
//                    mGpsTracker.showSettingsAlert();
//                }

            } else {
                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /*
    Below method will be called when user enters the pin code and at that time to get
    the latitude longitude from that address; we need the Lat Lang so we use below method.
    and this will be called when user selects the location from drop down.
     */
    public void getLocationPoints(String address) {
        System.out.println("------------------getLocationPoints()----------------");
        System.out.println("Address Received: " + address);
        System.out.println("Address Received Length: " + address.length());
        mAddressPostalAddressSelected = new com.waycreon.thinkpizza.datamodels.Address();
        Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> add;
        try {
            add = geo.getFromLocationName(address, 2);

            System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
            System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
            System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
            System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
            System.out.println("Admin Area: " + add.get(0).getAdminArea());
            System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
            System.out.println("Country Name: " + add.get(0).getCountryName());
            System.out.println("Country Code: " + add.get(0).getCountryCode());
            System.out.println("Feature Name: " + add.get(0).getFeatureName());
            System.out.println("Locality: " + add.get(0).getLocality());
            System.out.println("Phone: " + add.get(0).getPhone());
            System.out.println("Postal code: " + add.get(0).getPostalCode());
            System.out.println("Premises: " + add.get(0).getPremises());
            System.out.println("SubLocality: " + add.get(0).getSubLocality());
            System.out.println("Through Fare: " + add.get(0).getThoroughfare());
            System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
            System.out.println("Locale: " + add.get(0).getLocale());
            System.out.println("Extras: " + add.get(0).getExtras());
            System.out.println("Url " + add.get(0).getUrl());
            System.out.println("Longitude" + add.get(0).getLongitude());
            System.out.println("Latitude" + add.get(0).getLatitude());

//            for (int i = 0; i < add.size(); i++) {
            Log.e("getLocationPoint()", "Full Address: " + add.get(0));
            System.out.println("getLocationPoint() Longitude" + add.get(0).getLongitude());
            System.out.println("getLocationPoint() Latitude" + add.get(0).getLatitude());

            latitude = add.get(0).getLatitude();
            longitude = add.get(0).getLongitude();

            if (Tags.ADDRESS_NUMBER == 2) {
                Log.e("getLocationPoints().MapFragment", "Address line 1: " + add.get(0).getAddressLine(0));
                    /*if (add.get(0).getAddressLine(3).length() > 0) {
                        Log.e("getLocationPoints().MapFragment", "Address line 2" + add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                    } else {*/
                Log.e("getLocationPoints().MapFragment", "Address line 2 & 3: " + add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));
                    /*
                    mAddressPostalAddressSelected.setAddressline1(add.get(0).getAddressLine(0));
                    mAddressPostalAddressSelected.setAddressline2(add.get(0).getAddressLine(1));

                    if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) == null)
                        mAddressPostalAddressSelected.setAddressline3(add.get(0).getAddressLine(2));
                    else if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) != null)
                        mAddressPostalAddressSelected.setAddressline3(add.get(0).getAddressLine(2) + ", " + add.get(0).getAddressLine(3));
                    */

                // OLD WORKING CODE
                    /*
                    mAddressPostalAddressSelected.setTag_name("Manual Address (By Postal Code)");
                    mAddressCurrentLocationSelected.setLat("" + add.get(0).getLatitude());
                    mAddressCurrentLocationSelected.setLng("" + add.get(0).getLongitude());

                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));


                    if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) == null)
                        mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                                add.get(0).getAddressLine(2));
                    else if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) != null)
                        mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                                add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));


                    mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());
                    if (add.get(0).getSubAdminArea() == null)
                        mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                    else
                        mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                    mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());
//                    mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));


                    if (add.get(0).getMaxAddressLineIndex() == 1) {
                        Log.e("MapFragment.getLocationPoints()", "Postal Address has been set to Address Fragment");
                        Log.e("MapFragment.getLocationPoints()", "max address line: 1");
                        mNavigationActivity.mAddressFragment.setPostalAddress("", mAddressPostalAddressSelected, 1);
                    } else if (add.get(0).getMaxAddressLineIndex() == 2) {
                        Log.e("MapFragment.getLocationPoints()", "Postal Address has been set to Address Fragment");
                        Log.e("MapFragment.getLocationPoints()", "max address line: 2");
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), mAddressPostalAddressSelected, 2);
                    } else if (add.get(0).getMaxAddressLineIndex() == 3) {
                        Log.e("MapFragment.getLocationPoints()", "Postal Address has been set to Address Fragment");
                        Log.e("MapFragment.getLocationPoints()", "max address line: 3");
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), mAddressPostalAddressSelected, 3);
                    }
                    if (mAddressPostalAddressSelected != null) {
                        Log.e("MapFragment.getLocationPoints()", "Delivery Address has been set to Basket Fragment");
                        mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressPostalAddressSelected);
                    }*/


                mAddressPostalAddressSelected.setTag_name("Manual Address (By Postal Code)");
                if (add.get(0).getThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getFeatureName());
                else
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_no(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());

                if (add.get(0).getAdminArea() != null)
                    mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());
//                    else
//                        mAutoCompleteTextViewPincode.setText(zipcode);

                mAddressPostalAddressSelected.setLat("" + add.get(0).getLatitude());
                mAddressPostalAddressSelected.setLng("" + add.get(0).getLongitude());

                        /*
                                To set the Zipcode address on Address screen
                         */
                mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), mAddressPostalAddressSelected, 3);

                    /*
                                To set the Zipcode address on Basket screen
                         */
                if (mAddressPostalAddressSelected != null) {
                    Log.e("MapFragment.getLocationPoints()", "Delivery Address has been set to Basket Fragment");
                    mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddressPostalAddressSelected);
                }

                    /*}*/

            }

            googleMap.clear();
            System.out.println("Map Cleared to go on Postal address.");
            if (mGpsTracker.canGetLocation()) {

                try {
                    MapsInitializer.initialize(getActivity().getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                googleMap = mMapView.getMap();
                googleMap.setOnMapClickListener(this);
//                    CallRestaurantService();
//                    new getAreaName_CL_With_Pizzeria().execute();
                CallRestaurantService(add.get(0).getAddressLine(0) + "," + add.get(0).getAddressLine(1), add.get(0).getPostalCode());
                // create marker

                MarkerOptions marker;

                marker = new MarkerOptions().position(
                        new LatLng(latitude, longitude)).title("Selected location...");

                // Changing marker icon
//                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));

                // adding marker
                googleMap.addMarker(marker);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude)).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            } else {
                Toast.makeText(getActivity(), "Pizzerias: " + "Please enable gps...", Toast.LENGTH_SHORT).show();
                mGpsTracker.showSettingsAlert();
            }
//            }
        } catch (Exception e) {
            Tags.ADDRESS_NUMBER = 0;
            System.out.println("Address Is null:");
            Toast.makeText(getActivity(), "Unable to locate postal address.", Toast.LENGTH_SHORT).show();
        }

    }

    /*
   Below Service will be called when user enters the pin code of brazil to get address
    */
//    public class CEPBrazipService extends AsyncTask<String, Void, String> {
//        URL url = null;
//        HttpURLConnection HttpURLConnection = null;
//        String response = "";
//
//        @Override
//        protected String doInBackground(String... params) {
//            StringBuilder result = null;
//            int respCode = -1;
//////                url = new URL("http://cep.correiocontrol.com.br/" + params[0] + ".json");
//            try {
//                url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + params[0]);
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//            System.out.println("URL: " + url);
//
//            if (params.length == 0)
//                return "";
//            try {
//
//                HttpURLConnection = (HttpURLConnection) url.openConnection();
//
//                {
//                    if (HttpURLConnection != null) {
//                        respCode = HttpURLConnection.getResponseCode();
//                    }
//                }
//                while (respCode == -1) ;
//
//                if (respCode == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(new InputStreamReader(HttpURLConnection.getInputStream()));
//                    result = new StringBuilder();
//                    String line;
//                    while ((line = br.readLine()) != null) {
//                        result.append(line);
//                    }
//                    br.close();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if (HttpURLConnection != null) {
//                    HttpURLConnection.disconnect();
//                    HttpURLConnection = null;
//                }
//            }
//            System.out.println("Brazil Address Response: " + result.toString());
//
//            return (result != null) ? result.toString() :
//                    null;
//
//
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            String brazil_address = "";
//            try {
//
//
//                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//                DocumentBuilder db = dbf.newDocumentBuilder();
//                Document doc = db.parse(new InputSource(new StringReader(s)));
//                // normalize the document
//                doc.getDocumentElement().normalize();
//                // get the root node
//                NodeList nodeList = doc.getElementsByTagName("webservicecep");
//                Node node = nodeList.item(0);
//                System.out.println("webservicecep getTextContent :" + node.getTextContent());
//                System.out.println("webservicecep :" + node.getNodeValue());
//
//                // the  node has three child nodes
//                boolean isAddressfound = false;
//                if (node.getChildNodes().item(0).getNodeName().equalsIgnoreCase("resultado")) {
//                    isAddressfound = true;
//                    for (int i = 0; i < node.getChildNodes().getLength(); i++) {
//                        Node temp = node.getChildNodes().item(i);
//
////                        if (temp.getNodeName().equalsIgnoreCase("logradouro")) {
////                            brazil_address += " " + URLDecoder.decode(temp.getTextContent(), "iso-8859-1");
//////                            String val = new String(temp.getTextContent().getBytes("iso-8859-1"), "UTF-8");
//////                            System.out.println("Brazil Address:val:  " + val);
////                            brazil_address += " " + URLDecoder.decode(temp.getTextContent(), "iso-8859-1");
////                        } else if (temp.getNodeName().equalsIgnoreCase("bairro")) {
////                            brazil_address += " " + URLDecoder.decode(temp.getTextContent(), "utf-8");
//////                            String val = new String(temp.getTextContent().getBytes("iso-8859-1"), "UTF-8");
//////                            System.out.println("Brazil Address:val:  " + val);
////                        } else if (temp.getNodeName().equalsIgnoreCase("cidade")) {
////                            brazil_address += " " + URLDecoder.decode(temp.getTextContent(), "utf-8");
//////                            String val = new String(temp.getTextContent().getBytes("iso-8859-1"), "UTF-8");
//////                            System.out.println("Brazil Address:val:  " + val);
////                        }
//                        System.out.println("Brazil Address[" + i + "]: " + URLDecoder.decode(temp.getTextContent(), "utf-8"));
//                    }
//                } else
//                    isAddressfound = false;
//
//                System.out.println("Brazil Address: " + brazil_address);
//                System.out.println("Brazil Address:() " + URLDecoder.decode(brazil_address, "utf-8"));
//                System.out.println("Brazil Address:(encode- iso-8859-1) " + URLEncoder.encode(brazil_address, "iso-8859-1"));
//
//                System.out.println("São Paulo:(encode- iso-8859-1) " + URLEncoder.encode("São Paulo", "iso-8859-1"));
//                System.out.println("São Paulo:(decode- iso-8859-1) " + URLDecoder.decode("São Paulo", "iso-8859-1"));
//
//                System.out.println("S�o Paulo:(encode- iso-8859-1) " + URLEncoder.encode("S�o Paulo", "iso-8859-1"));
//                System.out.println("S�o Paulo:(decode- iso-8859-1) " + URLDecoder.decode("S�o Paulo", "iso-8859-1"));
//
//                String val = new String("São Paulo".getBytes("iso-8859-1"), "UTF-8");
//                System.out.println("Brazil Address:val:  " + val);
//
//                val = new String("São Paulo".getBytes("iso-8859-1"), "UTF-8");
//                System.out.println("Brazil Address:val:  " + val);
//
//                if (!isAddressfound) {
//
//                } else {
////                    getLocationPoints(URLEncoder.encode(brazil_address, "iso-8859-1"));
//                    new LoadLocationFromCEP().execute(URLEncoder.encode(brazil_address, "iso-8859-1"));
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }


    /*
  Below Service will be called when user enters the pin code of brazil to get address
   */
    String brazil_CEP_search = "";

    public void CallBrazilAddress(String Pin) {
        String cepurl = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + Pin;
        System.out.println("CEP URL: " + cepurl);
        brazil_CEP_search = Pin;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.GET, cepurl,
                BrazilAddresssuccess(), BrazilAddressfail());
        queue.add(myReq);
    }

    private Response.Listener<String> BrazilAddresssuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("webservicecep Response :" + response);
                String brazil_address = "";

                try {

                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new InputSource(new StringReader(response)));
                    // normalize the document
                    doc.getDocumentElement().normalize();
                    // get the root node
                    NodeList nodeList = doc.getElementsByTagName("webservicecep");
                    Node node = nodeList.item(0);
                    System.out.println("webservicecep TextContent :" + node.getTextContent());

                    // the  node has three child nodes
                    boolean isAddressfound = true;

                    for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                        System.out.println("J=" + j);
                        String nameofattribute = node.getChildNodes().item(j).getNodeName();
                        if (nameofattribute.equalsIgnoreCase("resultado")
                                && node.getChildNodes().item(j).getTextContent().equalsIgnoreCase("1")) {
                            System.out.println("GONE when j=" + j);
                            isAddressfound = true;
                            break;
                        } else
                            isAddressfound = false;
                    }

                    if (isAddressfound) {
                        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                            System.out.println("GONE  I=" + i);
                            Node temp = node.getChildNodes().item(i);
                            System.out.println("webservicecep getNodeName: " + temp.getNodeName());
                            Log.e("getTextContent: ", "webservicecep getTextContent: " + temp.getTextContent());

                            if (temp.getNodeName().equalsIgnoreCase("cidade")) {
                                System.out.println("GONE logradouro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("bairro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("tipo_logradouro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("logradouro")) {
                                System.out.println("GONE cidade");
                                brazil_address += " " + temp.getTextContent();
                            }
                            System.out.println("Brazil Address[" + i + "]: " + brazil_address);
                        }
                        System.out.println("Brazil Address: " + brazil_address);
                    }
                    /*
                    Now below condition
                    "if (brazil_address.replaceAll(" ", "").length() == 0)"
                    will decide that set the "mNavigationActivity.mAutoCompleteTextViewSearch" with
                    Google places api adapter or to return the brazil address

                     */
                    if (brazil_address.replaceAll(" ", "").length() == 0) {
                        Log.e("BrazilAddresssuccess().MapFragment", "could not find the give brazil address: " + brazil_CEP_search);
                        Toast.makeText(getActivity(), "Sorry, Address with CEP " + brazil_CEP_search + " could not be found.", Toast.LENGTH_SHORT).show();
//                        mNavigationActivity.showAutocompleteTextview();
//                        mNavigationActivity.mAutoCompleteTextViewSearch.setText(brazil_CEP_search);
//                        Timer timer = new Timer();
//                        timer.schedule(new TimerTask() {
//                            @Override
//                            public void run() {
//                                GoogleAddressesresultList = autocomplete(brazil_CEP_search);
//                                System.out.println("size of :" + GoogleAddressesresultList.size());
//                                /*
//                                here "GoogleAddressesresultList" i set the array of serach result to arraylist
//                                 to over come null pointer error in adapter of "GooglePlacesAutocompleteAdapter"
//                                 */
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        setAutocompleteTextAdapter();
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.setAdapter(
//                                                new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.showDropDown();
//                                    }
//                                });
//
//                            }
//                        }, 100);
//
//
//                        /*
//                        setAutocompleteTextAdapter();
//                        using above line the Placesapi adapter will be set for auto complete textview.
//                         */
                    } else {
//                        getLocationPoints(brazil_address);
                        new LoadLocationFromCEP().execute(brazil_address);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private Response.ErrorListener BrazilAddressfail() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Sorry, your address could not be found.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void setFavoriteAddressFromFragmentFavorite(com.waycreon.thinkpizza.datamodels.Address mAddress, int i) {

        if (mAddress != null) {

            Log.e("setFavoriteAddressFromFragmentFavorite()", "-----------------Favorite Address to FragFavorite----------------");
            mAddress.printAllData();

            Tags.ADDRESS_NUMBER = 3;
            mNavigationActivity.mAddressFragment.setFavoriteAddress(mAddress);
            if (mAddress != null)
                mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
        } else {
            Tags.ADDRESS_NUMBER = 0;
            System.out.println("Address Is null:");
            Toast.makeText(getActivity(), "Unable to locate postal address.", Toast.LENGTH_SHORT).show();
            Log.e("setFavoriteAddressInDialog()", "There is no Address in favorites.");
        }
    }

    /*
    This is the old method for showing single favorite pizza
    which was used to set the address in the Favorite dialog as well as in delivery address
    this is used in the FavoritePizzaAdapter.java which is not usable now;
     but sitll file is there without usage.
     */
    public void setFavoriteAddressInDialog(int i) {

        if (mOrderedItemsDatasFavorite.get(i).getAddress() != null) {
            Tags.ADDRESS_NUMBER = 3;
            com.waycreon.thinkpizza.datamodels.Address mAddress = mOrderedItemsDatasFavorite.get(i).getAddress();

            if (mLinearLayoutFavoriteAddress != null) {
                mLinearLayoutFavoriteAddress.setVisibility(View.VISIBLE);
                mTextViewTagNameFavorite.setText(mAddress.getTag_name());
                mTextViewFullAddressFavorite.setText(mAddress.getOnlyAddressLines());
            }
            mNavigationActivity.mAddressFragment.setFavoriteAddress(mAddress);
            if (mAddress != null)
                mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
        } else {
            Tags.ADDRESS_NUMBER = 0;
            System.out.println("Address Is null:");
            mLinearLayoutFavoriteAddress.setVisibility(View.VISIBLE);
            mTextViewTagNameFavorite.setText("No Address");
            mTextViewFullAddressFavorite.setText("Address not found with this favorite pizza");
            Toast.makeText(getActivity(), "Address not found with this favorite pizza.", Toast.LENGTH_SHORT).show();
            Log.e("setFavoriteAddressInDialog()", "There is no Address in favorites.");
        }

    }

    /*
        Old FavoriteService for single pizza not useful now.
     */
    public class FavoriteSerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading Favorites...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
//                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
            HttpGet mHttpGet = new HttpGet(Constants.GET_FAVORITES + "&user_id=" + Userid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("FavoriteSerivce().DoinBackground()", "Could not reach to server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Could not reach to server.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of Favorites:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("favourites");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasFavorite = new ArrayList<>();


                        for (int i = 0; i < mJsonArray.length(); i++) {
                            float total_tmp = 0;
                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setId(temp.getString("id"));
                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
//                            mOrderedItemsData.setAdditions(temp.getString("additions"));
//                            mOrderedItemsData.setId(temp.getString("custom_ingredients"));
//                            "custom_ingredients": [],
                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
                            mOrderedItemsData.setQty(1);

                            System.out.println("Orderdata : " + temp);

                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
                            System.out.println("pizza_details from Gson: " + pizza_details);
                            mOrderedItemsData.setPizza_details(pizza_details);


//                            JSONObject pizzadata = temp.getJSONObject("pizza_details");
//                            System.out.println("PizzaDAta by manual: " + pizzadata);
//                            PizzaListData mPizzaListData = new PizzaListData();
//                            mPizzaListData.setId(pizzadata.getString("id"));
//                            mPizzaListData.setName(pizzadata.getString("name"));

                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
                            System.out.println("pizzaria from Gson: " + pizzaria);
                            mOrderedItemsData.setPizzaria(pizzaria);

                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            if (temp.getString("pizzacrust").length() > 2) {
                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
                                System.out.println("pizzacrust from Gson: " + pizzacrust);
                                mOrderedItemsData.setPizzacrust(pizzacrust);
                                total_tmp += pizzacrust.getPrice();
                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
                            }
                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            if (temp.getString("extra_ingredients").length() > 2) {

                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
                                float total_additions = 0;
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
                                    System.out.println("AdditionData: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListAdditionsListDatas.add(mAdditionsListData);

                                    total_additions += mAdditionsListData.getPrice();
                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                total_tmp += total_additions;
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
                                mArrayListAdditionsListDatas = null;
                            }


                            /*
                            if (temp.getString("custom_ingredients").length() > 2) {

                                JSONArray mJsonArraycustom_ingredients = temp.getJSONArray("custom_ingredients");
                                ArrayList<AdditionsListData> mArrayListCustomIngredients = new ArrayList<>();
                                float total_customs = 0;
                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
                                for (int j = 0; j < mJsonArraycustom_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArraycustom_ingredients.getJSONObject(j);
                                    System.out.println("CustomIngredient: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListCustomIngredients.add(mAdditionsListData);

                                    total_customs += mAdditionsListData.getPrice();
                                    Log.e("Favorite Service.onPostExecute()-->", "Added Custom Pizza Toppings Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }
                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
                                total_tmp += total_customs;
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);


                                /*
                                30-9-2015 temporary comment
                                 */
                                /*mOrderedItemsData.setCustom_ingredients(mArrayListCustomIngredients);*/

                        /*
                                System.out.println("extra_ingredients from Gson: " + mJsonArraycustom_ingredients);
//                                mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListCustomIngredients = null;
                            }
                            */


                            /*
                            6-10-2015 code for Toppings selected is added
                             */

                            if (temp.getString("toppings").length() > 2) {

                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();


                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {

                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
                                    System.out.println("Toppings: " + temp_addition);
                                    Toppings mAdditionsListData = new Toppings();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));

                                    mArrayListToppingses.add(mAdditionsListData);

                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setToppings(mArrayListToppingses);


                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListToppingses = null;
                            }

                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
                            System.out.println("address from Gson: " + pizzaria);
                            mOrderedItemsData.setAddress(address);

                            Log.e("FavoriteSerivce()", "-----------------Favorite Pizzas----------------");
                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
                            mOrderedItemsData.printAllData();

                            if (temp.getString("pizza_size").equalsIgnoreCase("regular")) {
                                System.out.println("Favorite pizza's total set with regular size");
                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            } else if (temp.getString("pizza_size").equalsIgnoreCase("medium")) {
                                System.out.println("Favorite pizza's total set with meduim size");
                                Log.e("Favorite Service.onPostExecute()", "Small Medium: " + mOrderedItemsData.getPizza_details().getPrice_medium());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_medium());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            } else if (temp.getString("pizza_size").equalsIgnoreCase("large")) {
                                System.out.println("Favorite pizza's total set with large size");
                                Log.e("Favorite Service.onPostExecute()", "Small Large: " + mOrderedItemsData.getPizza_details().getPrice_large());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            }

                            mOrderedItemsData.setTotal(total_tmp);
                            mOrderedItemsData.setIsFromFavorite_History(true);
                            /*
                                Added on 10-10-2015
                            below line is added to set the favoorite id to pizza so that
                            when user add a pizza to basket at that time while orders the pizza
                            its address can be updated.

                             */
                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));

                        }
                        //LOOP ENDS

                        mAdapterFavoritePizza = new FavoritePizzaAdapter(getActivity(), mOrderedItemsDatasFavorite, "FavoritePizza");
                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
                        mDialogPizzaFavorite.show();

                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite_pizza);
                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);

                    } else {
                        Log.e("FavoriteSerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            }
            mImageViewFavorite.setEnabled(true);
        }
    }

//        mProgressDialog = new ProgressDialog(getActivity());
//        mProgressDialog.setMessage("Loading Favorites");
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.show();
//        RequestQueue queue = Volley.newRequestQueue(getActivity());
//
//        StringRequest myReq = new StringRequest(Request.Method.GET,
//                Constants.GET_FAVORITES + "&user_id=" + Userid,
//                createSuccessFavoriteAddress(),
//                createFailureFavoriteAddress());
//        queue.add(myReq);

//    }

//    private Response.Listener<String> createSuccessFavoriteAddress() {
//        return new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                mProgressDialog.dismiss();
//
//                System.out.println("createSuccessFavorite().MapFragment: Favorite Address Response: " + response);
//                /*com.waycreon.thinkpizza.datamodels.Address mAddress = mGson.fromJson(response, com.waycreon.thinkpizza.datamodels.Address.class);
//
//                if (mAddress.getResponseCode() == 1) {
//                    mAddressesFavorites = mAddress.getAddresses();
//
//                    if (mAddressesFavorites.size() > 0) {
//                        mAddressAdapterFavorite = new AddressAdapter(getActivity(), mAddressesFavorites);
//                        mListViewFavorite.setAdapter(mAddressAdapterFavorite);
//                        mDialogFavorite.show();
//                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite);
//                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
//                    }
//                } else {
//                    ShowToast(mAddress.getResponseMsg());
//                    Log.e("createSuccessFavorite().MapFragment", "Response: " + mAddress.getResponseMsg());
//                }*/
////                OrderedItemsData mOrderedItemsData = mGson.fromJson(response, OrderedItemsData.class);
//                OrderedItemsData mOrderedItemsData = mGson.fromJson(response, OrderedItemsData.class);
//
//                if (mOrderedItemsData.getResponseCode() == 1) {
//                    mOrderedItemsDatasFavorite = mOrderedItemsData.getFavourites();
//
//                    if (mOrderedItemsDatasFavorite.size() > 0) {
//                        mFavoritePizzaAdapter = new FavoritePizzaAdapter(getActivity(), mOrderedItemsDatasFavorite);
//                        mListViewFavorite.setAdapter(mAddressAdapterFavorite);
//                        mDialogFavorite.show();
//                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite);
//                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
//                    }
//                } else {
//                    ShowToast(mOrderedItemsData.getResponseMsg());
//                    Log.e("createSuccessFavorite().MapFragment", "Response: " + mOrderedItemsData.getResponseMsg());
//                }
//
//            }
//        };
//    }
//
//    private Response.ErrorListener createFailureFavoriteAddress() {
//        return new Response.ErrorListener() {
//            @SuppressLint("LongLogTag")
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                mProgressDialog.dismiss();
//                Log.e("createFailureFavorite().MapFragment", "Error: " + error.getMessage());
//                ShowToast(error.getMessage() + "");
//            }
//        };
//    }

    /*
    This is the old method for showing single History pizza
    which was used to set the address in the History dialog as well as in delivery address
    this is used in the FavoritePizzaAdapter.java which is not usable now;
     but sitll file is there without usage.
     */
    public void setHistoryAddressInDialog(int i) {

        if (mOrderedItemsDatasHistory.get(i).getAddress() != null) {

            mLinearLayoutHistoryAddress.setVisibility(View.VISIBLE);
            Tags.ADDRESS_NUMBER = 4;
            com.waycreon.thinkpizza.datamodels.Address mAddress;
            mAddress = mOrderedItemsDatasHistory.get(i).getAddress();
            String add = mAddress.getTag_name();
//                        add += "\n" + mAddress.getStreet_name();
//                        add += "\n" + mAddress.getBlock_number();
            add += "\n" + mAddress.getStreet_name();
            add += "\n" + mAddress.getStreet_no();
            add += "\n" + mAddress.getApt_no();
            add += "\n" + mAddress.getCity() + " - " + mAddress.getZip_code();
            add += "\n" + mAddress.getState();
//                        mNavigationActivity.mAddressFragment.setHistoryAddress(add);

            mTextViewTagNameHistory.setText(mAddress.getTag_name());
            mTextViewFullAddressHistory.setText(mAddress.getOnlyAddressLines());

            mNavigationActivity.mAddressFragment.setHistoryAddress(mAddress);
            if (mAddress != null)
                mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
        } else {

            Tags.ADDRESS_NUMBER = 0;
            mLinearLayoutFavoriteAddress.setVisibility(View.VISIBLE);
            mTextViewTagNameFavorite.setText("No Address");
            mTextViewFullAddressFavorite.setText("Address not found with this ordered pizza");
            Toast.makeText(getActivity(), "Address not found with this ordered pizza.", Toast.LENGTH_SHORT).show();
            Log.e("setFavoriteAddressInDialog()", "There is no Address in already ordered pizza.");
        }
    }

    /*
            Old HistoryService for single pizza not useful now.
         */
    public class HistorySerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading Orderes...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
//                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
            HttpGet mHttpGet = new HttpGet(Constants.GET_ORDERES + "&user_id=" + Userid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("HistorySerivce().DoinBackground()", "Could not reach to server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Could not reach to server.", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of Orders History:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("orders");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasHistory = new ArrayList<>();

                        for (int i = 0; i < mJsonArray.length(); i++) {

                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setId(temp.getString("id"));
                            mOrderedItemsData.setOrder_id(temp.getString("order_id"));
                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));

                            mOrderedItemsData.setQty(Integer.parseInt(temp.getString("qty")));
                            mOrderedItemsData.setTotal(Float.parseFloat(temp.getString("total")));

                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
//                            mOrderedItemsData.setAdditions(temp.getString("additions"));
//                            mOrderedItemsData.setId(temp.getString("custom_ingredients"));
//                            "custom_ingredients": [],
                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));

                            mOrderedItemsData.setPayment_type(temp.getString("payment_type"));
                            mOrderedItemsData.setPayment_details(temp.getString("payment_details"));
                            mOrderedItemsData.setOrder_date(temp.getString("order_date"));
                            mOrderedItemsData.setStatus(temp.getString("status"));
                            mOrderedItemsData.setAmount_received(temp.getDouble("amount_received"));

                            System.out.println("Orderdata : " + temp);

                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
                            System.out.println("pizza_details from Gson: " + pizza_details);
                            mOrderedItemsData.setPizza_details(pizza_details);

                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
                            System.out.println("pizzaria from Gson: " + pizzaria);
                            mOrderedItemsData.setPizzaria(pizzaria);

                            if (temp.getString("pizzacrust").length() > 2) {
                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
                                System.out.println("pizzacrust from Gson: " + pizzacrust);
                                mOrderedItemsData.setPizzacrust(pizzacrust);
                            }

                            if (temp.getString("extra_ingredients").length() > 2) {

                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();

                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
                                    System.out.println("AdditionData: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListAdditionsListDatas.add(mAdditionsListData);

                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
                                mArrayListAdditionsListDatas = null;
                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
                            }


                            /*if (temp.getString("custom_ingredients").length() > 2) {

                                JSONArray mJsonArraycustom_ingredients = temp.getJSONArray("custom_ingredients");
                                ArrayList<AdditionsListData> mArrayListCustomIngredients = new ArrayList<>();

                                for (int j = 0; j < mJsonArraycustom_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArraycustom_ingredients.getJSONObject(j);
                                    System.out.println("CustomIngredient: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListCustomIngredients.add(mAdditionsListData);

                                    mAdditionsListData = null;
                                }

                                *//*
                                30-9-2015 temporary comment
                                 *//*

                                *//*mOrderedItemsData.setCustom_ingredients(mArrayListCustomIngredients);*//*

                                mArrayListCustomIngredients = null;
                                System.out.println("extra_ingredients from Gson: " + mJsonArraycustom_ingredients);
//                                mOrderedItemsData.setExtra_ingredients(extra_ingredients);

                            }*/


                            /*
                            6-10-2015 code for Toppings selected is added
                             */

                            if (temp.getString("toppings").length() > 2) {

                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();


                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {

                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
                                    System.out.println("Toppings: " + temp_addition);
                                    Toppings mAdditionsListData = new Toppings();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));

                                    mArrayListToppingses.add(mAdditionsListData);

                                    Log.e("History Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setToppings(mArrayListToppingses);


                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListToppingses = null;
                            }


                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
                            System.out.println("address from Gson: " + pizzaria);
                            mOrderedItemsData.setAddress(address);

                            Log.e("HistorySerivce()", "-----------------Already Ordered Pizzas----------------");
                            mOrderedItemsDatasHistory.add(mOrderedItemsData);
                            mOrderedItemsData.printAllData();

                            mOrderedItemsData.setIsFromFavorite_History(true);
                        }
                        //LOOP ENDS

                        mAdapterHistoryPizza = new FavoritePizzaAdapter(getActivity(), mOrderedItemsDatasHistory, "HistoryPizza");
                        mListViewHistory.setAdapter(mAdapterHistoryPizza);
                        mDialogPizzaHistory.show();

                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_history_pizza);
                        mImageViewDialogImage.setImageResource(R.drawable.history_pizza_60);

                    } else {
                        Log.e("HistoryService().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            }
            mImageViewHistory.setEnabled(true);

        }

    }

    /*
    This method is used to show the list of Pizzeira from map screen when
    user click on the map mode image button.
     */
    public void ShowRestroListinNavigationScreen() {
        System.out.println("MapFragment: ShowRestroListinNavigationScreen() called");
        if (isRestaroAvailable && mPizzaRestros.size() > 0) {
            mRelativeLayoutMapMode.setVisibility(View.GONE);
            mRelativeLayoutListMode.setVisibility(View.VISIBLE);
            mLinearLayoutNoPizzaria.setVisibility(View.GONE);
        } else {
            mRelativeLayoutMapMode.setVisibility(View.GONE);
            mRelativeLayoutListMode.setVisibility(View.GONE);
            mLinearLayoutNoPizzaria.setVisibility(View.VISIBLE);
        }
        mNavigationActivity.mImageViewModeMap.setVisibility(View.VISIBLE);
        mNavigationActivity.mImageViewModeList.setVisibility(View.INVISIBLE);
        isModeMap = false;
        isModeList = true;
    }

    /*
    This method is used to show the Map from Pizzeria list screen when
    user click on the List mode image button.
     */
    public void ShowMapinNavigationScreen() {
        if (mRelativeLayoutMapMode != null) {
            mRelativeLayoutMapMode.setVisibility(View.VISIBLE);
            mRelativeLayoutListMode.setVisibility(View.GONE);
            mLinearLayoutNoPizzaria.setVisibility(View.GONE);
            mNavigationActivity.mImageViewModeMap.setVisibility(View.INVISIBLE);
            mNavigationActivity.mImageViewModeList.setVisibility(View.VISIBLE);
            isModeMap = true;
            isModeList = false;
        }
    }

    /*
    This is the new code for showing only favorite addresses in the dialog.
     */
    Dialog mDialogFavorite;
    ArrayList<com.waycreon.thinkpizza.datamodels.Address> mAddressesFavorites;
    AddressAdapter mAddressAdapterFavorite;
    ListView mListViewFavoriteAddresses;
    RelativeLayout mRelativeLayoutMainAddressLayout;

    private void initFavoriteAddressesDialog() {
        mDialogFavorite = new Dialog(getActivity());
        mDialogFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavorite.setContentView(R.layout.dialog_list_address);

        mRelativeLayoutMainAddressLayout = (RelativeLayout) mDialogFavorite.findViewById(R.id.dialog_list_address_mainlayout);
        mListViewFavoriteAddresses = (ListView) mDialogFavorite.findViewById(R.id.address_list_list_pizzas);
        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.address_list_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.address_list_image_dialog_title);

        CallFavoriteAddressService();
        mListViewFavoriteAddresses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                mDialogFavorite.dismiss();
                Tags.ADDRESS_NUMBER = 3;
                com.waycreon.thinkpizza.datamodels.Address mAddress = (com.waycreon.thinkpizza.datamodels.Address)
                        mListViewFavoriteAddresses.getItemAtPosition(i);
                String add = mAddress.getTag_name();
                add += "\n" + mAddress.getStreet_name();
                add += "\n" + mAddress.getStreet_no();
                add += "\n" + mAddress.getApt_no();
                add += "\n" + mAddress.getCity() + " - " + mAddress.getZip_code();
                add += "\n" + mAddress.getState();
//                        mNavigationActivity.mAddressFragment.setFavoriteAddress(add);
                mNavigationActivity.mAddressFragment.setFavoriteAddress(mAddress);
                if (mAddress != null)
                    mNavigationActivity.mBasketFragment.setDeliveryAddress(mAddress);
                setMapLocationFromFavoriteHistoryAddress(mAddress);

            }
        });
    }

    public void CallFavoriteAddressService() {
        mProgressDialog.setMessage("Loading Address");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mGson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.GET,
                Constants.GET_FAVORITE_ADDRESS + "&user_id=" + Userid,
                createSuccessFavoriteAddress(),
                createFailureFavoriteAddress());
        queue.add(myReq);

    }

    private Response.Listener<String> createSuccessFavoriteAddress() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();

                System.out.println("createSuccessFavorite().MapFragment: Favorite Address Response: " + response);
                com.waycreon.thinkpizza.datamodels.Address mAddress = mGson.fromJson(response, com.waycreon.thinkpizza.datamodels.Address.class);

                if (mAddress.getResponseCode() == 1) {
                    mAddressesFavorites = mAddress.getAddresses();

                    if (mAddressesFavorites.size() > 0) {
                        mAddressAdapterFavorite = new AddressAdapter(getActivity(), mAddressesFavorites);
                        mListViewFavoriteAddresses.setAdapter(mAddressAdapterFavorite);
                        mDialogFavorite.show();
                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite_address);
                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);

                        Timer mTimer = new Timer();
                        mTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
//                                        mDialogFavorite.show();
                                        Utils.changeLayoutHeight(getActivity(), mRelativeLayoutMainAddressLayout);

                                    }
                                });
                            }
                        }, 10);

                    }

                } else {
                    Toast.makeText(getActivity(), mAddress.getResponseMsg(), Toast.LENGTH_SHORT).show();
                    Log.e("createSuccessFavorite().MapFragment", "Response: " + mAddress.getResponseMsg());
                }
            }
        };
    }

    private Response.ErrorListener createFailureFavoriteAddress() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.dismiss();
                Log.e("createFailureFavorite().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), error.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        };
    }

    /*
    This is the new code for showing only favorite addresses in the dialog.
    Ends here
     */
    /*
        setMapLocationFromFavoriteHistoryManualAddress() method is used to show the restaurant when user
         selects the address from Favorite List of Addresses From map screen or selects the address from
         Address Screen from Favorite/History part.
     */
    public void setMapLocationFromFavoriteHistoryAddress(com.waycreon.thinkpizza.datamodels.Address mAddress) {
        googleMap.clear();
        latitude = Double.parseDouble(mAddress.getLat());
        longitude = Double.parseDouble(mAddress.getLng());

        googleMap = mMapView.getMap();
        googleMap.setOnMapClickListener(this);

        CallRestaurantService(mAddress.getStreet_name(), mAddress.getZip_code());
//        new getAreaName_CL_With_Pizzeria().execute();
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("You are hear...");
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}

