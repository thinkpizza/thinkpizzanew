package com.waycreon.thinkpizza.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.imageloader.ImageLoader;

public class RestaurantFullDetailFragment extends Fragment {

    ImageView mImageViewRestaurentImage;
    TextView mTextViewRestaurentName;
    TextView mTextViewRestaurentAddress;

    TextView mTextViewMonday;
    TextView mTextViewTuesday;
    TextView mTextViewWednesday;
    TextView mTextViewThursday;
    TextView mTextViewFriday;
    TextView mTextViewSaturday;
    TextView mTextViewSunday;

    PizzaRestro mRestaurentDataReceived = null;
    ImageLoader mImageLoader;
    NavigationActivity mNavigationActivity;

    RatingBar mRatingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_restaurant_full_detail, container, false);

        mImageLoader = new ImageLoader(getActivity());
        mNavigationActivity = (NavigationActivity) getActivity();

        mImageViewRestaurentImage = (ImageView) mView.findViewById(R.id.frag_restaurant_fulldetail_image_res);
        mTextViewRestaurentName = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_res_name);
        mTextViewRestaurentAddress = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_res_address);

        mTextViewMonday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_monday);
        mTextViewTuesday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_tuesday);
        mTextViewWednesday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_wednesday);
        mTextViewThursday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_thursday);
        mTextViewFriday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_friday);
        mTextViewSaturday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_saturday);
        mTextViewSunday = (TextView) mView.findViewById(R.id.frag_restaurant_fulldetail_text_sunday);

        mRatingBar = (RatingBar) mView.findViewById(R.id.frag_restaurant_fulldetail_ratingbar_res);
        mRatingBar.setClickable(false);
        mRatingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        return mView;

    }

    public void SetRetaurent(PizzaRestro mPizzaRestro) {

        mRestaurentDataReceived = mPizzaRestro;
        mImageLoader.DisplayImage(mPizzaRestro.getImg_url(), mImageViewRestaurentImage);
        mTextViewRestaurentName.setText(mPizzaRestro.getName());
        String add = mPizzaRestro.getStreet_name();
        add += ", " + mPizzaRestro.getStreet_no();
        add += "\n" + mPizzaRestro.getApt_no();
        mTextViewRestaurentAddress.setText(add);

        int total_rating = (int) (mPizzaRestro.getOverall_ratings() + mPizzaRestro.getCrust_ratings() + mPizzaRestro.getToppings_quality_ratings() + mPizzaRestro.getDelivery_time_ratings());
        int total_rating_count = (int) (mPizzaRestro.getOverall_count() + mPizzaRestro.getCrust_count() + mPizzaRestro.getToppings_quality_count() + mPizzaRestro.getDelivery_time_count());
        int rating = total_rating / total_rating_count;

        mRatingBar.setRating(rating);
        System.out.println("Calculated Ratings: " + rating);

        String days[] = mPizzaRestro.getOpen_days().split(",");
        mTextViewMonday.setText("Monday: " + days[0]);
        mTextViewTuesday.setText("Tuesday: " + days[1]);
        mTextViewWednesday.setText("Wednesday: " + days[2]);
        mTextViewThursday.setText("Thursday: " + days[3]);
        mTextViewFriday.setText("Friday: " + days[4]);
        mTextViewSaturday.setText("Saturday: " + days[5]);
        mTextViewSunday.setText("Sunday: " + days[6]);

    }

    public void callBasketRestroData() {
        mNavigationActivity.mBasketFragment.SetRestaurantData(mRestaurentDataReceived);
    }
}
