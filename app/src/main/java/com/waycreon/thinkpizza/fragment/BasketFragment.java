package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.adapter.BasketListAdapter;
import com.waycreon.thinkpizza.datamodels.Address;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.thinkpizza.imageloader.ImageLoaderBlur;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by pradip on 8/5/2015.
 */
public class BasketFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    public void setListViewHeightBasedOnChildren() {
        ListAdapter listAdapter = mListViewOrderedItems.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListViewOrderedItems.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        Log.e("setListViewHeightBasedOnChildren()", "List Size: " + listAdapter.getCount());
        int gap = 8;

        int substract_height = 0;
        int add_height = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            substract_height = 0;
            add_height = 0;

            view = listAdapter.getView(i, view, mListViewOrderedItems);
            OrderedItemsData mOrderedItemsData = (OrderedItemsData) listAdapter.getItem(i);

            if (i > 0 && i < 3)
                gap = 8;
            if (i >= 3)
                gap = 10;
            else if (i >= 6)
                gap = 12;
            else if (i >= 10)
                gap = 13;

            if (mOrderedItemsData.getPizza_details().getName().length() > 15)
                gap += 10;
            else if (mOrderedItemsData.getPizza_details().getName().length() > 20)
                gap += 12;
            else if (mOrderedItemsData.getPizza_details().getName().length() > 25 && mOrderedItemsData.getPizza_details().getName().length() < 30)
                gap += 15;
            if (mOrderedItemsData.getPizza_details().getName().length() > 35)
                gap += 20;

            if (mOrderedItemsData.getExtra_ingredients() != null) {
                String additions = "";

                for (int j = 0; j < mOrderedItemsData.getExtra_ingredients().size(); j++)
                    additions += mOrderedItemsData.getExtra_ingredients().get(j).getName() + ", ";
                Log.e("getPizzacrust() !=null BasketListAdapter.java", "Lets show additions." + additions);
                if (mOrderedItemsData.getExtra_ingredients().size() > 2 && mOrderedItemsData.getExtra_ingredients().size() < 4)
                    gap += 20;
                else if (mOrderedItemsData.getExtra_ingredients().size() > 4)
                    gap += 30;
                else
                    gap += 12;
            }

            if (mOrderedItemsData.getPizzacrust() != null) {
                Log.e("getPizzacrust() !=null BasketListAdapter.java", "Lets show PizzaCrust.");
                if (mOrderedItemsData.getPizzacrust().isPizzaCrustSelected()) {
                    gap += 12;
                }
            }


//            if (i == 0) {
            //      view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
//                System.out.println("Height of view:" + view.getLayoutParams().height);
//            }
//            System.out.println("measured Height of view(" + i + "):" + view.getMeasuredHeight());
//            System.out.println("Height of view(B):" + view.getLayoutParams().height);

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += (view.getMeasuredHeight() + gap);
            //   System.out.println("Height of view(A):" + view.getLayoutParams().height);
            System.out.println("totalHeight(in loop):" + totalHeight);
        }
        Log.e("setListViewHeightBasedOnChildren()", "Total Hieght (out of loop): " + totalHeight);
        ViewGroup.LayoutParams params = mListViewOrderedItems.getLayoutParams();
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        params.height = totalHeight + (mListViewOrderedItems.getDividerHeight() * (listAdapter.getCount()));
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        mListViewOrderedItems.setLayoutParams(params);
        mListViewOrderedItems.requestLayout();

        /*ListAdapter listAdapter = mListViewOrderedItems.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, mListViewOrderedItems);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = mListViewOrderedItems.getLayoutParams();
        params.height = totalHeight + (mListViewOrderedItems.getDividerHeight() * (listAdapter.getCount() - 1));
        mListViewOrderedItems.setLayoutParams(params);*/

//        ListAdapter listAdapter = mListViewOrderedItems.getAdapter();
//        if (listAdapter == null) {
//            // pre-condition
//            return;
//        }
//
//        int totalHeight = mListViewOrderedItems.getPaddingTop() + mListViewOrderedItems.getPaddingBottom();
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            View listItem = listAdapter.getView(i, null, mListViewOrderedItems);
//
//            RelativeLayout mRelativeLayout = (RelativeLayout) listItem.findViewById(R.id.basketlist_layout_mainparent);
//            System.out.println("mRelativeLayout .Height: " + mRelativeLayout.getHeight());
////            if (listItem instanceof ViewGroup) {
////                listItem.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
////            }
//            listItem.measure(0, 0);
////            listItem.measure(View.MeasureSpec.AT_MOST, View.MeasureSpec.UNSPECIFIED);
//            totalHeight += listItem.getMeasuredHeight();
//        }
//
//        ViewGroup.LayoutParams params = mListViewOrderedItems.getLayoutParams();
//        params.height = totalHeight + (mListViewOrderedItems.getDividerHeight() * (listAdapter.getCount() - 1));
//        mListViewOrderedItems.setLayoutParams(params);

    }

    /*
    layout 1 restaurant
     */
    /*ImageView mImageViewRestaurantImageBK;
    ImageView mImageViewRestaurantImage;
    TextView mTextViewRestaurantName;*/
    TextView mTextViewGrnadTotal;
    /*
    layout 2 process images
     */
    public ImageView mImageViewProcess1;
    public ImageView mImageViewProcess2;
    public ImageView mImageViewProcess3;
    /*
    layout 3 Items that are ordered
     */
    public RelativeLayout mRelativeLayoutOrderedItems;
    //    TextView mTextViewEditOrder;
    public ListView mListViewOrderedItems;
    TextView mTextViewAddItems;
    TextView mTextViewSubTotal;
    TextView mTextViewServiceTax;
    TextView mTextViewDeliveryCharge;
    TextView mTextViewTotalorder;
    TextView mTextViewTotalQuantity;
    //    TextView mTextViewChooseDeliveryAddress;
    Button mTextViewChooseDeliveryAddress;
    /*
    * layout 4 Address layout
//    * */
    public RelativeLayout mRelativeLayoutAddressLayout;
//    LinearLayout mLinearLayoutTextAddress;
//    TextView mTextViewAddressline1;
//    TextView mTextViewAddressline2;
//    TextView mTextViewAddressline3;

    //    LinearLayout mLinearLayoutEditAddress;
    EditText mEditTextTagName;
    EditText mEditTextAddress_StreetName;
    EditText mEditTextAddress_Street_no;
    EditText mEditTextAddress_Apt_no;
    //    EditText mEditTextAddressline1;
//    EditText mEditTextAddressline2;
//    EditText mEditTextAddressline3;
    EditText mEditTextCity;
    EditText mEditTextZipCode;
    EditText mEditTextState;

    //    TextView mTextViewCityName;
//    TextView mTextViewZipCode;
    TextView mTextViewChangeAddress;
    //    TextView mTextViewEditAddress;
    TextView mTextViewContactNumber;
    Button mButtonChoosePaymentMethod;

    /*
     layout 5 placing order and payment
    */
    public RelativeLayout mRelativeLayoutPayment;
    EditText mEditTextExchangeMoney;
    Button mButtonPlaceOrder;
//    ------------------------------
    /*
   -------------------------------------------------------------------------------------------------
   ------------------------------------------------------------------------------------------------
                   Add to Favorite Dialog Declaration Start
     -------------------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------------------------
	 */

    public Dialog mDialogFavorite;

    //    TextView mTextViewDialogHeading;
//    ImageView mImageViewDialogImage;
//    LinearLayout mRelativeLayoutAddtoFavorite_dialog;
//    TextView mTextViewYes;
//    TextView mTextViewNo;

    /*
    Order Confirmed Dialog
     */
    public Dialog mDialogOrderConfirm;

    NavigationActivity mNavigationActivity;
    //    public ArrayList<OrderedItemsData> mArrayListOrderedItem;
    public BasketListAdapter mBasketListAdapter;
    public double subtotal = 0;
    double GrandTotal = 0;

    PizzaRestro mPizzaRestroReceived;

    float servicetaxpercentage = 2;
    double servicetax = 0;
    double deliverycharge = 0;
    ImageLoader mImageLoader;
    ImageLoaderBlur mImageLoaderBlur;

    Address mAddressDeliveryAddress;
    Store_pref mStore_pref;
    String user_id = "";
    int is_order_favorite = 0;
    ProgressDialog mProgressDialog;
    ProgressDialog mProgressDialogUpdatingFavorites;
    String tmp = "";
    /*String tmp_restro_id = "";*/
    public int lastVisibleLayout = 1;
//    boolean isFavoritePizzaAdded = false;

    double quantityTotal = 0;

    /*
    below variable has been used to check if address service run successfully or not
    if yes then store address id in it. and while placing order it is sent.
    now if order is not placed then use this variable to send address id no
    need to call address service again.
     */
    boolean isAddressServiceSuccess = false;
    String AddressId = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View mView = inflater.inflate(R.layout.fragment_basket, container, false);
            /*
    layout 1 restaurant
     */
        Log.e("onCreateView()", "BasketFragment Fragment created.");
        /*mImageViewRestaurantImageBK = (ImageView) mView.findViewById(R.id.frag_basket_image_restaurent_background);
        mImageViewRestaurantImage = (ImageView) mView.findViewById(R.id.frag_basket_image_restaurentimage);
        mTextViewRestaurantName = (TextView) mView.findViewById(R.id.frag_basket_text_restaurentname);*/
        mTextViewGrnadTotal = (TextView) mView.findViewById(R.id.frag_basket_text_grandtotal);
    /*
    layout 2 process images
     */
        mImageViewProcess1 = (ImageView) mView.findViewById(R.id.frag_basket_image_process1);
        mImageViewProcess2 = (ImageView) mView.findViewById(R.id.frag_basket_image_process2);
        mImageViewProcess3 = (ImageView) mView.findViewById(R.id.frag_basket_image_process3);
    /*
    layout 3 Items that are ordered
     */
        mRelativeLayoutOrderedItems = (RelativeLayout) mView.findViewById(R.id.frag_basket_layout_ordereditemslist);
//        mTextViewEditOrder = (TextView) mView.findViewById(R.id.frag_basket_text_editorder);
        mListViewOrderedItems = (ListView) mView.findViewById(R.id.frag_basket_list_itemsordered);
        mTextViewAddItems = (TextView) mView.findViewById(R.id.frag_basket_text_additem);
        mTextViewSubTotal = (TextView) mView.findViewById(R.id.frag_basket_text_subtotal);
        mTextViewServiceTax = (TextView) mView.findViewById(R.id.frag_basket_text_servicetax);
        mTextViewDeliveryCharge = (TextView) mView.findViewById(R.id.frag_basket_text_deliverycharge);
        mTextViewTotalorder = (TextView) mView.findViewById(R.id.frag_basket_text_totalorder);
        mTextViewTotalQuantity = (TextView) mView.findViewById(R.id.frag_basket_text_total_quantity);
        mTextViewChooseDeliveryAddress = (Button) mView.findViewById(R.id.frag_basket_button_deliveryaddress);
    /*
    * layout 4 Address layout
    * */
        mRelativeLayoutAddressLayout = (RelativeLayout) mView.findViewById(R.id.frag_basket_layout_deliveryaddress);
//        mLinearLayoutTextAddress = (LinearLayout) mView.findViewById(R.id.frag_basket_layout_address_texts);
//        mTextViewAddressline1 = (TextView) mView.findViewById(R.id.frag_basket_text_address_line1);
//        mTextViewAddressline2 = (TextView) mView.findViewById(R.id.frag_basket_text_address_line2);
//        mTextViewAddressline3 = (TextView) mView.findViewById(R.id.frag_basket_text_address_line3);

//        mLinearLayoutEditAddress = (LinearLayout) mView.findViewById(R.id.frag_basket_layout_address_edits);
        mEditTextTagName = (EditText) mView.findViewById(R.id.frag_basket_edittext_tagname);
        mEditTextAddress_StreetName = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_street_name);
        mEditTextAddress_Street_no = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_street_no);
        mEditTextAddress_Apt_no = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_apt_no);
//        mEditTextAddress_area_no = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_street_no);
//        mEditTextAddressline3 = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_line3);
        mEditTextCity = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_cityname);
        mEditTextZipCode = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_zipcode);
        mEditTextState = (EditText) mView.findViewById(R.id.frag_basket_edittext_address_state);

//        mTextViewCityName = (TextView) mView.findViewById(R.id.frag_basket_text_address_cityname);
//        mTextViewZipCode= (TextView) mView.findViewById(R.id.frag_basket_text_address_pincode);
        mTextViewChangeAddress = (TextView) mView.findViewById(R.id.frag_basket_text_changeaddress);
//        mTextViewEditAddress = (TextView) mView.findViewById(R.id.frag_basket_text_editaddress);
        mTextViewContactNumber = (TextView) mView.findViewById(R.id.frag_basket_text_contactnumber);
        mButtonChoosePaymentMethod = (Button) mView.findViewById(R.id.frag_basket_button_paymentmethod);


    /*
     layout 5 placing order and payment
    */
        mRelativeLayoutPayment = (RelativeLayout) mView.findViewById(R.id.frag_basket_layout_paymentmethod);
        mEditTextExchangeMoney = (EditText) mView.findViewById(R.id.frag_basket_edittext_exchange);
        mButtonPlaceOrder = (Button) mView.findViewById(R.id.frag_basket_button_placeorder);

        /*
        Click Events Started
         */

        /*
        This method is for scroolling the mEditTextAddress_Apt_no in the scroll view
         */
        mEditTextAddress_Apt_no.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ScrollView mScrollView = (ScrollView) mView.findViewById(R.id.frag_basket_layout_deliveryaddress_scrollview);
                mScrollView.requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    /*
    layout 3 Items that are ordered
     */
//        mTextViewEditOrder.setOnClickListener(this);
        mListViewOrderedItems.setOnItemClickListener(this);
        mTextViewAddItems.setOnClickListener(this);
        mTextViewChooseDeliveryAddress.setOnClickListener(this);
    /*
     layout 4 Address layout
     */
        mTextViewChangeAddress.setOnClickListener(this);
//        mTextViewEditAddress.setOnClickListener(this);
        mButtonChoosePaymentMethod.setOnClickListener(this);

        /*
     layout 5 Address layout
     */
        mButtonPlaceOrder.setOnClickListener(this);

        mNavigationActivity = (NavigationActivity) getActivity();
//        mArrayListOrderedItem = new ArrayList<>();
        mBasketListAdapter = new BasketListAdapter(getActivity(), mNavigationActivity.mArrayListOrderedItem);
        mListViewOrderedItems.setAdapter(mBasketListAdapter);

        mImageLoader = new ImageLoader(getActivity());
        mImageLoaderBlur = new ImageLoaderBlur(getActivity());

        mStore_pref = new Store_pref(getActivity());
        if (mStore_pref.getUserId().length() > 0) {
            user_id = mStore_pref.getUserId();
            Log.e("BasketFragment.OnCreated()", "UserName: " + mStore_pref.getUser().getName());
            if (mStore_pref.getUser().getMobile().length() > 0)
                mTextViewContactNumber.setText(mStore_pref.getUser().getMobile());
        }
        System.out.println("onCreateView ends");
        Log.e("onCreateView()", "BasketFragment Fragment created end.");
        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*
    layout 3 Items that are ordered
     */
            /*case R.id.frag_basket_text_editorder:
                break;*/
            case R.id.frag_basket_text_additem:

                gotoPizzaAddScreen();

                break;
            case R.id.frag_basket_button_deliveryaddress:

//                Address mAddress = new Address();
//                mAddress.setId("001");
//                mAddress.setTag_name("Office Address");
////                mAddress.setStreet_name("Parimal Chowk");
//                mAddress.setAddressline1("Parimal Chowk");
//                mAddress.setAddressline2("Krishna Darshna Complex Office no 211");
//                mAddress.setAddressline3("Bhavnagar - 364002");

               /* JSONObject mJsonObject = new JSONObject();
                mJsonObject = mAddressDeliveryAddress.getJsonresponse(mJsonObject, mAddressDeliveryAddress);
                System.out.println("Address: " + mJsonObject);


                String json = new Gson().toJson(mArrayListOrderedItem);
                System.out.println("Json String of Ordered Items: " + json);
                JSONObject add = new JSONObject();
                try {
                    add.put("pizzaorder", json);
                    add.put("deliveryaddress", "" + mJsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("PizzaOrdered are:" + add);

                System.out.println("Json String of Ordered Items and Address: " + json);

                String jsonfinal = new Gson().toJson(add);
                System.out.println("Json String of Ordered Items and Address FINAL:   " + jsonfinal);
      */
                quantityTotal = Utils.getDoubleFormat(quantityTotal);

                double q = Utils.getDoubleFormat((quantityTotal % 1.00));
                System.out.println("Total Quantity: " + q);

                float min_order = 0;
                if (mNavigationActivity.mArrayListOrderedItem.size() > 0)
                    min_order = Float.parseFloat(mNavigationActivity.mArrayListOrderedItem.get(0).getPizzaria().getMin_order());

//                if ((q == 0.99)) {
//
//                    mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
//                    mRelativeLayoutOrderedItems.setVisibility(View.GONE);
//                    mRelativeLayoutPayment.setVisibility(View.GONE);
//                    mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
//                    lastVisibleLayout = 2;
//                } else if ((q == 0)) {
//                    mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
//                    mRelativeLayoutOrderedItems.setVisibility(View.GONE);
//                    mRelativeLayoutPayment.setVisibility(View.GONE);
//                    mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
//                    lastVisibleLayout = 2;
//                } else {
//                    Toast.makeText(getActivity(), "Total quantity should be whole number.", Toast.LENGTH_SHORT).show();
//                }

                Log.e("callPlaceOrder()", "callPlaceOrder() Size of PizzaList Size: " + mNavigationActivity.mArrayListOrderedItem.size());
                if (mNavigationActivity.mArrayListOrderedItem.size() <= 0 && GrandTotal <= 0 && subtotal <= 0) {
//                    lastVisibleLayout = 1;
//                    mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
//                    mRelativeLayoutAddressLayout.setVisibility(View.GONE);
//                    mRelativeLayoutPayment.setVisibility(View.GONE);
//                    mNavigationActivity.showFragments();
//                    mNavigationActivity.onBackPressed();
                    /*
                    above back press line is added to remove the
                    basket screen from backstack list
                     */
                    gotoPizzaAddScreen();
                    mNavigationActivity.showFragments();
                    Utils.showMyToast(getActivity(), "Please select at least one pizza to place an order.", Toast.LENGTH_SHORT);
                } else if (subtotal < min_order) {
                    Utils.showMyToast(getActivity(), "Please place order with minimum value " + min_order + ".", Toast.LENGTH_SHORT);
                } else {
                    if ((q == 0.99)) {

                        mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
                        mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                        mRelativeLayoutPayment.setVisibility(View.GONE);
                        mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
                        lastVisibleLayout = 2;
                    } else if ((q == 0)) {
                        mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
                        mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                        mRelativeLayoutPayment.setVisibility(View.GONE);
                        mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
                        lastVisibleLayout = 2;
                    } else {
                        Toast.makeText(getActivity(), "Total quantity should be whole number.", Toast.LENGTH_SHORT).show();
                    }
                }


                break;
/*
    * layout 4 Address layout
    * */
            case R.id.frag_basket_text_changeaddress:
//                mLinearLayoutTextAddress.setVisibility(View.VISIBLE);
//                mLinearLayoutEditAddress.setVisibility(View.GONE);
//                mEditTextTagName.setEnabled(false);
//                mEditTextAddress_StreetName.setEnabled(false);
//                mEditTextAddress_Street_no.setEnabled(false);
//                mEditTextAddress_Apt_no.setEnabled(false);
//                mEditTextAddressline3.setEnabled(false);
                mEditTextCity.setEnabled(false);
                mEditTextZipCode.setEnabled(false);
                mEditTextState.setEnabled(false);

                System.out.println("BasketFragment.ChangeAddressClick():");
                mNavigationActivity.showFragment(mNavigationActivity.mAddressFragment, true);
                System.out.println("BasketFragment: Size of Backstacklist: " + mNavigationActivity.backstacklist.size());
                if (mNavigationActivity.mAddressFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                    mNavigationActivity.backstacklist.add(mNavigationActivity.mAddressFragment);
                    Log.e("BackstackList", "mAddressFragment Fragment is added.");
                }
                mNavigationActivity.showFragments();
                mNavigationActivity.showBackImage();
                break;
          /*
            case R.id.frag_basket_text_editaddress:
//                mLinearLayoutTextAddress.setVisibility(View.GONE);
//                mLinearLayoutEditAddress.setVisibility(View.VISIBLE);
                mEditTextTagName.setEnabled(true);
                mEditTextAddressline1.setEnabled(true);
                mEditTextAddressline2.setEnabled(true);
                mEditTextAddressline3.setEnabled(true);
                mEditTextCity.setEnabled(true);
                mEditTextZipCode.setEnabled(true);
                mEditTextState.setEnabled(true);
                mEditTextTagName.requestFocus();
//                mEditTextAddressline1.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mEditTextAddressline1.requestFocus();
//                    }
//                });
//                mEditTextAddressline2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mEditTextAddressline2.requestFocus();
//                    }
//                });
//                mEditTextAddressline3.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        mEditTextAddressline3.requestFocus();
//                    }
//                });
                System.out.println("BasketFragment.EditAddressClick():");
                break;
            */
            case R.id.frag_basket_button_paymentmethod:


                if (!(mEditTextAddress_StreetName.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "Street Name should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextAddress_StreetName.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextAddress_StreetName);
//                    mNavigationActivity.onBackPressed();
                } else if (!(mEditTextAddress_Street_no.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "Street Number should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextAddress_Street_no.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextAddress_Street_no);
//                    mNavigationActivity.onBackPressed();
                } else if (!(mEditTextAddress_Apt_no.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "Apartment Number should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextAddress_Apt_no.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextAddress_Apt_no);
//                    mNavigationActivity.onBackPressed();
                } else if (!(mEditTextCity.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "City should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextCity.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextCity);
//                    mNavigationActivity.onBackPressed();
                } else if (!(mEditTextZipCode.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "Zipcode should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextZipCode.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextZipCode);
//                    mNavigationActivity.onBackPressed();
                } else if (!(mEditTextState.getText().length() > 0)) {
                    Toast.makeText(getActivity(), "State should not blank.", Toast.LENGTH_SHORT).show();
                    mEditTextState.requestFocus();
//                    mNavigationActivity.onBackPressed();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextState);
                } else {

                    mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                    mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                    mRelativeLayoutPayment.setVisibility(View.VISIBLE);
                    mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_finished_red);
                    lastVisibleLayout = 3;
                    mEditTextExchangeMoney.requestFocus();
                    Utils.showSoftKeyBoard(getActivity(), mEditTextExchangeMoney);
                }
                break;
            case R.id.frag_basket_button_placeorder:
                mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                mRelativeLayoutPayment.setVisibility(View.VISIBLE);
//                mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_finished_red);

               /*
               Code for setting favorite pizza old
                */
               /* for (int i = 0; i < mNavigationActivity.mArrayListOrderedItem.size(); i++) {
                    Log.e("frag_basket_button_placeorder", "----------------Ordered pizza " + (i + 1) + "--------------");
                    *//*mNavigationActivity.mArrayListOrderedItem.get(i).printAllData();
                    Log.e("inloop:-->", "I: " + i);
                    Log.i("inloop:-->", "I: " + i);*//*
                    if (mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_details().isPizzaAddedFavorite()) {
                        isFavoritePizzaAdded = true;
                    }
                }*/

//                initDialogAddAddresstoFavoritePizza("");
                callPlaceOrder();
//                CallPlaceOrderService("4556");
                lastVisibleLayout = 3;
                break;
        }

    }

    private void gotoPizzaAddScreen() {
        try {
            Log.e("gotoPizzaAddScreen()", "gotoPizzaAddScreen() method is called.");
            if ((mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 2) == mNavigationActivity.mRestaurentFragment)) {
                mNavigationActivity.onBackPressed();
                mNavigationActivity.showFragment(mNavigationActivity.mRestaurentFragment, false);
                System.out.println("BasketFragment: additem click: Last Fragment is Restaurant frag");
            } else if ((mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 2) == mNavigationActivity.mFoodMenuFragment)) {
                mNavigationActivity.onBackPressed();
                mNavigationActivity.showFragment(mNavigationActivity.mFoodMenuFragment, false);
                System.out.println("BasketFragment: additem click: Last Fragment is Foodmenu frag");
            } else if ((mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 2) == mNavigationActivity.mPizzaListFragment)) {
                mNavigationActivity.onBackPressed();
                mNavigationActivity.showFragment(mNavigationActivity.mPizzaListFragment, false);
                System.out.println("BasketFragment: additem click: Last Fragment is Pizzalist frag");
            } else if ((mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 2) == mNavigationActivity.mapFragment)
                    && mNavigationActivity.mapFragment.isRestaroAvailable) {
                mNavigationActivity.onBackPressed();
                mNavigationActivity.backstacklist.add(mNavigationActivity.mRestaurentFragment);
                mNavigationActivity.showFragment(mNavigationActivity.mRestaurentFragment, false);
                System.out.println("BasketFragment: additem click: Last Fragment is Map frag");
            } else {
                Toast.makeText(getActivity(), "Could not go to add items.", Toast.LENGTH_SHORT).show();
//                    mNavigationActivity.showFragment(mNavigationActivity.mRestaurentFragment, false);
//                    mNavigationActivity.backstacklist.add(mNavigationActivity.mRestaurentFragment);
//                    Log.e("BackstackList", "Restaurant Fragment is added (from Basket Fragment).");
//                    System.out.println("BasketFragment: additem click: Default Fragment is Restaurant frag");
//                    mNavigationActivity.showFragments();
            }
            mNavigationActivity.showFragments();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
    layout 3 Items that are ordered
    */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        System.out.println("Positoin: " + i);
        System.out.println("Selected PizzaItem: " + mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_details().getName());
        mNavigationActivity.backstacklist.add(mNavigationActivity.mOrderFragment);
        Log.e("BackstackList", "Order Fragment is added.");
        mNavigationActivity.showFragment(mNavigationActivity.mOrderFragment, true);
        mNavigationActivity.showFragments();
        mNavigationActivity.mOrderFragment.EditOrderedItem(mNavigationActivity.mArrayListOrderedItem.get(i), i);
//        mArrayListOrderedItem.get(i).printAllData();
    }

    /*
        Below method is called from Add to basket click on Order screen to add
            the new product in cart.
     */
    //    public void AddItemtoListOrder(OrderedItemsData mOrderedItemsData, PizzaRestro mPizzaRestroReceived) {
    public void AddItemtoListOrder(OrderedItemsData mOrderedItemsData) {

        System.out.print("AddItemtoListOrder()");
        mOrderedItemsData.printAllData();

        if (mOrderedItemsData.getPizzaria() != null) {

            Log.e("AddItemtoListOrder()", "Pizzeria data is not null: ");
            this.mPizzaRestroReceived = mOrderedItemsData.getPizzaria();
            /*mImageLoader.DisplayImage(mOrderedItemsData.getPizzaria().getImg_url(), mImageViewRestaurantImage);
            mImageLoaderBlur.DisplayImage(mOrderedItemsData.getPizzaria().getImg_url(), mImageViewRestaurantImageBK);
            mTextViewRestaurantName.setText(mOrderedItemsData.getPizzaria().getName());*/
        }

        System.out.println("in BasketFragment");
        System.out.println("Name: " + mOrderedItemsData.getPizza_details().getName());

        if (mOrderedItemsData.getExtra_ingredients() != null)
            System.out.println("additions size :" + mOrderedItemsData.getExtra_ingredients().size());

        System.out.println("Total item ordered: " + mOrderedItemsData.getTotal());

        mNavigationActivity.mArrayListOrderedItem.add(mOrderedItemsData);
        mBasketListAdapter.notifyDataSetChanged();

        mStore_pref.setOrderedData(mNavigationActivity.mArrayListOrderedItem, "From BasketFrag.java setOrderData_Prefrences()");

        subtotal += mOrderedItemsData.getTotal();
        CalculateTotal();

        System.out.println("Basket Fragment: Size of Orderlist: " + mNavigationActivity.mArrayListOrderedItem.size());
        mListViewOrderedItems.setVisibility(View.VISIBLE);
        mRelativeLayoutOrderedItems.setFocusableInTouchMode(true);
        mRelativeLayoutOrderedItems.requestFocus();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setListViewHeightBasedOnChildren();
                        mBasketListAdapter.notifyDataSetChanged();
                        System.out.println("mListViewOrderedItems.getMeasuredHeight(): " + mListViewOrderedItems.getMeasuredHeight());
                        System.out.println("mListViewOrderedItems.getHeight(): " + mListViewOrderedItems.getHeight());
//                        System.out.println("mListViewOrderedItems.getDisplay().getHeight(): " + mListViewOrderedItems.getDisplay().getHeight());
//                        if(mBasketListAdapter.BottomParameter!=0){
//                            ViewGroup.LayoutParams params = mListViewOrderedItems.getLayoutParams();
//                            params.height = mBasketListAdapter.BottomParameter + (mListViewOrderedItems.getDividerHeight() * (mBasketListAdapter.getCount() - 1));
//                            mListViewOrderedItems.setLayoutParams(params);
//                        }
                    }
                });
            }
        }, 200);
//        setListViewHeightBasedOnChildren();
    }

    /*
        Below method is called from Add to basket click on Order screen to add
            the new product in cart.
     */
    public void AddItemtoListOrder(int pos, OrderedItemsData mOrderedItemsData) {

        System.out.println("AddItemtoListOrder() with pos Positions to Add: " + pos);
        mOrderedItemsData.printAllData();

        if (mOrderedItemsData.getPizzaria() != null) {

            Log.e("AddItemtoListOrder() with pos", "Pizzeria data is not null: ");
            this.mPizzaRestroReceived = mOrderedItemsData.getPizzaria();
      /*      mImageLoader.DisplayImage(mOrderedItemsData.getPizzaria().getImg_url(), mImageViewRestaurantImage);
            mImageLoaderBlur.DisplayImage(mOrderedItemsData.getPizzaria().getImg_url(), mImageViewRestaurantImageBK);
            mTextViewRestaurantName.setText(mOrderedItemsData.getPizzaria().getName());*/
        }

        System.out.println("in BasketFragment Pizza Name: " + mOrderedItemsData.getPizza_details().getName());

        if (mOrderedItemsData.getExtra_ingredients() != null)
            System.out.println("additions size :" + mOrderedItemsData.getExtra_ingredients().size());

        System.out.println("Total item ordered: " + mOrderedItemsData.getTotal());

        mNavigationActivity.mArrayListOrderedItem.add(pos, mOrderedItemsData);
        mBasketListAdapter.notifyDataSetChanged();

        mStore_pref.setOrderedData(mNavigationActivity.mArrayListOrderedItem, "From BasketFrag.java setOrderData_Prefrences()");

        subtotal += mOrderedItemsData.getTotal();
        CalculateTotal();

        System.out.println("Basket Fragment: Size of Orderlist: " + mNavigationActivity.mArrayListOrderedItem.size());
        mListViewOrderedItems.setVisibility(View.VISIBLE);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setListViewHeightBasedOnChildren();
                        mBasketListAdapter.notifyDataSetChanged();
                    }
                });
            }
        }, 200);
//        setListViewHeightBasedOnChildren();
    }

    public void CalculateTotal() {

        subtotal = 0;
        quantityTotal = 0;
        if (mNavigationActivity.mArrayListOrderedItem.size() > 0) {

            for (int i = 0; i < mNavigationActivity.mArrayListOrderedItem.size(); i++) {
                subtotal += mNavigationActivity.mArrayListOrderedItem.get(i).getTotal();
                quantityTotal += mNavigationActivity.mArrayListOrderedItem.get(i).getQty();
            }
            subtotal = Utils.getDoubleFormat(subtotal);
            servicetax = (subtotal * servicetaxpercentage) / 100;
            servicetax = Utils.getDoubleFormat(servicetax);
            GrandTotal = subtotal + servicetax + deliverycharge;
            GrandTotal = Utils.getDoubleFormat(GrandTotal);

        } else {
            quantityTotal = 0;
            GrandTotal = 0;
            servicetax = 0;
            deliverycharge = 0;
            mEditTextExchangeMoney.setText("");
        }

        System.out.println("Grand Total: " + GrandTotal);
        mTextViewGrnadTotal.setText("" + GrandTotal);
        mTextViewTotalQuantity.setText("" + quantityTotal);
        System.out.println("sub Total: " + subtotal);
        mTextViewSubTotal.setText("" + subtotal);
        System.out.println("Service Tax: " + servicetax);
        mTextViewServiceTax.setText("" + servicetax);

        if (deliverycharge == 0)
            mTextViewDeliveryCharge.setText(R.string.f_basket_tv_deliverycharge);
        else
            mTextViewDeliveryCharge.setText("" + deliverycharge);

        System.out.println("Total Order: " + GrandTotal);
        mTextViewTotalorder.setText("" + GrandTotal);

    }

    public void SetRestaurantData(PizzaRestro mPizzaRestro) {
        mPizzaRestroReceived = mPizzaRestro;
        /*mImageLoader.DisplayImage(mPizzaRestroReceived.getImg_url(), mImageViewRestaurantImage);
        mImageLoaderBlur.DisplayImage(mPizzaRestroReceived.getImg_url(), mImageViewRestaurantImageBK);
        mTextViewRestaurantName.setText(mPizzaRestroReceived.getName());*/
    }

    public void setDeliveryAddress(Address mDeliveryAddressReceived) {

        Tags.LAST_SELECTED_DELIVERY_ADDRESS = mDeliveryAddressReceived;

        Log.e("setDeliveryAddress().Basketfrag.java", "-----------------Delivery Address---------------");
        mDeliveryAddressReceived.printAllData();

        if (mDeliveryAddressReceived.getTag_name().equalsIgnoreCase("No Address")) {
            mEditTextTagName.setText(mDeliveryAddressReceived.getTag_name());
            mEditTextAddress_StreetName.setText(mDeliveryAddressReceived.getStreet_name());
            mEditTextAddress_Street_no.setText("");
            mEditTextAddress_Apt_no.setText("");
            mEditTextCity.setText("");
            mEditTextZipCode.setText("");
            mEditTextState.setText("");
        } else {
            mAddressDeliveryAddress = mDeliveryAddressReceived;

//        mTextViewAddressline1.setText(mAddressDeliveryAddress.getAddressline1());
//        mTextViewAddressline2.setText(mAddressDeliveryAddress.getAddressline2());
//        mTextViewAddressline3.setText(mAddressDeliveryAddress.getAddressline3());
//        mTextViewCityName.setText(mAddressDeliveryAddress.getCity());
//        mTextViewZipCode.setText(mAddressDeliveryAddress.getZip_code());
            if (mAddressDeliveryAddress.getApt_no().length() > 0) {
                mEditTextAddress_Street_no.setEnabled(true);
            }
            mEditTextTagName.setText(mAddressDeliveryAddress.getTag_name());
            mEditTextAddress_StreetName.setText(mAddressDeliveryAddress.getStreet_name());
            mEditTextAddress_Street_no.setText(mAddressDeliveryAddress.getStreet_no());
            mEditTextAddress_Apt_no.setText(mAddressDeliveryAddress.getApt_no());
            mEditTextCity.setText(mAddressDeliveryAddress.getCity());
            mEditTextZipCode.setText(mAddressDeliveryAddress.getZip_code());
            mEditTextState.setText(mAddressDeliveryAddress.getState());
        }
    }

    private void callPlaceOrder() {

       /* if (!(mEditTextAddress_StreetName.getText().length() > 0)) {
            Toast.makeText(getActivity(), "Street Name should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextAddress_StreetName.requestFocus();
            mNavigationActivity.onBackPressed();
        } else if (!(mEditTextAddress_Street_no.getText().length() > 0)) {
            Toast.makeText(getActivity(), "Street Number should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextAddress_Street_no.requestFocus();
            mNavigationActivity.onBackPressed();
        } else if (!(mEditTextAddress_Apt_no.getText().length() > 0)) {
            Toast.makeText(getActivity(), "Apartment Number should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextAddress_Apt_no.requestFocus();
            mNavigationActivity.onBackPressed();
        } else if (!(mEditTextCity.getText().length() > 0)) {
            Toast.makeText(getActivity(), "City should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextCity.requestFocus();
            mNavigationActivity.onBackPressed();
        } else if (!(mEditTextZipCode.getText().length() > 0)) {
            Toast.makeText(getActivity(), "Zipcode should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextZipCode.requestFocus();
            mNavigationActivity.onBackPressed();
        } else if (!(mEditTextState.getText().length() > 0)) {
            Toast.makeText(getActivity(), "State should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextState.requestFocus();
            mNavigationActivity.onBackPressed();
        } else {
        */
        if (!(mEditTextExchangeMoney.getText().length() > 0)) {
            Toast.makeText(getActivity(), "Exchange Money should not blank.", Toast.LENGTH_SHORT).show();
            mEditTextExchangeMoney.requestFocus();
        } else if (GrandTotal > Integer.parseInt(mEditTextExchangeMoney.getText().toString())) {

//                Toast.makeText(getActivity(), "Please enter valid Exchange Money.", Toast.LENGTH_SHORT).show();
            Utils.showMyToast(getActivity(), "Exchange Money should be greater than or equal to Total of Order.", Toast.LENGTH_SHORT);
            mEditTextExchangeMoney.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextExchangeMoney);
        } /*else if (subtotal < min_order) {
                Utils.showMyToast(getActivity(), "Please place order with minimum value " + min_order + ".", Toast.LENGTH_SHORT);
            } */ else {
               /* Log.e("callPlaceOrder()", "callPlaceOrder() Size of PizzaList Size: " + mNavigationActivity.mArrayListOrderedItem.size());
                if (mNavigationActivity.mArrayListOrderedItem.size() <= 0 && GrandTotal <= 0 && subtotal <= 0) {
                    lastVisibleLayout = 1;
                    mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                    mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                    mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                    mNavigationActivity.showFragments();
//                    mNavigationActivity.onBackPressed();
                    *//*
                    above back press line is added to remove the
                    basket screen from backstack list
                     *//*
                    gotoPizzaAddScreen();
                    mNavigationActivity.showFragments();
                    Utils.showMyToast(getActivity(), "Please select at least one pizza to place an order.", Toast.LENGTH_SHORT);*/
               /* } else {*/
            mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_finished_red);
            /*
            new coditions is added because sometime order service not working so again clicking on placeorder
            button address service is called again to avoid it below condition is added.
             */
            if (isAddressServiceSuccess && AddressId.length() > 0) {
                Log.e("callPlaceOrder()", "Address service already run so call order place service.~");
                System.out.println("callPlaceOrder() Addressid: " + AddressId);
                initDialogAddOrderFavorite(AddressId);
            } else {
                Log.e("callPlaceOrder()", "Address service called firsttime.");
                new AddressService().execute();
            }
               /* }*/
        }

      /*  }*/
    }

    public class AddressService extends AsyncTask<Void, Void, Void> {

        String response = "";
        String url = "";

        AddressService() {
        /*
           tag= mEditTextTagName.getText().toString();
            line1 =  mEditTextAddress_StreetName.getText().toString();
            line2 =  mEditTextAddressline2.getText().toString();
            line3 =  mEditTextAddressline3.getText().toString();
            city =  mEditTextCity.getText().toString();
            zip =  mEditTextZipCode.getText().toString();
            state = mEditTextState.getText().toString();*/


            /*final String encodedURL = URLEncoder.encode(url, "UTF-8");*/


            try {
                url = Constants.SET_ADDRESS + "&user_id=" + user_id
                        + "&tag_name=" + URLEncoder.encode(String.valueOf(mEditTextTagName.getText()), "UTF-8")
                        + "&lat=" + URLEncoder.encode(String.valueOf(mAddressDeliveryAddress.getLat()), "UTF-8")
                        + "&lng=" + URLEncoder.encode(String.valueOf(mAddressDeliveryAddress.getLng()), "UTF-8")
                        + "&street_name=" + URLEncoder.encode(String.valueOf(mEditTextAddress_StreetName.getText()), "UTF-8")
                        + "&street_no=" + URLEncoder.encode(String.valueOf(mEditTextAddress_Street_no.getText()), "UTF-8")
                        + "&apt_no=" + URLEncoder.encode(String.valueOf(mEditTextAddress_Apt_no.getText()), "UTF-8")
                        + "&city=" + URLEncoder.encode(String.valueOf(mEditTextCity.getText()), "UTF-8")
                        + "&zip_code=" + URLEncoder.encode(String.valueOf(mEditTextZipCode.getText()), "UTF-8")
                        + "&state=" + URLEncoder.encode(String.valueOf(mEditTextState.getText()), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            Address mAddress;
//            if (Tags.LAST_SELECTED_DELIVERY_ADDRESS != null) {
//                mAddress = Tags.LAST_SELECTED_DELIVERY_ADDRESS;

//                String url = Constants.SET_ADDRESS + "&user_id=" + user_id
//                        + "&tag_name=" + mAddress.getTag_name()
//                        + "&addressline1=" + mAddress.getAddressline1()
//                        + "&addressline2=" + mAddress.getAddressline2()
//                        + "&addressline3=" + mAddress.getAddressline3()
//                        + "&city=" + mAddress.getCity()
//                        + "&zip_code=" + mAddress.getZip_code()
//                        + "&state=" + mAddress.getState();


            try {

                HttpClient mHttpClient = new DefaultHttpClient();
//                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
                System.out.println("Address Url: " + url);
                HttpGet mHttpGet = new HttpGet(url);
                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("AddressService().DoinBackground()", "Could not reach to server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
//            } else {
//                Toast.makeText(getActivity(), "Could not access the delivery address, " +
//                        "please select proper delivery address or enter manually.", Toast.LENGTH_SHORT).show();
//            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of set Address:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

//                        JSONArray mJsonArray = mJsonObject.getJSONArray("addresses");
//                        JSONObject temp = (JSONObject) mJsonArray.get(0);

//                        new UpdateFavoriteAddressService(mJsonObject.getString("address_id")).execute();
//                        CallPlaceOrderService(mJsonObject.getString("address_id"));

                        /*
                            Code for setting favorite pizza old
                        */
//                        if (isFavoritePizzaAdded)
//                            initDialogAddAddresstoFavoritePizza(mJsonObject.getString("address_id"));
//                        else
//                        CallPlaceOrderService(mJsonObject.getString("address_id"));

                        AddressId = mJsonObject.getString("address_id");
                        isAddressServiceSuccess = true;
                        initDialogAddOrderFavorite(AddressId);
                    } else {
                        Log.e("AddressService().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                        AddressId = "";
                        isAddressServiceSuccess = false;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("AddressService().onPostExecut()", "Unable to get response from server");
                Toast.makeText(getActivity(), "Unable to get response from server.", Toast.LENGTH_SHORT).show();
                AddressId = "";
                isAddressServiceSuccess = false;
            }
        }
    }

    /*
        Code for setting favorite pizza old
    */

/*    void initDialogAddAddresstoFavoritePizza(final String addressid) {

        mDialogFavorite = new Dialog(getActivity());
        mDialogFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavorite.setContentView(R.layout.dialog_favorite_address);

//        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.address_list_pizzas_text_dialog_title);
//        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.address_list_pizzas_image_dialog_title);


//        mRelativeLayoutAddtoFavorite_dialog = (LinearLayout) mDialogFavorite.findViewById(R.id.layout_button_add_favorite);
        mTextViewYes = (TextView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_yes);
        mTextViewNo = (TextView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_no);


//        mRelativeLayoutAddtoFavorite_dialog.setVisibility(View.VISIBLE);

//        mTextViewDialogHeading.setTextSize(R.dimen.t_textsize_15);
//        mTextViewDialogHeading.setVisibility(View.VISIBLE);
//        mTextViewDialogHeading.setText("Hello");
//        mTextViewDialogHeading.setText(R.string.dialog_list_pizza_add_deliveryaddress_favorite);
//        mImageViewDialogImage.setImageResource(R.drawable.delivery_icon_100);

        mDialogFavorite.show();


        mTextViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavorite.dismiss();
                new UpdateFavoriteAddressService(addressid).execute();

            }
        });

        mTextViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavorite.dismiss();
                CallPlaceOrderService(addressid);
            }
        });

        mDialogFavorite.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Toast.makeText(getActivity(), "Dialog Dismissed.", Toast.LENGTH_SHORT).show();
            }
        });
    }*/

//    public class UpdateFavoriteAddressService extends AsyncTask<Void, Void, Void> {
//
//        String response = "";
//        String url = "";
//        String add_id;
//
//        UpdateFavoriteAddressService(String addressid) {
//            add_id = addressid;
//        /*
//        http://incredibleapps.net/thinkpizza/action.php?do=set_fav_address&id=[121,125,456,56]&address_id=5
//        */
//            String favorite_ids = "";
//            String ids = "";
//            for (int i = 0; i < mNavigationActivity.mArrayListOrderedItem.size(); i++) {
//
//                if (mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_details().isPizzaAddedFavorite()) {
//                    ids += mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_details().getFavourite_id() + ",";
//                }
//            }
//            favorite_ids = "[" + Utils.RemoveLastComma_ID(ids) + "]";
//            System.out.println("Favorites IDS:" + favorite_ids);
//            url = Constants.UPDATE_FAVORITE_ADDRESS + "&set_fav_address&id=" + favorite_ids
//                    + "&address_id=" + addressid;
//            System.out.println("Url for UpdateFavoriteAddressService: " + url);
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mProgressDialogUpdatingFavorites = new ProgressDialog(getActivity());
//            mProgressDialogUpdatingFavorites.setMessage("Updating Favorites...");
//            mProgressDialogUpdatingFavorites.setCancelable(false);
//            mProgressDialogUpdatingFavorites.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            try {
//
//                HttpClient mHttpClient = new DefaultHttpClient();
////                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
//                HttpGet mHttpGet = new HttpGet(url);
//                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
//                response = mHttpClient.execute(mHttpGet, mResponseHandler);
//            } catch (IOException e) {
//                e.printStackTrace();
//                Log.e("UpdateFavoriteAddressService().DoinBackground()", "Could not reach to server.");
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity(), "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
////            } else {
////                Toast.makeText(getActivity(), "Could not access the delivery address, " +
////                        "please select proper delivery address or enter manually.", Toast.LENGTH_SHORT).show();
////            }
//            return null;
//        }
//
//        @SuppressLint("NewApi")
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//
//            mProgressDialogUpdatingFavorites.dismiss();
//            CallPlaceOrderService(add_id);
//
//            System.out.println("Response of set Address:" + response);
//            JSONObject mJsonObject;
//            if (response != null && response.length() > 0) {
//                try {
//                    mJsonObject = new JSONObject(response);
//                    System.out.println("Json Object: " + mJsonObject);
//
//                    if (mJsonObject.getString("responseCode").equals("1")) {
//
//                        Log.e("UpdateFavoriteAddressService().onPostExecut()", mJsonObject.getString("responseMsg"));
//                        Toast.makeText(getActivity(), "Address is updated to all Favorite Pizza that you have added.", Toast.LENGTH_SHORT).show();
//
//                    } else {
//                        Log.e("UpdateFavoriteAddressService().onPostExecut()", mJsonObject.getString("responseMsg"));
//                        Toast.makeText(getActivity(), "Address could not be updated to favorite pizzas.", Toast.LENGTH_SHORT).show();
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
////                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
//            } else {
//                Log.e("AddressService().onPostExecut()", "Unable to get response from server");
//                Toast.makeText(getActivity(), "Unable to get response from server.", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


    void initDialogAddOrderFavorite(final String addressid) {

        mDialogFavorite = new Dialog(getActivity());
        mDialogFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavorite.setContentView(R.layout.dialog_favorite_address);

        TextView mTextViewDialogHeading;
        ImageView mImageViewDialogImage;


        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_list_pizzas_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_list_pizzas_image_dialog_title);


//        mRelativeLayoutAddtoFavorite_dialog = (LinearLayout) mDialogFavorite.findViewById(R.id.layout_button_add_favorite);
//        mTextViewYes = (TextView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_yes);
//        mTextViewNo = (TextView) mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_no);


//        mRelativeLayoutAddtoFavorite_dialog.setVisibility(View.VISIBLE);

//        mTextViewDialogHeading.setTextSize(R.dimen.t_textsize_15);
//        mTextViewDialogHeading.setVisibility(View.VISIBLE);
//        mTextViewDialogHeading.setText("Hello");
        mTextViewDialogHeading.setText("Do you want to add this order to Favorites ?");
        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
        mDialogFavorite.show();

        mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_order_favorite = 1;
                mDialogFavorite.dismiss();
//                CallPlaceOrderService(addressid);

            }
        });

        mDialogFavorite.findViewById(R.id.dialog_favorite_address_text_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_order_favorite = 0;
                mDialogFavorite.dismiss();
//                CallPlaceOrderService(addressid);
            }
        });
        mDialogFavorite.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                CallPlaceOrderService(addressid);
            }
        });

    }

    public void CallPlaceOrderService(String addressid) {

        if (mNavigationActivity.mArrayListOrderedItem.size() > 0) {

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Placing Order...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            tmp = "{'data':[";
            String order_records = "";
            String all_order_records = "";

            for (int i = 0; i < mNavigationActivity.mArrayListOrderedItem.size(); i++) {

                order_records = "{";
                order_records += "'user_id':";
                order_records += "'" + user_id + "',";
                order_records += "'is_favorite':";
                order_records += "'" + is_order_favorite + "',";
                order_records += "'menu_id':";
                order_records += "'" + mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_details().getId() + "',";
                order_records += "'pizza_size':";
                order_records += "'" + mNavigationActivity.mArrayListOrderedItem.get(i).getPizza_size() + "',";

                if (mEditTextExchangeMoney.getText().length() > 0) {
                    order_records += "'amount_received':";
                    order_records += "'" + mEditTextExchangeMoney.getText() + "',";

                } else {
                    order_records += "'amount_received':";
                    order_records += "'0.0',";
                }

                if (mNavigationActivity.mArrayListOrderedItem.get(i).getExtra_ingredients() != null) {
                    String additions = "";
                    for (int j = 0; j < mNavigationActivity.mArrayListOrderedItem.get(i).getExtra_ingredients().size(); j++) {
                        additions += mNavigationActivity.mArrayListOrderedItem.get(i).getExtra_ingredients().get(j).getId() + ",";
                    }
                    additions = Utils.RemoveLastComma_ID(additions);
                    order_records += "'additions':";
                    order_records += "'[" + additions + "]',";
                } else {
                    order_records += "'additions':";
                    order_records += "'',";
                }

            /*
            code for the custom ingredients is removed
             */
            /*if (mNavigationActivity.mArrayListOrderedItem.get(i).getCustom_ingredients() != null) {
                String custom_ingredients = "";
                for (int j = 0; j < mNavigationActivity.mArrayListOrderedItem.get(i).getCustom_ingredients().size(); j++) {
                    custom_ingredients += mNavigationActivity.mArrayListOrderedItem.get(i).getCustom_ingredients().get(j).getId() + ",";
                }
                custom_ingredients = Utils.RemoveLastComma_ID(custom_ingredients);
                order_records += "'custom_ingredients':";
                order_records += "'[" + custom_ingredients + "]',";
            } else {
                order_records += "'custom_ingredients':";
                order_records += "'',";
            }*/


            /*
            6-10-2015 toppings code added
             */

                if (mNavigationActivity.mArrayListOrderedItem.get(i).getToppings() != null) {
                    String custom_ingredients = "";
                    for (int j = 0; j < mNavigationActivity.mArrayListOrderedItem.get(i).getToppings().size(); j++) {
                        custom_ingredients += mNavigationActivity.mArrayListOrderedItem.get(i).getToppings().get(j).getId() + ",";
                    }
                    custom_ingredients = Utils.RemoveLastComma_ID(custom_ingredients);
                    order_records += "'toppings':";
                    order_records += "'[" + custom_ingredients + "]',";
                } else {
                    order_records += "'toppings':";
                    order_records += "'',";
                }

                if (mNavigationActivity.mArrayListOrderedItem.get(i).getPizzacrust() != null) {
                    order_records += "'pizza_crust_id':";
                    order_records += "'" + mNavigationActivity.mArrayListOrderedItem.get(i).getPizzacrust().getId() + "',";
                } else {
                    order_records += "'pizza_crust_id':";
                    order_records += "'',";
                }

                order_records += "'qty':";
                order_records += "'" + mNavigationActivity.mArrayListOrderedItem.get(i).getQty() + "',";

                order_records += "'total':";
                order_records += "'" + mNavigationActivity.mArrayListOrderedItem.get(i).getTotal() + "',";

                order_records += "'address_id':";
                order_records += "'" + addressid + "',";

                order_records += "'payment_type':";
                order_records += "'Cash-On-Delivery',";

                order_records += "'payment_details':";
                order_records += "'" + "Cash on Delivery" + "',";

                order_records += "'status':";
                order_records += "'Not Ready'";


                order_records += "},";
                all_order_records += order_records;

                System.out.println("Order record[" + i + "]: " + order_records);

            }
            System.out.println("All Order record: " + all_order_records);
            all_order_records = all_order_records.substring(0, all_order_records.length() - 1);
            System.out.println("Order record: " + all_order_records);
            tmp += all_order_records + "]}";
            System.out.println("Json Order: " + tmp);

            tmp = tmp.replace("'", getActivity().getResources().getString(R.string.double_quote));
            Log.e("CallPlaceOrderService().BasketFragment.java", "Final Json Order: " + tmp);
        }

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.SET_ORDERES, createMyReqSuccessListener(addressid), createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("data", tmp);

                return params;
            }
        };
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener(final String addressid) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();


                System.out.println("MapFragment: Restarurant Response: " + response);
                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {

                        Toast.makeText(getActivity(), "Ordere has been taken successfully.", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getActivity(), "Your Order Number is " + mJsonObject.getString("order_id"), Toast.LENGTH_SHORT).show();
                        initOrderConfirmedDialog(mJsonObject.getString("order_id"));
                        Log.e("createMyReqSuccessListener().BasketFrag", "Response: " + mJsonObject.getString("responseMsg"));

                        mNavigationActivity.mBasketFragment.mNavigationActivity.mArrayListOrderedItem.clear();
                        mNavigationActivity.backstacklist.clear();
                        mEditTextExchangeMoney.setText("");
                        mStore_pref.removeOrderedData();
                        mListViewOrderedItems.setVisibility(View.GONE);
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mapFragment);
                        mNavigationActivity.showFragment(mNavigationActivity.mapFragment, false);
                        mNavigationActivity.showFragments();
                        mNavigationActivity.ShowHideIcons();
                        mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_not_finished);
//                        isFavoritePizzaAdded = false;
                        is_order_favorite = 0;
                        lastVisibleLayout = 1;
                        mNavigationActivity.mapFragment.getFullAddress_CurrentLocation();

                        AddressId = "";
                        isAddressServiceSuccess = false;

                    } else {
                        Log.e("createMyReqSuccessListener().BasketFrag", "Response: " + mJsonObject.getString("responseMsg"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.e("onHiddenChanged()", "onHiddenChanged().BasketFragment.java is called");
        Log.e("onHiddenChanged()", "lastVisibleLayout: " + lastVisibleLayout);
        Log.e("onHiddenChanged()", "mRelativeLayoutOrderedItems.isShown: " + mRelativeLayoutOrderedItems.isShown());
        Log.e("onHiddenChanged()", "mRelativeLayoutAddressLayout.isShown: " + mRelativeLayoutAddressLayout.isShown());
        Log.e("onHiddenChanged()", "mRelativeLayoutPayment.isShown: " + mRelativeLayoutPayment.isShown());
        if (!hidden) {

            Log.e("onHiddenChanged()", "lastVisibleLayout: " + lastVisibleLayout);
            Log.e("onHiddenChanged()", "mRelativeLayoutOrderedItems.isShown: " + mRelativeLayoutOrderedItems.isShown());
            Log.e("onHiddenChanged()", "mRelativeLayoutAddressLayout.isShown: " + mRelativeLayoutAddressLayout.isShown());
            Log.e("onHiddenChanged()", "mRelativeLayoutPayment.isShown: " + mRelativeLayoutPayment.isShown());
            CalculateTotal();

            if (lastVisibleLayout == 3) {
                mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                mRelativeLayoutPayment.setVisibility(View.VISIBLE);
            } else if (lastVisibleLayout == 2) {
                mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
                mRelativeLayoutPayment.setVisibility(View.GONE);
            } else if (lastVisibleLayout == 1) {

//                mProgressDialog = new ProgressDialog(getActivity());
//                mProgressDialog.setMessage("Rendering...");
//                mProgressDialog.setCancelable(false);
//                mProgressDialog.show();
                mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                mRelativeLayoutPayment.setVisibility(View.GONE);
//                new Timer().schedule(new TimerTask() {
//                    @Override
//                    public void run() {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                if (mNavigationActivity.mArrayListOrderedItem != null && mNavigationActivity.mArrayListOrderedItem.size() > 0) {
//                                    mBasketListAdapter = new BasketListAdapter(getActivity(), mNavigationActivity.mArrayListOrderedItem);
//                                    mListViewOrderedItems.setAdapter(mBasketListAdapter);
//                                    setListViewHeightBasedOnChildren(mListViewOrderedItems);
//                                }
//                                mProgressDialog.dismiss();
//                                mProgressDialog = null;
//                            }
//                        });
//                    }
//                }, 500);
                System.out.println("onHiddenChanged() Order List will be redrawn");

            }
        }
    }



    /*@Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && mNavigationActivity.mArrayListOrderedItem != null) {
            Log.e("onHiddenChanged()", "Pizzeria data is not null: ");
            if (mNavigationActivity.mArrayListOrderedItem.size() > 0)
                try {
                    mImageLoader.DisplayImage(mNavigationActivity.mArrayListOrderedItem.
                            get(mNavigationActivity.mArrayListOrderedItem.size() - 1).getPizzaria().getImg_url(), mImageViewRestaurantImage);
                    mImageLoaderBlur.DisplayImage(mNavigationActivity.mArrayListOrderedItem.
                            get(mNavigationActivity.mArrayListOrderedItem.size() - 1).getPizzaria().getImg_url(), mImageViewRestaurantImageBK);
                    mTextViewRestaurantName.setText(mNavigationActivity.mArrayListOrderedItem.
                            get(mNavigationActivity.mArrayListOrderedItem.size() - 1).getPizzaria().getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }

    }*/

    void initOrderConfirmedDialog(String order_id) {

        TextView mTextViewOrderNumber;
        Button mButtonOK;
        mDialogOrderConfirm = new Dialog(getActivity());
        mDialogOrderConfirm.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogOrderConfirm.setContentView(R.layout.dialog_order_number);
        mTextViewOrderNumber = (TextView) mDialogOrderConfirm.findViewById(R.id.dialog_order_text_order_number);
        mTextViewOrderNumber.setText(order_id);

        mButtonOK = (Button) mDialogOrderConfirm.findViewById(R.id.dialog_order_button_ok);
        mButtonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogOrderConfirm.dismiss();
                mDialogOrderConfirm = null;

            }
        });
        mDialogOrderConfirm.show();
    }


}
