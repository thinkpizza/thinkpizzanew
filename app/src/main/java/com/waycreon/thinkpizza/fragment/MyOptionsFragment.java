package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.Response.Register;
import com.waycreon.thinkpizza.datamodels.User;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.thinkpizza.imageloader.ImageLoaderBlur;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class MyOptionsFragment extends Fragment implements View.OnClickListener {


    LinearLayout mLinearLayoutMyOptions;

    ImageView mImageViewUserProfile;
    ImageView mImageViewEditProfile;
    LinearLayout mLayoutCamera;
    LinearLayout mLayoutGallery;
    TextView mTextViewUserName;
    TextView mTextViewCPFNumber;

    EditText mEditTextUserName;
    EditText mEditTextCPFNumber;

    //    EditText mEditTextTagName;
    EditText mEditTextStreetName;
    EditText mEditTextStreetNo;
    EditText mEditTextAptNo;
    EditText mEditTextCityName;
    AutoCompleteTextView mEditTextZipCode;
    EditText mEditTextState;
    EditText mEditTextContactNumber;

    RelativeLayout mLayoutTextChangePassword;

    Button mButtonUpdateProfile;

    Dialog mDialogChangePassword;
    //    RelativeLayout mRelativeLayoutChangePassword;
    EditText mEditTextCurrentPassword;
    EditText mEditTextNewPassword;
    EditText mEditTextConfirmNewPassword;
    Button mButtonSavePassword;

    Gson mGson;
    Store_pref mStore_pref;
    User mUser;
    NavigationActivity mNavigationActivity;

    ImageLoader mImageLoader;
    ImageLoaderBlur mImageLoaderBlur;
    ProgressDialog mProgressDialog;

    /*
    Image Variables
     */
    String timestamp;
    String picturePath = "";
    Uri mUriselectedimage;
    //String imagepath = null;
    Bitmap imagebitmap = null;
    Uri uriSavedImage;
    File mFile;
    public static String PhotoPath = Environment.getExternalStorageDirectory().getAbsolutePath();

    //    File photostorage = Environment.getExternalStoragePublicDirectory(
//            Environment.DIRECTORY_PICTURES).getAbsoluteFile();
//    String photostorage = Environment.getExternalStorageDirectory().getAbsolutePath();
//    File file = new File(photostorage + "/" + "THINKPIZZA_"
//            + (System.currentTimeMillis()) + ".jpg");

    boolean isProfileImageChange = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_my_options, container, false);
        initChangePasswordDialog();
        mGson = new Gson();
        mNavigationActivity = (NavigationActivity) getActivity();
        mStore_pref = new Store_pref(getActivity());
        mUser = mStore_pref.getUser();
        mImageLoader = new ImageLoader(getActivity());
        mImageLoaderBlur = new ImageLoaderBlur(getActivity());

        mLinearLayoutMyOptions = (LinearLayout) mView.findViewById(R.id.f_myoptions_layout_myoptions);
        mImageViewUserProfile = (ImageView) mView.findViewById(R.id.f_myoptions_image_user);
        mImageViewEditProfile = (ImageView) mView.findViewById(R.id.f_myoptions_image_edit_profile);
        mLayoutCamera = (LinearLayout) mView.findViewById(R.id.f_myoptions_layout_camera);
        mLayoutGallery = (LinearLayout) mView.findViewById(R.id.f_myoptions_layout_gallery);
        mTextViewUserName = (TextView) mView.findViewById(R.id.f_myoptions_text_user_name);
        mTextViewCPFNumber = (TextView) mView.findViewById(R.id.f_myoptions_text_cpf_no);

        mEditTextUserName = (EditText) mView.findViewById(R.id.f_myoptions_edittext_username);
        mEditTextCPFNumber = (EditText) mView.findViewById(R.id.f_myoptions_edittext_cpf_number);
//        mEditTextTagName = (EditText) mView.findViewById(R.id.f_myoptions_edittext_tagname);
        mEditTextStreetName = (EditText) mView.findViewById(R.id.f_myoptions_edittext_address_street_name);
        mEditTextStreetNo = (EditText) mView.findViewById(R.id.f_myoptions_edittext_address_street_no);
        mEditTextAptNo = (EditText) mView.findViewById(R.id.f_myoptions_edittext_address_apt_no);
        mEditTextCityName = (EditText) mView.findViewById(R.id.f_myoptions_edittext_address_cityname);
        mEditTextZipCode = (AutoCompleteTextView) mView.findViewById(R.id.f_myoptions_edittext_address_zipcode);
        mEditTextState = (EditText) mView.findViewById(R.id.f_myoptions_edittext_address_state);
        mEditTextContactNumber = (EditText) mView.findViewById(R.id.f_myoptions_edittext_contactnumber);

        mLayoutTextChangePassword = (RelativeLayout) mView.findViewById(R.id.f_myoptions_layout_text_changepassword);
        mImageViewEditProfile.setOnClickListener(this);
        mLayoutGallery.setOnClickListener(this);
        mLayoutCamera.setOnClickListener(this);
        mLayoutTextChangePassword.setOnClickListener(this);

        mButtonUpdateProfile = (Button) mView.findViewById(R.id.f_myoptions_button_update_profile);
        mButtonUpdateProfile.setOnClickListener(this);

        mImageViewEditProfile.setVisibility(View.VISIBLE);
        mLayoutGallery.setVisibility(View.GONE);
        mLayoutCamera.setVisibility(View.GONE);
        setUserData_FromSharedPrefrence();

        mEditTextZipCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                zipcode = mEditTextZipCode.getText().toString();
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mEditTextZipCode.showDropDown();
                    Utils.hide_keyboard(getActivity());
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    Utils.hide_keyboard(getActivity());
                    CallBrazilAddress(mEditTextZipCode.getText().toString().replaceAll(" ", ""));
                    return true;
                }
                return false;
            }
        });

//        mEditTextZipCode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//                                    long arg3) {
//
//                String str = (String) arg0.getItemAtPosition(arg2);
//                Toast.makeText(getActivity(), "Address: " + str, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), "zipcode: " + zipcode, Toast.LENGTH_SHORT).show();
//                new LoadAddress(mEditTextZipCode.getText().toString()).execute();
//                mEditTextZipCode.setVisibility(View.GONE);
//                mEditTextZipCode.setVisibility(View.VISIBLE);
////                mAddressFragment.setPostalAddress(mAutoCompleteTextViewSearch.getText().toString());
//                Utils.HideSoftKeyBoard(getActivity(), mEditTextZipCode);
//            }
//        });
        return mView;

    }


    String brazil_CEP_search = "";

    public void CallBrazilAddress(String Pin) {

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String cepurl = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + Pin;
        System.out.println("CEP URL: " + cepurl);
        brazil_CEP_search = Pin;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.GET, cepurl,
                BrazilAddresssuccess(), BrazilAddressfail());
        queue.add(myReq);
    }

    private Response.Listener<String> BrazilAddresssuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();
                mProgressDialog = null;

                System.out.println("webservicecep Response :" + response);
                String brazil_address = "";

                try {

                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new InputSource(new StringReader(response)));
                    // normalize the document
                    doc.getDocumentElement().normalize();
                    // get the root node
                    NodeList nodeList = doc.getElementsByTagName("webservicecep");
                    Node node = nodeList.item(0);
                    System.out.println("webservicecep TextContent :" + node.getTextContent());

                    // the  node has three child nodes
                    boolean isAddressfound = true;

                    for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                        System.out.println("J=" + j);
                        String nameofattribute = node.getChildNodes().item(j).getNodeName();
                        if (nameofattribute.equalsIgnoreCase("resultado")
                                && node.getChildNodes().item(j).getTextContent().equalsIgnoreCase("1")) {
                            System.out.println("GONE when j=" + j);
                            isAddressfound = true;
                            break;
                        } else
                            isAddressfound = false;
                    }

                    if (isAddressfound) {
                        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                            System.out.println("GONE  I=" + i);
                            Node temp = node.getChildNodes().item(i);
                            System.out.println("webservicecep getNodeName: " + temp.getNodeName());
                            Log.e("getTextContent: ", "webservicecep getTextContent: " + temp.getTextContent());

                            if (temp.getNodeName().equalsIgnoreCase("cidade")) {
                                System.out.println("GONE logradouro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("bairro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("tipo_logradouro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("logradouro")) {
                                System.out.println("GONE cidade");
                                brazil_address += " " + temp.getTextContent();
                            }
                            System.out.println("Brazil Address[" + i + "]: " + brazil_address);
                        }
                        System.out.println("Brazil Address: " + brazil_address);
                    }
                    /*
                    Now below condition
                    "if (brazil_address.replaceAll(" ", "").length() == 0)"
                    will decide that set the "mNavigationActivity.mAutoCompleteTextViewSearch" with
                    Google places api adapter or to return the brazil address

                     */
                    if (brazil_address.replaceAll(" ", "").length() == 0) {
                        Log.e("BrazilAddresssuccess().MapFragment", "could not find the give brazil address: " + brazil_CEP_search);
                        Toast.makeText(getActivity(), "Sorry, Address with CEP " + brazil_CEP_search + " could not be found.", Toast.LENGTH_SHORT).show();
//                        mNavigationActivity.showAutocompleteTextview();
//                        mNavigationActivity.mAutoCompleteTextViewSearch.setText(brazil_CEP_search);
//                        Timer timer = new Timer();
//                        timer.schedule(new TimerTask() {
//                            @Override
//                            public void run() {
//                                GoogleAddressesresultList = autocomplete(brazil_CEP_search);
//                                System.out.println("size of :" + GoogleAddressesresultList.size());
//                                /*
//                                here "GoogleAddressesresultList" i set the array of serach result to arraylist
//                                 to over come null pointer error in adapter of "GooglePlacesAutocompleteAdapter"
//                                 */
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        setAutocompleteTextAdapter();
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.setAdapter(
//                                                new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.showDropDown();
//                                    }
//                                });
//
//                            }
//                        }, 100);
//
//
//                        /*
//                        setAutocompleteTextAdapter();
//                        using above line the Placesapi adapter will be set for auto complete textview.
//                         */
                    } else {
//                        getLocationPoints(brazil_address);
                        new LoadLocationFromCEPForMyOptions().execute(brazil_address);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private Response.ErrorListener BrazilAddressfail() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mProgressDialog.dismiss();
                mProgressDialog = null;

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Sorry, your address could not be found.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.f_myoptions_layout_gallery:

                /*Intent i = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(i, "Select a photo To upload."), Constants.GALLERY_PHOTO_REQUEST_CODE);

                */
                if (Build.VERSION.SDK_INT < 19) {
                    Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
                    gallerypickerIntent.setType("image/*");
                    startActivityForResult(gallerypickerIntent, Constants.GALLERY_PHOTO_REQUEST_CODE);
                } else {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            Constants.GALLERY_PHOTO_REQUEST_CODE_KITKAT);
                }
                break;

            case R.id.f_myoptions_image_edit_profile:
                setEnableEditables();
                break;

            case R.id.f_myoptions_layout_camera:
                capturepicture();
                break;
            case R.id.f_myoptions_button_update_profile:
                checkValidations();
                break;
            case R.id.f_myoptions_layout_text_changepassword:

                mDialogChangePassword.show();
//                mRelativeLayoutChangePassword.startAnimation(Utils.top);
//                mRelativeLayoutChangePassword.setVisibility(View.VISIBLE);
                Utils.showSoftKeyBoard(getActivity(), mEditTextCurrentPassword);

                Utils.setDefaultImage(mEditTextCurrentPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_current_password));
                Utils.setDefaultImage(mEditTextNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_password_first));
                Utils.setDefaultImage(mEditTextConfirmNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_confirm_password));
                break;

            case R.id.layout_change_password_button_save:
                validatePassword();
                break;
        }
    }

    public void setUserData_FromSharedPrefrence() {

        mUser = mStore_pref.getUser();
        mEditTextUserName.setVisibility(View.GONE);
        mEditTextCPFNumber.setVisibility(View.GONE);

        mLayoutCamera.setVisibility(View.GONE);
        mLayoutGallery.setVisibility(View.GONE);

        mTextViewUserName.setVisibility(View.VISIBLE);
        mTextViewCPFNumber.setVisibility(View.VISIBLE);

        mTextViewUserName.setText(mUser.getName());
        mTextViewCPFNumber.setText(mUser.getCpf_no());

        mEditTextUserName.setText(mUser.getName());
        mEditTextCPFNumber.setText(mUser.getCpf_no());

        mEditTextStreetName.setText(mUser.getStreet_name());
        mEditTextStreetNo.setText(mUser.getStreet_no());
        mEditTextAptNo.setText(mUser.getApt_no());
        mEditTextCityName.setText(mUser.getCity());
        mEditTextZipCode.setText(mUser.getPincode());
        mEditTextState.setText(mUser.getState());
        mEditTextContactNumber.setText(mUser.getMobile());

        mImageLoader.DisplayImage(mUser.getImg_url(), mImageViewUserProfile);
        mButtonUpdateProfile.setVisibility(View.GONE);
        isProfileImageChange = false;
        mUser.printAllData();

    }


    private void setEnableEditables() {
        mEditTextUserName.setEnabled(true);
        mEditTextCPFNumber.setEnabled(true);
//        mEditTextTagName.setEnabled(true);
        mEditTextStreetName.setEnabled(true);
        mEditTextStreetNo.setEnabled(true);
        mEditTextAptNo.setEnabled(true);
        mEditTextCityName.setEnabled(true);
        mEditTextZipCode.setEnabled(true);
        mEditTextState.setEnabled(true);
        mEditTextContactNumber.setEnabled(true);

        mEditTextContactNumber.setEnabled(true);

        mButtonUpdateProfile.setVisibility(View.VISIBLE);

        mEditTextUserName.setVisibility(View.VISIBLE);
        mEditTextCPFNumber.setVisibility(View.VISIBLE);

        mLayoutCamera.setVisibility(View.VISIBLE);
        mLayoutGallery.setVisibility(View.VISIBLE);

//        mTextViewUserName.setVisibility(View.GONE);
//        mTextViewCPFNumber.setVisibility(View.GONE);

//        mNavigationActivity.mImageViewFloatingEditProfile.setVisibility(View.GONE);
        mImageViewEditProfile.setVisibility(View.GONE);
        mEditTextUserName.requestFocus();
//        mNavigationActivity.mapFragment.setAutocompleteTextAdapterForMyOption();

    }

    public void setDisableEditables() {
        mEditTextUserName.setEnabled(false);
        mEditTextCPFNumber.setEnabled(false);
//        mEditTextTagName.setEnabled(false);
        mEditTextStreetName.setEnabled(false);
        mEditTextStreetNo.setEnabled(false);
        mEditTextAptNo.setEnabled(false);
        mEditTextCityName.setEnabled(false);
        mEditTextZipCode.setEnabled(false);
        mEditTextState.setEnabled(false);
        mEditTextContactNumber.setEnabled(false);

        mEditTextContactNumber.setEnabled(false);

        mButtonUpdateProfile.setVisibility(View.GONE);

        mEditTextUserName.setVisibility(View.GONE);
        mEditTextCPFNumber.setVisibility(View.GONE);

        mLayoutCamera.setVisibility(View.GONE);
        mLayoutGallery.setVisibility(View.GONE);

//        mTextViewUserName.setVisibility(View.VISIBLE);
//        mTextViewCPFNumber.setVisibility(View.VISIBLE);

//        mNavigationActivity.mImageViewFloatingEditProfile.setVisibility(View.VISIBLE);
        mImageViewEditProfile.setVisibility(View.VISIBLE);
    }

    public void checkValidations() {

        if (mEditTextUserName.getText().toString().length() < 0) {

            Toast.makeText(getActivity(), "Enter Valid Name.", Toast.LENGTH_LONG).show();
            mEditTextUserName.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextUserName);

        } else if (mEditTextCPFNumber.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter Valid CPF.", Toast.LENGTH_LONG).show();
            mEditTextCPFNumber.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextCPFNumber);


        } else if (mEditTextStreetName.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter Street Name.", Toast.LENGTH_LONG).show();
            mEditTextStreetName.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextStreetName);

        } else if (mEditTextStreetNo.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter Street Number.", Toast.LENGTH_LONG).show();
            mEditTextStreetNo.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextStreetNo);

        } else if (mEditTextAptNo.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter Apartment Number.", Toast.LENGTH_LONG).show();
            mEditTextAptNo.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextAptNo);

        } else if (mEditTextCityName.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter City Name.", Toast.LENGTH_LONG).show();
            mEditTextCityName.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextCityName);

        } else if (mEditTextState.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter State Name.", Toast.LENGTH_LONG).show();
            mEditTextState.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextState);

        } else if (mEditTextZipCode.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter Zincode.", Toast.LENGTH_LONG).show();
            mEditTextZipCode.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextZipCode);

        } else if (mEditTextContactNumber.getText().toString().length() == 0) {

            Toast.makeText(getActivity(), "Enter mobile number.", Toast.LENGTH_LONG).show();
            mEditTextContactNumber.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextContactNumber);

        } else if (mEditTextContactNumber.getText().toString().length() > 0 && mEditTextContactNumber.getText().toString().length() < 9) {

            Toast.makeText(getActivity(), "Please enter 9 digit subscriber number.", Toast.LENGTH_LONG).show();
            mEditTextContactNumber.requestFocus();
            Utils.ShowSoftKeyBoard(getActivity(), mEditTextContactNumber);
//            Utils.showSoftKeyBoard(getActivity(), mEditTextContactNumber);


        } else {

            if (Utils.isConnectingToInternet(getActivity()))
                new CallEditProfileService().execute();
            else {
                Utils.showInternetDialog(getActivity());
            }

        }

    }

    // a method for capturing a picture from inbuilt camera.
    private void capturepicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String timestamp = String.valueOf(System.currentTimeMillis());
        uriSavedImage = Uri.fromFile(new File(PhotoPath + "/" + timestamp + ".png"));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        startActivityForResult(intent, Constants.CAMERA_PHOTO_REQUEST_CODE);
    }
    // capturepicture() ends


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case Constants.GALLERY_PHOTO_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    handleResultFromChooser(data);
                }

                break;
            case Constants.GALLERY_PHOTO_REQUEST_CODE_KITKAT:
                if (resultCode == Activity.RESULT_OK) {
                    handleResultFromChooserKikat(data);
                }

                break;

            case Constants.CAMERA_PHOTO_REQUEST_CODE:

                if (resultCode == Activity.RESULT_OK) {
                    handleResultFromCamera(data);
                }

                break;
            case Activity.RESULT_CANCELED:
                Toast.makeText(getActivity(), "Photto can not be loaded.", Toast.LENGTH_SHORT).show();
                Log.e("User canceled to  select a picture", "Canceled by user to select/take photo.");
                isProfileImageChange = false;
                break;
        }

    }

    private void handleResultFromCamera(Intent data) {
        Bitmap b = null;
        if (uriSavedImage != null) {
            picturePath = uriSavedImage.getPath().toString();
            try {
                b = getImageBitmap(picturePath);
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        compressImage(b);
    }

    private void handleResultFromChooser(Intent data) {

        if (data != null) {


            Uri selectedImage = data.getData();
            if (selectedImage != null) {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();

                Bitmap b = null;
                try {
                    b = getImageBitmap(picturePath);
                } catch (IOException e) {

                    e.printStackTrace();
                }
                compressImage(b);
            }
        }
    }

    private void handleResultFromChooserKikat(Intent data) {

        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap b = null;
            try {
                b = getImageBitmap(picturePath);
            } catch (IOException e) {

                e.printStackTrace();
            }

            compressImage(b);
        }

    }

    public void compressImage(Bitmap bitmap) {
        timestamp = String.valueOf(System.currentTimeMillis());

        picturePath = PhotoPath + "/" + timestamp + ".png";

        //mImageViewCrop.setImageBitmap(croppedImage);
        //getCropImage();

        //croppedImage = mImageViewCrop.getDrawingCache();

        if (bitmap != null) {
            if (bitmap.getHeight() > 800 || bitmap.getWidth() > 800) {
                File file = new File(picturePath);
                FileOutputStream fOut = null;
                try {
                    Log.e("in try", "try");
                    Log.e("in try", "try" + picturePath);
                    fOut = new FileOutputStream(file);
                    Log.e("in try", "try after fout");
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (fOut != null) {
                    Log.e("in try", "try before bitmap");
                    bitmap = Bitmap.createScaledBitmap(bitmap, 600, 600, false);
                    Log.e("in try", "try before 100");
                    bitmap.compress(Bitmap.CompressFormat.PNG, 80, fOut);
                    Log.e("in try", "try after 100");
                }

            } else {
                File file = new File(picturePath);
                FileOutputStream fOut = null;

                try {
                    Log.e("in try else", "try else");
                    Log.e("in try", "try else" + picturePath);
                    fOut = new FileOutputStream(file);
                    Log.e("in try", "try after fout else");
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (fOut != null) {
                    Log.e("in try", "try before bitmap else");
                    bitmap = Bitmap.createScaledBitmap(bitmap, 600, 600, false);
                    Log.e("in try", "try before 100 else");
                    bitmap.compress(Bitmap.CompressFormat.PNG, 80, fOut);
                    Log.e("in try", "try after 100 else");
                }
            }
        }

        mImageViewUserProfile.setImageBitmap(bitmap);
    }

    public Bitmap getImageBitmap(String path) throws IOException {
        // Allocate files and objects outside of timingoops

        Bitmap bmp = null;
        try {
            File file = new File(path);
            RandomAccessFile in = new RandomAccessFile(file, "rws");
            final FileChannel channel = in.getChannel();
            final int fileSize = (int) channel.size();
            final byte[] testBytes = new byte[fileSize];
            final ByteBuffer buff = ByteBuffer.allocate(testBytes.length);
            final byte[] buffArray = buff.array();
            //@SuppressWarnings("unused")
            //final int buffBase = buff.arrayOffset();

            // Read from channel into buffer, and batch read from buffer to byte array;
            long time1 = System.currentTimeMillis();
            channel.position(0);
            channel.read(buff);
            buff.flip();
            buff.get(testBytes, 0, testBytes.length);


            bmp = Bitmap_process(buffArray);


            long time2 = System.currentTimeMillis();
            //			System.out.println("Time taken to load: " + (time2 - time1) + "ms");
        } catch (BufferUnderflowException e) {

        }
        return bmp;
    }

    public static Bitmap Bitmap_process(byte[] buffArray) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inDither = false;                     //Disable Dithering mode
        options.inPurgeable = true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
        options.inInputShareable = true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
        options.inTempStorage = new byte[32 * 1024];  //Allocate some temporal memory for decoding

        options.inSampleSize = 1;

        Bitmap imageBitmap = BitmapFactory.decodeByteArray(buffArray, 0, buffArray.length, options);
        return imageBitmap;
    }

    /*
    public void CallUpdateProfileService() {

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Updating Profile...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.UPDATE_USER_PROFILE, createMyReqSuccessListener(), createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id", mUser.getId());
                params.put("name", "" + mEditTextUserName.getText());
                params.put("street_name", "" + mEditTextStreetName.getText());
                params.put("street_no", "" + mEditTextStreetNo.getText());
                params.put("apt_no", "" + mEditTextAptNo.getText());
                params.put("city", "" + mEditTextCityName.getText());
                params.put("state", "" + mEditTextState.getText());
                params.put("pincode", "" + mEditTextZipCode.getText());
                params.put("mobile", "" + mEditTextContactNumber.getText());
                params.put("cpf_no", "" + mEditTextCPFNumber.getText());
//                params.put("img_url", "");

                if (imagepath != null) {
                    MultipartEntity entity = new MultipartEntity();
                    Log.e("Image Set", "Image is set from java");
                    File sourceFile0 = new File(imagepath);
                    if (!imagepath.equalsIgnoreCase("")) {
                        entity.addPart("img_url", new FileBody(sourceFile0,
                                "image*//*"));
                    }
                }
                return params;
            }
        };
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressDialog.dismiss();
                Toast.makeText(getActivity(), response + "", Toast.LENGTH_SHORT).show();

                Log.e("result: ", "" + response);
                response = response.replace("[", "");
                response = response.replace("]", "");
                Log.i("result: ", "" + response);
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {

                    mStore_pref.setUser(register.getUser());
                    mStore_pref.setUserId(register.getUser().getId());
                    setUserData_FromSharedPrefrence();
                    setDisableEditables();
                }
                Toast.makeText(getActivity(), register.getResponseMsg(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                Log.e("createMyReqErrorListener().MyOptionsFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
            }
        };
    }*/


    public class CallEditProfileService extends AsyncTask<Void, Void, Void> {

        String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Updating Profile...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            response = uploadService();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            Toast.makeText(getActivity(), response + "", Toast.LENGTH_SHORT).show();

            Log.e("CallEditProfileService: Response: ", "" + response);
            response = response.replace("[", "");
            response = response.replace("]", "");
            Log.i("CallEditProfileService: Response: ", "" + response);

            Register register = mGson.fromJson(response, Register.class);

            if (register != null && register.getResponseCode() == 1) {

                mStore_pref.setUser(register.getUser());
                mStore_pref.setUserId(register.getUser().getId());
                setUserData_FromSharedPrefrence();
                setDisableEditables();
                mNavigationActivity.setProfile();
                Toast.makeText(getActivity(), register.getResponseMsg(), Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();


        }
    }

    public String uploadService() {
        String responseBody = "";
        try {

            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

            HttpClient httpclient = new DefaultHttpClient(params);
            HttpPost httppost = null;

            httppost = new HttpPost(Constants.UPDATE_USER_PROFILE);

            /*

            MultipartEntity entity = new MultipartEntity();

            entity.addPart("user_id", new StringBody(mUser.getId()));
            entity.addPart("name", new StringBody("" + mEditTextStreetName.getText()));
            entity.addPart("street_name", new StringBody("" + mEditTextStreetName.getText()));
            entity.addPart("street_no", new StringBody("" + mEditTextStreetNo.getText()));
            entity.addPart("apt_no", new StringBody("" + mEditTextAptNo.getText()));
            entity.addPart("city", new StringBody("" + mEditTextCityName.getText()));
            entity.addPart("state", new StringBody("" + mEditTextState.getText()));
            entity.addPart("pincode", new StringBody("" + mEditTextZipCode.getText()));
            entity.addPart("mobile", new StringBody("" + mEditTextContactNumber.getText()));
            entity.addPart("cpf_no", new StringBody("" + mEditTextCPFNumber.getText()));
            if (isProfileImageChange && imagepath != null) {

                Log.e("Image Set", "Image is set from java");
                File sourceFile0 = new File(imagepath);
                if (!imagepath.equalsIgnoreCase("")) {
                    entity.addPart("img_url", new FileBody(sourceFile0,
                            "image*//*"));
                }
            } else {
                entity.addPart("img_url", new StringBody(""));
            }

            */

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, null, Charset.forName("UTF-8"));

            String tmp = "";
            entity.addPart("id", new StringBody(mUser.getId()));

            tmp = /*URLEncoder.encode(*/mEditTextUserName.getText().toString()/*)*/;
            entity.addPart("name", new StringBody(tmp));


            tmp = /*URLEncoder.encode(*/mEditTextStreetName.getText().toString()/*)*/;
            entity.addPart("street_name", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextStreetNo.getText().toString()/*)*/;
            entity.addPart("street_no", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextAptNo.getText().toString()/*)*/;
            entity.addPart("apt_no", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextCityName.getText().toString()/*)*/;
            entity.addPart("city", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextState.getText().toString()/*)*/;
            entity.addPart("state", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextZipCode.getText().toString()/*)*/;
            entity.addPart("pincode", new StringBody(tmp));

            entity.addPart("lat", new StringBody("" + lat));
            entity.addPart("lng", new StringBody("" + lng));


            tmp = /*URLEncoder.encode(*/mEditTextContactNumber.getText().toString()/*)*/;
            entity.addPart("mobile", new StringBody(tmp));

            tmp = /*URLEncoder.encode(*/mEditTextCPFNumber.getText().toString()/*)*/;
            entity.addPart("cpf_no", new StringBody(tmp));

            Log.e("uploadService()", "isProfileImageChange: " + isProfileImageChange);
           /* if (isProfileImageChange && picturePath.length() > 0) {

                Log.e("Image Set", "Image is set from java");
                File sourceFile0 = new File(picturePath);
                if (!picturePath.equalsIgnoreCase("")) {
                    entity.addPart("img_url", new FileBody(sourceFile0,
                            "image/*"));
                }

            } else {
                entity.addPart("img_url", new StringBody(""));
            }*/
            File sourceFile0 = new File(picturePath);
            if (!picturePath.equalsIgnoreCase("")) {
                entity.addPart("img_url", new FileBody(sourceFile0, "image/*"));
            }
            Log.e("User Information", "UserData " + picturePath);
            mUser.printAllData();
            Log.e("User Information", "UserData");
            System.out.println("uploadService() Name: " + mEditTextUserName.getText());
            System.out.println("uploadService() street_name: " + mEditTextStreetName.getText());
            System.out.println("uploadService() street_no: " + mEditTextStreetNo.getText());
            System.out.println("uploadService() apt_no: " + mEditTextAptNo.getText());
            System.out.println("uploadService() city: " + mEditTextCityName.getText());
            System.out.println("uploadService() state: " + mEditTextState.getText());
            System.out.println("uploadService() pincode: " + mEditTextZipCode.getText());
            System.out.println("uploadService() mobile: " + mEditTextContactNumber.getText());
            System.out.println("uploadService() cpf_no: " + mEditTextCPFNumber.getText());

            httppost.setEntity(entity);

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpclient.execute(httppost, responseHandler);

            Log.e("response: ", responseBody);

        } catch (Exception e) {
            Log.e("Error in uploadService()", e.toString());
            e.printStackTrace();
        }

        return responseBody;

    }

    private void initChangePasswordDialog() {
        mDialogChangePassword = new Dialog(getActivity());
        mDialogChangePassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogChangePassword.setContentView(R.layout.dialog_change_password);

//        mRelativeLayoutChangePassword = (RelativeLayout) mDialogChangePassword.findViewById(R.id.f_myoptions_layout_change_password);
        mEditTextCurrentPassword = (EditText) mDialogChangePassword.findViewById(R.id.layout_change_password_edit_current_password);
        mEditTextNewPassword = (EditText) mDialogChangePassword.findViewById(R.id.layout_change_password_edit_new_password_first);
        mEditTextConfirmNewPassword = (EditText) mDialogChangePassword.findViewById(R.id.layout_change_password_edit_new_confirm_password);

        mButtonSavePassword = (Button) mDialogChangePassword.findViewById(R.id.layout_change_password_button_save);

        mDialogChangePassword.findViewById(R.id.layout_change_password_show_current_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.setShowHidePassword(mEditTextCurrentPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_current_password));
            }
        });

        mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_password_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setShowHidePassword(mEditTextNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_password_first));
            }
        });

        mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_confirm_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.setShowHidePassword(mEditTextConfirmNewPassword, (ImageView) mDialogChangePassword.findViewById(R.id.layout_change_password_show_new_confirm_password));
            }
        });


        mButtonSavePassword.setOnClickListener(this);

//        Utils.TopAnimation();
//        Utils.BottomAnimation();
//        mRelativeLayoutChangePassword.setVisibility(View.GONE);

    }

    void validatePassword() {
        if (mEditTextCurrentPassword.getText().toString().length() <= 4) {

            Toast.makeText(getActivity(), "Current Password must be more then 4 characters.", Toast.LENGTH_SHORT).show();
            mEditTextCurrentPassword.requestFocus();
            mEditTextCurrentPassword.setSelection(0, mEditTextCurrentPassword.length());
            Utils.showSoftKeyBoard(getActivity(), mEditTextCurrentPassword);

        }
//        else if ((mEditTextCurrentPassword.getText().toString().length() > 4) && (!mUser.getPassword().equalsIgnoreCase(Utils.md5(mEditTextCurrentPassword.getText().toString())))) {
        else if (!mUser.getPassword().equalsIgnoreCase(Utils.md5(mEditTextCurrentPassword.getText().toString()))) {

            Toast.makeText(getActivity(), "You entered invalid password in current password field.", Toast.LENGTH_SHORT).show();
            Log.e("validatePassword()", "CurrentPassword: " + mUser.getPassword());
            Log.e("validatePassword()", "CurrentPassword Entered: " + Utils.md5(mEditTextCurrentPassword.getText().toString()));
            mEditTextCurrentPassword.requestFocus();
            mEditTextCurrentPassword.setSelection(0, mEditTextCurrentPassword.length());
            Utils.showSoftKeyBoard(getActivity(), mEditTextCurrentPassword);

        } else if (mEditTextNewPassword.getText().toString().length() < 4) {

            Toast.makeText(getActivity(), "New Password must be more then 4 characters.", Toast.LENGTH_SHORT).show();
            mEditTextNewPassword.requestFocus();
            mEditTextNewPassword.setSelection(0, mEditTextNewPassword.length());
            Utils.showSoftKeyBoard(getActivity(), mEditTextNewPassword);

        } else if (mEditTextConfirmNewPassword.getText().toString().length() < 4) {

            Toast.makeText(getActivity(), "Confirm New Password must be more then 4 characters.", Toast.LENGTH_SHORT).show();
            mEditTextConfirmNewPassword.requestFocus();
            mEditTextConfirmNewPassword.setSelection(0, mEditTextConfirmNewPassword.length());
            Utils.showSoftKeyBoard(getActivity(), mEditTextConfirmNewPassword);

        } else {
            String newpd = mEditTextNewPassword.getText().toString();
            String confirmpd = mEditTextConfirmNewPassword.getText().toString();
            if (!newpd.equalsIgnoreCase(confirmpd)) {
                Toast.makeText(getActivity(), "Password does not match, please re-enter.", Toast.LENGTH_SHORT).show();
                mEditTextNewPassword.requestFocus();
                mEditTextNewPassword.setSelection(0, mEditTextNewPassword.length());
                Utils.showSoftKeyBoard(getActivity(), mEditTextNewPassword);
            } else {
                Utils.hide_keyboard(getActivity());
                new ChangePasswordService().execute();
            }
        }
    }

    public class ChangePasswordService extends AsyncTask<Void, Void, Void> {

        String response = "";
        String url = "";

        ChangePasswordService() {

            try {

                url = Constants.CHANGE_PASSWORD + "&id=" + mStore_pref.getUserId()
                        + "&new_password=" + URLEncoder.encode(String.valueOf(mEditTextNewPassword.getText()), "UTF-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                HttpClient mHttpClient = new DefaultHttpClient();
                System.out.println("Address Url: " + url);
                HttpGet mHttpGet = new HttpGet(url);
                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("AddressService().DoinBackground()", "Could not reach to server.");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Could not reach to server, try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Register register = null;
            mProgressDialog.dismiss();
            Log.e("Response", "Response: " + response);
            Toast.makeText(getActivity(), response + "", Toast.LENGTH_SHORT).show();

            try {
                JSONObject mJsonObject = new JSONObject(response);
                if (mJsonObject.getJSONArray("user").length() > 0) {
                    Log.e("result: ", "" + response);
                    response = response.replace("[", "");
                    response = response.replace("]", "");
                    Log.i("result: ", "" + response);
                    register = mGson.fromJson(response, Register.class);
                    if (register.getResponseCode() == 1) {

                        mStore_pref.setUser(register.getUser());
                        mStore_pref.setUserId(register.getUser().getId());
                        setUserData_FromSharedPrefrence();
                        setDisableEditables();

//                        if (mRelativeLayoutChangePassword.getVisibility() == View.VISIBLE) {
//                            mRelativeLayoutChangePassword.setVisibility(View.GONE);
//                            mRelativeLayoutChangePassword.startAnimation(Utils.bottom);
//                            Utils.HideLayout(mRelativeLayoutChangePassword);
//                        }
//                        mRelativeLayoutChangePassword.setVisibility(View.GONE);

                    }
                    Toast.makeText(getActivity(), register.getResponseMsg(), Toast.LENGTH_SHORT).show();

                }// main if Json object ends

                mDialogChangePassword.dismiss();
            } catch (JSONException ej) {
                ej.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setUserData_FromSharedPrefrence();
        }
    }


    public class LoadLocationFromCEPForMyOptions extends AsyncTask<String, Void, String> {

        String response = "";
        List<Address> add;
        String brazil_address_received = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Finding CEP Location...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (params.length == 0)
                return "";
            try {
                System.out.println("------------------getLocationPoints()----------------");
                System.out.println("Address Received: " + params[0]);
                brazil_address_received = params[0];
                System.out.println("Address Received Length: " + params[0].length());

                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocationName(params[0], 1);
                Tags.ADDRESS_NUMBER = 2;
            } catch (Exception e) {
                e.printStackTrace();
                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            String address[] = brazil_address_received.split("#");
            if (add != null && add.size() > 0) {
                Log.e("LoadLocationFromCEP()", "-----------------------LoadLocationFromCEP--------------");
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());

                Log.e("getLocationPoint()", "Full Address: " + add.get(0));
                System.out.println("getLocationPoint() Longitude" + add.get(0).getLongitude());
                System.out.println("getLocationPoint() Latitude" + add.get(0).getLatitude());


                if (add.get(0).getThoroughfare() != null)
                    mEditTextStreetName.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mEditTextStreetName.setText(add.get(0).getFeatureName());
                else
                    mEditTextStreetName.setText(add.get(0).getAddressLine(0));


                if (add.get(0).getSubThoroughfare() != null)
                    mEditTextStreetNo.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mEditTextCityName.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mEditTextCityName.setText(add.get(0).getLocality());
                else {
                        /*
                        this else part is added because some time city name is not coming in G places
                         */
                    mEditTextCityName.setText(address[0]);
                }

                if (add.get(0).getAdminArea() != null)
                    mEditTextState.setText(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mEditTextZipCode.setText(add.get(0).getPostalCode());
                else
                    mEditTextZipCode.setText(brazil_CEP_search);
//                    mEditTextZipCode.setText(zipcode);

                lat = add.get(0).getLatitude();
                lng = add.get(0).getLongitude();

                zipcode = "";

                Log.e("getLocationPoints().MapFragment", "Address line 1: " + add.get(0).getAddressLine(0));
                    /*if (add.get(0).getAddressLine(3).length() > 0) {
                        Log.e("getLocationPoints().MapFragment", "Address line 2" + add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                    } else {*/
                Log.e("getLocationPoints().MapFragment", "Address line 2 & 3: " + add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));

            } else {
                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    String zipcode = "";
    double lat;
    double lng;

    public class LoadAddress extends AsyncTask<Void, Void, Void> {

        List<Address> add;
        ProgressDialog mProgressDialog;

        String searchAddress = "";

        LoadAddress(String address) {
            searchAddress = address;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            try {
                mProgressDialog.setMessage("Finding Location...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());

                add = geo.getFromLocationName(searchAddress, 2);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();

            if (add != null && add.size() > 0) {

                System.out.println("-------------------- Primary Address (MyFragment.java) ----------------");
                System.out.println("Max addressline index: " + add.get(0).getMaxAddressLineIndex());
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));

                System.out.println("Longitude: " + add.get(0).getLongitude());
                System.out.println("Latitude: " + add.get(0).getLatitude());

                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Postal code: " + add.get(0).getPostalCode());

                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());

        /*
                mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) == null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2));
                else if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) != null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));

                mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());

                if (add.get(0).getSubAdminArea() == null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                else
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                street_name_et.setText(add.get(0).getAddressLine(0));
//                city_et.setText(add.get(0).get);
        */
                if (add.get(0).getThoroughfare() != null)
                    mEditTextStreetName.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mEditTextStreetName.setText(add.get(0).getFeatureName());
                else
                    mEditTextStreetName.setText(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mEditTextStreetNo.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mEditTextCityName.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mEditTextCityName.setText(add.get(0).getLocality());

                if (add.get(0).getAdminArea() != null)
                    mEditTextState.setText(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mEditTextZipCode.setText(add.get(0).getPostalCode());
                else
                    mEditTextZipCode.setText(zipcode);

                lat = add.get(0).getLatitude();
                lng = add.get(0).getLongitude();

                zipcode = "";

            } else {
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
