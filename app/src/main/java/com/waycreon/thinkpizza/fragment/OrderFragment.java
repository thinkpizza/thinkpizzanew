package com.waycreon.thinkpizza.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.Response.GetAdditionsAndCrust;
import com.waycreon.thinkpizza.adapter.AdditionsListAdapter;
import com.waycreon.thinkpizza.adapter.FavoritePizzaAdapter;
import com.waycreon.thinkpizza.adapter.PizzaCrustListAdapter;
import com.waycreon.thinkpizza.adapter.ToppingAdapter;
import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.thinkpizza.imageloader.ImageLoaderBlur;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class OrderFragment extends Fragment implements OnClickListener {

    /*
        Restaurant layout controls
         */
    ImageView mImageViewPizzaimagebk;
    ImageView mImageViewPizzaImage;
    TextView mTextViewPizzaName;
    TextView mTextViewPizzaDescription;
    /*
    pizza order control
     */
    RelativeLayout mRelativeLayoutPizzaSizes;
    TextView mTextViewBasicPrice;
    LinearLayout mLinearLayoutRegular;
    TextView mTextViewRegularPrice;
    ImageView mImageViewRegular;
    //    LinearLayout mLinearLayoutMedium;
//    TextView mTextViewMediumPrice;
//    ImageView mImageViewMedium;
    LinearLayout mLinearLayoutLarge;
    TextView mTextViewLargePrice;
    ImageView mImageViewLarge;

    /*
    Pizza Crust (Pizza Base Layout
     */
    LinearLayout mLinearLayoutPizzaBase;
    ImageView mImageViewPizzabase;
    TextView mTextViewPizzabaseSelected;
    TextView mTextViewPizzaBasePrice;
    /*
    Pizza Additoin of toppings
     */
    LinearLayout mLinearLayoutAdditions;
    ImageView mImageViewAddition;
    TextView mTextViewAdditionsSelected;
    TextView mTextViewAdditionsTotalPrice;

    /*
   Remove Pizza Toppings
    */
    LinearLayout mLinearLayoutRemoveToppings;
    ImageView mImageViewToppings;
    TextView mTextViewToppingsSelected;
//    TextView mTextViewToppingsTotalPrice;


    ImageView mImageViewIncrementQuantity;
    ImageView mImageViewDecrementQuantity;
    //    TextView mTextViewQuantity;
    Spinner mSpinnerQuantity;
    //    String quantity_array[];
    TextView mTextViewTotalOrder;
    Button mButtonAddtoBasket;

    /*
    -------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------
                                 Addition Layout declaration start
     -------------------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------------------------
	 */

    Dialog mDialogAdditions;
    View mViewDialogAdditions;
    ImageView mImageViewRefreshAdditions;

    ListView mListViewAdditionListItems_dialog;
    public TextView mTextViewTotalAdditoin_dialog;
    Button mButtonApplyAdditions_dialog;
    /*String[] AdditionsName = {"Extra Cheese", "Black Olives", "Chilli Flex",
            "Extra Corn", "Extra Paneer"};
    int[] AdditionsPrice = {55, 45, 25, 30, 60};*/

    ArrayList<AdditionsListData> mAdditionsListDatas_dialog;
    AdditionsListAdapter mAdditionsListAdapter_dialog;

    /*
   -------------------------------------------------------------------------------------------------
   ------------------------------------------------------------------------------------------------
                    PizzaCrust (base) Layout declaration End
    -------------------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------------------------
	 */

    Dialog mDialogPizzaCrust;
    View mViewDialogPizzaCrust;
    ImageView mImageViewRefreshPizzaCrust;

    //    public LinearLayout mLinearLayoutPizzaBaseScreen;
    ListView mListViewPizzaCrustListItems_dialog;
    public TextView mTextViewTotalPizzaCrust_dialog;
    Button mButtonApplyCrust_dialog;
    /*String[] CrustName = {"Classic Hand Tossed", "Fresh Pan Pizza", "Wheat Thin Crust",
            "Cheese Burst", "Cheesy Wonder"};
    int[] CrustPrice = {0, 45, 50, 80, 90};*/

    ArrayList<PizzaCrustData> mPizzaCrustDatas_dialog;
    PizzaCrustListAdapter mPizzaCrustListAdapter_dialog;

/*
   -------------------------------------------------------------------------------------------------
   ------------------------------------------------------------------------------------------------
                   Add to Favorite Dialog Declaration Start
     -------------------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------------------------
	 */

    public Dialog mDialogFavorite;
    List<OrderedItemsData> mOrderedItemsDatas_Favorite_dialog;
    FavoritePizzaAdapter mAdapterFavorite_dialog;
    ListView mListViewFavorite_dialog;

    TextView mTextViewDialogHeading;
    ImageView mImageViewDialogImage;
    ImageView mImageViewClose;
    LinearLayout mRelativeLayoutAddtoFavorite_dialog;
    TextView mTextViewYes;
    TextView mTextViewNo;


    /*
   -------------------------------------------------------------------------------------------------
   ------------------------------------------------------------------------------------------------
                    Remove Toppings Layout declaration End
    -------------------------------------------------------------------------------------------------
     -------------------------------------------------------------------------------------------------
	 */

    Dialog mDialogRemoveToppings;
    View mViewDialogRemoveTopping;

    TextView mTextViewHeadingTopping_dialog;
    ListView mListViewRemoveTopping_dialog;
    public TextView mTextViewTotal_Topping_dialog;
    Button mButtonApplyTopping_dialog;

    ArrayList<Toppings> mToppingses_array_dialog;
    ToppingAdapter mToppingAdapter_dialog;

    boolean isRemoveToppingsSelected = false;
    boolean isApplayButtonClickToppings = false;
    String selectedToppingsIDs = "";

    //----------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------

    NavigationActivity mNavigationActivity;
    ProgressDialog mProgressDialog;
    Gson mGson;
    Store_pref mStore_pref;


    PizzaRestro mPizzaRestroReceived;
    PizzaListData mPizzaListDataReceived;
    int item_position = -1;
    double quantities[] = {0.25, 0.33, 0.5, 1, 1.25, 1.33, 1.5, 2, 2.25, 2.33, 2.5, 3, 3.25, 3.33, 3.5, 4, 4.25, 4.33, 4.5, 5, 5.25, 5.33, 5.5,
            6, 6.25, 6.33, 6.5, 7, 7.25, 7.33, 7.5, 8, 8.25, 8.33, 8.5, 9, 9.25, 9.33, 9.5, 10, 10.25, 10.33, 10.5};
    int quantity_position = 0;
    double quantity = 0.25;
    float basicpay = 0;
    double totalpay = 0;
    double totaladditions_selected = 0;
    double totalpizzacrust_selected = 0;
    double totaltoppings_selected = 0;
    double totaltoppings_removed = 0;
    int count_toppings_selected = 0;
    int count_additions_selected = 0;


    String pizzasize;
    boolean isadditionsselected = false;
    boolean ispizzacrustselected = false;


    boolean isEditOrderItem = false;
    OrderedItemsData orderedItemsDataEdit = null;
    int position_selected_for_edit;

    String selectedAdditionsIds = "";
    String selectedCrustIds = "";
    ArrayList<AdditionsListData> mArrayListAdditionsSelected = new ArrayList<>();
    boolean isApplayButtonClickAdditions = false;
    boolean isApplayButtonClickPizzaCrust = false;


    /*private void SetPizzaCrustItemsList() {
        mPizzaCrustDatas_dialog = new ArrayList<>();

        for (int i = 0; i < CrustName.length; i++)
            mPizzaCrustDatas_dialog.add(new PizzaCrustData(CrustName[i],
                    CrustPrice[i]));

        mPizzaCrustListAdapter_dialog = new PizzaCrustListAdapter(getActivity(), mPizzaCrustDatas_dialog);
        mListViewPizzaCrustListItems_dialog.setAdapter(mPizzaCrustListAdapter_dialog);
    }*/


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.copyoffragment_order, container,
                false);
        mViewDialogAdditions = inflater.inflate(R.layout.dialog_pizza_additions,
                (ViewGroup) mView, false);
        mViewDialogPizzaCrust = inflater.inflate(R.layout.dialog_pizza_crust,
                (ViewGroup) mView, false);
        mViewDialogRemoveTopping = inflater.inflate(R.layout.dialog_pizza_additions,
                (ViewGroup) mView, false);

        initDialogPizzaCrust();
        initDialogAddition();
        initDialogRemoveToppings();


        mNavigationActivity = (NavigationActivity) getActivity();
        mGson = new Gson();
        mStore_pref = new Store_pref(getActivity());
        mProgressDialog = new ProgressDialog(mNavigationActivity);
            /*
                Restaurant layout controls
            */
        mImageViewPizzaimagebk = (ImageView) mView.findViewById(R.id.frag_order_image_pizza_background);
        mImageViewPizzaImage = (ImageView) mView.findViewById(R.id.frag_order_image_pizza);
        mTextViewPizzaName = (TextView) mView.findViewById(R.id.frag_order_text_pizzaname);
        mTextViewPizzaDescription = (TextView) mView.findViewById(R.id.frag_order_text_pizzadescription);

            /*
                pizza order control
            */

        mRelativeLayoutPizzaSizes = (RelativeLayout) mView.findViewById(R.id.frag_order_layout_pizzasizes);

        mTextViewBasicPrice = (TextView) mView.findViewById(R.id.frag_order_textview_basicprice);

        mLinearLayoutRegular = (LinearLayout) mView.findViewById(R.id.frag_order_layout_regular);
        mTextViewRegularPrice = (TextView) mView.findViewById(R.id.frag_order_text_regularprice);
        mImageViewRegular = (ImageView) mView.findViewById(R.id.frag_order_image_regular);

//        mLinearLayoutMedium = (LinearLayout) mView.findViewById(R.id.frag_order_layout_medium);
//        mTextViewMediumPrice = (TextView) mView.findViewById(R.id.frag_order_text_mediumprice);
//        mImageViewMedium = (ImageView) mView.findViewById(R.id.frag_order_image_medium);

        mLinearLayoutLarge = (LinearLayout) mView.findViewById(R.id.frag_order_layout_large);
        mTextViewLargePrice = (TextView) mView.findViewById(R.id.frag_order_text_largeprice);
        mImageViewLarge = (ImageView) mView.findViewById(R.id.frag_order_image_large);

        //Pizza Crust (Pizza Base Layout
        mLinearLayoutPizzaBase = (LinearLayout) mView.findViewById(R.id.frag_order_layout_pizzabase);
        mImageViewPizzabase = (ImageView) mView.findViewById(R.id.frag_order_image_selectpizzacrust);
        mTextViewPizzabaseSelected = (TextView) mView.findViewById(R.id.frag_order_text_selectpizzacrust);
        mTextViewPizzaBasePrice = (TextView) mView.findViewById(R.id.frag_order_text_selectpizzacrust_price);

        // additions layout
        mLinearLayoutAdditions = (LinearLayout) mView.findViewById(R.id.frag_order_layout_additions);
        mImageViewAddition = (ImageView) mView.findViewById(R.id.frag_order_image_additioncheck);
        mTextViewAdditionsSelected = (TextView) mView.findViewById(R.id.frag_order_text_additionselected);
        mTextViewAdditionsTotalPrice = (TextView) mView.findViewById(R.id.frag_order_text_additiontotalprice);

        // Remove pizza Toppings layout

        mLinearLayoutRemoveToppings = (LinearLayout) mView.findViewById(R.id.frag_order_layout_remove_pizza_toppings);
        mImageViewToppings = (ImageView) mView.findViewById(R.id.frag_order_image_toppings);
        mTextViewToppingsSelected = (TextView) mView.findViewById(R.id.frag_order_text_toppingselected);
//        mTextViewToppingsTotalPrice = (TextView) mView.findViewById(R.id.frag_order_text_toppings_total_price);


        mImageViewIncrementQuantity = (ImageView) mView.findViewById(R.id.frag_order_image_price_increase);
        mImageViewDecrementQuantity = (ImageView) mView.findViewById(R.id.frag_order_image_price_decrease);
//        mTextViewQuantity = (TextView) mView.findViewById(R.id.frag_order_text_quantity);
        mSpinnerQuantity = (Spinner) mView.findViewById(R.id.frag_order_spinner_quantiy);
        mTextViewTotalOrder = (TextView) mView.findViewById(R.id.frag_order_textview_totalprice);
        mButtonAddtoBasket = (Button) mView.findViewById(R.id.frag_order_buton_addtobasket);

        /*
        Click events of Order screen
         */

        mImageViewPizzabase.setOnClickListener(this);
        mImageViewAddition.setOnClickListener(this);
        mImageViewToppings.setOnClickListener(this);
        mImageViewIncrementQuantity.setOnClickListener(this);
        mImageViewDecrementQuantity.setOnClickListener(this);

        mLinearLayoutRegular.setOnClickListener(this);
//        mLinearLayoutMedium.setOnClickListener(this);
        mLinearLayoutLarge.setOnClickListener(this);

        mButtonAddtoBasket.setOnClickListener(this);

//        quantity_array = getActivity().getResources().getStringArray(R.array.quantity_array);
//        mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, quantity_array));
//        mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, getActivity().getResources().getStringArray(R.array.quantity_array)));
        mSpinnerQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("SpinnerClick: Selected Position:" + i);
                System.out.println("SpinnerClick: Selected Quantity:" + mSpinnerQuantity.getItemAtPosition(i));
//                quantity = i + 1;
                quantity = quantities[i];
                quantity_position = i;
                mSpinnerQuantity.setSelection(i);
                DoTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        mTextViewQuantity.setText("Quantity " + quantity);
        mSpinnerQuantity.setSelection(0);


        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.frag_order_image_selectpizzacrust:

                if (mPizzaCrustDatas_dialog.size() == 0)
                    CallPizzaCrustServices();

                else if (mPizzaCrustDatas_dialog.size() > 0) {
                    Log.e("SelectPizzacrust Image.click()", "selectedCrustIds: " + selectedCrustIds);
                    Log.e("SelectPizzacrust Image.click()", "totalpizzacrust_selected: " + totalpizzacrust_selected);
                    for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {

                        if (selectedCrustIds.equalsIgnoreCase(mPizzaCrustDatas_dialog.get(i).getId())) {

                            System.out.println("In Loop Pizza Crust Name:" + mPizzaCrustDatas_dialog.get(i).getPid());
//                            mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected = mOrderedItemEdit.getPizzacrust();
                            mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(true);

                            totalpizzacrust_selected = mPizzaCrustDatas_dialog.get(i).getPrice();

                        } else {
                            mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(false);
                        }
                    }
                    mTextViewTotalPizzaCrust_dialog.setText("" + totalpizzacrust_selected);
                    mPizzaCrustListAdapter_dialog.notifyDataSetChanged();
                }

                mDialogPizzaCrust.show();
                break;
            case R.id.frag_order_image_additioncheck:

                if (mAdditionsListDatas_dialog.size() == 0)
                    CallAdditionsServices();

                else if (mAdditionsListDatas_dialog.size() > 0) {
                    System.out.println("Additions selected are: " + selectedAdditionsIds);
                    float tot = 0;
                    for (int j = 0; j < mAdditionsListDatas_dialog.size(); j++) {

                        if (selectedAdditionsIds.contains(mAdditionsListDatas_dialog.get(j).getId())) {
                            System.out.println("Addition Name:" + mAdditionsListDatas_dialog.get(j).getName());
                            mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(true);
                            tot += mAdditionsListDatas_dialog.get(j).getPrice();

                        } else {
                            mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(false);
                        }
                    }
                    totaladditions_selected = tot;
                    mTextViewTotalAdditoin_dialog.setText("" + tot);
                    mAdditionsListAdapter_dialog.notifyDataSetChanged();

                }
                mDialogAdditions.show();


                break;

            case R.id.frag_order_image_toppings:

                Log.e("Remove Toppings Image.click()", "selectedToppingsIDs: " + selectedToppingsIDs);
                Log.e("Remove Toppings Image.click()", "totaltoppings_selected: " + totaltoppings_selected);
                Log.e("Remove Toppings Image.click()", "totaltoppings_removed: " + totaltoppings_removed);

                Log.i("Remove Toppings Image.click()", "Size of Toppings in Dialog: " + mToppingses_array_dialog.size());

                if (mToppingses_array_dialog.size() > 0 && selectedToppingsIDs.length() > 0) {

                    float tot = 0;
                    for (int i = 0; i < mToppingses_array_dialog.size(); i++) {

                        if (selectedToppingsIDs.contains(mToppingses_array_dialog.get(i).getId())) {

                            System.out.println("In Loop Pizza Topping Name:" + mToppingses_array_dialog.get(i).getName());
                            mToppingses_array_dialog.get(i).setIsToppingSelected(true);
                            tot += mToppingses_array_dialog.get(i).getPrice();
                        } else {
                            mToppingses_array_dialog.get(i).setIsToppingSelected(false);
                        }
                    }
                    totaltoppings_selected = tot;
                    mTextViewTotal_Topping_dialog.setText("" + tot);
                    mToppingAdapter_dialog.notifyDataSetChanged();
//                    mDialogRemoveToppings.show();
                }/* else {
                    Toast.makeText(getActivity(), "Toppings could not be loaded, please try again.", Toast.LENGTH_SHORT).show();
                }*/
                mDialogRemoveToppings.show();


                break;

            case R.id.frag_order_layout_regular:

                mImageViewRegular.setImageResource(R.drawable.icon_s_red_90);
//                mImageViewMedium.setImageResource(R.drawable.icon_m_gray_60);
                mImageViewLarge.setImageResource(R.drawable.icon_b_gray_90);

                basicpay = Float.parseFloat(mPizzaListDataReceived.getPrice_small());
                pizzasize = "Small";

                mTextViewBasicPrice.setText("" + basicpay);
                mLinearLayoutPizzaBase.setVisibility(View.GONE);

                DoTotal();
                break;

            /*case R.id.frag_order_layout_medium:

                mImageViewRegular.setImageResource(R.drawable.icon_s_gray_90);
                mImageViewMedium.setImageResource(R.drawable.icon_m_fill_green_60);
                mImageViewLarge.setImageResource(R.drawable.icon_b_gray_90);

                basicpay = Float.parseFloat(mPizzaListDataReceived.getPrice_medium());
                pizzasize = "medium";

                mTextViewBasicPrice.setText("" + basicpay);
                mLinearLayoutPizzaBase.setVisibility(View.VISIBLE);

                DoTotal();
                break;*/

            case R.id.frag_order_layout_large:

                mImageViewRegular.setImageResource(R.drawable.icon_s_gray_90);
//                mImageViewMedium.setImageResource(R.drawable.icon_m_gray_60);
                mImageViewLarge.setImageResource(R.drawable.icon_b_red_90);

                basicpay = Float.parseFloat(mPizzaListDataReceived.getPrice_large());
                pizzasize = "Big";

                mTextViewBasicPrice.setText("" + basicpay);
                mLinearLayoutPizzaBase.setVisibility(View.VISIBLE);

                DoTotal();
                break;

            case R.id.frag_order_image_price_increase:
                if (quantity < Tags.MAXIMUM_ORDER_QUANTITY) {
//                    quantity += 1;
                    quantity_position += 1;
                    quantity = quantities[quantity_position];
//                    mTextViewQuantity.setText("Quantity " + quantity);
//                    mSpinnerQuantity.setSelection((int) quantity - 1);
                    mSpinnerQuantity.setSelection(quantity_position);
                    DoTotal();
                }
                break;
            case R.id.frag_order_image_price_decrease:
                if (quantity > Tags.MINIMUM_ORDER_QUANTITY) {
//                    quantity -= 1;
                    quantity_position -= 1;
                    quantity = quantities[quantity_position];
//                    mTextViewQuantity.setText("Quantity " + quantity);
//                    mSpinnerQuantity.setSelection((int) quantity - 1);
                    mSpinnerQuantity.setSelection(quantity_position);
                    DoTotal();
                }

                break;
            case R.id.frag_order_buton_addtobasket:

                printAllDefaultValues();
//                if (isRemoveToppingsSelected && (isadditionsselected == false) && (count_toppings_selected == -1) || count_toppings_selected < 2) {
                if (isRemoveToppingsSelected && (!isadditionsselected)
                        && (count_toppings_selected < 2)
                        ) {

                    Log.e("AddtoBasket click", "Main Condition");
                    Toast.makeText(getActivity(), "Please Select at least 2 toppings for pizza.", Toast.LENGTH_SHORT).show();

                } else if (isRemoveToppingsSelected && isadditionsselected
                        && /*(*/    ((count_toppings_selected > 0 && (count_additions_selected + count_toppings_selected) < 2)
                        || (count_toppings_selected == -1 && (count_additions_selected) < 2))
                            /*|| (count_toppings_selected < 2) || (count_additions_selected < 2))*/
                        ) {




                    /*
                    else if (isRemoveToppingsSelected && isadditionsselected
                        && (   (  (count_additions_selected + count_toppings_selected) < 2)
                            || (count_toppings_selected < 2) || (count_additions_selected < 2))
                        )
                     */
                    Log.e("AddtoBasket click", "else if Condition");
                    Toast.makeText(getActivity(), "Please Select at least 2 toppings for pizza.", Toast.LENGTH_SHORT).show();

                    /*if (((count_additions_selected > 0 || count_additions_selected < 2) ||
                            (count_toppings_selected > 0 || count_toppings_selected < 2))
                            )*/
                    /*if ((count_toppings_selected + count_additions_selected) < 2) {
                        Log.e("AddtoBasket click", "else if Condition");
                        Toast.makeText(getActivity(), "Please Select at least 2 toppings for pizza.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "go ahed", Toast.LENGTH_SHORT).show();
                    }*/
                } else {


                    if (isEditOrderItem) {

                        System.out.println("OrderFrag().addtobasket click: Gone in Item Edit mode");
                        mNavigationActivity.onBackPressed();
                        mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                        mNavigationActivity.ShowHideIcons();
                        mNavigationActivity.showFragments();

                        OrderedItemsData mOrderedPizza = new OrderedItemsData();
                        DoTotal();
                        mOrderedPizza.setPizza_details(orderedItemsDataEdit.getPizza_details());

                    /*
                    new code for additions
                     */
                        if (isadditionsselected) {
                            Log.e("Addto basket button click", "Lets save the Extra Ingredients");
                            mArrayListAdditionsSelected = new ArrayList<>();
                            for (int i = 0; i < mAdditionsListDatas_dialog.size(); i++) {
                                if (selectedAdditionsIds.contains(mAdditionsListDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Addition Name: " + mAdditionsListDatas_dialog.get(i).getName());
                                    mArrayListAdditionsSelected.add(mAdditionsListDatas_dialog.get(i));
                                }
                            }
                            mOrderedPizza.setExtra_ingredients(mArrayListAdditionsSelected);
                            mArrayListAdditionsSelected = null;
                            for (int i = 0; i < mOrderedPizza.getExtra_ingredients().size(); i++) {
                                Log.e("For confirmation", "Name: " + mOrderedPizza.getExtra_ingredients().get(i).getName());
                            }
                        }

                    /*
                    new code for crust medium
                     */

                        /*if (ispizzacrustselected && pizzasize.equalsIgnoreCase("medium")) {
                            Log.e("Addto basket button click", "Lets save the Pizza Crust");
                            for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
                                if (selectedCrustIds.contains(mPizzaCrustDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Crust Name: " + mPizzaCrustDatas_dialog.get(i).getName());
                                    mOrderedPizza.setPizzacrust(mPizzaCrustDatas_dialog.get(i));
                                }
                            }
                        }*/
/*
                    new code for crust big
                     */
                        if (ispizzacrustselected && pizzasize.equalsIgnoreCase("big")) {
                            Log.e("Addto basket button click", "Lets save the Pizza Crust");
                            for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
                                if (selectedCrustIds.contains(mPizzaCrustDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Crust Name: " + mPizzaCrustDatas_dialog.get(i).getName());
                                    mOrderedPizza.setPizzacrust(mPizzaCrustDatas_dialog.get(i));
                                }
                            }
                        }


                        if (isRemoveToppingsSelected) {
                            Log.e("Addto basket button click", "Lets save the Toppings in Custom toppings field");
                            ArrayList<Toppings> mToppingsesSelected = new ArrayList<>();
                            for (int i = 0; i < mToppingses_array_dialog.size(); i++) {
                                if (selectedToppingsIDs.contains(mToppingses_array_dialog.get(i).getId())) {
                                    System.out.println("Selected Topping Name: " + mToppingses_array_dialog.get(i).getName());
                                    mToppingsesSelected.add(mToppingses_array_dialog.get(i));
                                }
                            }
//                            mOrderedPizza.setCustom_ingredients(mToppingsesSelected);
                            mOrderedPizza.setToppings(mToppingsesSelected);
                            mToppingsesSelected = null;
                        }


                        mOrderedPizza.setQty(quantity);
                        mOrderedPizza.setPizza_size(pizzasize);
                        mOrderedPizza.setTotal(Utils.getDoubleFormat(totalpay));
                        mOrderedPizza.setPizzaria(mPizzaRestroReceived);


                        mNavigationActivity.mArrayListOrderedItem.remove(position_selected_for_edit);
                        mNavigationActivity.mBasketFragment.AddItemtoListOrder(position_selected_for_edit, mOrderedPizza);
                        mNavigationActivity.mBasketFragment.CalculateTotal();
                        mNavigationActivity.mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
                        isEditOrderItem = false;
                    } else {
                        System.out.println("OrderFrag().addtobasket click: Gone in else of Item Edit mode");
                        OrderedItemsData mOrderedPizza = new OrderedItemsData();
                        DoTotal();
                        mOrderedPizza.setPizza_details(mPizzaListDataReceived);


                    /*
                    new code for additions
                     */
                        if (isadditionsselected) {
                            Log.e("Addto basket button click", "Lets Add the Extra Ingredients");
                            mArrayListAdditionsSelected = new ArrayList<>();
                            for (int i = 0; i < mAdditionsListDatas_dialog.size(); i++) {
                                if (selectedAdditionsIds.contains(mAdditionsListDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Addition Name: " + mAdditionsListDatas_dialog.get(i).getName());
                                    mArrayListAdditionsSelected.add(mAdditionsListDatas_dialog.get(i));
                                }
                            }
                            mOrderedPizza.setExtra_ingredients(mArrayListAdditionsSelected);
                            mArrayListAdditionsSelected = null;
                            for (int i = 0; i < mOrderedPizza.getExtra_ingredients().size(); i++) {
                                Log.e("For confirmation", "Name: " + mOrderedPizza.getExtra_ingredients().get(i).getName());
                            }
                        }


                    /*
                    new code for crust
                     */

                        /*if (ispizzacrustselected && pizzasize.equalsIgnoreCase("medium")) {
                            Log.e("Addto basket button click", "Lets Add the Pizza Crust");
                            for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
                                if (selectedCrustIds.contains(mPizzaCrustDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Crust Name: " + mPizzaCrustDatas_dialog.get(i).getName());
                                    mOrderedPizza.setPizzacrust(mPizzaCrustDatas_dialog.get(i));
                                }
                            }
                        }*/
                    /*
                    new code for crust big
                     */
                        if (ispizzacrustselected && pizzasize.equalsIgnoreCase("big")) {
                            Log.e("Addto basket button click", "Lets Add the Pizza Crust");
                            for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
                                if (selectedCrustIds.contains(mPizzaCrustDatas_dialog.get(i).getId())) {
                                    System.out.println("Selected Crust Name: " + mPizzaCrustDatas_dialog.get(i).getName());
                                    mOrderedPizza.setPizzacrust(mPizzaCrustDatas_dialog.get(i));
                                }
                            }
                        }

                        if (isRemoveToppingsSelected) {
                            Log.e("Addto basket button click", "Lets save the Toppings in Custom toppings field");
                            ArrayList<Toppings> mToppingsesSelected = new ArrayList<>();
                            for (int i = 0; i < mToppingses_array_dialog.size(); i++) {
                                if (selectedToppingsIDs.contains(mToppingses_array_dialog.get(i).getId())) {
                                    System.out.println("Selected Topping Name: " + mToppingses_array_dialog.get(i).getName());
                                    mToppingsesSelected.add(mToppingses_array_dialog.get(i));
                                }
                            }
//                            mOrderedPizza.setCustom_ingredients(mToppingsesSelected);
                            mOrderedPizza.setToppings(mToppingsesSelected);
                            mToppingsesSelected = null;

                            /*for (int i = 0; i < mOrderedPizza.getCustom_ingredients().size(); i++) {
                                Log.e("For confirmation", "Topping Name: " + mOrderedPizza.getCustom_ingredients().get(i).getName());
                            }*/
                            for (int i = 0; i < mOrderedPizza.getToppings().size(); i++) {
                                Log.e("For confirmation", "Topping Name: " + mOrderedPizza.getToppings().get(i).getName());
                            }
                        }


                        mOrderedPizza.setQty(quantity);
                        mOrderedPizza.setPizza_size(pizzasize);
//                        mOrderedPizza.setTotal(totalpay);
                        mOrderedPizza.setTotal(Utils.getDoubleFormat(totalpay));
                        mOrderedPizza.setPizzaria(mPizzaRestroReceived);

                        mOrderedPizza.setIsFromFavorite_History(false);

//                        initDialogFavorite(mOrderedPizza);
                        AddtoBasketList(mOrderedPizza);
                    }
                    mStore_pref.removeAdditionsIDS();
                    mStore_pref.removeToppingsIDs();
                }
                break;

		/*-------------------------------------------------------------
         * Click Events from Additoin Layout
         * -------------------------------------------------------------
		 */
            case R.id.dialog_additions_button_apply_addition:

                isApplayButtonClickAdditions = true;
                mDialogAdditions.dismiss();
                totaladditions_selected = GetAdditionSelectedTotal();
                break;

            /*
         * Click Events from Pizza Crust(Base) Layout/screen
		 */
            case R.id.dialog_pizzacrust_button_applycrust:

                isApplayButtonClickPizzaCrust = true;
                mDialogPizzaCrust.dismiss();
                GetPizzaCrustSelectedTotal();
                break;


        }

    }

    public void SetPizzaData(PizzaListData mData, PizzaRestro mPizzaRestro, int position) {

        item_position = position;
        mPizzaRestroReceived = mPizzaRestro;
        mPizzaListDataReceived = mData;
        isEditOrderItem = false;

        System.out.println("SetPizzaData()");
        System.out.println("Position received: " + position);
        System.out.println("OrderFragment: PizzaData: " + mData);
        SetDatatoControls(mPizzaListDataReceived);
        InitControlsVar();
//        mPizzaListDataReceived.printAllData();
    }

    private void InitControlsVar() {

        quantity = 0.25;
        quantity_position = 0;
        totalpay = 0;
        totaladditions_selected = 0;
        totalpizzacrust_selected = 0;
        totaltoppings_selected = 0;
        totaltoppings_removed = 0;
        count_toppings_selected = 0;
        count_additions_selected = 0;

        isadditionsselected = false;
        ispizzacrustselected = false;
        isRemoveToppingsSelected = false;

        isEditOrderItem = false;

        isApplayButtonClickPizzaCrust = false;
        isApplayButtonClickAdditions = false;
        isApplayButtonClickToppings = false;

        /*
        below code is to activate the click of controls (due to Orderstatus)
         */
        Constants.isOrderStatusShown = false;
        System.out.println("Gone in isOrderStatusShown = False");
        mButtonAddtoBasket.setVisibility(View.VISIBLE);
        mButtonApplyAdditions_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
        mButtonApplyCrust_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
        mButtonApplyTopping_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
        mLinearLayoutRegular.setEnabled(true);
        mLinearLayoutLarge.setEnabled(true);
        mImageViewIncrementQuantity.setEnabled(true);
        mImageViewDecrementQuantity.setEnabled(true);
        mSpinnerQuantity.setEnabled(true);
        /*
        ends
         */

//        pizzasize = "regular";

        selectedCrustIds = "";
        selectedAdditionsIds = "";

        /*
        this should not be initialized here because it is initialize in
        SetDatatoControls() method.

        selectedToppingsIDs = "";
         */

        if (mPizzaListDataReceived.getShow_options().equalsIgnoreCase("1")) {
            CallAdditionsServices();
            CallPizzaCrustServices();
            CallToppingsSelection();
        }

        mStore_pref.removeAdditionsIDS();
        selectedAdditionsIds = "";

//        SetAdditionItemsList();
//        SetPizzaCrustItemsList();


        mTextViewTotalOrder.setText("" + basicpay);

        mImageViewRegular.setImageResource(R.drawable.icon_s_red_90);
//        mImageViewMedium.setImageResource(R.drawable.icon_m_gray_60);
        mImageViewLarge.setImageResource(R.drawable.icon_b_gray_90);

        mLinearLayoutPizzaBase.setVisibility(View.GONE);

        mImageViewPizzabase.setBackgroundResource(R.drawable.unchecked);
        mTextViewPizzabaseSelected.setText(R.string.pizza_crust_name);
        mTextViewPizzaBasePrice.setText("0.0");
        mTextViewTotalPizzaCrust_dialog.setText("0.0");

        mImageViewAddition.setBackgroundResource(R.drawable.unchecked);
        mTextViewAdditionsSelected.setText(R.string.ingredients_name);
        mTextViewAdditionsTotalPrice.setText("0.0");
        mTextViewTotalAdditoin_dialog.setText("0.0");

        mImageViewToppings.setBackgroundResource(R.drawable.unchecked);
        mTextViewToppingsSelected.setText(R.string.f_order_tv_custompizza_heading);
//        mTextViewToppingsTotalPrice.setText("0.0");
        mTextViewTotal_Topping_dialog.setText("0.0");

//        mTextViewQuantity.setText("Quantity " + quantity);
        mSpinnerQuantity.setSelection(0);

    }

    private void SetDatatoControls(PizzaListData mData) {

        ImageLoaderBlur mImageLoaderBlur = new ImageLoaderBlur(getActivity());
        mImageLoaderBlur.DisplayImage(mData.getImg_url(), mImageViewPizzaimagebk);
        ImageLoader mImageLoader = new ImageLoader(getActivity());
        mImageLoader.DisplayImage(mData.getImg_url(), mImageViewPizzaImage);
        mTextViewPizzaName.setText(mData.getName());

        String toppings = "";
        selectedToppingsIDs = "";
        for (int i = 0; i < mData.getToppings().size(); i++) {
            toppings += mData.getToppings().get(i).getName() + ", ";
            selectedToppingsIDs += mData.getToppings().get(i).getId() + ",";
        }

        toppings = Utils.RemoveLastComma(toppings);
        selectedToppingsIDs = Utils.RemoveLastComma_ID(selectedToppingsIDs);
//        mStore_pref.setToppingsIDs(selectedToppingsIDs, "SetDatatoControls()");
        System.out.println("SetDatatoControls() Toppings selected :" + selectedToppingsIDs);

        mTextViewPizzaDescription.setText(toppings);

//        mTextViewPizzaDescription.setText(mData.getDescription());
        mTextViewBasicPrice.setText("" + mData.getPrice_small());
        mTextViewRegularPrice.setText("" + mData.getPrice_small());
//        mTextViewMediumPrice.setText("" + mData.getPrice_medium());
        mTextViewLargePrice.setText("" + mData.getPrice_large());
        basicpay = Float.parseFloat(mData.getPrice_small());

        if (isEditOrderItem)
            mButtonAddtoBasket.setText(R.string.f_order_button_save_item);
        else
            mButtonAddtoBasket.setText(R.string.f_order_button_addtobasket);

        if (mPizzaListDataReceived.getShow_options().equalsIgnoreCase("1")) {
            mRelativeLayoutPizzaSizes.setVisibility(View.VISIBLE);
            mLinearLayoutAdditions.setVisibility(View.VISIBLE);
            mLinearLayoutRemoveToppings.setVisibility(View.VISIBLE);
            pizzasize = "Small";
        } else {
            mRelativeLayoutPizzaSizes.setVisibility(View.GONE);
            mLinearLayoutAdditions.setVisibility(View.GONE);
            mLinearLayoutRemoveToppings.setVisibility(View.GONE);
            mLinearLayoutPizzaBase.setVisibility(View.GONE);
            pizzasize = "";
        }

        /*
            below code is added here to fill the Quantity Arraylist as selected menu.
         */
        if (mPizzaListDataReceived.getShow_options().equalsIgnoreCase("1")) {
            mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, getActivity().getResources().getStringArray(R.array.quantity_array)));
             quantities = new double[]{
                0.25, 0.33, 0.5, 1, 1.25, 1.33, 1.5, 2, 2.25, 2.33, 2.5, 3, 3.25, 3.33, 3.5, 4, 4.25, 4.33, 4.5, 5, 5.25, 5.33, 5.5,
                        6, 6.25, 6.33, 6.5, 7, 7.25, 7.33, 7.5, 8, 8.25, 8.33, 8.5, 9, 9.25, 9.33, 9.5, 10, 10.25, 10.33, 10.5
            } ;
        } else {
            mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, getActivity().getResources().getStringArray(R.array.quantity_beverages_array)));
            quantities = new double[]{1,2,3,4,5,6,7,8,9,10} ;
        }

    }

    void printAllDefaultValues() {

        System.out.println("-----------Totals-----------");
        System.out.println("Quantity: " + quantity);
        System.out.println("totalpay: " + totalpay);
        System.out.println("totaladditions_selected: " + totaladditions_selected);
        System.out.println("totalpizzacrust_selected: " + totalpizzacrust_selected);
        System.out.println("totaltoppings_selected: " + totaltoppings_selected);
        System.out.println("totaltoppings_removed: " + totaltoppings_removed);
        System.out.println("count_toppings_selected: " + count_toppings_selected);
        System.out.println("count_additions_selected: " + count_additions_selected);
        System.out.println("-----------Booleans-----------");
        System.out.println("isadditionsselected: " + isadditionsselected);
        System.out.println("ispizzacrustselected: " + ispizzacrustselected);
        System.out.println("isRemoveToppingsSelected: " + isRemoveToppingsSelected);
        System.out.println("isEditOrderItem: " + isEditOrderItem);
        System.out.println("isApplayButtonClickPizzaCrust: " + isApplayButtonClickPizzaCrust);
        System.out.println("isApplayButtonClickAdditions: " + isApplayButtonClickAdditions);
        System.out.println("isApplayButtonClickToppings: " + isApplayButtonClickToppings);
        System.out.println("-----------Others-----------");
        System.out.println("pizzasize: " + pizzasize);
        System.out.println("selectedCrustIds: " + selectedCrustIds);
        System.out.println("selectedAdditionsIds: " + selectedAdditionsIds);
        System.out.println("selectedToppingsIDs: " + selectedToppingsIDs);
    }


    /*
    ------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------
                                        Main Methods are stated
    ------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------
     */
    private float GetAdditionSelectedTotal() {

        /*
        new code for additions
         */
        float totaladdition = 0;
        String additions_name = "";
        selectedAdditionsIds = "";
        count_additions_selected = 0;

        Log.e("GetAdditionSelectedTotal() -->", "AdditionsTotal: " + totaladdition);
        Log.e("GetAdditionSelectedTotal()", "Total of Additions selected from dialog: " + mAdditionsListAdapter_dialog.TotalofSelectedAdditions);


//        if ((isApplayButtonClick && mAdditionsListAdapter_dialog.TotalofSelectedAdditions > 0)) {
        if (isApplayButtonClickAdditions) {

            System.out.println("((isApplayButtonClick && mAdditionsListAdapter_dialog.TotalofSelectedAdditions > 0))");
            Log.e("GetAdditionSelectedTotal().getAdditionsIDs -->", "Additions are not there in shared pref.");
            for (int i = 0; i < mAdditionsListDatas_dialog.size(); i++) {
                if (mAdditionsListDatas_dialog.get(i).isAdditionSelected()) {

                    System.out.println("Item Check is: " + mAdditionsListDatas_dialog.get(i).getName());
                    selectedAdditionsIds += mAdditionsListDatas_dialog.get(i).getId() + ",";
                    additions_name += mAdditionsListDatas_dialog.get(i).getName() + ", ";
                    totaladdition += mAdditionsListDatas_dialog.get(i).getPrice();
                    count_additions_selected++;
                }
            }
            if (totaladdition > 0) {
                selectedAdditionsIds = Utils.RemoveLastComma_ID(selectedAdditionsIds);
                additions_name = Utils.RemoveLastComma(additions_name);
                mStore_pref.setAdditionsID(selectedAdditionsIds, "GetAdditionSelectedTotal()");
                mTextViewAdditionsSelected.setText(additions_name);
                mTextViewAdditionsTotalPrice.setText("" + totaladdition);

                mAdditionsListAdapter_dialog.TotalofSelectedAdditions = totaladdition;
                mTextViewTotalAdditoin_dialog.setText("" + totaladdition);

                Log.e("GetAdditionSelectedTotal() -->", "AdditionsTotal: " + totaladdition);
                mImageViewAddition.setBackgroundResource(R.drawable.checked);

                isadditionsselected = true;
            } else {

                System.out.println("Else part of GetAdditionSelectedTotal()");
                mTextViewAdditionsSelected.setText(R.string.select_ingredients);
                mTextViewAdditionsTotalPrice.setText("0.0");
                mImageViewAddition.setBackgroundResource(R.drawable.unchecked);
                isadditionsselected = false;
                mStore_pref.removeAdditionsIDS();
            }
            isApplayButtonClickAdditions = false;

        } else {
            System.out.println("Else part of GetAdditionSelectedTotal()");
            mTextViewAdditionsSelected.setText(R.string.select_ingredients);
            mTextViewAdditionsTotalPrice.setText("0.0");
            mImageViewAddition.setBackgroundResource(R.drawable.unchecked);
            isadditionsselected = false;
            mStore_pref.removeAdditionsIDS();
        }
        totaladditions_selected = totaladdition;
        DoTotal();

        Log.e("GetAdditionSelectedTotal()", "Total of Additions selected from dialog: " + mAdditionsListAdapter_dialog.TotalofSelectedAdditions);
        return (totaladdition);

    }

    private float GetPizzaCrustSelectedTotal() {

        /*
        new code for curst
         */
        float totalcrust = 0;
        String crust_name = "";
        selectedCrustIds = "";

        Log.e("GetPizzaCrustSelectedTotal()-->", "Crust Total: " + totalcrust);
//        Log.e("GetPizzaCrustSelectedTotal()-->", "Selected Crust Name: " + mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName());

        /*for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
            mPizzaCrustDatas_dialog.get(i).printAllData();
        }*/

        if (isApplayButtonClickPizzaCrust) {
            System.out.println("((isApplayButtonClickPizzaCrust && mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected))");
            Log.e("GetPizzaCrustSelectedTotal().getcrustID -->", "Crust is not there in shared pref.");

            for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {
                if (mPizzaCrustDatas_dialog.get(i).isPizzaCrustSelected()) {

                    System.out.println("Selected Crust name: " + mPizzaCrustDatas_dialog.get(i).getName());
                    System.out.println("Selected Crust Price: " + mPizzaCrustDatas_dialog.get(i).getPrice());

                    selectedCrustIds = mPizzaCrustDatas_dialog.get(i).getId();
                    crust_name = mPizzaCrustDatas_dialog.get(i).getName();
                    totalcrust = mPizzaCrustDatas_dialog.get(i).getPrice();
                }
            }
            if (totalcrust > 0 || (crust_name.length() > 0 && selectedCrustIds.length() > 0)) {

//                totalcrust = mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getPrice();
//                System.out.println("in OrderFragment: Total Crust Selected: " + totalcrust);

//                System.out.println("Name: " + mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName());
//                System.out.println("Total of crust: " + totalcrust);

                mTextViewPizzabaseSelected.setText(crust_name);
                mTextViewTotalPizzaCrust_dialog.setText("" + totalcrust);
                mTextViewPizzaBasePrice.setText("" + totalcrust);
                mImageViewPizzabase.setBackgroundResource(R.drawable.checked);
                Log.e("GetPizzaCrustSelectedTotal() -->", "Crust Total: " + totalcrust);

                ispizzacrustselected = true;
                isApplayButtonClickPizzaCrust = false;

            } else {
                System.out.println("Else part of GetPizzaCrustSelectedTotal()");
                mTextViewPizzabaseSelected.setText(R.string.select_pizza_crust);
                mTextViewTotalPizzaCrust_dialog.setText("0.0");
                mImageViewPizzabase.setBackgroundResource(R.drawable.unchecked);
                ispizzacrustselected = false;
                selectedCrustIds = "";
            }

        }


       /* if (mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected != null) {
            System.out.println("Selected Crust name: " + mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName()
                    + "  Price: " + mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getPrice());
            if (mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getPrice() > 0
                    || mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName().length() > 0) {
                totalcrust = mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getPrice();
                System.out.println("in OrderFragment: Total Crust Selected: " + totalcrust);

                System.out.println("Name: " + mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName());
                System.out.println("Total of crust: " + totalcrust);

                mTextViewPizzabaseSelected.setText(mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected.getName());
                mTextViewTotalPizzaCrust_dialog.setText("" + totalcrust);
                mTextViewPizzaBasePrice.setText("" + totalcrust);
                mImageViewPizzabase.setBackgroundResource(R.drawable.checked);
                Log.e("GetPizzaCrustSelectedTotal() -->", "Crust Total: " + totalcrust);

                ispizzacrustselected = true;

            }
        }//main if ends
        else {

            mTextViewTotalPizzaCrust_dialog.setText("0.0");
            mImageViewPizzabase.setBackgroundResource(R.drawable.unchecked);
            isadditionsselected = false;
        }*/

        totalpizzacrust_selected = totalcrust;
        DoTotal();
        return (totalcrust);

    }

    void printAllPrices() {
        System.out.println("printAllPrices() called");
        System.out.println("Basic Pay: " + basicpay);
        System.out.println("Addition pay: " + totaladditions_selected);
        System.out.println("PizzaCrust pay: " + totalpizzacrust_selected);
        System.out.println("Total Selected Toppings: " + totaltoppings_selected);
        System.out.println("Total Removed Toppings: " + totaltoppings_removed);
        System.out.println("Quantity: " + quantity);
        System.out.println("Total: " + totalpay);
        System.out.println("----------------------------");
        System.out.println("     TotalPay: " + totalpay);
        System.out.println("----------------------------");
    }

    private void DoTotal() {

        double totalordertopay = 0;
        totalpay = 0;

        printAllPrices();

        if (isadditionsselected && !isRemoveToppingsSelected) {
            Log.e("DoTotal() main if ", "Additions is selected but Remove Toppings is not selected.");
            System.out.println("Addition Selected(" + pizzasize + ")");
            System.out.println("Addition added :" + totaladditions_selected);
            totalordertopay += totaladditions_selected + basicpay;
            System.out.println("----------------------------");
            System.out.println("totalordertopay = totaladditions_selected + basicpay");
            System.out.println("totalordertopay = " + totaladditions_selected + " + " + basicpay);
            System.out.println("     Totalordertopay: " + totalordertopay);
            System.out.println("----------------------------");

        } else if (isRemoveToppingsSelected && !isadditionsselected) {
            Log.e("DoTotal() main if ", "Remove Toppings is selected but Additions is not");
            totalordertopay = basicpay;

        } else if (isRemoveToppingsSelected && isadditionsselected) {
            Log.e("DoTotal() main if ", "Remove Toppings & Additions both are selected");
            System.out.println("Remove Topping option Selected(" + pizzasize + ")");
            System.out.println("Toppings Total (Selected) added :" + totaltoppings_selected);
            System.out.println("Toppings Total (Removed) added :" + totaltoppings_removed);
            totalordertopay = (totaladditions_selected + basicpay) - totaltoppings_removed;
            System.out.println("----------------------------");
            System.out.println("totalordertopay = (totaladditions_selected + basicpay) - totaltoppings_removed");
            System.out.println("totalordertopay = (" + totaladditions_selected + " + " + basicpay + ") - " + totaltoppings_removed);
            System.out.println("     Totalordertopay: " + totalordertopay);
            System.out.println("----------------------------");
        } else {
            Log.e("DoTotal() main if ", "Both are not selected");
            totalordertopay = basicpay;
        }

        if (totalordertopay < basicpay) {
            Log.e("DoTotal() if(totalordertopay <basicpay)", "totalordertopay < basicpay");
            totalordertopay = basicpay;
        } else {
            Log.e("DoTotal() else part", "totalordertopay < basicpay");
            System.out.println("so totalordertopay is as it is.");
        }


        if (ispizzacrustselected && pizzasize.equalsIgnoreCase("big")) {

            System.out.println("Pizza Crust selected(" + pizzasize + ")");
            System.out.println("----------------------------");
            System.out.println("totalordertopay = (totalordertopay) + totalpizzacrust_selected");
            System.out.println("totalordertopay = (" + totalordertopay + ") + " + totalpizzacrust_selected);
            totalordertopay += totalpizzacrust_selected;
            System.out.println("     Totalordertopay: " + totalordertopay);
            System.out.println("----------------------------");
        }

//        totalordertopay += basicpay;
        totalordertopay = totalordertopay * quantity;
        totalpay = Utils.getDoubleFormat(totalordertopay);
        mTextViewTotalOrder.setText("" + totalpay);

        System.out.println("----------------------------");
        System.out.println("totalordertopay = (totalordertopay) * quantity");
        System.out.println("totalordertopay = (" + totalordertopay + ") * " + quantity);
        System.out.println("     TotalPay: " + totalpay);
        System.out.println("----------------------------");

    }

    public void EditOrderedItem(OrderedItemsData mOrderedItemEdit, int itempos) {

        Log.e("OrderFrag.java: EditOrderedItem()", "-----------------Edit Favorite Pizza----------------");
        mOrderedItemEdit.printAllData();
        mPizzaListDataReceived = mOrderedItemEdit.getPizza_details();
        mPizzaRestroReceived = mOrderedItemEdit.getPizzaria();
        orderedItemsDataEdit = mOrderedItemEdit;

        isEditOrderItem = true;

        position_selected_for_edit = itempos;

        System.out.println("OrderFrag().EditOrderedItem() Position of Item to edit: " + position_selected_for_edit);

        totaladditions_selected = 0;
        totalpizzacrust_selected = 0;
        totaltoppings_selected = 0;
        totaltoppings_removed = 0;

        count_toppings_selected = 0;
        count_additions_selected = 0;

        if (mOrderedItemEdit.getPizzacrust() != null) {
            Log.e("EditOrderedItem().PizzaCrust", "PizzaCrust is available with pizza.");
            ispizzacrustselected = true;
        } else {
            ispizzacrustselected = false;
        }


        if (mOrderedItemEdit.getExtra_ingredients() != null && mOrderedItemEdit.getExtra_ingredients().size() > 0) {
            isadditionsselected = true;
            Log.e("EditOrderedItem().Extra Ingredients", "Extra Ingredients is available with pizza.");
        } else {
            isadditionsselected = false;
        }

        SetDatatoControls(mOrderedItemEdit.getPizza_details());

        if (mOrderedItemEdit.getToppings() != null && mOrderedItemEdit.getToppings().size() > 0) {
            isRemoveToppingsSelected = true;
            Log.e("EditOrderedItem().Custom Toppings", "Remove Toppings (Custom Toppings) is available with pizza.");
        } else {
            isRemoveToppingsSelected = false;
        }

        quantity = mOrderedItemEdit.getQty();
//        quantity_array = getActivity().getResources().getStringArray(R.array.quantity_array);
//        mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, quantity_array));

//        mSpinnerQuantity.setAdapter(null);
//        mSpinnerQuantity.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.textview_only, getActivity().getResources().getStringArray(R.array.quantity_array)));

        Log.e("EditOrderedItem().Quantity Selected", "Size of mSpinnerQuantity:" + mSpinnerQuantity.getCount());
        Log.e("EditOrderedItem().Quantity Selected", "Quantity:" + mOrderedItemEdit.getQty());
        for (int i = 0; i < quantities.length; i++) {
            double val = quantities[i];
            if (quantity == val) {
                Log.e("EditOrderedItem().Quantity Selected", "Quantity Matched:" + val);
                Log.e("EditOrderedItem().Quantity Selected", "Position:" + i);
                quantity_position = i;

                break;
            }
        }
        Log.e("EditOrderedItem().Quantity Selected", "quantity_position:" + quantity_position);
        mSpinnerQuantity.setSelection(quantity_position);
        totalpay = mOrderedItemEdit.getTotal();

        Log.e("EditOrderedItem()", "Pizza Size: " + mOrderedItemEdit.getPizza_size());
        if (mOrderedItemEdit.getPizza_size().equalsIgnoreCase("small")) {

            mTextViewBasicPrice.setText("" + mOrderedItemEdit.getPizza_details().getPrice_small());
            basicpay = Float.parseFloat(mOrderedItemEdit.getPizza_details().getPrice_small());
            mImageViewRegular.setImageResource(R.drawable.icon_s_red_90);
//            mImageViewMedium.setImageResource(R.drawable.icon_m_gray_60);
            mImageViewLarge.setImageResource(R.drawable.icon_b_gray_90);

            pizzasize = "Small";
            mLinearLayoutPizzaBase.setVisibility(View.GONE);

        } /*else if (mOrderedItemEdit.getPizza_size().equalsIgnoreCase("medium")) {

            mTextViewBasicPrice.setText("" + mOrderedItemEdit.getPizza_details().getPrice_medium());
            basicpay = Float.parseFloat(mOrderedItemEdit.getPizza_details().getPrice_medium());
            mImageViewMedium.setImageResource(R.drawable.icon_m_fill_green_60);
            mImageViewRegular.setImageResource(R.drawable.icon_s_gray_90);
            mImageViewLarge.setImageResource(R.drawable.icon_b_gray_90);
            pizzasize = "medium";

            mLinearLayoutPizzaBase.setVisibility(View.VISIBLE);

        }*/ else if (mOrderedItemEdit.getPizza_size().equalsIgnoreCase("big")) {

            mTextViewBasicPrice.setText("" + mOrderedItemEdit.getPizza_details().getPrice_large());
            basicpay = Float.parseFloat(mOrderedItemEdit.getPizza_details().getPrice_large());
            mImageViewLarge.setImageResource(R.drawable.icon_b_red_90);
            mImageViewRegular.setImageResource(R.drawable.icon_s_gray_90);
//            mImageViewMedium.setImageResource(R.drawable.icon_m_gray_60);

            pizzasize = "Big";
            mLinearLayoutPizzaBase.setVisibility(View.VISIBLE);

        }
        /*
        for additions
         */

        /*
        new code for additions
         */
        if (mOrderedItemEdit.getExtra_ingredients() != null) {
            CallAdditionsServices();
            //condition ends
        } else

        {
            Log.e("EditOrderedItem().mOrderedItemEdit.getExtra_ingredients() != null) else part: ", "Additions are not selected");
            mTextViewAdditionsSelected.setText(R.string.select_ingredients);
            mTextViewAdditionsTotalPrice.setText("0.0");
            mImageViewAddition.setBackgroundResource(R.drawable.unchecked);
            isadditionsselected = false;
            selectedAdditionsIds = "";
            mStore_pref.removeAdditionsIDS();
        }

        /*
        new code for crust
        */

        if (mOrderedItemEdit.getPizzacrust() != null) {
            CallPizzaCrustServices();
        } else {
            Log.e("EditOrderedItem().(mOrderedItemEdit.getPizzacrust() != null) else part: ", "Crust is not selected");
            mTextViewPizzabaseSelected.setText(R.string.select_pizza_crust);
            mTextViewPizzaBasePrice.setText("0.0");
            mImageViewPizzaImage.setBackgroundResource(R.drawable.unchecked);
            ispizzacrustselected = false;
            selectedCrustIds = "";
        }

        CallToppingsSelection();
        if (mOrderedItemEdit.getToppings() == null) {

            System.out.println("Else there is not custome topping to change");
            mTextViewToppingsSelected.setText(R.string.f_order_tv_custompizza_heading);
//                mTextViewToppingsTotalPrice.setText("0.0");
            mImageViewToppings.setBackgroundResource(R.drawable.unchecked);
            isRemoveToppingsSelected = false;
            mStore_pref.removeToppingsIDs();
            selectedToppingsIDs = "";
        }

//         mTextViewQuantity.setText("Quantity " + quantity);
//        mSpinnerQuantity.setSelection((int) quantity - 1);
        mTextViewTotalOrder.setText("" + mOrderedItemEdit.getTotal());
        DoTotal();

        Log.e("EditOrderedItem().OrderFrag.java", "This method called from OrderStatusFragment.java");
        System.out.println("isOrderStatusShown: " + Constants.isOrderStatusShown);
        if (Constants.isOrderStatusShown) {
            System.out.println("Gone in isOrderStatusShown= true");
            mButtonAddtoBasket.setVisibility(View.GONE);
            mButtonApplyAdditions_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_text_dark));
            mButtonApplyCrust_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_text_dark));
            mButtonApplyTopping_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_text_dark));
            mLinearLayoutRegular.setEnabled(false);
            mLinearLayoutLarge.setEnabled(false);
            mImageViewIncrementQuantity.setEnabled(false);
            mImageViewDecrementQuantity.setEnabled(false);
            mSpinnerQuantity.setEnabled(false);
        } else {
            System.out.println("Gone in isOrderStatusShown = False");
            mButtonAddtoBasket.setVisibility(View.VISIBLE);
            mButtonApplyAdditions_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
            mButtonApplyCrust_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
            mButtonApplyTopping_dialog.setBackgroundColor(getActivity().getResources().getColor(R.color.t_light_red));
            mLinearLayoutRegular.setEnabled(true);
            mLinearLayoutLarge.setEnabled(true);
            mImageViewIncrementQuantity.setEnabled(true);
            mImageViewDecrementQuantity.setEnabled(true);
            mSpinnerQuantity.setEnabled(true);
        }

    }

    /*
        -----------------------------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------------------------
                            Additions and Pizza Crust Services
        -----------------------------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------------------------
     */


    public void initDialogAddition() {

        mDialogAdditions = new Dialog(getActivity());
//        mDialogAdditions.getWindow().setBackgroundDrawableResource(R.color.t_transperent);
        mDialogAdditions.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogAdditions.setContentView(mViewDialogAdditions);

        mImageViewRefreshAdditions = (ImageView) mDialogAdditions.findViewById(R.id.dialog_additions_refresh_additions);
//        mLinearLayoutAdditionScreen = (LinearLayout) mDialogAdditions.findViewById(R.id.frag_order_layout_addtions);
        mListViewAdditionListItems_dialog = (ListView) mDialogAdditions.findViewById(R.id.dialog_additions_list_itemslist);
        mTextViewTotalAdditoin_dialog = (TextView) mDialogAdditions.findViewById(R.id.dialog_additions_text_totaladdtionprice);
        mButtonApplyAdditions_dialog = (Button) mDialogAdditions.findViewById(R.id.dialog_additions_button_apply_addition);

        mButtonApplyAdditions_dialog.setOnClickListener(this);

        mImageViewRefreshAdditions.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallAdditionsServices();
                System.out.println("Additions selected are: " + selectedAdditionsIds);
                float tot = 0;
                for (int j = 0; j < mAdditionsListDatas_dialog.size(); j++) {

//                    if (mStore_pref.getAdditionsIDs().contains(mAdditionsListDatas_dialog.get(j).getId())) {
                    if (selectedAdditionsIds.contains(mAdditionsListDatas_dialog.get(j).getId())) {
                        System.out.println("Addition Name:" + mAdditionsListDatas_dialog.get(j).getName());
                        mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(true);
//                        selectedAdditionsIds = orderedItemsDataEdit.getExtra_ingredients().get(j).getId() + ",";
                        tot += mAdditionsListDatas_dialog.get(j).getPrice();

                    } else {
                        mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(false);
                    }
                }
                mTextViewTotalAdditoin_dialog.setText("" + tot);
                mAdditionsListAdapter_dialog.notifyDataSetChanged();

            }
        });

        mAdditionsListDatas_dialog = new ArrayList<>();
        mAdditionsListAdapter_dialog = new AdditionsListAdapter(getActivity(), mAdditionsListDatas_dialog);
        mListViewAdditionListItems_dialog.setAdapter(mAdditionsListAdapter_dialog);


    }

    public void CallAdditionsServices() {
        mProgressDialog.setMessage("Please wait");
        mProgressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url = "";

        url = Constants.GET_ADDITIONS + "&pid=" + mPizzaRestroReceived.getId();
        Log.e("CallAdditionsServices().OrderFragment: ", "Pid: " + mPizzaRestroReceived.getId());

        StringRequest req_additions = new StringRequest(Request.Method.GET, url,
                createAdditionsSuccess(),
                createAdditionsError());

        req_additions.setRetryPolicy(new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req_additions);
    }

    private Response.Listener<String> createAdditionsSuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Additions Response", "" + response);
                GetAdditionsAndCrust mGetAdditionsAndCrust = mGson.fromJson(response, GetAdditionsAndCrust.class);
                mProgressDialog.dismiss();
                if (mGetAdditionsAndCrust.getResponseCode() == 1) {
                    mAdditionsListDatas_dialog = mGetAdditionsAndCrust.getExtra_ingredients();
                    mAdditionsListAdapter_dialog = new AdditionsListAdapter(getActivity(), mAdditionsListDatas_dialog);
                    mListViewAdditionListItems_dialog.setAdapter(mAdditionsListAdapter_dialog);


                    /*
                    29-9-2015
                    new code for showing the items selected
                    this code is pasted from the EditOrderedItems() method
                    but here
                    if (isEditOrderItem){

                    }
                    above condition was added only. other code is same.
                     */

                    if (isEditOrderItem && isadditionsselected) {
                        Log.e("EditOrderedItem().mOrderedItemEdit.getExtra_ingredients() != null): ", "Additions will be stored in shared pref and loaded on controls");

                        selectedAdditionsIds = "";
                        String additions_name = "";

                        for (int j = 0; j < mAdditionsListDatas_dialog.size(); j++) {

                            for (int i = 0; i < orderedItemsDataEdit.getExtra_ingredients().size(); i++) {

                                if (mAdditionsListDatas_dialog.get(j).getName().
                                        equalsIgnoreCase(orderedItemsDataEdit.getExtra_ingredients().get(i).getName())) {

                                    System.out.println("In Loop Addition Name:" + orderedItemsDataEdit.getExtra_ingredients().get(i).getName());
                                    mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(true);
                                    selectedAdditionsIds += orderedItemsDataEdit.getExtra_ingredients().get(i).getId() + ",";
                                    additions_name += orderedItemsDataEdit.getExtra_ingredients().get(i).getName() + ", ";
                                    totaladditions_selected += orderedItemsDataEdit.getExtra_ingredients().get(i).getPrice();

                                    count_additions_selected++;

                                } else {
                                    mAdditionsListDatas_dialog.get(j).setIsAdditionSelected(false);
                                }

                            }

                        }
                        selectedAdditionsIds = Utils.RemoveLastComma_ID(selectedAdditionsIds);
                        additions_name = Utils.RemoveLastComma(additions_name);
                        mStore_pref.setAdditionsID(selectedAdditionsIds, "EditOrderedItem(). not from fav/history");

                        mTextViewAdditionsSelected.setText(additions_name);
                        mTextViewAdditionsTotalPrice.setText("" + totaladditions_selected);
                        mImageViewAddition.setBackgroundResource(R.drawable.checked);
                        mAdditionsListAdapter_dialog.TotalofSelectedAdditions = totaladditions_selected;
                        mTextViewTotalAdditoin_dialog.setText("" + totaladditions_selected);

                        mAdditionsListAdapter_dialog.notifyDataSetChanged();
//                mArrayListAdditionsSelected = mData.getExtra_ingredients();

                        isadditionsselected = true;
                        DoTotal();

                    }


                } else {
                    Toast.makeText(getActivity(), "Additions Sucess Response: " + mGetAdditionsAndCrust.getResponseMsg(), Toast.LENGTH_SHORT).show();
                }

            }
        };


    }

    private Response.ErrorListener createAdditionsError() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                Toast.makeText(getActivity(), "Additions Error Response: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    public void initDialogPizzaCrust() {

        mDialogPizzaCrust = new Dialog(getActivity());
//        mDialogPizzaCrust.getWindow().setBackgroundDrawableResource(R.color.t_transperent);
        mDialogPizzaCrust.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogPizzaCrust.setContentView(mViewDialogPizzaCrust);

        mImageViewRefreshPizzaCrust = (ImageView) mDialogPizzaCrust.findViewById(R.id.dialog_pizzacrust_image_refresh_pizzacrust);
//        mLinearLayoutAdditionScreen = (LinearLayout) mDialogPizzaCrust.findViewById(R.id.frag_order_layout_addtions);
        mListViewPizzaCrustListItems_dialog = (ListView) mDialogPizzaCrust.findViewById(R.id.dialog_pizzacrust_list_itemslist);
        mTextViewTotalPizzaCrust_dialog = (TextView) mDialogPizzaCrust.findViewById(R.id.dialog_pizzacrust_text_totalpizzacrustrice);
        mButtonApplyCrust_dialog = (Button) mDialogPizzaCrust.findViewById(R.id.dialog_pizzacrust_button_applycrust);

        mButtonApplyCrust_dialog.setOnClickListener(this);

        mImageViewRefreshPizzaCrust.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CallPizzaCrustServices();
            }
        });

        mPizzaCrustDatas_dialog = new ArrayList<>();
        mPizzaCrustListAdapter_dialog = new PizzaCrustListAdapter(getActivity(), mPizzaCrustDatas_dialog);
        mListViewPizzaCrustListItems_dialog.setAdapter(mPizzaCrustListAdapter_dialog);
    }

    public void CallPizzaCrustServices() {
        mProgressDialog.setMessage("Please wait");
        mProgressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        String url = "";
        /*
        if condition is added when we edit ordered item in basket
        screen which is added from the favorites/history.
         */
//        if (isEditOrderItem) {
//            url = Constants.GET_PIZZA_CRUST + "&pid=" + orderedItemsDataEdit.getId();
//            Log.e("CallAdditionsServices().OrderFragment: ", "Pid: " + orderedItemsDataEdit.getId());
//        } else {
        url = Constants.GET_PIZZA_CRUST + "&pid=" + mPizzaRestroReceived.getId();
        Log.e("CallAdditionsServices().OrderFragment: ", "Pid: " + mPizzaRestroReceived.getId());
//        }

        StringRequest req_pizzacrust = new StringRequest(Request.Method.GET, url,
                createPizzaCrustSuccess(),
                createPizzaCrustError());
        req_pizzacrust.setRetryPolicy(new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req_pizzacrust);

    }

    private Response.Listener<String> createPizzaCrustSuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("PizzaCrust Response", "" + response);
                GetAdditionsAndCrust mGetAdditionsAndCrust = mGson.fromJson(response, GetAdditionsAndCrust.class);
                mProgressDialog.dismiss();
                if (mGetAdditionsAndCrust.getResponseCode() == 1) {
                    mPizzaCrustDatas_dialog = mGetAdditionsAndCrust.getPizzacrust();
                    mPizzaCrustListAdapter_dialog = new PizzaCrustListAdapter(getActivity(), mPizzaCrustDatas_dialog);
                    mListViewPizzaCrustListItems_dialog.setAdapter(mPizzaCrustListAdapter_dialog);
                    mAdditionsListAdapter_dialog.notifyDataSetChanged();

                    /*
                    new code for showing the items selected
                    this code is pasted from the EditOrderedItems() method
                    but here
                    if (isEditOrderItem){

                    }
                    above condition was added only. other code is same.
                     */
                    if (isEditOrderItem && ispizzacrustselected) {
                        selectedCrustIds = "";
                        for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {

                            if (mPizzaCrustDatas_dialog.get(i).getName().
                                    equalsIgnoreCase(orderedItemsDataEdit.getPizzacrust().getName())) {

                                System.out.println("In Loop Pizza Crust Name:" + orderedItemsDataEdit.getPizzacrust().getName());
                                mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected = orderedItemsDataEdit.getPizzacrust();
                                mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(true);

                                selectedCrustIds = orderedItemsDataEdit.getPizzacrust().getId();
                                totalpizzacrust_selected = orderedItemsDataEdit.getPizzacrust().getPrice();
                            } else {
                                mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(false);
                            }

                        }

                        Log.e("EditOrderedItem().PizzaCrust-->", "Crust ID:" + selectedCrustIds);
                        Log.e("EditOrderedItem().PizzaCrust-->", "Crust Name:" + orderedItemsDataEdit.getPizzacrust().getName());
                        Log.e("EditOrderedItem().PizzaCrust-->", "Crust Price:" + totalpizzacrust_selected);

                        mTextViewTotalPizzaCrust_dialog.setText("" + totalpizzacrust_selected);

                        mTextViewPizzabaseSelected.setText(orderedItemsDataEdit.getPizzacrust().getName());
                        mTextViewPizzaBasePrice.setText("" + orderedItemsDataEdit.getPizzacrust().getPrice());
                        mImageViewPizzabase.setBackgroundResource(R.drawable.checked);


                        ispizzacrustselected = true;
                        mPizzaCrustListAdapter_dialog.notifyDataSetChanged();
                        DoTotal();
                    }

                } else {
                    Toast.makeText(getActivity(), "PizzaCrust Sucess Response: " + mGetAdditionsAndCrust.getResponseMsg(), Toast.LENGTH_SHORT).show();
                }

            }
        };
    }

    private Response.ErrorListener createPizzaCrustError() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                Toast.makeText(getActivity(), "PizzaCrust Error Response: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }


        };
    }


    /*
        -----------------------------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------------------------
                            init Add to  Favorite Service
        -----------------------------------------------------------------------------------------------------
        -----------------------------------------------------------------------------------------------------
     */

    void initDialogFavorite(final OrderedItemsData mOrderedPizzaFavorite) {

        mDialogFavorite = new Dialog(getActivity());
        mDialogFavorite.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavorite.setContentView(R.layout.dialog_list_pizzas);

        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.address_list_pizzas_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.address_list_pizzas_image_dialog_title);
        mImageViewClose = (ImageView) mDialogFavorite.findViewById(R.id.address_list_pizzas_image_dialog_close);

//        mLinearLayoutFavorite_dialog = (LinearLayout) mDialogFavorite.findViewById(R.id.dialog_list_pizzas_address_layout_selectedaddress);
//        mTextViewTagNameFavorite_dialog = (TextView) mDialogFavorite.findViewById(R.id.address_list_pizzas_text_address_tagname);
//        mTextViewFullAddressFavorite_dialog = (TextView) mDialogFavorite.findViewById(R.id.address_list_pizzas_text_address_full_address);

        mListViewFavorite_dialog = (ListView) mDialogFavorite.findViewById(R.id.address_list_pizzas_list_pizzas);
        mRelativeLayoutAddtoFavorite_dialog = (LinearLayout) mDialogFavorite.findViewById(R.id.layout_button_add_favorite);
        mTextViewYes = (TextView) mDialogFavorite.findViewById(R.id.dialog_list_pizzas_text_add_favorite_yes);
        mTextViewNo = (TextView) mDialogFavorite.findViewById(R.id.dialog_list_pizzas_text_add_favorite_no);


        mRelativeLayoutAddtoFavorite_dialog.setVisibility(View.VISIBLE);

        mTextViewDialogHeading.setText(R.string.dialog_list_pizza_addtofavorite);
        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);


        mOrderedItemsDatas_Favorite_dialog = new ArrayList<>();
        mOrderedItemsDatas_Favorite_dialog.add(mOrderedPizzaFavorite);
        mAdapterFavorite_dialog = new FavoritePizzaAdapter(getActivity(), mOrderedItemsDatas_Favorite_dialog, "AddToFavorite");
        mListViewFavorite_dialog.setAdapter(mAdapterFavorite_dialog);
        mDialogFavorite.show();

        mImageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavorite.dismiss();
            }
        });


        mTextViewYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                isChangeScreen = true;
//                AddtoBackstackList();
                CallAddFavoriteService(mOrderedPizzaFavorite);
                mDialogFavorite.dismiss();
            }
        });

        mTextViewNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                isChangeScreen = false;
                AddtoBasketList(mOrderedPizzaFavorite);
                mDialogFavorite.dismiss();
            }
        });

    }

    public void AddtoBasketList(OrderedItemsData mOrderedPizzaFavorite) {

//        if (isChangeScreen) {
//            System.out.println("You clicked YES button");
//            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mOrderedPizza);
//            mNavigationActivity.mPizzaListFragment.SetOrderDetail(item_position);
//            mNavigationActivity.onBackPressed();
//            mNavigationActivity.backstacklist.add(mNavigationActivity.mAddressFragment);
//            Log.e("BackstackList", "Address Fragment is added.");
//            mNavigationActivity.showFragment(mNavigationActivity.mAddressFragment, true);
//            mNavigationActivity.ShowHideIcons();
//            mNavigationActivity.showFragments();
//            isChangeScreen = false;
//        } else {
        System.out.println("AddtoBasketList() called.");
        mNavigationActivity.mBasketFragment.AddItemtoListOrder(mOrderedPizzaFavorite);
        mNavigationActivity.mPizzaListFragment.SetOrderDetail(item_position);
//                    Log.e("BackstackList", "Order Fragment is added.");
        mNavigationActivity.onBackPressed();
        mNavigationActivity.showFragment(mNavigationActivity.mPizzaListFragment, true);
        mNavigationActivity.ShowHideIcons();
//                mNavigationActivity.backstacklist.add(mNavigationActivity.mOrderFragment);
        mNavigationActivity.showFragments();

//        }

    }

    public void CallAddFavoriteService(OrderedItemsData mOrderedPizzaFavorite) {

        /*
        Additions and crust code is done for sending to server.
         */

        mOrderedPizzaFavorite.printAllData();
        String userid = "";
        String additions = "{";
        String custom_ingredients = "{";
        String pizzacrust = "";

        if (mStore_pref.getUserId().length() > 0) {
            userid = mStore_pref.getUserId();
        }
        if (mOrderedPizzaFavorite.getExtra_ingredients() != null && mOrderedPizzaFavorite.getExtra_ingredients().size() > 0) {

            for (int i = 0; i < mOrderedPizzaFavorite.getExtra_ingredients().size(); i++) {
                additions += mOrderedPizzaFavorite.getExtra_ingredients().get(i).getId() + ",";
            }

            additions = Utils.RemoveLastComma_ID(additions);
            additions += "}";
        } else {
            additions = "";
        }

        if (mOrderedPizzaFavorite.getToppings() != null && mOrderedPizzaFavorite.getToppings().size() > 0) {

            for (int i = 0; i < mOrderedPizzaFavorite.getToppings().size(); i++) {
                custom_ingredients += mOrderedPizzaFavorite.getToppings().get(i).getId() + ",";
            }
            custom_ingredients = Utils.RemoveLastComma_ID(custom_ingredients);
            custom_ingredients += "}";
        } else {
            custom_ingredients = "";
        }

        if (mOrderedPizzaFavorite.getPizzacrust() != null) {
            pizzacrust += "" + mOrderedPizzaFavorite.getPizzacrust().getId() + "";
        } else {
            pizzacrust = "";
        }

        System.out.println("CallAddFavoriteService() Additions:" + additions);
        System.out.println("CallAddFavoriteService() custom_ingredients:" + custom_ingredients);
        System.out.println("CallAddFavoriteService() pizzacrust:" + pizzacrust);

        String url = Constants.SET_FAVORITES + "&user_id=" + userid + "&menu_id=" + mOrderedPizzaFavorite.getPizza_details().getId()
                + "&pizza_size=" + mOrderedPizzaFavorite.getPizza_size() + "&additions=" + additions
                + "&toppings=" + custom_ingredients + "&pizza_crust_id=" + pizzacrust
                + "&address_id=0";

        Log.e("Add Favorite URL", "AddFavorite URL: " + url);
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.GET, url,
                createAddFavoriteSuccessListener(mOrderedPizzaFavorite),
                createAddFavoriteErrorListener());
        myReq.setRetryPolicy(new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }

    private Response.Listener<String> createAddFavoriteSuccessListener(final OrderedItemsData mOrderedPizzaFavorite) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("OrderFragment: SetFavorite Response: " + response);
                mOrderedPizzaFavorite.printAllData();

                if (response != null && response.length() > 0) {
                    try {
                        JSONObject mJsonObject = new JSONObject(response);
                        if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {


                            mOrderedItemsDatas_Favorite_dialog.get(0).getPizza_details()
                                    .setFavourite_id(mJsonObject.getString("favourite_id"));
                            mOrderedItemsDatas_Favorite_dialog.get(0).getPizza_details()
                                    .setIsPizzaAddedFavorite(true);

                            mOrderedPizzaFavorite.getPizza_details().setFavourite_id(mJsonObject.getString("favourite_id"));
                            mOrderedPizzaFavorite.getPizza_details().setIsPizzaAddedFavorite(true);

                            System.out.println("mOrderedItemsDatas_Favorite_dialog.Favorite ID:" + mOrderedItemsDatas_Favorite_dialog.get(0).getPizza_details().getFavourite_id());
                            System.out.println("mOrderedPizza.Favorite ID:" + mOrderedPizzaFavorite.getPizza_details().getFavourite_id());
                            Toast.makeText(getActivity(), "Pizza Added to Favorite", Toast.LENGTH_SHORT).show();
//                            isChangeScreen = true;

                        } else {
                            Log.e("createAddFavoriteSuccessListener().OrderFragment.java", "Favorite is not added. " +
                                    "Response:" + mJsonObject.getString("responseMsg"));
                            Toast.makeText(getActivity(), "Could not add to Favorites, try again.", Toast.LENGTH_SHORT).show();
//                            isChangeScreen = false;
                        }
                        AddtoBasketList(mOrderedPizzaFavorite);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
    }

    private Response.ErrorListener createAddFavoriteErrorListener() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("createAddFavoriteErrorListener().OrderFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Could not add pizza to favorites. " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void initDialogRemoveToppings() {

        mDialogRemoveToppings = new Dialog(getActivity());
//        mDialogRemoveToppings.getWindow().setBackgroundDrawableResource(R.color.t_transperent);
        mDialogRemoveToppings.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogRemoveToppings.setContentView(mViewDialogRemoveTopping);

        mTextViewHeadingTopping_dialog = (TextView) mDialogRemoveToppings.findViewById(R.id.dialog_additions_heading);
        mListViewRemoveTopping_dialog = (ListView) mDialogRemoveToppings.findViewById(R.id.dialog_additions_list_itemslist);
        mTextViewTotal_Topping_dialog = (TextView) mDialogRemoveToppings.findViewById(R.id.dialog_additions_text_totaladdtionprice);
        mButtonApplyTopping_dialog = (Button) mDialogRemoveToppings.findViewById(R.id.dialog_additions_button_apply_addition);
        mDialogRemoveToppings.findViewById(R.id.dialog_additions_layout_total).setVisibility(View.GONE);

        mTextViewTotal_Topping_dialog.setVisibility(View.GONE);

        mButtonApplyTopping_dialog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                isApplayButtonClickToppings = true;
                mDialogRemoveToppings.dismiss();
                TotalToppings();

            }
        });

        mTextViewHeadingTopping_dialog.setText(R.string.f_order_tv_custompizza_heading);
        mButtonApplyTopping_dialog.setText(R.string.fragorder_layout_pizzacrust_button_applytoppings);


        mToppingses_array_dialog = new ArrayList<>();
        mToppingAdapter_dialog = new ToppingAdapter(getActivity(), mToppingses_array_dialog);
        mListViewRemoveTopping_dialog.setAdapter(mToppingAdapter_dialog);

    }

    void CallToppingsSelection() {
        System.out.println("CallToppingsSelection() service method called.");
        mToppingses_array_dialog = mPizzaListDataReceived.getToppings();
        mToppingAdapter_dialog = new ToppingAdapter(getActivity(), mToppingses_array_dialog);
        mListViewRemoveTopping_dialog.setAdapter(mToppingAdapter_dialog);


        if (isEditOrderItem && isRemoveToppingsSelected) {
            Log.e("CallToppingsSelection().if (isEditOrderItem)", "Pizza is to be edited.");

            selectedToppingsIDs = "";
            String name = "";
            totaltoppings_selected = 0;
            float totaltoppings = 0;

            for (int j = 0; j < mToppingses_array_dialog.size(); j++) {

                for (int i = 0; i < orderedItemsDataEdit.getToppings().size(); i++) {

                    if (mToppingses_array_dialog.get(j).getName().
                            equalsIgnoreCase(orderedItemsDataEdit.getToppings().get(i).getName())) {

                        System.out.println("In Item Edit Loop RemoveTopping Name:" + orderedItemsDataEdit.getToppings().get(i).getName());
                        System.out.println("In Item Edit Loop RemoveTopping Price:" + orderedItemsDataEdit.getToppings().get(i).getPrice());
                        mToppingses_array_dialog.get(j).setIsToppingSelected(true);
                        selectedToppingsIDs += orderedItemsDataEdit.getToppings().get(i).getId() + ",";
                        name += orderedItemsDataEdit.getToppings().get(i).getName() + ", ";
                        totaltoppings_selected += orderedItemsDataEdit.getToppings().get(i).getPrice();

                    } else {
                        mToppingses_array_dialog.get(j).setIsToppingSelected(false);
                        System.out.println("In Item Edit Loop RemoveTopping Name:" + orderedItemsDataEdit.getToppings().get(i).getName());
                        System.out.println("In Item Edit Loop RemoveTopping Price:" + orderedItemsDataEdit.getToppings().get(i).getPrice());
                    }

                }
                totaltoppings += mToppingses_array_dialog.get(j).getPrice();
                System.out.println("totaltoppings:" + totaltoppings);
            }
            totaltoppings_removed = totaltoppings - totaltoppings_selected;
            selectedToppingsIDs = Utils.RemoveLastComma_ID(selectedToppingsIDs);
            name = Utils.RemoveLastComma(name);
            mStore_pref.setToppingsIDs(selectedToppingsIDs, "CallToppingsSelection(). not from fav/history");

            mTextViewToppingsSelected.setText(name);
//            mTextViewToppingsTotalPrice.setText("" + totaladditions_selected);
            mImageViewToppings.setBackgroundResource(R.drawable.checked);
            mToppingAdapter_dialog.TotalofSelectedToppings = totaltoppings_selected;
            mTextViewTotal_Topping_dialog.setText("" + totaltoppings_selected);

            mToppingAdapter_dialog.notifyDataSetChanged();

            isRemoveToppingsSelected = true;
            DoTotal();
        }
    }

    void TotalToppings() {
        count_toppings_selected = 0;
        float totaltoppings = 0;
        float tot_remove = 0;
        String name = "";
        selectedToppingsIDs = "";

        Log.e("mButtonApplyTopping_dialog.setOnClickListener()", "Applying the Toppings selected.");
        if (isApplayButtonClickToppings) {

            for (int i = 0; i < mToppingses_array_dialog.size(); i++) {
                if (mToppingses_array_dialog.get(i).isToppingSelected()) {
                    totaltoppings += mToppingses_array_dialog.get(i).getPrice();
                    name += mToppingses_array_dialog.get(i).getName() + ", ";
                    selectedToppingsIDs += mToppingses_array_dialog.get(i).getId() + ",";
                    count_toppings_selected++;
                } else {
                    mToppingses_array_dialog.get(i).setIsToppingSelected(false);
                    tot_remove += mToppingses_array_dialog.get(i).getPrice();
                }
            }

            selectedToppingsIDs = Utils.RemoveLastComma_ID(selectedToppingsIDs);
            name = Utils.RemoveLastComma(name);

            System.out.println("Topping Selected Total: " + totaltoppings);
            System.out.println("Topping Removed Total: " + tot_remove);
            System.out.println("count_toppings_selected: " + count_toppings_selected);
            System.out.println("Selected Toppings Names: " + name);

//            if (totaltoppings > 0) {
            if (count_toppings_selected == 0 && selectedToppingsIDs.length() == 0 && count_additions_selected < 2) {

                mTextViewToppingsSelected.setText("Please select at least 2 Toppings");
                //mTextViewToppingsTotalPrice.setText("0.0");
                mImageViewToppings.setBackgroundResource(R.drawable.checked);

                isRemoveToppingsSelected = true;
                mStore_pref.removeToppingsIDs();
                count_toppings_selected = -1;
                Toast.makeText(getActivity(), "Please select at least 2 Toppings", Toast.LENGTH_SHORT).show();

            } else if (count_toppings_selected < mToppingses_array_dialog.size()) {

                mStore_pref.setToppingsIDs(selectedToppingsIDs, "mButtonApplyTopping_dialog.setOnClickListener()");
                mTextViewToppingsSelected.setText(name);
//                mTextViewToppingsTotalPrice.setText("" + totaltoppings);
                mImageViewToppings.setBackgroundResource(R.drawable.checked);

                mToppingAdapter_dialog.TotalofSelectedToppings = totaltoppings;
                mTextViewTotal_Topping_dialog.setText("" + totaltoppings);

                Log.e("mButtonApplyTopping_dialog.setOnClickListener() -->", "Toppings Total: " + totaltoppings);

                isRemoveToppingsSelected = true;

            } else {
                System.out.println("Else part of mButtonApplyTopping_dialog.setOnClickListener()");
                mTextViewToppingsSelected.setText(R.string.f_order_tv_custompizza_heading);
//                mTextViewToppingsTotalPrice.setText("0.0");
                mImageViewToppings.setBackgroundResource(R.drawable.unchecked);
                isRemoveToppingsSelected = false;
                mStore_pref.removeToppingsIDs();
            }
            isApplayButtonClickToppings = false;

        } else

        {
            System.out.println("Else part of mButtonApplyTopping_dialog.setOnClickListener()");
            mTextViewToppingsSelected.setText(R.string.f_order_tv_custompizza_heading);
//            mTextViewToppingsTotalPrice.setText("0.0");
            mImageViewToppings.setBackgroundResource(R.drawable.unchecked);
            isRemoveToppingsSelected = false;
            mStore_pref.removeToppingsIDs();
            selectedToppingsIDs = "";
        }

        totaltoppings_selected = totaltoppings;
        totaltoppings_removed = tot_remove;

        DoTotal();

    }
}

/*
http://incredibleapps.net/thinkpizza/action.php?do=set_favourites&user_id=999&menu_id=99
&pizza_size=medium&additions={38,39,40}&custom_ingredients={1,2,3}&pizza_crust_id={49}&address_id=5
 */