package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class ShareAppFragment extends Fragment implements View.OnClickListener {

    RadioButton mRadioButtonShareApp;
    RadioButton mRadioButtonRecommendApp;
    RadioButton mRadioButtonRecommendPizzeria;
    RadioButton mCurrentlyCheckedRB;

    /*
    Sharing App
     */
    RelativeLayout mRelativeLayoutSharing;
    TextView mTextViewShareFB;
    TextView mTextViewShareGmail;
    TextView mTextViewShareWhatsApp;
    TextView mTextViewShareCancel;
    // Animation
    Animation animSlideDown;
    Animation animSlideUP;

    /*
    Recommend Pizzeria
     */
    Dialog mDialogRecommendPizzeria;
    RelativeLayout mLinearLayoutRecommendPizzeria;
    ScrollView mScrollView;
    EditText mEditTextPizzeriaName;
    EditText mEditTextPersonName;
    EditText mEditTextPizzeriaEmail;
    EditText mEditTextTelephoneNumber;

    AutoCompleteTextView mAutoCompleteTextViewPincode;
    EditText mEditTextStreetName;
    EditText mEditTextStreetNo;
    EditText mEditTextAptNo;
    EditText mEditTextCity;
    EditText mEditTextState;
    double latitude = 21.1305697;
    double longitude = 72.7903613;
    Button mButtonSend;
    // Animation
    Animation animSlideDownPizzeria;
    Animation animSlideUPPizzeria;

    NavigationActivity mNavigationActivity;
    ProgressDialog mProgressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_share_app, container, false);
        mNavigationActivity = (NavigationActivity) getActivity();

        mRadioButtonShareApp = (RadioButton) mView.findViewById(R.id.f_share_radio_share);
        mRadioButtonRecommendApp = (RadioButton) mView.findViewById(R.id.f_share_radio_recommend_app);
        mRadioButtonRecommendPizzeria = (RadioButton) mView.findViewById(R.id.f_share_radio_recommend_pizzeria);

        mRadioButtonShareApp.setOnClickListener(this);
        mRadioButtonRecommendApp.setOnClickListener(this);
        mRadioButtonRecommendPizzeria.setOnClickListener(this);

        mRadioButtonShareApp.setChecked(true);
        mCurrentlyCheckedRB = mRadioButtonShareApp;

        InitSharingDialog(mView);
        return mView;

    }

    public void InitSharingDialog(View mView) {
        mRelativeLayoutSharing = (RelativeLayout) mView.findViewById(R.id.navigationactivity_layout_sharing);
        mTextViewShareFB = (TextView) mView.findViewById(R.id.navigation_activity_share_fb);
        mTextViewShareGmail = (TextView) mView.findViewById(R.id.navigation_activity_share_gmail);
        mTextViewShareWhatsApp = (TextView) mView.findViewById(R.id.navigation_activity_share_whatsapp);
        mTextViewShareCancel = (TextView) mView.findViewById(R.id.navigation_activity_share_cancel);


        mTextViewShareFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
//                selectApp("facebook");
                        Toast.makeText(getActivity(), "Feature not implement.", Toast.LENGTH_SHORT).show();
                        InitSharingWithFacebook();
                    }
                }, 1000);
                mRelativeLayoutSharing.startAnimation(animSlideUP);
                mRelativeLayoutSharing.setVisibility(View.GONE);
                mRelativeLayoutSharing.getAnimation().cancel();

            }
        });
        mTextViewShareGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        // Actions to do after 10 seconds
//                selectApp("facebook");
//                Animation animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
//                mRelativeLayoutSharing.startAnimation(animSlideUp);
//                        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                        intent.setType("*/*");
//                        intent.putExtra(Intent.EXTRA_SUBJECT, "Share the ThinkPizza App.");
//                        intent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now !");
//                        try {
//                            intent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        final PackageManager pm = getActivity().getPackageManager();
//                        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
//                        ResolveInfo best = null;
//                        for (final ResolveInfo info : matches)
//                            if (info.activityInfo.packageName.endsWith(".gm") ||
//                                    info.activityInfo.name.toLowerCase().contains("gmail"))
//                                best = info;
//                        if (best != null)
//                            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
//                        startActivity(intent);
//                        selectApp("gm");
                        selectAppToShare("gmail", 1);
                    }
                }, 1000);
                mRelativeLayoutSharing.startAnimation(animSlideUP);
                mRelativeLayoutSharing.setVisibility(View.GONE);
                mRelativeLayoutSharing.getAnimation().cancel();
                mNavigationActivity.onBackPressed();
            }
        });
        mTextViewShareWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
//                        selectApp("whatsapp");
                        selectAppToShare("whatsapp", 1);
                    }
                }, 1000);
                mRelativeLayoutSharing.startAnimation(animSlideUP);
                mRelativeLayoutSharing.setVisibility(View.GONE);
                mRelativeLayoutSharing.getAnimation().cancel();
                mNavigationActivity.onBackPressed();

            }
        });
        mTextViewShareCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRelativeLayoutSharing.startAnimation(animSlideUP);
                mRelativeLayoutSharing.setVisibility(View.GONE);
                mRelativeLayoutSharing.getAnimation().cancel();
//                mNavigationActivity.onBackPressed();
//                mRelativeLayoutSharing.setAnimation(null);
            }
        });
        // load the animation
        animSlideDown = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animSlideUP = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);

        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animSlideDown.cancel();
                animSlideDown.reset();
                mRelativeLayoutSharing.setAnimation(null);
                mNavigationActivity.showFragments();
                System.out.println("shown from onAnimationEnd Down");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animSlideUP.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animSlideUP.cancel();
                animSlideUP.reset();
                mRelativeLayoutSharing.setAnimation(null);
                mRelativeLayoutSharing.setVisibility(View.GONE);
                mNavigationActivity.showFragments();
                System.out.println("shown from onAnimationEnd UP");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void InitRecommendPizzeria() {
//        mDialogRecommendPizzeria = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        mDialogRecommendPizzeria = new Dialog(getActivity());
        mDialogRecommendPizzeria.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogRecommendPizzeria.setContentView(R.layout.dialog_recommend_pizzeria);

        mDialogRecommendPizzeria.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        mLinearLayoutRecommendPizzeria = (RelativeLayout) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_parent_layout);
        mScrollView = (ScrollView) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_scroll);

        mEditTextPizzeriaName = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_pizzeria_name);
        mEditTextPersonName = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_pizzeria_person_name);
        mEditTextPizzeriaEmail = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_pizzeria_email);
        mEditTextTelephoneNumber = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_telephone_number);

        mAutoCompleteTextViewPincode = (AutoCompleteTextView) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_pincode);
        mEditTextStreetName = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_street_name);
        mEditTextStreetNo = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_street_no);
        mEditTextAptNo = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_apt_no);
        mEditTextCity = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_city);
        mEditTextState = (EditText) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_state);

        mButtonSend = (Button) mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_button_send);

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });
        mDialogRecommendPizzeria.findViewById(R.id.dialog_recommend_pizzeria_button_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        mDialogRecommendPizzeria.dismiss();
                        mLinearLayoutRecommendPizzeria.setVisibility(View.GONE);
                    }
                }, 700);
                mLinearLayoutRecommendPizzeria.startAnimation(animSlideUPPizzeria);
//                mLinearLayoutRecommendPizzeria.setVisibility(View.GONE);
                mLinearLayoutRecommendPizzeria.getAnimation().cancel();
            }
        });
        mAutoCompleteTextViewPincode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    mAutoCompleteTextViewPincode.showDropDown();
                    Utils.HideSoftKeyBoard(getActivity(), mAutoCompleteTextViewPincode);
                    return true;
                }
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    if (mAutoCompleteTextViewPincode.getText().length() > 0) {
                        Utils.hide_keyboard(getActivity());
                        CallBrazilAddress(mAutoCompleteTextViewPincode.getText().toString().replaceAll(" ", ""));
                        return true;
                    } else {
                        Toast.makeText(getActivity(), "Please enter CEP code to search address.", Toast.LENGTH_SHORT).show();
                    }
                }
                zipcode = mAutoCompleteTextViewPincode.getText().toString();
                return false;
            }
        });

        mAutoCompleteTextViewPincode.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                String str = (String) arg0.getItemAtPosition(arg2);
                Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
                new LoadAddress(mAutoCompleteTextViewPincode.getText().toString()).execute();
                mAutoCompleteTextViewPincode.dismissDropDown();
                Utils.HideSoftKeyBoard(getActivity(), mAutoCompleteTextViewPincode);
                mAutoCompleteTextViewPincode.clearFocus();
            }
        });
//        setAutocompleteTextAdapter();
        // load the animation
        animSlideDownPizzeria = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        animSlideUPPizzeria = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);

        animSlideDownPizzeria.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animSlideDownPizzeria.cancel();
                animSlideDownPizzeria.reset();
                mLinearLayoutRecommendPizzeria.setAnimation(null);
                mNavigationActivity.showFragments();
                System.out.println("shown from onAnimationEnd Down");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animSlideUPPizzeria.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animSlideUPPizzeria.cancel();
                animSlideUPPizzeria.reset();
                mLinearLayoutRecommendPizzeria.setAnimation(null);
                mLinearLayoutRecommendPizzeria.setVisibility(View.GONE);
                mNavigationActivity.showFragments();
                System.out.println("shown from onAnimationEnd UP");
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onClick(View view) {
//        if (mCurrentlyCheckedRB != null)
//            System.out.println("Current Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
//
//        if (mCurrentlyCheckedRB == null) {
//            System.out.println("in null condition");
//            Log.e("ShareAppFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
//            mCurrentlyCheckedRB = (RadioButton) view;
//            mCurrentlyCheckedRB.setChecked(true);
//        }
//        if (mCurrentlyCheckedRB == view) {
//            System.out.println("in if current radio condition");
//            Log.e("ShareAppFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());
//            return;
//        } else {
//            System.out.println("in else of current radio condition");
//            Log.e("ShareAppFragment: ", "Radiobutton Previous Selected: " + mCurrentlyCheckedRB.getText());
//            mCurrentlyCheckedRB.setChecked(false);
//            ((RadioButton) view).setChecked(true);
//            mCurrentlyCheckedRB = (RadioButton) view;
//            Log.e("ShareAppFragment: ", "Radiobutton Selected: " + mCurrentlyCheckedRB.getText());

        switch (view.getId()) {
            case R.id.f_share_radio_share:
                mRadioButtonShareApp.setChecked(true);
                mRadioButtonRecommendApp.setChecked(false);
                mRadioButtonRecommendPizzeria.setChecked(false);
                startBottomanim();
                break;
            case R.id.f_share_radio_recommend_app:
                mRadioButtonShareApp.setChecked(false);
                mRadioButtonRecommendApp.setChecked(true);
                mRadioButtonRecommendPizzeria.setChecked(false);
//                Toast.makeText(getActivity(), "Not implemented", Toast.LENGTH_SHORT).show();
                selectAppToShare("gmail", 1);
                break;
            case R.id.f_share_radio_recommend_pizzeria:
                mRadioButtonShareApp.setChecked(false);
                mRadioButtonRecommendApp.setChecked(false);
                mRadioButtonRecommendPizzeria.setChecked(true);
//                Toast.makeText(getActivity(), "Not implemented", Toast.LENGTH_SHORT).show();
                InitRecommendPizzeria();
                startBottomanimPizzeira();
                break;

        }
//        }

    }

    public void startBottomanim() {
        mRadioButtonShareApp.setChecked(true);
        mRadioButtonRecommendApp.setChecked(false);
        mRadioButtonRecommendPizzeria.setChecked(false);
        mRelativeLayoutSharing.setVisibility(View.VISIBLE);
        mRelativeLayoutSharing.startAnimation(animSlideDown);
    }

    public void startRecommendApp() {
        mRadioButtonShareApp.setChecked(false);
        mRadioButtonRecommendApp.setChecked(true);
        mRadioButtonRecommendPizzeria.setChecked(false);
        selectAppToShare("gmail", 1);
    }

    public void startBottomanimPizzeira() {
        mDialogRecommendPizzeria.show();

        mRadioButtonShareApp.setChecked(false);
        mRadioButtonRecommendApp.setChecked(false);
        mRadioButtonRecommendPizzeria.setChecked(true);
        mRelativeLayoutSharing.setVisibility(View.GONE);
        mLinearLayoutRecommendPizzeria.setVisibility(View.VISIBLE);
        mLinearLayoutRecommendPizzeria.startAnimation(animSlideDownPizzeria);
    }

    void selectApp(String appnamne) {
        try {

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("*/*");
            if (!appnamne.equalsIgnoreCase("facebook"))
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now ! ");
            shareIntent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());

                    /*ShareLinkContent content = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse("https://developers.facebook.com"))
                            .build();
                    SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap).build();
                    SharePhotoContent mSharePhotoContent= new SharePhotoContent.Builder().addPhoto(photo).build();*/


            PackageManager pm = getActivity().getPackageManager();

            List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
            boolean found = false;
            for (final ResolveInfo app : activityList) {
                if ((app.activityInfo.name).contains(appnamne)) {

                    final ActivityInfo activity = app.activityInfo;

                    final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    shareIntent.setComponent(name);
                    startActivity(shareIntent);
                    found = true;
                    break;
                }
            }
            if (!found) {
                Toast.makeText(getActivity(), "Application is not installed.", Toast.LENGTH_SHORT).show();
            }
            startActivity(Intent.createChooser(shareIntent, "Share ThinkPizza App"));
            Animation animSlideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
            mRelativeLayoutSharing.startAnimation(animSlideUp);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    void selectAppToShare(String appnamne, int i) {
        try {

//             Intent shareIntent = new Intent(Intent.ACTION_SEND);
//            shareIntent.setType("*/*");
//            if (!appnamne.equalsIgnoreCase("facebook"))
//                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now ! ");
//            shareIntent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());
//
//                    /*ShareLinkContent content = new ShareLinkContent.Builder()
//                            .setContentUrl(Uri.parse("https://developers.facebook.com"))
//                            .build();
//                    SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap).build();
//                    SharePhotoContent mSharePhotoContent= new SharePhotoContent.Builder().addPhoto(photo).build();*/

/*
           PackageManager pm = getActivity().getPackageManager();
            List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
            boolean found = false;
            for (final ResolveInfo app : activityList) {
                if ((app.activityInfo.name).contains(appnamne)) {

                    final ActivityInfo activity = app.activityInfo;

                    final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    shareIntent.setComponent(name);
                    startActivity(shareIntent);
                    found = true;
                    break;
                }
            }
            if (!found) {
                Toast.makeText(getActivity(), "Application is not installed.", Toast.LENGTH_SHORT).show();
            }*/


            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("*/*");
            if (i == 1) {
                intent.putExtra(Intent.EXTRA_SUBJECT, "Recommendation of App.");
                intent.putExtra(Intent.EXTRA_TEXT, "I would kindly request you to register your self in the ThinkPizza app.");
            } else {
                intent.putExtra(Intent.EXTRA_SUBJECT, "Share the ThinkPizza App.");
                intent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now !");
            }
            try {
                intent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());
            } catch (IOException e) {
                e.printStackTrace();
            }
            final PackageManager pm = getActivity().getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
            ResolveInfo best = null;
            for (final ResolveInfo info : matches) {
//                System.out.println("Package Name: " + info.activityInfo.packageName);
//                System.out.println("ActivityInfo Name: " + info.activityInfo.name);

                if (appnamne.equalsIgnoreCase("gmail")) {
                    if (info.activityInfo.packageName.endsWith(".gm") ||
                            info.activityInfo.name.toLowerCase().contains("gmail"))
                        best = info;
                } else if (appnamne.equalsIgnoreCase("whatsapp")) {
                    if (info.activityInfo.packageName.endsWith("com.whatsapp") ||
                            info.activityInfo.name.toLowerCase().contains("whatsapp"))
                        best = info;
                }
                if (best != null) {
                    intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                    startActivity(intent);
                    break;
                }

            }
            if (best == null) {
                Toast.makeText(getActivity(), "Application is not installed.", Toast.LENGTH_SHORT).show();
            }
//            startActivity(Intent.createChooser(shareIntent, "Share ThinkPizza App"));
            if (i != 1) {
                Animation animSlideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
                mRelativeLayoutSharing.startAnimation(animSlideUp);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    Uri getImagefileUri() throws IOException {
        String photoPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Share ThinkPizza/";
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
        File dir = new File(photoPath);
        dir.mkdirs();
        // Create a name for the saved image
        File file = new File(dir, "thinkpizza.png");
        OutputStream output = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
        output.flush();
        output.close();
        Uri uri = Uri.fromFile(file);
        return uri;
    }

    public void InitSharingWithFacebook() {
        final Dialog mDialogSharingFB = new Dialog(getActivity());
        mDialogSharingFB.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogSharingFB.setContentView(R.layout.dialog_share_facebook);
        ImageView mImageViewShareImage = (ImageView) mDialogSharingFB.findViewById(R.id.dialog_share_fb_image_sharing_image);
        EditText mEditTextSharingMessage = (EditText) mDialogSharingFB.findViewById(R.id.dialog_share_fb_edit_sharing_text);

        mEditTextSharingMessage.setText("Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now !");
        mImageViewShareImage.setImageResource(R.drawable.ic_launcher);
        mDialogSharingFB.findViewById(R.id.dialog_share_fb_text_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Feature not implement.", Toast.LENGTH_SHORT).show();
                mDialogSharingFB.dismiss();
                mNavigationActivity.onBackPressed();
            }
        });
        mDialogSharingFB.findViewById(R.id.dialog_share_fb_text_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogSharingFB.dismiss();
//                mNavigationActivity.onBackPressed();
            }
        });
        mDialogSharingFB.show();
    }

    /*
           Autocomplete Address Methods starts
        */
    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(Constants.PLACES_API_BASE + Constants.TYPE_AUTOCOMPLETE + Constants.OUT_JSON);
            sb.append("?key=" + Constants.GOOGLE_PLACES_API);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("Mapfragment: URL: " + url);
            Log.e("Mapfragment: URL: ", "url" + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("autocomplete().Mapfragment", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("autocomplete().Mapfragment", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("autocomplete().Share App Fragment", "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public void setAutocompleteTextAdapter() {
        System.out.println("setAutocompleteTextAdapter() is called");
        mAutoCompleteTextViewPincode.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
    }

    /*
        Autocomplete Address Methods Ends
     */
    String zipcode = "";

    public class LoadAddress extends AsyncTask<Void, Void, Void> {

        List<Address> add;
        ProgressDialog mProgressDialog;
        String searchAddress = "";

        LoadAddress(String address) {
            searchAddress = address;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Finding Location...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocationName(searchAddress, 2);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();

            if (add != null && add.size() > 0) {

                System.out.println("Max addressline index: " + add.get(0).getMaxAddressLineIndex());
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));

                System.out.println("Longitude: " + add.get(0).getLongitude());
                System.out.println("Latitude: " + add.get(0).getLatitude());

                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Postal code: " + add.get(0).getPostalCode());

                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());

        /*
                mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) == null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2));
                else if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) != null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));

                mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());

                if (add.get(0).getSubAdminArea() == null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                else
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                street_name_et.setText(add.get(0).getAddressLine(0));
//                city_et.setText(add.get(0).get);
        */
                if (add.get(0).getThoroughfare() != null)
                    mEditTextStreetName.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mEditTextStreetName.setText(add.get(0).getFeatureName());
                else
                    mEditTextStreetName.setText(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mEditTextStreetNo.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mEditTextCity.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mEditTextCity.setText(add.get(0).getLocality());

                if (add.get(0).getAdminArea() != null)
                    mEditTextState.setText(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mAutoCompleteTextViewPincode.setText(add.get(0).getPostalCode());
                else
                    mAutoCompleteTextViewPincode.setText(zipcode);

                latitude = add.get(0).getLatitude();
                longitude = add.get(0).getLongitude();

            } else {
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void validate() {

        if (mEditTextPizzeriaName.getText().length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter Valid Pizzeria Name.", Toast.LENGTH_SHORT);
            mEditTextPizzeriaName.requestFocus();
            mScrollView.smoothScrollTo(0, 0);
            Utils.showSoftKeyBoard(getActivity(), mEditTextPizzeriaName);
        } else if (mAutoCompleteTextViewPincode.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter Zip Code.", Toast.LENGTH_SHORT);
            mAutoCompleteTextViewPincode.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mAutoCompleteTextViewPincode);

        } else if (mEditTextStreetName.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter street name.", Toast.LENGTH_SHORT);
            mEditTextStreetName.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextStreetName);

        } else if (mEditTextStreetNo.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter street number.", Toast.LENGTH_SHORT);
            mEditTextStreetNo.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextStreetNo);

        } else if (mEditTextAptNo.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter appartment no.", Toast.LENGTH_SHORT);
            mEditTextAptNo.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextAptNo);

        } else if (mEditTextCity.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter city.", Toast.LENGTH_SHORT);
            mEditTextCity.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextCity);

        } else if (mEditTextState.length() <= 0) {
            Utils.showMyToast(getActivity(), "Enter State", Toast.LENGTH_SHORT);
            mEditTextState.requestFocus();
            Utils.showSoftKeyBoard(getActivity(), mEditTextState);

        } else {
            Utils.hide_keyboard(getActivity());
            callRecommendPizzeriaService();
        }
    }

    private void callRecommendPizzeriaService() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.SET_RECOMMEND_PIZZERIA, RecommendPizzeriaServiceSuccess(), RecommendPizzeriaServiceFailure()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", new Store_pref(getActivity()).getUser().getId());
                params.put("pizzeria_name", mEditTextPizzeriaName.getText().toString());
                params.put("pizzeria_email", mEditTextPizzeriaEmail.getText().toString());
                params.put("person_name", mEditTextPersonName.getText().toString());
                params.put("telephone", mEditTextTelephoneNumber.getText().toString());
                params.put("lat", "" + latitude);
                params.put("lng", "" + longitude);
                params.put("street_name", mEditTextStreetName.getText().toString());
                params.put("street_no", mEditTextStreetNo.getText().toString());
                params.put("apt_no", mEditTextAptNo.getText().toString());
                params.put("city", mEditTextCity.getText().toString());
                params.put("state", mEditTextState.getText().toString());
                params.put("zipcode", mAutoCompleteTextViewPincode.getText().toString());
                return params;
            }
        };
        queue.add(myReq);
    }

    private Response.Listener<String> RecommendPizzeriaServiceSuccess() {
        return new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();


                Log.e("result", "" + response);
//                MyResponse myResponse = mGson.fromJson(response, MyResponse.class);

                try {
                    JSONObject mJsonObject = new JSONObject(response);
                    if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                mDialogRecommendPizzeria.dismiss();
                                mLinearLayoutRecommendPizzeria.setVisibility(View.GONE);
                            }
                        }, 700);
                        mLinearLayoutRecommendPizzeria.startAnimation(animSlideUPPizzeria);
//                mLinearLayoutRecommendPizzeria.setVisibility(View.GONE);
                        mLinearLayoutRecommendPizzeria.getAnimation().cancel();

                        Toast.makeText(getActivity(), "Recommended Pizzeria is added successfully.", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        };
    }

    private Response.ErrorListener RecommendPizzeriaServiceFailure() {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    /*
        Below Service will be called when user enters the pin code of brazil to get address
        Starts
    */
    String brazil_CEP_search = "";
    ProgressDialog mDialogCEP;

    public void CallBrazilAddress(String Pin) {
        Utils.hide_keyboard(getActivity());
        mDialogCEP = new ProgressDialog(getActivity());
        mDialogCEP.setMessage("Please Wait...");
        mDialogCEP.setCancelable(false);
        mDialogCEP.show();

        String cepurl = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + Pin;
        System.out.println("CEP URL: " + cepurl);
        brazil_CEP_search = Pin;
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest myReq = new StringRequest(Request.Method.GET, cepurl,
                BrazilAddresssuccess(), BrazilAddressfail());
        queue.add(myReq);
    }

    private Response.Listener<String> BrazilAddresssuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mDialogCEP.dismiss();
                System.out.println("webservicecep Response :" + response);
                String brazil_address = "";

                try {

                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new InputSource(new StringReader(response)));
                    // normalize the document
                    doc.getDocumentElement().normalize();
                    // get the root node
                    NodeList nodeList = doc.getElementsByTagName("webservicecep");
                    Node node = nodeList.item(0);
                    System.out.println("webservicecep TextContent :" + node.getTextContent());

                    // the  node has three child nodes
                    boolean isAddressfound = true;

                    for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                        System.out.println("J=" + j);
                        String nameofattribute = node.getChildNodes().item(j).getNodeName();
                        if (nameofattribute.equalsIgnoreCase("resultado")
                                && node.getChildNodes().item(j).getTextContent().equalsIgnoreCase("1")) {
                            System.out.println("GONE when j=" + j);
                            isAddressfound = true;
                            break;
                        } else
                            isAddressfound = false;
                    }

                    if (isAddressfound) {
                        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                            System.out.println("GONE  I=" + i);
                            Node temp = node.getChildNodes().item(i);
                            System.out.println("webservicecep getNodeName: " + temp.getNodeName());
                            Log.e("getTextContent: ", "webservicecep getTextContent: " + temp.getTextContent());

                            if (temp.getNodeName().equalsIgnoreCase("cidade")) {
                                System.out.println("GONE logradouro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("bairro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("tipo_logradouro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("logradouro")) {
                                System.out.println("GONE cidade");
                                brazil_address += " " + temp.getTextContent();
                            }
                            System.out.println("Brazil Address[" + i + "]: " + brazil_address);
                        }
                        System.out.println("Brazil Address: " + brazil_address);
                    }
                    /*
                    Now below condition
                    "if (brazil_address.replaceAll(" ", "").length() == 0)"
                    will decide that set the "mNavigationActivity.mAutoCompleteTextViewSearch" with
                    Google places api adapter or to return the brazil address

                     */
                    if (brazil_address.replaceAll(" ", "").length() == 0) {
                        Log.e("BrazilAddresssuccess().MapFragment", "could not find the give brazil address: " + brazil_CEP_search);
                        Toast.makeText(getActivity(), "Sorry, Address with CEP " + brazil_CEP_search + " could not be found.", Toast.LENGTH_SHORT).show();
                        mEditTextStreetName.setText("");
                        mEditTextStreetNo.setText("");
                        mEditTextAptNo.setText("");
                        mEditTextCity.setText("");
                        mEditTextState.setText("");
                        latitude = 0;
                        longitude = 0;

                    } else {
                        new LoadLocationFromCEP().execute(brazil_address);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private Response.ErrorListener BrazilAddressfail() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mDialogCEP.dismiss();

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Sorry, your address could not be found.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public class LoadLocationFromCEP extends AsyncTask<String, Void, String> {

        String response = "";
        List<Address> add;
        String brazil_address_received = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            Dialog = new ProgressDialog(getActivity());
//            Dialog.setMessage("Finding CEP Location...");
//            Dialog.setCancelable(false);
//            Dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (params.length == 0)
                return "";
            try {
                System.out.println("------------------getLocationPoints()----------------");
                System.out.println("Address Received: " + params[0]);
                brazil_address_received = params[0];
                System.out.println("Address Received Length: " + params[0].length());

                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                add = geo.getFromLocationName(params[0], 1);

            } catch (Exception e) {
                e.printStackTrace();
                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

//            Dialog.dismiss();
            String address[] = brazil_address_received.split("#");
            if (add != null && add.size() > 0) {
                Log.e("LoadLocationFromCEP()", "-----------------------LoadLocationFromCEP--------------");
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());

                Log.e("getLocationPoint()", "Full Address: " + add.get(0));
                System.out.println("getLocationPoint() Longitude" + add.get(0).getLongitude());
                System.out.println("getLocationPoint() Latitude" + add.get(0).getLatitude());

                if (add.get(0).getThoroughfare() != null)
                    mEditTextStreetName.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mEditTextStreetName.setText(add.get(0).getFeatureName());
                else
                    mEditTextStreetName.setText(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mEditTextStreetNo.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mEditTextCity.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mEditTextCity.setText(add.get(0).getLocality());
                else {
                        /*
                        this else part is added because some time city name is not coming in G places
                         */
                    mEditTextCity.setText(address[0]);
                }


                if (add.get(0).getAdminArea() != null)
                    mEditTextState.setText(add.get(0).getAdminArea());


                if (add.get(0).getPostalCode() != null)
                    mAutoCompleteTextViewPincode.setText(add.get(0).getPostalCode());
                else
//                    pincode_et.setText(zipcode);
                    mAutoCompleteTextViewPincode.setText(brazil_CEP_search);


                latitude = add.get(0).getLatitude();
                longitude = add.get(0).getLongitude();

//                Original code
                /*

                latitude = add.get(0).getLatitude();
                longitude = add.get(0).getLongitude();


                Log.e("getLocationPoints().MapFragment", "Address line 1: " + add.get(0).getAddressLine(0));
                    *//*if (add.get(0).getAddressLine(3).length() > 0) {
                        Log.e("getLocationPoints().MapFragment", "Address line 2" + add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                    } else {*//*
                Log.e("getLocationPoints().MapFragment", "Address line 2 & 3: " + add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));

                mAddressPostalAddressSelected.setTag_name("Manual Address (By CEP)");
                if (add.get(0).getThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getFeatureName());
                else
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_no(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                else {
                        *//*
                        this else part is added because some time city name is not coming in G places
                         *//*
                    mAddressPostalAddressSelected.setCity(address[0]);
                }

                if (add.get(0).getAdminArea() != null)
                    mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());
                else {
                    mAddressPostalAddressSelected.setZip_code(brazil_CEP_search);
                }
//                    else
//                        mAutoCompleteTextViewPincode.setText(zipcode);

                mAddressPostalAddressSelected.setLat("" + add.get(0).getLatitude());
                mAddressPostalAddressSelected.setLng("" + add.get(0).getLongitude());
*/

            } else {
                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(getActivity(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }
    /*
        Below Service will be called when user enters the pin code of brazil to get address
        Emds
    */
}
