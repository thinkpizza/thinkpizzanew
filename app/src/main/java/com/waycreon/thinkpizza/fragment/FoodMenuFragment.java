package com.waycreon.thinkpizza.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.Response.GetMenu;
import com.waycreon.thinkpizza.datamodels.Menu;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.thinkpizza.imageloader.ImageLoaderBlur;
import com.waycreon.waycreon.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FoodMenuFragment extends Fragment {

    Gson mGson;
//    GetMenu menu;

    List<Menu> menulist = new ArrayList<>();
    // ListView mListView;
    RelativeLayout mRelativeLayoutRestaurant;
    ImageView mImageViewRestaurentImage;
    ImageView mImageViewRestaurantBk;
    TextView mTextViewRestaurentName;
    TextView mTextViewRestaurentAddress;
    PizzaRestro mRestaurentDataReceived = null;

    ProgressDialog mProgressDialog;

    GridView menugrid;
    // int menuicon[] = { R.drawable.menuicon_pizza_100,
    // R.drawable.menuicon_combo, R.drawable.menuicon_beverage_100 };
    // String Menu[] = { "Pizza", "Combo Meal", "Beverages" };
    // ArrayList<NavigationListItemModel> mArrayListMenuList;
    NavigationActivity mNavigationActivity;

    RelativeLayout mRelativeLayoutFoodMenu;
    RelativeLayout mLinearLayoutNoFoodMenu;
    ImageView mImageViewRefreshService;

    ImageLoader mImageLoader;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_foodmenu, container,
                false);
        mRelativeLayoutRestaurant = (RelativeLayout) mView.findViewById(R.id.frag_foodmenu_layout_restaurant);
        mImageViewRestaurentImage = (ImageView) mView.findViewById(R.id.frag_foodmenu_image_restaurent);
        mImageViewRestaurantBk = (ImageView) mView.findViewById(R.id.frag_foodmenu_image_restaurent_background);
        mTextViewRestaurentName = (TextView) mView.findViewById(R.id.frag_foodmenu_text_restaurentname);
        mTextViewRestaurentAddress = (TextView) mView.findViewById(R.id.frag_foodmenu_text_restaurentaddress);

        mRelativeLayoutFoodMenu = (RelativeLayout) mView.findViewById(R.id.frag_foodmenu_layout_foodmenu);
        mLinearLayoutNoFoodMenu = (RelativeLayout) mView.findViewById(R.id.frag_foodmenu_layout_no_foodmenu);
        mImageViewRefreshService = (ImageView) mView.findViewById(R.id.frag_foodmenu_layout_no_foodmenu_refresh_image);
        mImageViewRefreshService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callMenu();
            }
        });

        mNavigationActivity = (NavigationActivity) getActivity();
        // mArrayListMenuList = new ArrayList<NavigationListItemModel>();

        mProgressDialog = new ProgressDialog(mNavigationActivity);
        mImageLoader = new ImageLoader(getActivity());

        // mListView = (ListView) mView
        // .findViewById(R.id.frag_foodmenu_list_menulist);
        mGson = new Gson();
        menugrid = (GridView) mView.findViewById(R.id.menugrid);


        // NavigationListItemModel mItemModel;
        // for (int i = 0; i < menuicon.length; i++) {
        // mItemModel = new NavigationListItemModel(Menu[i], menuicon[i]);
        // mArrayListMenuList.add(mItemModel);
        // mItemModel = null;
        // }

        // mListView.setAdapter(new NavigationListAdapter(getActivity(),
        // mArrayListMenuList));
        //
        // mListView.setOnItemClickListener(new OnItemClickListener() {
        //
        // @Override
        // public void onItemClick(AdapterView<?> parent, View view,
        // int position, long id) {
        //
        // mNavigationActivity.showFragment(
        // mNavigationActivity.mPizzaListFragment, false);
        // mNavigationActivity.backstacklist
        // .add(mNavigationActivity.mFoodMenuFragment);
        // }
        // });
        mRelativeLayoutRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNavigationActivity.showFragment(
                        mNavigationActivity.mRestaurantFullDetailFragment, true);
                mNavigationActivity.mRestaurantFullDetailFragment.SetRetaurent((mRestaurentDataReceived));
                System.out.println("Last Fragment:" + (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1).getTag()));
                if (mNavigationActivity.mRestaurantFullDetailFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                    mNavigationActivity.backstacklist
                            .add(mNavigationActivity.mRestaurantFullDetailFragment);
                    Log.e("BackstackList", "mRestaurantFullDetailFragment Fragment is added.");
                }
                mNavigationActivity.showFragments();
            }
        });

        menugrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("GridClicked");
                mNavigationActivity.showFragment(mNavigationActivity.mPizzaListFragment, true);
                mNavigationActivity.mPizzaListFragment.GetPizzainfo(mRestaurentDataReceived, menulist.get(i).getId());
                System.out.println("Foodmenufrag.onGridItemClick(): Size of Backstacklist: " + mNavigationActivity.backstacklist.size());
//                System.out.println("Last Fragment:" + (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1).getTag()));
                if (mNavigationActivity.mPizzaListFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                    mNavigationActivity.backstacklist.add(mNavigationActivity.mPizzaListFragment);
                    Log.e("BackstackList", "pizzalist Fragment is added.");
                }
                mNavigationActivity.showFragments();

            }
        });
        return mView;
    }

    public void SetRetaurent(PizzaRestro mPizzaRestro) {
        System.out.println("FoodMenuFragment.SetRetaurent() called.");
        ImageLoaderBlur mImageLoaderBlur = new ImageLoaderBlur(getActivity());

        mRestaurentDataReceived = mPizzaRestro;
//        Picasso.with(getActivity()).load(mPizzaRestro.getImg_url()).placeholder(R.drawable.res1).into(mImageViewRestaurentImage);
        mImageLoader.DisplayImage(mPizzaRestro.getImg_url(), mImageViewRestaurentImage);
        //   Picasso.with(getActivity()).load(mPizzaRestro.getImg_url()).placeholder(R.drawable.res1).into(mImageViewRestaurantBk);
        mImageLoaderBlur.DisplayImage(mPizzaRestro.getImg_url(), mImageViewRestaurantBk);

        mTextViewRestaurentName.setText(mPizzaRestro.getName());
        String add = mPizzaRestro.getStreet_name();
        add += ", " + mPizzaRestro.getStreet_no();
        add += "\n" + mPizzaRestro.getApt_no();
        mTextViewRestaurentAddress.setText(add);
        callMenu();

//        Bitmap bitmap;
//        bitmap = BitmapFactory.decodeResource(getActivity().getResources(), mDataModel.getImageID());
//        mImageViewRestaurantBk.setImageBitmap(Utils.fastblur(bitmap, 5));


    }


    public void callMenu() {
        System.out.println("FoodMenuFragment.callMenu() called.");
        mProgressDialog.setMessage("Please wait");
        mProgressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.GET_MENU,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<>();
                System.out.println("Selected Restaurant ID: " + mRestaurentDataReceived.getId());
                params.put("pid", "" + mRestaurentDataReceived.getId());
                return params;
            }
        };
        queue.add(myReq);

    }


    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("FoodMenuFragment.createMyReqSuccessListener() called.");
                Log.e("result Menutype", "" + response);
                GetMenu getMenu = mGson.fromJson(response, GetMenu.class);

                mProgressDialog.dismiss();
                if (getMenu.getResponseCode() == 1) {
                    System.out.println("FoodMenuFragment. gone in responsecode=1");
                    menulist = getMenu.getMenutype();

                    menugrid.setAdapter(new MenuAdapter(getActivity()));
                    mRelativeLayoutFoodMenu.setVisibility(View.VISIBLE);
                    mLinearLayoutNoFoodMenu.setVisibility(View.GONE);

                } else {
                    ShowToast(getMenu.getResponseMsg());
                    mRelativeLayoutFoodMenu.setVisibility(View.GONE);
                    mLinearLayoutNoFoodMenu.setVisibility(View.VISIBLE);
                }

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println("FoodMenuFragment.createMyReqErrorListener() called.");
                mProgressDialog.dismiss();

                ShowToast(error.getMessage() + "");
                mRelativeLayoutFoodMenu.setVisibility(View.GONE);
                mLinearLayoutNoFoodMenu.setVisibility(View.VISIBLE);
            }


        };
    }

    public void ShowToast(String s) {

        Toast.makeText(getActivity(), "" + s, Toast.LENGTH_SHORT).show();
    }

    public void callBasketRestroData() {
        mNavigationActivity.mBasketFragment.SetRestaurantData(mRestaurentDataReceived);
    }


    public class MenuAdapter extends ArrayAdapter<Menu> {
        ViewHolder mViewHolder;

        public MenuAdapter(Context context) {
            super(context, R.layout.menu_item, menulist);
            System.out.println("FoodFragment.MenuAdapter() called.");
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Menu mCurrentData = getItem(position);

            if (convertView == null) {
                mViewHolder = new ViewHolder();
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.menu_item, null);

                mViewHolder.menu_listitem_image = (ImageView) convertView
                        .findViewById(R.id.menu_listitem_image);
                mViewHolder.menu_listitem_itemtext = (TextView) convertView
                        .findViewById(R.id.menu_listitem_itemtext);

                DisplayMetrics displaymetrics = new DisplayMetrics();
                mNavigationActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int height = displaymetrics.heightPixels;
                int width = displaymetrics.widthPixels;
                width /= 2;

                mViewHolder.menu_listitem_image.getLayoutParams().height = width;
                mViewHolder.menu_listitem_image.requestLayout();


                convertView.setTag(mViewHolder);

            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }


//            Picasso.with(getContext()).load(mCurrentData.getImg_url()).placeholder(R.drawable.res1).into(mViewHolder.menu_listitem_image);
            mImageLoader.DisplayImage(mCurrentData.getImg_url(), mViewHolder.menu_listitem_image);
            mViewHolder.menu_listitem_itemtext.setText(mCurrentData
                    .getName());


            return convertView;
        }

        public class ViewHolder {
            ImageView menu_listitem_image;
            TextView menu_listitem_itemtext;
        }

    }
}
