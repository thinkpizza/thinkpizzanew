package com.waycreon.thinkpizza.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.adapter.FavoritePizzaNewAdapter;
import com.waycreon.thinkpizza.datamodels.OrderedItemsDataNew;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FavoriteFragment extends Fragment {

    Store_pref mStore_pref;
    String Userid = "";

    ProgressDialog mProgressDialog;
    Gson mGson;

    ImageView mImageViewRefreshService;
    RelativeLayout mLayoutNo_FavoriteList;
    LinearLayout mLayoutFavoriteList;
    List<OrderedItemsDataNew> mOrderedItemsDatasFavorite;
    public FavoritePizzaNewAdapter mAdapterFavoritePizza;
    ListView mListViewFavorite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_favorite, container, false);
        mLayoutNo_FavoriteList = (RelativeLayout) mView.findViewById(R.id.frag_favorite_layout_no_favorite);
        mLayoutFavoriteList = (LinearLayout) mView.findViewById(R.id.frag_favorite_layout_favoritelist);
        mListViewFavorite = (ListView) mView.findViewById(R.id.frag_favorite_list_favorite_list);
        mImageViewRefreshService = (ImageView) mView.findViewById(R.id.frag_favorite_layout_no_favorite_refresh_image);
        mImageViewRefreshService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callFavoriteService();
            }
        });

        mStore_pref = new Store_pref(getActivity());
        if (mStore_pref.getUserId().length() > 0) {
            Userid = mStore_pref.getUserId();
        }
        mGson = new Gson();


//        new FavoriteSerivce().execute();
        return mView;

    }

    public void callFavoriteService() {
        mLayoutFavoriteList.setVisibility(View.GONE);
        mLayoutNo_FavoriteList.setVisibility(View.GONE);
        new FavoriteSerivce().execute();
    }

    public void callHistoryService() {
        mLayoutFavoriteList.setVisibility(View.GONE);
        mLayoutNo_FavoriteList.setVisibility(View.GONE);
        new HistorySerivce().execute();
    }


    private class FavoriteSerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading Favorites...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
//                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
            HttpGet mHttpGet = new HttpGet(Constants.GET_FAVORITES_NEW + "&user_id=" + Userid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("FavoriteSerivce().DoinBackground()", "Could not reach to server.");
//                Toast.makeText(getActivity(), "Could not reach to server.", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of Favorites:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

                        mLayoutFavoriteList.setVisibility(View.VISIBLE);
                        mLayoutNo_FavoriteList.setVisibility(View.GONE);

                        JSONArray mJsonArray = mJsonObject.getJSONArray("favourites");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasFavorite = new ArrayList<>();

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            OrderedItemsDataNew mOrderedItemsData = new OrderedItemsDataNew();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setOrder_id(temp.getString("order_id"));
                            mOrderedItemsData.setOrder_date(temp.getString("order_date"));
                            mOrderedItemsData.setName(temp.getString("name"));

                            com.waycreon.thinkpizza.datamodels.Address mAddress = new com.waycreon.thinkpizza.datamodels.Address();
                            mAddress.setId(temp.getString("id"));
                            mAddress.setTag_name(temp.getString("tag_name"));
                            mAddress.setLat(temp.getString("lat"));
                            mAddress.setLng(temp.getString("lng"));
                            mAddress.setStreet_name(temp.getString("street_name"));
                            mAddress.setStreet_no(temp.getString("street_no"));
                            mAddress.setApt_no(temp.getString("apt_no"));
                            mAddress.setCity(temp.getString("city"));
                            mAddress.setZip_code(temp.getString("zip_code"));
                            mAddress.setState(temp.getString("state"));

                            mOrderedItemsData.setDeliveryAddress(mAddress);

                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
                        }
                        //LOOP ENDS

                        mAdapterFavoritePizza = new FavoritePizzaNewAdapter(getActivity(), mOrderedItemsDatasFavorite, "FragmentFavorite");
                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
                        mAdapterFavoritePizza.notifyDataSetChanged();

                    } else {
                        Log.e("FavoriteSerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                        mLayoutFavoriteList.setVisibility(View.GONE);
                        mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    mLayoutFavoriteList.setVisibility(View.GONE);
                    mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class HistorySerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading History Order...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
//                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
            HttpGet mHttpGet = new HttpGet(Constants.GET_ORDERS_NEW + "&user_id=" + Userid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("HistorySerivce().DoinBackground()", "Could not reach to server.");
//                Toast.makeText(getActivity(), "Could not reach to server.", Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of History:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

                        mLayoutFavoriteList.setVisibility(View.VISIBLE);
                        mLayoutNo_FavoriteList.setVisibility(View.GONE);

                        JSONArray mJsonArray = mJsonObject.getJSONArray("orders");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasFavorite = new ArrayList<>();

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            OrderedItemsDataNew mOrderedItemsData = new OrderedItemsDataNew();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setOrder_id(temp.getString("order_id"));
                            mOrderedItemsData.setOrder_date(temp.getString("order_date"));
                            mOrderedItemsData.setName(temp.getString("name"));

                            com.waycreon.thinkpizza.datamodels.Address mAddress = new com.waycreon.thinkpizza.datamodels.Address();
                            mAddress.setId(temp.getString("id"));
                            mAddress.setTag_name(temp.getString("tag_name"));
                            mAddress.setLat(temp.getString("lat"));
                            mAddress.setLng(temp.getString("lng"));
                            mAddress.setStreet_name(temp.getString("street_name"));
                            mAddress.setStreet_no(temp.getString("street_no"));
                            mAddress.setApt_no(temp.getString("apt_no"));
                            mAddress.setCity(temp.getString("city"));
                            mAddress.setZip_code(temp.getString("zip_code"));
                            mAddress.setState(temp.getString("state"));

                            mOrderedItemsData.setDeliveryAddress(mAddress);

                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
                        }
                        //LOOP ENDS

                        mAdapterFavoritePizza = new FavoritePizzaNewAdapter(getActivity(), mOrderedItemsDatasFavorite, "HistoryPizza");
                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
                        mAdapterFavoritePizza.notifyDataSetChanged();

                    } else {
                        Log.e("HistorySerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                        mLayoutFavoriteList.setVisibility(View.GONE);
                        mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    mLayoutFavoriteList.setVisibility(View.GONE);
                    mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
                }
                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
            }
        }
    }


//    public class FavoriteSerivce extends AsyncTask<Void, Void, Void> {
//
//        String response = "";
//        List<Address> add;
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mProgressDialog = new ProgressDialog(getActivity());
//            mProgressDialog.setMessage("Loading Favorites...");
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            HttpClient mHttpClient = new DefaultHttpClient();
////                HttpPost mHttpPost = new HttpPost(Constants.GET_FAVORITES + "&user_id=" + Userid);
//            HttpGet mHttpGet = new HttpGet(Constants.GET_FAVORITES + "&user_id=" + Userid);
//            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
//            try {
//                response = mHttpClient.execute(mHttpGet, mResponseHandler);
//            } catch (IOException e) {
//                e.printStackTrace();
//                Log.e("FavoriteSerivce().DoinBackground()", "Could not reach to server.");
////                Toast.makeText(getActivity(), "Could not reach to server.", Toast.LENGTH_SHORT).show();
//            }
//            return null;
//        }
//
//        @SuppressLint("NewApi")
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//
//            mProgressDialog.dismiss();
//            System.out.println("Response of Favorites:" + response);
//            JSONObject mJsonObject;
//            if (response != null && response.length() > 0) {
//                try {
//                    mJsonObject = new JSONObject(response);
//                    System.out.println("Json Object: " + mJsonObject);
//
//                    if (mJsonObject.getString("responseCode").equals("1")) {
//
//                        mLayoutFavoriteList.setVisibility(View.VISIBLE);
//                        mLayoutNo_FavoriteList.setVisibility(View.GONE);
//
//                        JSONArray mJsonArray = mJsonObject.getJSONArray("favourites");
//                        System.out.println("Json Array Object: " + mJsonArray);
//
//                        mOrderedItemsDatasFavorite = new ArrayList<>();
//
//                        for (int i = 0; i < mJsonArray.length(); i++) {
//                            float total_tmp = 0;
//                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
//                            JSONObject temp = mJsonArray.getJSONObject(i);
//
//                            mOrderedItemsData.setId(temp.getString("id"));
//                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
//                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
//                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
////                            mOrderedItemsData.setAdditions(temp.getString("additions"));
////                            mOrderedItemsData.setId(temp.getString("custom_ingredients"));
////                            "custom_ingredients": [],
//                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
//                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
//                            mOrderedItemsData.setQty(1);
//
//                            System.out.println("Orderdata : " + temp);
//
//                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
//                            System.out.println("pizza_details from Gson: " + pizza_details);
//                            mOrderedItemsData.setPizza_details(pizza_details);
//
//
////                            JSONObject pizzadata = temp.getJSONObject("pizza_details");
////                            System.out.println("PizzaDAta by manual: " + pizzadata);
////                            PizzaListData mPizzaListData = new PizzaListData();
////                            mPizzaListData.setId(pizzadata.getString("id"));
////                            mPizzaListData.setName(pizzadata.getString("name"));
//
//                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
//                            System.out.println("pizzaria from Gson: " + pizzaria);
//                            mOrderedItemsData.setPizzaria(pizzaria);
//
//                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            if (temp.getString("pizzacrust").length() > 2) {
//                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
//                                System.out.println("pizzacrust from Gson: " + pizzacrust);
//                                mOrderedItemsData.setPizzacrust(pizzacrust);
//                                total_tmp += pizzacrust.getPrice();
//                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
//                            }
//                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                            if (temp.getString("extra_ingredients").length() > 2) {
//
//                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
//                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
//                                float total_additions = 0;
//                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
//                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
//                                    System.out.println("AdditionData: " + temp_addition);
//                                    AdditionsListData mAdditionsListData = new AdditionsListData();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
////                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//                                    mAdditionsListData.setIsAdditionSelected(true);
//                                    mArrayListAdditionsListDatas.add(mAdditionsListData);
//
//                                    total_additions += mAdditionsListData.getPrice();
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
//                                total_tmp += total_additions;
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
//                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
//                                mArrayListAdditionsListDatas = null;
//                            }
//
//
//                            /*
//                            if (temp.getString("custom_ingredients").length() > 2) {
//
//                                JSONArray mJsonArraycustom_ingredients = temp.getJSONArray("custom_ingredients");
//                                ArrayList<AdditionsListData> mArrayListCustomIngredients = new ArrayList<>();
//                                float total_customs = 0;
//                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
//                                for (int j = 0; j < mJsonArraycustom_ingredients.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArraycustom_ingredients.getJSONObject(j);
//                                    System.out.println("CustomIngredient: " + temp_addition);
//                                    AdditionsListData mAdditionsListData = new AdditionsListData();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//                                    mAdditionsListData.setIsAdditionSelected(true);
//                                    mArrayListCustomIngredients.add(mAdditionsListData);
//
//                                    total_customs += mAdditionsListData.getPrice();
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Custom Pizza Toppings Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
//                                total_tmp += total_customs;
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//
//                                /*
//                                30-9-2015 temporary comment
//                                 */
//                                /*mOrderedItemsData.setCustom_ingredients(mArrayListCustomIngredients);*/
//
//                        /*
//                                System.out.println("extra_ingredients from Gson: " + mJsonArraycustom_ingredients);
////                                mOrderedItemsData.setExtra_ingredients(extra_ingredients);
//                                mArrayListCustomIngredients = null;
//                            }
//                            */
//
//
//                            /*
//                            6-10-2015 code for Toppings selected is added
//                             */
//
//                            if (temp.getString("toppings").length() > 2) {
//
//                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
//                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();
//
//
//                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
//                                    System.out.println("Toppings: " + temp_addition);
//                                    Toppings mAdditionsListData = new Toppings();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
////                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//
//                                    mArrayListToppingses.add(mAdditionsListData);
//
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//
//                                mOrderedItemsData.setToppings(mArrayListToppingses);
//
//
//                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
//                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
//                                mArrayListToppingses = null;
//                            }
//
//                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
//                            System.out.println("address from Gson: " + pizzaria);
//                            mOrderedItemsData.setAddress(address);
//
//                            Log.e("FavoriteSerivce()", "-----------------Favorite Pizzas----------------");
//                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
//                            mOrderedItemsData.printAllData();
//
//                            if (temp.getString("pizza_size").equalsIgnoreCase("regular")) {
//                                System.out.println("Favorite pizza's total set with regular size");
//                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                            } else if (temp.getString("pizza_size").equalsIgnoreCase("medium")) {
//                                System.out.println("Favorite pizza's total set with meduim size");
//                                Log.e("Favorite Service.onPostExecute()", "Small Medium: " + mOrderedItemsData.getPizza_details().getPrice_medium());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_medium());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            } else if (temp.getString("pizza_size").equalsIgnoreCase("large")) {
//                                System.out.println("Favorite pizza's total set with large size");
//                                Log.e("Favorite Service.onPostExecute()", "Small Large: " + mOrderedItemsData.getPizza_details().getPrice_large());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            }
//
//                            mOrderedItemsData.setTotal(total_tmp);
//                            mOrderedItemsData.setIsFromFavorite_History(true);
//                            /*
//                                Added on 10-10-2015
//                            below line is added to set the favoorite id to pizza so that
//                            when user add a pizza to basket at that time while orders the pizza
//                            its address can be updated.
//
//                             */
//                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));
//
//                        }
//
//
//                        /*
//                        old favorite code that was not calculating the total when added to cart
//                         */
//
//                        /*{
//                            float total_tmp = 0;
//                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
//                            JSONObject temp = mJsonArray.getJSONObject(i);
//
//                            mOrderedItemsData.setId(temp.getString("id"));
//                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
//                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
//                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
//
//                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
//                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
//                           *//* mOrderedItemsData.setQty(1);
//                            mOrderedItemsData.setTotal(total_tmp);
//                            System.out.println("Orderdata : " + temp);*//*
//
//                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
//                            System.out.println("pizza_details from Gson: " + pizza_details);
//                            mOrderedItemsData.setPizza_details(pizza_details);
//
//                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
//                            System.out.println("pizzaria from Gson: " + pizzaria);
//                            mOrderedItemsData.setPizzaria(pizzaria);
//
//                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            if (temp.getString("pizzacrust").length() > 2) {
//                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
//                                System.out.println("pizzacrust from Gson: " + pizzacrust);
//                                mOrderedItemsData.setPizzacrust(pizzacrust);
//                                total_tmp += pizzacrust.getPrice();
//                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
//                            }
//                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                            if (temp.getString("extra_ingredients").length() > 2) {
//
//                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
//                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
//                                float total_additions = 0;
//                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
//                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
//                                    System.out.println("AdditionData: " + temp_addition);
//                                    AdditionsListData mAdditionsListData = new AdditionsListData();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//                                    mAdditionsListData.setIsAdditionSelected(true);
//                                    mArrayListAdditionsListDatas.add(mAdditionsListData);
//
//                                    total_additions += mAdditionsListData.getPrice();
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
//                                total_tmp += total_additions;
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
//                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
//                                mArrayListAdditionsListDatas = null;
//                            }
//
//
//                           *//* if (temp.getString("custom_ingredients").length() > 2) {
//
//                                JSONArray mJsonArraycustom_ingredients = temp.getJSONArray("custom_ingredients");
//                                ArrayList<AdditionsListData> mArrayListCustomIngredients = new ArrayList<>();
//                                float total_customs = 0;
//                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
//                                for (int j = 0; j < mJsonArraycustom_ingredients.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArraycustom_ingredients.getJSONObject(j);
//                                    System.out.println("CustomIngredient: " + temp_addition);
//                                    AdditionsListData mAdditionsListData = new AdditionsListData();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//                                    mAdditionsListData.setIsAdditionSelected(true);
//                                    mArrayListCustomIngredients.add(mAdditionsListData);
//
//                                    total_customs += mAdditionsListData.getPrice();
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Custom Pizza Toppings Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//                                Log.e("Favorite Service.onPostExecute()", "Total Custom Pizza Toppings: " + total_customs);
//                                total_tmp += total_customs;
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//
//                                *//**//*
//                                30-9-2015 temporary comment
//                                 *//**//*
//
//                                *//**//*mOrderedItemsData.setCustom_ingredients(mArrayListCustomIngredients);*//**//*
//
//                                System.out.println("extra_ingredients from Gson: " + mJsonArraycustom_ingredients);
//                                mArrayListCustomIngredients = null;
//                            }*//*
//
//
//
//                            *//*
//                            6-10-2015 code for Toppings selected is added
//                             *//*
//
//                            if (temp.getString("toppings").length() > 2) {
//
//                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
//                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();
//
//
//                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {
//
//                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
//                                    System.out.println("Toppings: " + temp_addition);
//                                    Toppings mAdditionsListData = new Toppings();
//                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
//                                    mAdditionsListData.setName(temp_addition.getString("name"));
//                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
//
//                                    mArrayListToppingses.add(mAdditionsListData);
//
//                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
//                                    mAdditionsListData = null;
//                                }
//
//                                mOrderedItemsData.setToppings(mArrayListToppingses);
//
//
//                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
//                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
//                                mArrayListToppingses = null;
//                            }
//
//
//                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
//                            System.out.println("address from Gson: " + pizzaria);
//                            mOrderedItemsData.setAddress(address);
//
//                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
//                            mOrderedItemsData.printAllData();
//
////                            if (temp.getString("pizza_size").equalsIgnoreCase("regular")) {
////                                System.out.println("Favorite pizza's total set with regular size");
////                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
////                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
////                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
////
////                            } else if (temp.getString("pizza_size").equalsIgnoreCase("medium")) {
////                                System.out.println("Favorite pizza's total set with meduim size");
////                                Log.e("Favorite Service.onPostExecute()", "Small Medium: " + mOrderedItemsData.getPizza_details().getPrice_medium());
////                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_medium());
////                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
////                            } else if (temp.getString("pizza_size").equalsIgnoreCase("large")) {
////                                System.out.println("Favorite pizza's total set with large size");
////                                Log.e("Favorite Service.onPostExecute()", "Small Large: " + mOrderedItemsData.getPizza_details().getPrice_large());
////                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
////                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
////                            }
//
//                            mOrderedItemsData.setTotal(0);
//                            mOrderedItemsData.setIsFromFavorite_History(true);
//                            *//*
//                                Added on 10-10-2015
//                            below line is added to set the favoorite id to pizza so that
//                            when user add a pizza to basket at that time while orders the pizza
//                            its address can be updated.
//
//                             *//*
//                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));
//
//                            mOrderedItemsData.setQty(1);
//                            mOrderedItemsData.setTotal(total_tmp);
//                            System.out.println("Orderdata : " + temp);
//                        }*/
//                        //LOOP ENDS
//
//                        mAdapterFavoritePizza = new FavoritePizzaNewAdapter(getActivity(), mOrderedItemsDatasFavorite, "FragmentFavorite");
//                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
//                        mAdapterFavoritePizza.notifyDataSetChanged();
//
//                    } else {
//                        Log.e("FavoriteSerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
//                        Toast.makeText(getActivity(), mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
//                        mLayoutFavoriteList.setVisibility(View.GONE);
//                        mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    mLayoutFavoriteList.setVisibility(View.GONE);
//                    mLayoutNo_FavoriteList.setVisibility(View.VISIBLE);
//                }
//                Toast.makeText(getActivity(), "Response Success", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (!hidden)
//            new FavoriteSerivce().execute();
//    }

}
