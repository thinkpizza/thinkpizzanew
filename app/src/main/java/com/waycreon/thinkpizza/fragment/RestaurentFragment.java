package com.waycreon.thinkpizza.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.adapter.RestaurentListAdapter;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;

import java.util.ArrayList;
import java.util.List;

public class RestaurentFragment extends Fragment {

    //    Gson mGson;
    RestaurentListAdapter adapter;
//    double latitude = 17.385044;
//    double longitude = 78.486671;
//    GPSTracker mGpsTracker;

    List<PizzaRestro> mPizzaRestros = new ArrayList<>();

    ListView mListView;
    NavigationActivity mNavigationActivity;
    LinearLayout mLinearLayoutPizzeriaList;
    LinearLayout mLinearLayoutNoPizzeria;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.listview_only, container, false);
        mLinearLayoutPizzeriaList = (LinearLayout) mView.findViewById(R.id.listview_only_layout_pizzerialist);
        mLinearLayoutNoPizzeria = (LinearLayout) mView.findViewById(R.id.listview_only_layout_nopizzeria);

        mNavigationActivity = (NavigationActivity) getActivity();

        mListView = (ListView) mView.findViewById(R.id.listview_only_list);

//        mGpsTracker = new GPSTracker(getActivity());
//        mGson = new Gson();

//        if (mGpsTracker.canGetLocation()) {
//            latitude = mGpsTracker.getLatitude();
//            longitude = mGpsTracker.getLongitude();
//        }
//        CallRestaurantService();

        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                mNavigationActivity.showFragment(
                        mNavigationActivity.mFoodMenuFragment, true);
                mNavigationActivity.mFoodMenuFragment.SetRetaurent(mPizzaRestros.get(position));
                mNavigationActivity.backstacklist.add(mNavigationActivity.mFoodMenuFragment);
                mNavigationActivity.showFragments();
                Log.e("BackstackList", "FoodMenu Fragment is added.");
            }
        });

        return mView;

    }


    /*public void CallRestaurantService() {
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.GET_PIZARIA_LOC,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {


            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("lat", "" + latitude);
                params.put("lng", "" + longitude);

                return params;
            }

            ;
        };
        queue.add(myReq);

    }*/


   /* private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("result", "" + response);
                GetPizzaria mGetPizzaria = mGson.fromJson(response, GetPizzaria.class);

                if (mGetPizzaria.getResponseCode() == 1) {
                    mPizzaRestros = mGetPizzaria.getPizzaria();
                    adapter = new RestaurentListAdapter(getActivity());
                    mListView.setAdapter(adapter);

                } else {
                    ShowToast(mGetPizzaria.getResponseMsg());
                }

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ShowToast(error.getMessage() + "");
            }


        };
    }

    public void ShowToast(String s) {

        Toast.makeText(getActivity(), "" + s, Toast.LENGTH_SHORT).show();
    }
*/


    void setRestaurantData(List<PizzaRestro> mRestaurantData) {

        try {
            if (mRestaurantData != null && mRestaurantData.size() > 0) {
                mLinearLayoutPizzeriaList.setVisibility(View.VISIBLE);
                mLinearLayoutNoPizzeria.setVisibility(View.GONE);
                mPizzaRestros = mRestaurantData;
                adapter = new RestaurentListAdapter(getActivity(), mPizzaRestros);
                mListView.setAdapter(adapter);
            }
        } catch (Exception e) {
            mLinearLayoutPizzeriaList.setVisibility(View.GONE);
            mLinearLayoutNoPizzeria.setVisibility(View.VISIBLE);
            Log.e("setRestaurantData().RestaurantFragment:", "Error: " + e.getMessage());
        }
    }
}
