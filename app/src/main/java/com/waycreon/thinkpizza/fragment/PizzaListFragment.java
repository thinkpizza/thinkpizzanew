package com.waycreon.thinkpizza.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.Response.GetPizzalist;
import com.waycreon.thinkpizza.adapter.PizzaListItemAdapter;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.thinkpizza.imageloader.ImageLoaderBlur;
import com.waycreon.waycreon.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PizzaListFragment extends Fragment {

    RelativeLayout mRelativeLayoutRestaurant;
    ImageView mImageViewRestaurentImage;
    ImageView mImageViewRestaurantBk;
    TextView mTextViewRestaurentName;
    TextView mTextViewRestaurentAddress;

    LinearLayout mLinearLayoutPizza;
    RelativeLayout mLinearLayoutNoPizza;
    ImageView mImageViewRefreshService;

    Gson mGson;
    PizzaRestro mPizzaRestro;
    String tid;
   public  ListView mListView;
    public GetPizzalist mGetPizzalist;
    public List<PizzaListData> mArrayListPizzaListDatas;
    NavigationActivity mNavigationActivity;

    ProgressDialog mProgressDialog;
    int item_position = -1;
    ImageLoader mImageLoader;

//    ArrayList<Toppings> allToppings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mView = inflater.inflate(R.layout.fragment_pizzalist, container,
                false);
        mNavigationActivity = (NavigationActivity) getActivity();

        mProgressDialog = new ProgressDialog(mNavigationActivity);
        mRelativeLayoutRestaurant = (RelativeLayout) mView.findViewById(R.id.frag_pizzalist_layout_restaurant);
        mImageViewRestaurentImage = (ImageView) mView.findViewById(R.id.frag_pizzalist_image_restaurentimage);
        mImageViewRestaurantBk = (ImageView) mView.findViewById(R.id.frag_pizzalist_image_restaurent_background);
        mTextViewRestaurentName = (TextView) mView.findViewById(R.id.frag_pizzalist_text_restaurentname);
        mTextViewRestaurentAddress = (TextView) mView.findViewById(R.id.frag_pizzalist_text_restaurentaddress);

        mLinearLayoutPizza = (LinearLayout) mView.findViewById(R.id.frag_pizzalist_layout_pizza);
        mLinearLayoutNoPizza = (RelativeLayout) mView.findViewById(R.id.frag_pizzalist_layout_no_pizza);
        mImageViewRefreshService = (ImageView) mView.findViewById(R.id.frag_pizzalist_layout_no_pizza_refresh_image);

        mImageViewRefreshService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callPIzzasList();
            }
        });


        mGson = new Gson();
        mListView = (ListView) mView
                .findViewById(R.id.frag_pizzalist_list_pizzalist);

        mImageLoader = new ImageLoader(getActivity());

        mRelativeLayoutRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNavigationActivity.showFragment(
                        mNavigationActivity.mRestaurantFullDetailFragment, true);
                mNavigationActivity.mRestaurantFullDetailFragment.SetRetaurent((mPizzaRestro));
                System.out.println("Last Fragment:" + (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1).getTag()));
                if (mNavigationActivity.mRestaurantFullDetailFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                    mNavigationActivity.backstacklist
                            .add(mNavigationActivity.mRestaurantFullDetailFragment);
                    Log.e("BackstackList", "mRestaurantFullDetailFragment Fragment is added.");
                }
                mNavigationActivity.showFragments();
            }
        });

        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                PizzaListData mData = mArrayListPizzaListDatas.get(position);
//				mData.setRestaurentData(mRestaurentDataReceived);

                mNavigationActivity.showFragment(mNavigationActivity.mOrderFragment, true);
                mNavigationActivity.mOrderFragment.SetPizzaData(mData, mPizzaRestro, position);
                System.out.println("Last Fragment:" + (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1).getTag()));
                if (mNavigationActivity.mOrderFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                    mNavigationActivity.backstacklist.add(mNavigationActivity.mOrderFragment);
                    Log.e("BackstackList", "mOrderFragment Fragment is added.");
                }
                mNavigationActivity.showFragments();
            }
        });


        return mView;

    }

    public void GetPizzainfo(PizzaRestro mDataModel, String tid) {

        System.out.println("PizzalistFrag.GetPizzainfo() called");
//        if (mNavigationActivity.mBasketFragment.mArrayListOrderedItem.size() > 0) {
//
//        } else {
        mPizzaRestro = mDataModel;
        this.tid = tid;
        mArrayListPizzaListDatas = new ArrayList<>();
        callPIzzasList();
        mListView.setAdapter(new PizzaListItemAdapter(getActivity(), mArrayListPizzaListDatas));

//		mRestaurentDataReceived = mDataModel;

        mImageLoader.DisplayImage(mDataModel.getImg_url(), mImageViewRestaurentImage);
//            Picasso.with(getActivity()).load(mDataModel.getImg_url()).placeholder(R.drawable.res1).into(mImageViewRestaurentImage);
        //  Picasso.with(getActivity()).load(mDataModel.getImg_url()).placeholder(R.drawable.res1).into(mImageViewRestaurantBk);

        ImageLoaderBlur mImageLoaderBlur = new ImageLoaderBlur(getActivity());
        mImageLoaderBlur.DisplayImage(mPizzaRestro.getImg_url(), mImageViewRestaurantBk);

        mTextViewRestaurentName.setText(mDataModel.getName());
        String add = mDataModel.getStreet_name();
        add += ", " + mDataModel.getStreet_no();
        add += "\n" + mDataModel.getApt_no();
        mTextViewRestaurentAddress.setText(add);

//        }

//        allToppings = new ArrayList<>();
    }


    public void callPIzzasList() {
        System.out.println("PizzalistFrag.callPIzzasList() web service called");
        mProgressDialog.setMessage("Please wait");
        mProgressDialog.show();
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.GET_PIZZAS,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {

                Map<String, String> params = new HashMap<>();
                System.out.println("Pid: " + mPizzaRestro.getId());
                System.out.println("Tid: " + tid);

                params.put("pid", "" + mPizzaRestro.getId());
                params.put("tid", "" + tid);
                return params;
            }
        };

        myReq.setRetryPolicy(new DefaultRetryPolicy(5000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(myReq);

    }


    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("PizzalistFrag.createMyReqSuccessListener() web service called");
                Log.e("pizzalist result", "" + response);
                mGetPizzalist = mGson.fromJson(response, GetPizzalist.class);
                mProgressDialog.dismiss();
                if (mGetPizzalist.getResponseCode() == 1) {

                    mArrayListPizzaListDatas = mGetPizzalist.getMenus();
                    mListView.setAdapter(new PizzaListItemAdapter(getActivity(), mArrayListPizzaListDatas));

                    mLinearLayoutPizza.setVisibility(View.VISIBLE);
                    mLinearLayoutNoPizza.setVisibility(View.GONE);

//                    for (int j = 0; j < mArrayListPizzaListDatas.size() - 1; j++) {
//
//                        for (int i = 0; i < mArrayListPizzaListDatas.get(j).getToppings().size() - 1; i++) {
//
//                            if (allToppings.size() == 0) {
//                                allToppings.add(0, mArrayListPizzaListDatas.get(j).getToppings().get(i));
//                            } else {
//                                allToppings.add(allToppings.size(), mArrayListPizzaListDatas.get(j).getToppings().get(i));
//                            }
//                            Log.e("Alltoppings: ", "Topping(" + j + ")(" + i + "): " + allToppings.get(i).getName());
//                        }
//
//                    }


                } else {
                    ShowToast(mGetPizzalist.getResponseMsg());
                    mLinearLayoutPizza.setVisibility(View.GONE);
                    mLinearLayoutNoPizza.setVisibility(View.VISIBLE);
                }

            }
        };
    }


    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("PizzalistFrag.createMyReqErrorListener() web service called");
                mProgressDialog.dismiss();
                ShowToast(error.getMessage() + "");
                mLinearLayoutPizza.setVisibility(View.GONE);
                mLinearLayoutNoPizza.setVisibility(View.VISIBLE);
            }


        };
    }

    public void ShowToast(String s) {

        Toast.makeText(getActivity(), "" + s, Toast.LENGTH_SHORT).show();
    }

    public void callBasketRestroData() {
        mNavigationActivity.mBasketFragment.SetRestaurantData(mPizzaRestro);
    }


    public void SetOrderDetail(int pos) {
//        item_position = pos;
//        if (pos > -1) {
//            PizzaListItemAdapter mItemAdapter = (PizzaListItemAdapter) mListView.getAdapter();
//            System.out.println("PizzaFragment: Position received is " + pos);
//            System.out.println("PizzaFragment: Pizzaname: " + mItemAdapter.mViewHolder.mTextViewPizzaname.getText());
//            mItemAdapter.mViewHolder.mRelativeLayoutPrice.setVisibility(View.GONE);
//            mItemAdapter.mViewHolder.mRelativeLayoutOrderDetail.setVisibility(View.VISIBLE);
//            Float tot = mNavigationActivity.mOrderFragment.mOrderedPizza.getTotalOrder();
//            int q = mNavigationActivity.mOrderFragment.mOrderedPizza.getOrderQuantity();
//            mItemAdapter.mViewHolder.mTextViewOrderDetail.setText("" + q + " X " + (tot / q) + " = " + tot);
//            mItemAdapter.notifyDataSetChanged();
//        } else {
//            Log.e("SetOrderDetail().PizzlistFragment", "invalid Positions received: " + pos);
//        }
    }

}
