package com.waycreon.thinkpizza;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.Response.Register;
import com.waycreon.thinkpizza.datamodels.User;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.GPSTracker;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class SignupActivity extends ActionBarActivity {


    ProgressDialog Dialog;

    Gson mGson;
    User mUser;
    String name = "";
    String cpf_no = "";
    String email = "";
    String password = "";

    String mobile = "";

    String street_name = "";
    String street_no = "";
    String apt_no = "";
    String city = "";
    String state = "";
    String pincode = "";


    EditText name_et;
    EditText cpf_no_et;
    EditText email_et;
    EditText password_et;

    Spinner mSpinnerAreacode;
    EditText mobile_et;

    EditText street_name_et;
    EditText street_no_et;
    EditText apt_no_et;
    EditText city_et;
    EditText state_et;
    AutoCompleteTextView pincode_et;

    ImageView show_password_iv;
    LinearLayout mLinearLayoutSignin;

    android.app.Dialog mDialogAccountActivation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().hide();
        init();
//        setAutocompleteTextAdapter();
        new LoadAddress("").execute();

//        if (getIntent().getExtras().getString("Email").length() > 0) {
        if (getIntent().hasExtra("UserData")) {
            mUser.setEmail(getIntent().getExtras().getString("Email"));
            mUser.setName(getIntent().getExtras().getString("Name"));
            mUser = (User) getIntent().getSerializableExtra("UserData");
            mUser.printAllData();
            email_et.setText(mUser.getEmail());
            email_et.setEnabled(false);
            name_et.setText(mUser.getName());
            cpf_no_et.requestFocus();
        } else {
            Log.e("onCreate.signup.java", "Data is null");
        }

    }

    public void init() {

        mUser = new User();
        mGson = new Gson();

        name_et = (EditText) findViewById(R.id.et_name);
        cpf_no_et = (EditText) findViewById(R.id.et_cpf_number);
        email_et = (EditText) findViewById(R.id.et_email);
        password_et = (EditText) findViewById(R.id.et_password);
        show_password_iv = (ImageView) findViewById(R.id.iv_showpassword);

        mSpinnerAreacode = (Spinner) findViewById(R.id.spinner_area_code);
        mobile_et = (EditText) findViewById(R.id.et_mobile);

        street_name_et = (EditText) findViewById(R.id.et_street_name);
        street_no_et = (EditText) findViewById(R.id.et_street_no);
        apt_no_et = (EditText) findViewById(R.id.et_apt_no);
        city_et = (EditText) findViewById(R.id.et_city);
        state_et = (EditText) findViewById(R.id.et_state);
        pincode_et = (AutoCompleteTextView) findViewById(R.id.et_pincode);


        mLinearLayoutSignin = (LinearLayout) findViewById(R.id.layout_register);
        mLinearLayoutSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        show_password_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("clicked: isPasswordVisible: " + password_et.getInputType());
                if (password_et.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)) {
                    password_et.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    System.out.println(" go in if: isPasswordVisible: " + password_et.getInputType());
                    show_password_iv.setImageResource(R.drawable.e_gray);
                } else {
                    password_et.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    System.out.println("Gone in else: isPasswordVisible: " + password_et.getInputType());
                    show_password_iv.setImageResource(R.drawable.e_red);
                }
                password_et.setSelection(password_et.getText().length());
            }
        });

//        pincode_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH && pincode_et.getText().length() > 0) {
//                    Utils.hide_keyboard(SignupActivity.this);
//                    new LoadAddress(pincode_et.getText().toString()).execute();
//                } else {
//                    Utils.showMyToast(SignupActivity.this, "Please enter Zip Code.", Toast.LENGTH_SHORT);
//                    pincode_et.requestFocus();
//                }
//                return false;
//            }
//        });
        pincode_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

//                    mImageViewSearch.setVisibility(View.VISIBLE);
//                    mAutoCompleteTextViewSearch.setVisibility(View.INVISIBLE);
                    pincode_et.showDropDown();
                    Utils.hide_keyboard(SignupActivity.this);
                    return true;
                }
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

//                    pincode_et.showDropDown();
                    Utils.hide_keyboard(SignupActivity.this);
                    CallBrazilAddress(pincode_et.getText().toString().replaceAll(" ", ""));
                    return true;
                }
                zipcode = pincode_et.getText().toString();
                return false;
            }
        });

        pincode_et.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                String str = (String) arg0.getItemAtPosition(arg2);
                Toast.makeText(SignupActivity.this, str, Toast.LENGTH_SHORT).show();
                new LoadAddress(pincode_et.getText().toString()).execute();
                pincode_et.dismissDropDown();
                Utils.HideSoftKeyBoard(getApplicationContext(), pincode_et);
                pincode_et.clearFocus();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.textview_only, getResources().getStringArray(R.array.area_code));

//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.textview_only);
        mSpinnerAreacode.setAdapter(adapter);

    }

    public void SignUp(View v) {

        name = getText(name_et);
        cpf_no = getText(cpf_no_et);
        email = getText(email_et);
        password = getText(password_et);

        mobile = getText(mobile_et);

        street_name = getText(street_name_et);
        street_no = getText(street_no_et);
        apt_no = getText(apt_no_et);
        city = getText(city_et);
        state = getText(state_et);
        pincode = getText(pincode_et);


        if (name.length() < 2) {

//            ShowToast("Enter Valid Name...");
            Utils.showMyToast(SignupActivity.this, "Enter Valid Name.", Toast.LENGTH_SHORT);
            name_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), name_et);

        } else if (cpf_no.length() < 1) {

//            ShowToast("Enter Valid CPF Number...");
            Utils.showMyToast(SignupActivity.this, "Enter Valid CPF number.", Toast.LENGTH_SHORT);
            cpf_no_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), cpf_no_et);

        } else if (!isValidEmail(email)) {
//            ShowToast("Enter Valid email...");
            Utils.showMyToast(SignupActivity.this, "Enter Valid email.", Toast.LENGTH_SHORT);
            email_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), email_et);

        } else if (password.length() < 01) {
//            ShowToast("Enter is short ...");
            Utils.showMyToast(SignupActivity.this, "Enter password.", Toast.LENGTH_SHORT);
            password_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), password_et);
        } else if (mobile.length() <= 01) {

//            ShowToast("Enter mobile number...");
            Utils.showMyToast(SignupActivity.this, "Enter mobile number.", Toast.LENGTH_SHORT);
            mobile_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), mobile_et);
        } else if (mobile.length() < 9) {

//            ShowToast("Enter mobile number...");
            Utils.showMyToast(SignupActivity.this, "Please enter 9 digit subscriber number.", Toast.LENGTH_SHORT);
            mobile_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), mobile_et);

        } else if (street_name.length() < 01) {

//            ShowToast("Enter street name...");
            Utils.showMyToast(SignupActivity.this, "Enter street name.", Toast.LENGTH_SHORT);
            street_no_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), street_name_et);

        } else if (street_no.length() < 01) {

//            ShowToast("Enter street no...");
            Utils.showMyToast(SignupActivity.this, "Enter street number.", Toast.LENGTH_SHORT);
            street_no_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), street_no_et);

        } else if (apt_no.length() < 1) {

//            ShowToast("Enter appartment no...");
            Utils.showMyToast(SignupActivity.this, "Enter appartment no.", Toast.LENGTH_SHORT);
            apt_no_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), apt_no_et);

        } else if (city.length() < 1) {

//            ShowToast("Enter City...");
            Utils.showMyToast(SignupActivity.this, "Enter city.", Toast.LENGTH_SHORT);
            city_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), city_et);

        } else if (state.length() < 1) {

//            ShowToast("Enter State...");
            Utils.showMyToast(SignupActivity.this, "Enter State", Toast.LENGTH_SHORT);
            state_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), state_et);

        } else if (pincode.length() < 1) {

//            ShowToast("Enter Pincode...");
            Utils.showMyToast(SignupActivity.this, "Enter Pincode.", Toast.LENGTH_SHORT);
            pincode_et.requestFocus();
            Utils.showSoftKeyBoard(getApplicationContext(), pincode_et);

        } else {

            mUser.setName(name);
            mUser.setCpf_no(cpf_no);
            mUser.setEmail(email);
            mUser.setPassword(password);

//            mobile = "+55" + mSpinnerAreacode.getSelectedItem() + " " + mobile_et.getText();
            mobile = mSpinnerAreacode.getSelectedItem() + " " + mobile_et.getText();
            mUser.setMobile(mobile);

            mUser.setStreet_name(street_name);
            mUser.setStreet_no(street_no);
            mUser.setApt_no(apt_no);
            mUser.setCity(city);
            mUser.setState(state);
            mUser.setPincode(pincode);

            callReg();
        }
    }

    private void dismissProgressDialog() {
        if (Dialog != null && Dialog.isShowing()) {
            Dialog.dismiss();
        }
    }

    public String getText(EditText et) {

        if (et != null) {
            return et.getText().toString().trim();
        } else {
            return "";
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void ShowToast(String s) {

        if (s.length() > 0) {
            Toast.makeText(SignupActivity.this, s, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mUser.getActivate().equalsIgnoreCase("no")) {
            callDeleteService();
        }
        Store_pref mStore_pref;
        mStore_pref = new Store_pref(this);
        if (!mStore_pref.isLogin()) {
            Intent main = new Intent(SignupActivity.this, MainActivity.class);
            startActivity(main);
            finish();
        }
    }


    /*
    Register Service
     */
    public void callReg() {
        Dialog = new ProgressDialog(SignupActivity.this);
        Dialog.setMessage("Loading...");
        Dialog.setCancelable(false);
        Dialog.show();
        RequestQueue queue = Volley.newRequestQueue(SignupActivity.this);

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.REGISTER,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String userdata = mGson.toJson(mUser);
                params.put("data", userdata);
                Log.e("callReg().SignupAcvtivity.java", "Data:--> " + userdata);
                return params;
            }
        };
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("RegisterResponse: " + response);
                dismissProgressDialog();
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {

//                    Intent i = new Intent(SignupActivity.this, LoginActivity.class);
//                    i.putExtra("email", mUser.getEmail());
//                    i.putExtra("UserData", mUser);
//                    i.putExtra("showdialog", "1");
//                    startActivity(i);
//                    finish();
                    mUser.setId(register.getUser_id());
                    mUser.setActivate("no");
                    showAccountActivationDialog();

                } else if (register.getResponseCode() == 2) {
                    email_et.setEnabled(true);
                    email_et.requestFocus();
                    email_et.selectAll();
                } else if (register.getResponseCode() == 3) {
                    cpf_no_et.requestFocus();
                }
                ShowToast(register.getResponseMsg());

            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dismissProgressDialog();
                ShowToast(error.getMessage() + "");
            }


        };
    }

    /*
    Register service ends
     */
    void showAccountActivationDialog() {

        mDialogAccountActivation = new Dialog(SignupActivity.this);
        mDialogAccountActivation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogAccountActivation.setContentView(R.layout.dialog_email_confirm);

        EditText mEditTextCpfno;
        EditText mEditTextEmail;
        final EditText mEditTextCode;

//        mEditTextCpfno = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_cpfno);
        mEditTextEmail = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_email);
        mEditTextCode = (EditText) mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_code);
//        Button mButtonActivateAccount mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_code);

        mEditTextEmail.setText(mUser.getEmail());
//        mEditTextCpfno.setText(mUser.getCpf_no());

        mDialogAccountActivation.findViewById(R.id.dialog_email_confirm_button_activateaccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEditTextCode.getText().toString().length() > 0 && mEditTextCode.getText().toString().length() == 7) {

                    ShowToast("Please enter six digit activation code sent to your mail.");
                } else {
                    mUser.setCode(mEditTextCode.getText().toString());
                    callLogin();
                }

            }
        });

        mDialogAccountActivation.show();
    }

    /*
    Login Service
     */
    public void callLogin() {
        RequestQueue queue = Volley.newRequestQueue(this);
        Dialog.show();
        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.LOGIN,
                createLoginSuccessListener(),
                createLoginErrorListener()) {

            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String userdata = mGson.toJson(mUser);
                Log.e("Login.java", "User Gson: " + userdata);
                params.put("data", userdata);
                return params;
            }
        };
        queue.add(myReq);
    }

    private Response.Listener<String> createLoginSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Dialog.dismiss();

                ShowToast(response + "");
                Log.e("result", "" + response);
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {

                    Store_pref mStore_pref = new Store_pref(getApplicationContext());
                    mStore_pref.setUser(register.getUser());
                    mStore_pref.setUserId(register.getUser().getId());

                    Intent i = new Intent(SignupActivity.this, NavigationActivity.class);
                    i.putExtra("email", mUser.getEmail());
                    startActivity(i);
                    finish();
                    mDialogAccountActivation.dismiss();
                }
                ShowToast(register.getResponseMsg());
            }
        };
    }

    private Response.ErrorListener createLoginErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Dialog.dismiss();
                ShowToast(error.getMessage() + "");
            }
        };
    }

    /*
    Delete User serice
     */

    String zipcode = "";

    public class LoadAddress extends AsyncTask<Void, Void, Void> {

        String response = "";
        List<Address> add;
        ProgressDialog mProgressDialog;
        double latitude = 21.1305697;
        double longitude = 72.7903613;
        GPSTracker mGpsTracker;
//        com.waycreon.thinkpizza.datamodels.Address mAddressPostalAddressSelected;

        String searchAddress = "";

        LoadAddress(String address) {
            searchAddress = address;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(SignupActivity.this);
            mGpsTracker = new GPSTracker(getApplicationContext());

            if (mGpsTracker.canGetLocation()) {

                try {

                    mProgressDialog.setMessage("Finding Location...");
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                    latitude = mGpsTracker.getLatitude();
                    longitude = mGpsTracker.getLongitude();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
                if (searchAddress.length() > 0) {
                    add = geo.getFromLocationName(searchAddress, 5);
                } else {
                    Log.e("doInBackground", "latitude: " + latitude);
                    Log.e("doInBackground", "longitude: " + longitude);
                    add = geo.getFromLocation(latitude, longitude, 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();

            if (add != null && add.size() > 0) {

                System.out.println("Max addressline index: " + add.get(0).getMaxAddressLineIndex());
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));

                System.out.println("Longitude: " + add.get(0).getLongitude());
                System.out.println("Latitude: " + add.get(0).getLatitude());

                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Postal code: " + add.get(0).getPostalCode());

                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());

        /*
                mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) == null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2));
                else if (add.get(0).getAddressLine(2) != null && add.get(0).getAddressLine(3) != null)
                    mAddressPostalAddressSelected.setApt_no(add.get(0).getAddressLine(1) + ",\n" +
                            add.get(0).getAddressLine(2) + ",\n" + add.get(0).getAddressLine(3));

                mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());

                if (add.get(0).getSubAdminArea() == null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                else
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                street_name_et.setText(add.get(0).getAddressLine(0));
//                city_et.setText(add.get(0).get);
        */
                if (add.get(0).getThoroughfare() != null)
                    street_name_et.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    street_name_et.setText(add.get(0).getFeatureName());
                else
                    street_name_et.setText(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    street_no_et.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    city_et.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    city_et.setText(add.get(0).getLocality());

                if (add.get(0).getAdminArea() != null)
                    state_et.setText(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    pincode_et.setText(add.get(0).getPostalCode());
                else
                    pincode_et.setText(zipcode);

                mUser.setLat("" + add.get(0).getLatitude());
                mUser.setLng("" + add.get(0).getLongitude());

            } else {
                System.out.println("Address Is null:");
                Toast.makeText(getApplicationContext(), "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /*
        Autocomplete Address Methods starts
     */
    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(Constants.PLACES_API_BASE + Constants.TYPE_AUTOCOMPLETE + Constants.OUT_JSON);
            sb.append("?key=" + Constants.GOOGLE_PLACES_API);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("Mapfragment: URL: " + url);
            Log.e("Mapfragment: URL: ", "url" + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("autocomplete().Mapfragment", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("autocomplete().Mapfragment", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("autocomplete().Mapfragment", "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public void setAutocompleteTextAdapter() {
        System.out.println("setAutocompleteTextAdapter() is called");
        pincode_et.setAdapter(new GooglePlacesAutocompleteAdapter(SignupActivity.this, R.layout.list_item));
    }
    /*
        Autocomplete Address Methods Ends
     */

    /*
        Delete User
     */

    public void callDeleteService() {
        Dialog = new ProgressDialog(SignupActivity.this);
        Dialog.setMessage("Please wait...");
        Dialog.setCancelable(false);
        Dialog.show();
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest myReq = new StringRequest(Request.Method.GET, Constants.DELETE_USER + "&user_id=" + mUser.getId(),
                DeleteSuccess(),
                DeleteFailure());
        queue.add(myReq);
    }

    private Response.Listener<String> DeleteSuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Dialog.dismiss();

                ShowToast(response + "");
                Log.e("result", "" + response);
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {
                    Utils.showMyToast(SignupActivity.this, "Registration process cleared successfully.", Toast.LENGTH_SHORT);
                }
//                ShowToast(register.getResponseMsg());
            }
        };
    }

    private Response.ErrorListener DeleteFailure() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Dialog.dismiss();
                ShowToast(error.getMessage() + "");
            }
        };
    }

    /*
    New CEP Code started
     */
      /*
  Below Service will be called when user enters the pin code of brazil to get address
   */
    String brazil_CEP_search = "";
ProgressDialog mDialogCEP;
    public void CallBrazilAddress(String Pin) {

        mDialogCEP = new ProgressDialog(SignupActivity.this);
        mDialogCEP.setMessage("Please Wait...");
        mDialogCEP.setCancelable(false);
        mDialogCEP.show();

        String cepurl = "http://cep.republicavirtual.com.br/web_cep.php?cep=" + Pin;
        System.out.println("CEP URL: " + cepurl);
        brazil_CEP_search = Pin;
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest myReq = new StringRequest(Request.Method.GET, cepurl,
                BrazilAddresssuccess(), BrazilAddressfail());
        queue.add(myReq);
    }

    private Response.Listener<String> BrazilAddresssuccess() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mDialogCEP.dismiss();
                System.out.println("webservicecep Response :" + response);
                String brazil_address = "";

                try {

                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new InputSource(new StringReader(response)));
                    // normalize the document
                    doc.getDocumentElement().normalize();
                    // get the root node
                    NodeList nodeList = doc.getElementsByTagName("webservicecep");
                    Node node = nodeList.item(0);
                    System.out.println("webservicecep TextContent :" + node.getTextContent());

                    // the  node has three child nodes
                    boolean isAddressfound = true;

                    for (int j = 0; j < node.getChildNodes().getLength(); j++) {
                        System.out.println("J=" + j);
                        String nameofattribute = node.getChildNodes().item(j).getNodeName();
                        if (nameofattribute.equalsIgnoreCase("resultado")
                                && node.getChildNodes().item(j).getTextContent().equalsIgnoreCase("1")) {
                            System.out.println("GONE when j=" + j);
                            isAddressfound = true;
                            break;
                        } else
                            isAddressfound = false;
                    }

                    if (isAddressfound) {
                        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                            System.out.println("GONE  I=" + i);
                            Node temp = node.getChildNodes().item(i);
                            System.out.println("webservicecep getNodeName: " + temp.getNodeName());
                            Log.e("getTextContent: ", "webservicecep getTextContent: " + temp.getTextContent());

                            if (temp.getNodeName().equalsIgnoreCase("cidade")) {
                                System.out.println("GONE logradouro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("bairro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("tipo_logradouro")) {
                                System.out.println("GONE bairro");
                                brazil_address += " " + temp.getTextContent() + "#";
                            } else if (temp.getNodeName().equalsIgnoreCase("logradouro")) {
                                System.out.println("GONE cidade");
                                brazil_address += " " + temp.getTextContent();
                            }
                            System.out.println("Brazil Address[" + i + "]: " + brazil_address);
                        }
                        System.out.println("Brazil Address: " + brazil_address);
                    }
                    /*
                    Now below condition
                    "if (brazil_address.replaceAll(" ", "").length() == 0)"
                    will decide that set the "mNavigationActivity.mAutoCompleteTextViewSearch" with
                    Google places api adapter or to return the brazil address

                     */
                    if (brazil_address.replaceAll(" ", "").length() == 0) {
                        Log.e("BrazilAddresssuccess().MapFragment", "could not find the give brazil address: " + brazil_CEP_search);
                        Toast.makeText(SignupActivity.this, "Sorry, Address with CEP " + brazil_CEP_search + " could not be found.", Toast.LENGTH_SHORT).show();
//                        mNavigationActivity.showAutocompleteTextview();
//                        mNavigationActivity.mAutoCompleteTextViewSearch.setText(brazil_CEP_search);
//                        Timer timer = new Timer();
//                        timer.schedule(new TimerTask() {
//                            @Override
//                            public void run() {
//                                GoogleAddressesresultList = autocomplete(brazil_CEP_search);
//                                System.out.println("size of :" + GoogleAddressesresultList.size());
//                                /*
//                                here "GoogleAddressesresultList" i set the array of serach result to arraylist
//                                 to over come null pointer error in adapter of "GooglePlacesAutocompleteAdapter"
//                                 */
//
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        setAutocompleteTextAdapter();
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.setAdapter(
//                                                new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.list_item));
//                                        mNavigationActivity.mAutoCompleteTextViewSearch.showDropDown();
//                                    }
//                                });
//
//                            }
//                        }, 100);
//
//
//                        /*
//                        setAutocompleteTextAdapter();
//                        using above line the Placesapi adapter will be set for auto complete textview.
//                         */
                    } else {
//                        getLocationPoints(brazil_address);
                        new LoadLocationFromCEP().execute(brazil_address);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };
    }

    private Response.ErrorListener BrazilAddressfail() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                mDialogCEP.dismiss();

                Log.e("createMyReqErrorListener().MapFragment", "Error: " + error.getMessage());
                Toast.makeText(SignupActivity.this, "Sorry, your address could not be found.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    public class LoadLocationFromCEP extends AsyncTask<String, Void, String> {

        String response = "";
        List<Address> add;
        String brazil_address_received = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            Dialog = new ProgressDialog(SignupActivity.this);
//            Dialog.setMessage("Finding CEP Location...");
//            Dialog.setCancelable(false);
//            Dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            if (params.length == 0)
                return "";
            try {
                System.out.println("------------------getLocationPoints()----------------");
                System.out.println("Address Received: " + params[0]);
                brazil_address_received = params[0];
                System.out.println("Address Received Length: " + params[0].length());

                Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
                add = geo.getFromLocationName(params[0], 1);

            } catch (Exception e) {
                e.printStackTrace();
                Tags.ADDRESS_NUMBER = 0;
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

//            Dialog.dismiss();
            String address[] = brazil_address_received.split("#");
            if (add != null && add.size() > 0) {
                Log.e("LoadLocationFromCEP()", "-----------------------LoadLocationFromCEP--------------");
                System.out.println("Address Line (0): " + add.get(0).getAddressLine(0));
                System.out.println("Address Line (1): " + add.get(0).getAddressLine(1));
                System.out.println("Address Line (2): " + add.get(0).getAddressLine(2));
                System.out.println("Address Line (3): " + add.get(0).getAddressLine(3));
                System.out.println("Admin Area: " + add.get(0).getAdminArea());
                System.out.println("SubAdmin Area: " + add.get(0).getSubAdminArea());
                System.out.println("Country Name: " + add.get(0).getCountryName());
                System.out.println("Country Code: " + add.get(0).getCountryCode());
                System.out.println("Feature Name: " + add.get(0).getFeatureName());
                System.out.println("Locality: " + add.get(0).getLocality());
                System.out.println("Phone: " + add.get(0).getPhone());
                System.out.println("Postal code: " + add.get(0).getPostalCode());
                System.out.println("Premises: " + add.get(0).getPremises());
                System.out.println("SubLocality: " + add.get(0).getSubLocality());
                System.out.println("Through Fare: " + add.get(0).getThoroughfare());
                System.out.println("Sub Through Fare: " + add.get(0).getSubThoroughfare());
                System.out.println("Locale: " + add.get(0).getLocale());
                System.out.println("Extras: " + add.get(0).getExtras());
                System.out.println("Url " + add.get(0).getUrl());
                System.out.println("Longitude" + add.get(0).getLongitude());
                System.out.println("Latitude" + add.get(0).getLatitude());

                Log.e("getLocationPoint()", "Full Address: " + add.get(0));
                System.out.println("getLocationPoint() Longitude" + add.get(0).getLongitude());
                System.out.println("getLocationPoint() Latitude" + add.get(0).getLatitude());


                if (add.get(0).getThoroughfare() != null)
                    street_name_et.setText(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    street_name_et.setText(add.get(0).getFeatureName());
                else
                    street_name_et.setText(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    street_no_et.setText(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    city_et.setText(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    city_et.setText(add.get(0).getLocality());
                else {
                        /*
                        this else part is added because some time city name is not coming in G places
                         */
                    city_et.setText(address[0]);
                }


                if (add.get(0).getAdminArea() != null)
                    state_et.setText(add.get(0).getAdminArea());


                if (add.get(0).getPostalCode() != null)
                    pincode_et.setText(add.get(0).getPostalCode());
                else
//                    pincode_et.setText(zipcode);
                    pincode_et.setText(brazil_CEP_search);


                mUser.setLat("" + add.get(0).getLatitude());
                mUser.setLng("" + add.get(0).getLongitude());

//                Original code
                /*

                latitude = add.get(0).getLatitude();
                longitude = add.get(0).getLongitude();


                Log.e("getLocationPoints().MapFragment", "Address line 1: " + add.get(0).getAddressLine(0));
                    *//*if (add.get(0).getAddressLine(3).length() > 0) {
                        Log.e("getLocationPoints().MapFragment", "Address line 2" + add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                        mNavigationActivity.mAddressFragment.setPostalAddress(add.get(0).getAddressLine(0), add.get(0).getAddressLine(2) + " " + add.get(0).getAddressLine(3));
                    } else {*//*
                Log.e("getLocationPoints().MapFragment", "Address line 2 & 3: " + add.get(0).getAddressLine(1) + " " + add.get(0).getAddressLine(2));

                mAddressPostalAddressSelected.setTag_name("Manual Address (By CEP)");
                if (add.get(0).getThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getThoroughfare());
                else if (add.get(0).getFeatureName() != null)
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getFeatureName());
                else
                    mAddressPostalAddressSelected.setStreet_name(add.get(0).getAddressLine(0));

                if (add.get(0).getSubThoroughfare() != null)
                    mAddressPostalAddressSelected.setStreet_no(add.get(0).getSubThoroughfare());

                if (add.get(0).getSubAdminArea() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getSubAdminArea());
                else if (add.get(0).getLocality() != null)
                    mAddressPostalAddressSelected.setCity(add.get(0).getLocality());
                else {
                        *//*
                        this else part is added because some time city name is not coming in G places
                         *//*
                    mAddressPostalAddressSelected.setCity(address[0]);
                }

                if (add.get(0).getAdminArea() != null)
                    mAddressPostalAddressSelected.setState(add.get(0).getAdminArea());

                if (add.get(0).getPostalCode() != null)
                    mAddressPostalAddressSelected.setZip_code(add.get(0).getPostalCode());
                else {
                    mAddressPostalAddressSelected.setZip_code(brazil_CEP_search);
                }
//                    else
//                        mAutoCompleteTextViewPincode.setText(zipcode);

                mAddressPostalAddressSelected.setLat("" + add.get(0).getLatitude());
                mAddressPostalAddressSelected.setLng("" + add.get(0).getLongitude());
*/

            } else {
                Tags.ADDRESS_NUMBER = 0;
                System.out.println("Address Is null:");
                Toast.makeText(SignupActivity.this, "Unable to locate current address.", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
