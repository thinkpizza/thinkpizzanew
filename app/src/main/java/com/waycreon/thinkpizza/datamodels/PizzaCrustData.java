package com.waycreon.thinkpizza.datamodels;

/**
 * Created by abcd on 8/6/2015.
 */
public class PizzaCrustData {


    String id = "";
    String menus_id = "";
    String name = "";
    float price = 0;
    String pid = "";
    boolean isPizzaCrustSelected = false;

    public PizzaCrustData(String name, float price) {

        this.name = name;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenus_id() {
        return menus_id;
    }

    public void setMenus_id(String menus_id) {
        this.menus_id = menus_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public boolean isPizzaCrustSelected() {
        return isPizzaCrustSelected;
    }

    public void setIsPizzaCrustSelected(boolean isPizzaCrustSelected) {
        this.isPizzaCrustSelected = isPizzaCrustSelected;
    }

    public void printAllData() {
        System.out.println("-------------------pizzacrust Data---------------------------");

        System.out.println("id: "+this.id);
        System.out.println("menus_id: "+this.menus_id);
        System.out.println("pid: "+this.pid);
        System.out.println("name: "+this.name);
        System.out.println("price: "+this.price);
        System.out.println("isPizzaCrustSelected: "+this.isPizzaCrustSelected);

    }
}
