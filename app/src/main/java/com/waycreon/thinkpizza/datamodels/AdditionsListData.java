package com.waycreon.thinkpizza.datamodels;

import java.io.Serializable;

public class AdditionsListData implements Serializable {

    String id = "";
//    String menus_id = "";
    String pid = "";
    String name = "";
    float price = 0;


    boolean isAdditionSelected = false;

    public AdditionsListData(String name, float price) {

        this.name = name;
        this.price = price;
    }

    public AdditionsListData() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

/*
    public String getMenus_id() {
        return menus_id;
    }

    public void setMenus_id(String menus_id) {
        this.menus_id = menus_id;
    }
*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public boolean isAdditionSelected() {
        return isAdditionSelected;
    }

    public void setIsAdditionSelected(boolean isAdditionSelected) {
        this.isAdditionSelected = isAdditionSelected;
    }

    public void printAllData(String tmp) {
        System.out.println("-------------------" + tmp + "Data---------------------------");

        System.out.println("id: " + this.id);
//        System.out.println("menus_id: " + this.menus_id);
        System.out.println("pid: " + this.pid);
        System.out.println("name: " + this.name);
        System.out.println("price: " + this.price);
        System.out.println("isAdditionSelected: " + this.isAdditionSelected);

    }
}
