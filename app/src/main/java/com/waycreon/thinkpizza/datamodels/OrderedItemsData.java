package com.waycreon.thinkpizza.datamodels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abcd on 8/6/2015.
 */
public class OrderedItemsData implements Serializable {

    String responseMsg;
    int responseCode;
    List<OrderedItemsData> favourites = new ArrayList<>();

    public List<OrderedItemsData> getFavourites() {
        return favourites;
    }

    public void setFavourites(List<OrderedItemsData> pizzaria) {
        this.favourites = pizzaria;
    }

    List<OrderedItemsData> orders = new ArrayList<>();

    public List<OrderedItemsData> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderedItemsData> pizzaria) {
        this.orders = pizzaria;
    }


    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }


    String id = "";
    String order_id = "";
    String user_id = "";
    //    String menus_id = "";
    String menu_id = "";
    String address_id = "";
    //    String additions = "";
    //    String custom_ingredients = "";
    String pizza_crust_id = "";
    String pizza_size = "";
    double  qty = 0;
    double total = 0;

    String payment_type = "";
    String payment_details = "";
    String order_date = "";
    String status = "";
//CustomPizza

    PizzaListData pizza_details;
    PizzaRestro pizzaria;
    PizzaCrustData pizzacrust;
    ArrayList<AdditionsListData> extra_ingredients;
    //        ArrayList<AdditionsListData> custom_ingredients;
    ArrayList<Toppings> toppings;
    Address address;

    boolean isFromFavorite_History = false;
    double amount_received = 0;

    public void printAllData() {
        System.out.println("-------------------Order/Favorite Data---------------------------");

        System.out.println("id: " + this.id);
        System.out.println("user_id: " + this.user_id);
//        if (menus_id != null)
//            System.out.println("menus_id: " + this.menus_id);
        if (menu_id != null)
            System.out.println("menu_id: " + this.menu_id);
        System.out.println("address_id: " + this.address_id);
//        System.out.println("additions: " + this.additions);
//        System.out.println("custom_ingredients: " + this.custom_ingredients);
        System.out.println("pizza_crust_id: " + this.pizza_crust_id);
        System.out.println("pizza_size: " + this.pizza_size);
        System.out.println("qty: " + this.qty);
        System.out.println("total: " + this.total);


        if (pizza_details != null)
            pizza_details.printAllData();

        if (pizzaria != null)
            pizzaria.printAllData();

        if (pizzacrust != null)
            pizzacrust.printAllData();
        if (extra_ingredients != null)
            for (int i = 0; i < extra_ingredients.size(); i++)
                extra_ingredients.get(i).printAllData("Extra Ingredients");
        /*if (custom_ingredients != null) {
            System.out.println("---------------custom_ingredients------------");
            for (int i = 0; i < custom_ingredients.size(); i++)
                custom_ingredients.get(i).printAllData();
//                custom_ingredients.get(i).printAllData("Custom Pizza");
        }*/
        if (toppings != null) {
            System.out.println("---------------Selected Toppings------------");
            for (int i = 0; i < toppings.size(); i++)
                toppings.get(i).printAllData();
//                custom_ingredients.get(i).printAllData("Custom Pizza");
        }

        if (address != null)
            address.printAllData();

        if (isFromFavorite_History) {
            System.out.println("----------isFromFavorite_History: " + isFromFavorite_History);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

//    public String getMenus_id() {
//        return menus_id;
//    }
//
//    public void setMenus_id(String menus_id) {
//        this.menus_id = menus_id;
//    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

//    public String getAdditions() {
//        return additions;
//    }
//
//    public void setAdditions(String additions) {
//        this.additions = additions;
//    }

//    public String getCustom_ingredients() {
//        return custom_ingredients;
//    }
//
//    public void setCustom_ingredients(String custom_ingredients) {
//        this.custom_ingredients = custom_ingredients;
//    }

    public String getPizza_crust_id() {
        return pizza_crust_id;
    }

    public void setPizza_crust_id(String pizza_crust_id) {
        this.pizza_crust_id = pizza_crust_id;
    }

    public String getPizza_size() {
        return pizza_size;
    }

    public void setPizza_size(String pizza_size) {
        this.pizza_size = pizza_size;
    }

    public double  getQty() {
        return qty;
    }

    public void setQty(double  qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPayment_details() {
        return payment_details;
    }

    public void setPayment_details(String payment_details) {
        this.payment_details = payment_details;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PizzaListData getPizza_details() {
        return pizza_details;
    }

    public void setPizza_details(PizzaListData pizza_details) {
        this.pizza_details = pizza_details;
    }

    public PizzaRestro getPizzaria() {
        return pizzaria;
    }

    public void setPizzaria(PizzaRestro pizzaria) {
        this.pizzaria = pizzaria;
    }

    public PizzaCrustData getPizzacrust() {
        return pizzacrust;
    }

    public void setPizzacrust(PizzaCrustData pizzacrust) {
        this.pizzacrust = pizzacrust;
        pizzacrust.setIsPizzaCrustSelected(true);
    }

    public ArrayList<AdditionsListData> getExtra_ingredients() {
        return extra_ingredients;
    }

    public void setExtra_ingredients(ArrayList<AdditionsListData> extra_ingredients) {
        this.extra_ingredients = extra_ingredients;
    }

//    public ArrayList<AdditionsListData> getCustom_ingredients() {
//        return custom_ingredients;
//    }
//
//    public void setCustom_ingredients(ArrayList<AdditionsListData> custom_ingredients) {
//        this.custom_ingredients = custom_ingredients;
//    }


    public ArrayList<Toppings> getToppings() {
        return toppings;
    }

    public void setToppings(ArrayList<Toppings> toppings) {
        this.toppings = toppings;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isFromFavorite_History() {
        return isFromFavorite_History;
    }

    public void setIsFromFavorite_History(boolean isFromFavorite_History) {
        this.isFromFavorite_History = isFromFavorite_History;
    }

    public double getAmount_received() {
        return amount_received;
    }

    public void setAmount_received(double amount_received) {
        this.amount_received = amount_received;
    }
}
