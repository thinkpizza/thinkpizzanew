package com.waycreon.thinkpizza.datamodels;

public class Toppings {

    String id = "";
    String menus_id = "";
    String pid = "";
    String name = "";
    float price = 0;

    boolean isToppingSelected = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenus_id() {
        return menus_id;
    }

    public void setMenus_id(String menus_id) {
        this.menus_id = menus_id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isToppingSelected() {
        return isToppingSelected;
    }

    public void setIsToppingSelected(boolean isToppingSelected) {
        this.isToppingSelected = isToppingSelected;
    }

    public void printAllData() {
        System.out.println("------------------------------Toppings----------------------");
        System.out.println("id: " + id);
        System.out.println("menus_id: " + menus_id);
        System.out.println("name: " + name);
        System.out.println("price: " + price);
        System.out.println("isToppingSelected: " + isToppingSelected);
        System.out.println("-------------------------------------------------------------");

    }

    //    String FavoriteID = "";
//
//    PizzaListData mPizzaListData;
//    PizzaCrustData mPizzaCrustData;
//    AdditionsListData mAdditionsListDataExtra;
//    AdditionsListData mAdditionsListDataCustom;
//    Address mAddress;
//
//
//    public String getFavoriteID() {
//        return FavoriteID;
//    }
//
//    public void setFavoriteID(String favoriteID) {
//        FavoriteID = favoriteID;
//    }
//
//    public PizzaListData getmPizzaListData() {
//        return mPizzaListData;
//    }
//
//    public void setmPizzaListData(PizzaListData mPizzaListData) {
//        this.mPizzaListData = mPizzaListData;
//    }
//
//    public PizzaCrustData getmPizzaCrustData() {
//        return mPizzaCrustData;
//    }
//
//    public void setmPizzaCrustData(PizzaCrustData mPizzaCrustData) {
//        this.mPizzaCrustData = mPizzaCrustData;
//    }
//
//    public AdditionsListData getmAdditionsListDataExtra() {
//        return mAdditionsListDataExtra;
//    }
//
//    public void setmAdditionsListDataExtra(AdditionsListData mAdditionsListDataExtra) {
//        this.mAdditionsListDataExtra = mAdditionsListDataExtra;
//    }
//
//    public AdditionsListData getmAdditionsListDataCustom() {
//        return mAdditionsListDataCustom;
//    }
//
//    public void setmAdditionsListDataCustom(AdditionsListData mAdditionsListDataCustom) {
//        this.mAdditionsListDataCustom = mAdditionsListDataCustom;
//    }
//
//    public Address getmAddress() {
//        return mAddress;
//    }
//
//    public void setmAddress(Address mAddress) {
//        this.mAddress = mAddress;
//    }
//
//    public void printAllData() {
//        System.out.println("-------------------Favorite Data---------------------------");
//        System.out.println("id: " + this.FavoriteID);
//
//        mPizzaListData.printAllData();
//
//        if (mPizzaCrustData != null) {
//            mPizzaCrustData.printAllData();
//        }
//
//        if (mAdditionsListDataExtra != null) {
//            mAdditionsListDataExtra.printAllData("Extra Ingredients");
//        }
//
//        if (mAdditionsListDataCustom != null) {
//            mAdditionsListDataCustom.printAllData("Custom Pizza Toppings Ingredients");
//        }
//        if (mAddress != null) {
//            mAddress.printAllData();
//        }
//
//
//    }
}
