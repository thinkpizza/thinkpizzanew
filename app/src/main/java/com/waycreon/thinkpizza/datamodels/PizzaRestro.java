package com.waycreon.thinkpizza.datamodels;

/**
 * Created by Waycreon on 8/7/2015.
 */
public class PizzaRestro {

    String id = "";
    String name = "";
    String email = "";
    String password = "";
    //    String address;
    String street_name = "";
    String street_no = "";
    String apt_no = "";
    String city = "";
    String lat = "";
    String lng = "";
    //    String logo = "";
    String phone = "";
    String img_url = "";
    String distance = "";

    String delivery_time = "";
    String min_order = "";
    String open_time = "";
    String close_time = "";
    boolean isOpen = false;
    //    int rating_count = 0;
//    int ratings = 0;
    float radius;

    double overall_count;
    double overall_ratings;
    double crust_count;
    double crust_ratings;
    double toppings_quality_count;
    double toppings_quality_ratings;
    double delivery_time_count;
    double delivery_time_ratings;

    String open_days;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

  /*  public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
*/

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_no() {
        return street_no;
    }

    public void setStreet_no(String street_no) {
        this.street_no = street_no;
    }

    public String getApt_no() {
        return apt_no;
    }

    public void setApt_no(String apt_no) {
        this.apt_no = apt_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

/*
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
*/

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getMin_order() {
        return min_order;
    }

    public void setMin_order(String min_order) {
        this.min_order = min_order;
    }

    public String getClose_time() {
        return close_time;
    }

    public void setClose_time(String close_time) {
        this.close_time = close_time;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public boolean isOpen() {
        return isOpen;
    }

//    public int getRating_count() {
//        return rating_count;
//    }
//
//    public void setRating_count(int rating_count) {
//        this.rating_count = rating_count;
//    }

//    public int getRatings() {
//        return ratings;
//    }
//
//    public void setRatings(int ratings) {
//        this.ratings = ratings;
//    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public double getOverall_count() {
        return overall_count;
    }

    public void setOverall_count(double overall_count) {
        this.overall_count = overall_count;
    }

    public double getOverall_ratings() {
        return overall_ratings;
    }

    public void setOverall_ratings(double overall_ratings) {
        this.overall_ratings = overall_ratings;
    }

    public double getCrust_count() {
        return crust_count;
    }

    public void setCrust_count(double crust_count) {
        this.crust_count = crust_count;
    }

    public double getCrust_ratings() {
        return crust_ratings;
    }

    public void setCrust_ratings(double crust_ratings) {
        this.crust_ratings = crust_ratings;
    }

    public double getToppings_quality_count() {
        return toppings_quality_count;
    }

    public void setToppings_quality_count(double toppings_quality_count) {
        this.toppings_quality_count = toppings_quality_count;
    }

    public double getToppings_quality_ratings() {
        return toppings_quality_ratings;
    }

    public void setToppings_quality_ratings(double toppings_quality_ratings) {
        this.toppings_quality_ratings = toppings_quality_ratings;
    }

    public double getDelivery_time_count() {
        return delivery_time_count;
    }

    public void setDelivery_time_count(double delivery_time_count) {
        this.delivery_time_count = delivery_time_count;
    }

    public double getDelivery_time_ratings() {
        return delivery_time_ratings;
    }

    public void setDelivery_time_ratings(double delivery_time_ratings) {
        this.delivery_time_ratings = delivery_time_ratings;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public String getOpen_days() {
        return open_days;
    }

    public void setOpen_days(String open_days) {
        this.open_days = open_days;
    }

    public void printAllData() {
        System.out.println("-------------------pizzaria Data---------------------------");

        System.out.println("id: " + this.id);
        System.out.println("name: " + this.name);
        System.out.println("email: " + this.email);
        System.out.println("password: " + this.password);
//        System.out.println("address: " + this.address);
        System.out.println("street_name: " + this.street_name);
        System.out.println("street_no: " + this.street_no);
        System.out.println("apt_no: " + this.apt_no);
        System.out.println("city: " + this.city);
        System.out.println("lat: " + this.lat);
        System.out.println("lng: " + this.lng);
//        System.out.println("logo: " + this.logo);
        System.out.println("phone: " + this.phone);
        System.out.println("img_url: " + this.img_url);
        System.out.println("delivery_time: " + this.delivery_time);
        System.out.println("min_order: " + this.min_order);
        System.out.println("open_time: " + this.open_time);
        System.out.println("close_time: " + this.close_time);
        System.out.println("isOpen: " + this.isOpen);
//        System.out.println("rating_count: " + this.rating_count);
//        System.out.println("ratings: " + this.ratings);
        System.out.println("overall_count: " + this.overall_count);
        System.out.println("overall_ratings: " + this.overall_ratings);
        System.out.println("crust_count: " + this.crust_count);
        System.out.println("crust_ratings: " + this.crust_ratings);
        System.out.println("toppings_quality_count: " + this.toppings_quality_count);
        System.out.println("toppings_quality_ratings: " + this.toppings_quality_ratings);
        System.out.println("delivery_time_count: " + this.delivery_time_count);
        System.out.println("delivery_time_ratings: " + this.delivery_time_ratings);
        System.out.println("radius: " + this.radius);
        System.out.println("open_days: " + this.open_days);
    }


}
