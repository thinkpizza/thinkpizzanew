package com.waycreon.thinkpizza.datamodels;

/**
 * Created by Waycreon on 8/11/2015.
 */
public class Menu {

    String id;
    String pizzaria_id;
    String name;
    String img_url;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPizzaria_id() {
        return pizzaria_id;
    }

    public void setPizzaria_id(String pizzaria_id) {
        this.pizzaria_id = pizzaria_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
}
