package com.waycreon.thinkpizza.datamodels;

import java.io.Serializable;

public class RestaurentData implements Serializable {

	int ImageID = 0;
	String RestaurentName = "";
	String RestaurantsAddress = "";
	String DeliveryTime = "";
	int MinimumOrderPrice = 0;

	public int getImageID() {
		return ImageID;
	}

	public void setImageID(int imageID) {
		ImageID = imageID;
	}

	public String getRestaurentName() {
		return RestaurentName;
	}

	public void setRestaurentName(String restaurentName) {
		RestaurentName = restaurentName;
	}

	public String getRestaurantsAddress() {
		return RestaurantsAddress;
	}

	public void setRestaurantsAddress(String restaurantsAddress) {
		RestaurantsAddress = restaurantsAddress;
	}

	public String getDeliveryTime() {
		return DeliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		DeliveryTime = deliveryTime;
	}

	public int getMinimumOrderPrice() {
		return MinimumOrderPrice;
	}

	public void setMinimumOrderPrice(int minimumOrderPrice) {
		MinimumOrderPrice = minimumOrderPrice;
	}

}
