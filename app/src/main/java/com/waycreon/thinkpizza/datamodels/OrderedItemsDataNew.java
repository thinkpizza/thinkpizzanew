package com.waycreon.thinkpizza.datamodels;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by abcd on 8/6/2015.
 */
public class OrderedItemsDataNew implements Serializable {

    String order_id = "";
    String order_date = "";
    String order_status = "";
    String name = "";
    String img_url = "";

    Address DeliveryAddress;
    ArrayList<Items> Items;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Address getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }


    public ArrayList<Items> getItems() {
        return Items;
    }

    public void setItems(ArrayList<Items> items) {
        Items = items;
    }

    public void printAllData() {
        System.out.println("------------------------------Orders----------------------");
        System.out.println("id: " + order_id);
        System.out.println("order_date: " + order_date);
        System.out.println("order_status: " + order_status);
        System.out.println("name: " + name);
        System.out.println("img_url: " + img_url);
        System.out.println("DeliveryAddress: " + DeliveryAddress);
        for (int i = 0; i < Items.size(); i++)
            Items.get(i).printAllData();
        System.out.println("-------------------------------------------------------------");

    }


}
