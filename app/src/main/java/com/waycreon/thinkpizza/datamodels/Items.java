package com.waycreon.thinkpizza.datamodels;

public class Items {


    String res_name = "";
    String menu_id = "";
    String name = "";
    String qty = "";
    String total = "";
    String pid = "";


    public void printAllData() {
        System.out.println("------------------------------Items----------------------");
        System.out.println("res_name: " + res_name);
        System.out.println("menu_id: " + menu_id);
        System.out.println("name: " + name);
        System.out.println("qty: " + qty);
        System.out.println("total: " + total);
        System.out.println("-------------------------------------------------------------");

    }

    public String getRes_name() {
        return res_name;
    }

    public void setRes_name(String res_name) {
        this.res_name = res_name;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
