package com.waycreon.thinkpizza.datamodels;

public class NavigationListItemModel {

    private String ListItemName;
    private int ItemIcon;
    private int SelectedItemIcon;
    boolean flag = false;

    // private String ItemCount = "0";
    // private boolean isCountVisible = false;

    public NavigationListItemModel(String itemname, int i, int item) {
        this.ListItemName = itemname;
        this.ItemIcon = i;
        this.SelectedItemIcon = item;
    }

    /*public NavigationListItemModel(String itemname, int i, String c,
                                   boolean iscountervisible) {
        this.ListItemName = itemname;
        this.ItemIcon = i;
        // this.ItemCount = c;
        // this.isCountVisible = iscountervisible;
    }*/

    public String getListItemName() {
        return ListItemName;
    }

    public void setListItemName(String listItemName) {
        ListItemName = listItemName;
    }

    public int getItemIcon() {
        return ItemIcon;
    }

    public void setItemIcon(int itemIcon) {
        ItemIcon = itemIcon;
    }


    public int getSelectedItemIcon() {
        return SelectedItemIcon;
    }

    public void setSelectedItemIcon(int selectedItemIcon) {
        SelectedItemIcon = selectedItemIcon;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
// public String getItemCount() {
    // return ItemCount;
    // }
    //
    // public void setItemCount(String itemCount) {
    // ItemCount = itemCount;
    // }
    //
    // public boolean isCountVisible() {
    // return isCountVisible;
    // }
    //
    // public void setCountVisible(boolean isCountVisible) {
    // this.isCountVisible = isCountVisible;
    // }

}
