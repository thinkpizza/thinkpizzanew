package com.waycreon.thinkpizza.datamodels;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Waycreon on 8/11/2015.
 */
public class Address {

    String responseMsg;
    int responseCode;
    ArrayList<Address> addresses = new ArrayList<>();

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }


    String id = "";
    String user_id = "";
    String tag_name = "";
    String lat = "";
    String lng = "";
    String street_name = "";
    String street_no = "";
    String apt_no = "";
    String zip_code = "";
    String city = "";
    String state = "";

    Address mAddress;
    JSONObject obj;

    public void printAddress(/*Address mAddress*/) {
        System.out.println("TagName: " + this.tag_name);
        System.out.println("street_name: " + this.street_name);
        System.out.println("street_no: " + this.street_no);
        System.out.println("apt_no: " + this.apt_no);
        System.out.println("City: " + this.city);
        System.out.println("Postal: " + this.zip_code);
        System.out.println("State: " + this.state);
    }

    public String getOnlyAddressLines() {
        String add = "";
        add += street_name;
        if (street_no.length() > 0)
            add += ",\n" + street_no;
        if (apt_no.length() > 0)
            add += ",\n" + apt_no;
//        return tag_name + "\n" + add;
        return add;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_no() {
        return street_no;
    }

    public void setStreet_no(String street_no) {
        this.street_no = street_no;
    }

    public String getApt_no() {
        return apt_no;
    }

    public void setApt_no(String apt_no) {
        this.apt_no = apt_no;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public JSONObject getJsonresponse(JSONObject mJsonObject, Address mAddress) {
        obj = mJsonObject;
        try {
            obj.put("id", id);
            obj.put("tag", tag_name);
//            obj.put("streetname", street_name);
            obj.put("street_name", street_name);
            obj.put("street_no", street_no);
            obj.put("apt_no", apt_no);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;

    }

    public void printAllData() {
        System.out.println("-------------------address Data---------------------------");

        System.out.println("id: " + this.id);
        System.out.println("user_id: " + this.user_id);
        System.out.println("tag_name: " + this.tag_name);
        System.out.println("Lat: " + this.lat);
        System.out.println("Lng: " + this.lng);
        System.out.println("street_name: " + this.street_name);
        System.out.println("street_no: " + this.street_no);
        System.out.println("apt_no: " + this.apt_no);
        System.out.println("zip_code: " + this.zip_code);
        System.out.println("city: " + this.city);
        System.out.println("state: " + this.state);

    }
}
