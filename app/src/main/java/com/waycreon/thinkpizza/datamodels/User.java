package com.waycreon.thinkpizza.datamodels;

import java.io.Serializable;

/**
 * Created by Waycreon on 8/5/2015.
 */
// test commit by sagar
public class User implements Serializable {


    String id = "";
    String name = "";
    String cpf_no = "";
    String email = "";
    String password = "";
    String img_url = "";

    String lat = "";
    String lng = "";
    String street_name = "";
    String street_no = "";
    String apt_no = "";
    String city = "";
    String state = "";
    String pincode = "";

    String mobile = "";
    String gcm = "";
    String google_id = "";
    String facebook_id = "";
    String activate = "";
    String code = "";

    public void printAllData() {

        System.out.println("-----------------------USER DATA--------------------------");
        System.out.println("id:" + id);
        System.out.println("name:" + name);
        System.out.println("email:" + email);
        System.out.println("street_name:" + street_name);
        System.out.println("street_no:" + street_no);
        System.out.println("apt_no:" + apt_no);
        System.out.println("city:" + city);
        System.out.println("state:" + state);
        System.out.println("pincode:" + pincode);
        System.out.println("mobile:" + mobile);
        System.out.println("password:" + password);
        System.out.println("gcm:" + gcm);
        System.out.println("google_id:" + google_id);
        System.out.println("facebook_id:" + facebook_id);
        System.out.println("cpf_no:" + cpf_no);
        System.out.println("img_url:" + img_url);
        System.out.println("activate:" + activate);
        System.out.println("code:" + code);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf_no() {
        return cpf_no;
    }

    public void setCpf_no(String cpf_no) {
        this.cpf_no = cpf_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_no() {
        return street_no;
    }

    public void setStreet_no(String street_no) {
        this.street_no = street_no;
    }

    public String getApt_no() {
        return apt_no;
    }

    public void setApt_no(String apt_no) {
        this.apt_no = apt_no;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGcm() {
        return gcm;
    }

    public void setGcm(String gcm) {
        this.gcm = gcm;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getActivate() {
        return activate;
    }

    public void setActivate(String activate) {
        this.activate = activate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}