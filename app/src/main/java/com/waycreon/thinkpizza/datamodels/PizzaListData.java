package com.waycreon.thinkpizza.datamodels;

import java.util.ArrayList;

public class PizzaListData {

    String id = "";
    String name = "";
    String pid = "";
    String menutype_id = "";
    String custom_ingredients = "";
    String price_small = "";
    String price_medium = "";
    String price_large = "";
    String description = "";
    String img_url = "";

    String favourite_id = "";
    boolean isPizzaAddedFavorite = false;

    ArrayList<Toppings> toppings;

    String show_options = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPrice_small() {
        return price_small;
    }

    public void setPrice_small(String price_small) {
        this.price_small = price_small;
    }

    public String getPrice_medium() {
        return price_medium;
    }

    public void setPrice_medium(String price_medium) {
        this.price_medium = price_medium;
    }

    public String getPrice_large() {
        return price_large;
    }

    public void setPrice_large(String price_large) {
        this.price_large = price_large;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getMenutype_id() {
        return menutype_id;
    }

    public void setMenutype_id(String menutype_id) {
        this.menutype_id = menutype_id;
    }

    public String getCustom_ingredients() {
        return custom_ingredients;
    }

    public void setCustom_ingredients(String custom_ingredients) {
        this.custom_ingredients = custom_ingredients;
    }

    public String getFavourite_id() {
        return favourite_id;
    }

    public void setFavourite_id(String favourite_id) {
        this.favourite_id = favourite_id;
    }

    public boolean isPizzaAddedFavorite() {
        return isPizzaAddedFavorite;
    }

    public void setIsPizzaAddedFavorite(boolean isPizzaAddedFavorite) {
        this.isPizzaAddedFavorite = isPizzaAddedFavorite;
    }

    public void printAllData() {
        System.out.println("-------------------pizza_details Data---------------------------");
        System.out.println("id: " + this.id);
        System.out.println("name: " + this.name);
        System.out.println("pid: " + this.pid);
        System.out.println("menutype_id: " + this.menutype_id);
        System.out.println("custom_ingredients: " + this.custom_ingredients);
        System.out.println("price_small: " + this.price_small);
        System.out.println("price_medium: " + this.price_medium);
        System.out.println("price_large: " + this.price_large);
        System.out.println("description: " + this.description);
        System.out.println("img_url: " + this.img_url);

        if (favourite_id != null && favourite_id.length() > 0) {
            System.out.println("favourite_id: " + this.favourite_id);
        }
        if (isPizzaAddedFavorite) {

            System.out.println("isItemFavorite: " + this.isPizzaAddedFavorite);
        }

        if (toppings != null) {
            for (int i = 0; i < toppings.size(); i++) {
//                System.out.println("-------------Toppings--------------");
//                System.out.println("id: " + toppings.get(i).getId());
//                System.out.println("menus_id: " + toppings.get(i).getMenus_id());
//                System.out.println("name: " + toppings.get(i).getName());
//                System.out.println("price: " + toppings.get(i).getPrice());
                toppings.get(i).printAllData();
            }
        }

        System.out.println("show_options: " + show_options);
    }

    public ArrayList<Toppings> getToppings() {
        return toppings;
    }

    public void setToppings(ArrayList<Toppings> toppings) {

        this.toppings = toppings;
    }

    public String getShow_options() {
        return show_options;
    }

    public void setShow_options(String show_options) {
        this.show_options = show_options;
    }

    public void getAllPizzaToppings() {

    }
}
