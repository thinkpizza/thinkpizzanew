package com.waycreon.thinkpizza;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.waycreon.thinkpizza.adapter.NavigationListAdapter;
import com.waycreon.thinkpizza.adapter.PizzaListItemAdapter;
import com.waycreon.thinkpizza.datamodels.NavigationListItemModel;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.thinkpizza.fragment.AddressFragment;
import com.waycreon.thinkpizza.fragment.BasketFragment;
import com.waycreon.thinkpizza.fragment.FavoriteFragment;
import com.waycreon.thinkpizza.fragment.FoodMenuFragment;
import com.waycreon.thinkpizza.fragment.MapFragment;
import com.waycreon.thinkpizza.fragment.MyOptionsFragment;
import com.waycreon.thinkpizza.fragment.OrderFragment;
import com.waycreon.thinkpizza.fragment.OrderStatusFragment;
import com.waycreon.thinkpizza.fragment.PizzaListFragment;
import com.waycreon.thinkpizza.fragment.RecommendAppFragment;
import com.waycreon.thinkpizza.fragment.RestaurantFullDetailFragment;
import com.waycreon.thinkpizza.fragment.RestaurentFragment;
import com.waycreon.thinkpizza.fragment.ShareAppFragment;
import com.waycreon.thinkpizza.fragment.TermsConditionsFragment;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;
import com.waycreon.waycreon.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

import static com.waycreon.thinkpizza.R.drawable.basket_icon_white_60;
import static com.waycreon.thinkpizza.R.drawable.delivery_icon_100;
import static com.waycreon.thinkpizza.R.drawable.favourite_pizza_100;
import static com.waycreon.thinkpizza.R.drawable.history_pizza_60;
import static com.waycreon.thinkpizza.R.drawable.location_map_icon_60;
import static com.waycreon.thinkpizza.R.drawable.order_status_white;
import static com.waycreon.thinkpizza.R.drawable.restaurant_icon_60;

public class NavigationActivity extends ActionBarActivity implements
        OnClickListener {

    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout mDrawerLayout;
    ListView mListView;
    ImageView mImageViewNavigate;
    ImageView mImageViewBack;
    RelativeLayout mRelativeLayoutMap;

    LinearLayout mLinearLayoutNavigationList;

    RelativeLayout mRelativeLayoutProfile;
    ImageView mImageViewProfilePic;
    TextView mTextViewUserName;
    TextView mTextViewLocation;
    ImageLoader mImageLoader;

    EditText mEditTextSearchPizza;
    public AutoCompleteTextView mAutoCompleteTextViewSearch;
    public ImageView mImageViewSearch;
    public ImageView mImageViewModeList;
    public ImageView mImageViewModeMap;
    public LinearLayout mLinearLayoutScreenNameLogo;
    ImageView mImageViewScreenIcon;
    TextView mTextViewScreenName;
    //    ImageView mImageViewLogout;
    RelativeLayout mLinearLayoutCart;
    ImageView mImageViewCart;
    TextView mTextViewCartItemNo;
    ImageView mImageViewFloatingCart;
//    public ImageView mImageViewFloatingEditProfile;


    //    ActionBarDrawerToggle mActionBarDrawerToggle;
    int menuicons[] = {R.drawable.icon_home, R.drawable.icon_pizzeria, R.drawable.icon_favourites,
            R.drawable.order_status_gray, R.drawable.icon_delivery_address, R.drawable.icon_my_options,
            R.drawable.icon_terms, R.drawable.icon_share, R.drawable.icon_recommend, R.drawable.icon_logout};
    int menuicons_selected[] = {R.drawable.icon_home_hover, R.drawable.icon_pizzeria_hover, R.drawable.icon_favourites_hover,
            R.drawable.order_status_red, R.drawable.icon_delivery_address_hover, R.drawable.icon_my_options_hover,
            R.drawable.icon_terms_hover, R.drawable.icon_share_hover, R.drawable.icon_recommend_hover, R.drawable.icon_logout_hover};
    String menuitems[] = {"Home", "Pizzeria", "Favourites", "Order Status", "Delivery Address",
            "My Options", "Terms & Conditions", "Share App", "Recommend App", "Logout"};
    ArrayList<NavigationListItemModel> mArrayListNavigationItem;
    public NavigationListAdapter mNavigationListAdapter;

    public ArrayList<Fragment> backstacklist = new ArrayList<>();

    public MapFragment mapFragment;
    public RestaurentFragment mRestaurentFragment;
    public FoodMenuFragment mFoodMenuFragment;
    public PizzaListFragment mPizzaListFragment;
    public OrderFragment mOrderFragment;
    public BasketFragment mBasketFragment;
    public AddressFragment mAddressFragment;
    public RestaurantFullDetailFragment mRestaurantFullDetailFragment;
    public FavoriteFragment mFavoriteFragment;
    public MyOptionsFragment myOptionsFragment;
    public RecommendAppFragment mRecommendAppFragment;
    public ShareAppFragment mShareAppFragment;
    public TermsConditionsFragment mTermsConditionsFragment;
    public OrderStatusFragment mOrderStatusFragment;

    Fragment mFragmentCurrentVisible;
    boolean isNavigationLocked = false;

    FragmentManager fm;
    FragmentTransaction ft;

    boolean doubleBackToExitPressedOnce = false;
    Store_pref mStore_pref;
    public ArrayList<OrderedItemsData> mArrayListOrderedItem;

    /*
    sharing Controls
     */

//    RelativeLayout mRelativeLayoutSharing;
//    TextView mTextViewShareFB;
//    TextView mTextViewShareGmail;
//    TextView mTextViewShareWhatsApp;
//    TextView mTextViewShareCancel;
//    // Animation
//    Animation animSlideDown;
//    Animation animSlideUP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_navigation);
        getSupportActionBar().hide();

        mListView = (ListView) findViewById(R.id.navigationactivity_list);
        mLinearLayoutNavigationList = (LinearLayout) findViewById(R.id.navigation_layout_list);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigationactivity_drawerlayout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.icon_menu, R.string.app_name,
                R.string.app_name) {

            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
                isDrawerOpen = false;
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                mImageViewFloatingCart.setVisibility(View.VISIBLE);
//                ShowHideIcons();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
                isDrawerOpen = true;
                mImageViewFloatingCart.setVisibility(View.GONE);
                Utils.hide_keyboard(NavigationActivity.this);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mImageViewNavigate = (ImageView) findViewById(R.id.navigationactivity_image_navigation);
        mImageViewBack = (ImageView) findViewById(R.id.navigationactivity_image_back);

        mRelativeLayoutMap = (RelativeLayout) findViewById(R.id.navigation_layout_map);

        mRelativeLayoutProfile = (RelativeLayout) findViewById(R.id.navigation_layout_profile);
        mImageViewProfilePic = (ImageView) findViewById(R.id.navigation_profile_image);
        mTextViewUserName = (TextView) findViewById(R.id.navigation_profile_user_name);
        mTextViewLocation = (TextView) findViewById(R.id.navigation_profile_user_location);

        mEditTextSearchPizza = (EditText) findViewById(R.id.navigationactivity_edittext_pizza_search);
        mAutoCompleteTextViewSearch = (AutoCompleteTextView) findViewById(R.id.navigationactivity_atv_search);
        mImageViewSearch = (ImageView) findViewById(R.id.navigationactivity_image_search);
        mImageViewModeList = (ImageView) findViewById(R.id.navigationactivity_image_mode_list);
        mImageViewModeMap = (ImageView) findViewById(R.id.navigationactivity_image_mode_map);

        mLinearLayoutScreenNameLogo = (LinearLayout) findViewById(R.id.navigation_layout_screen_name_icon);
        mImageViewScreenIcon = (ImageView) findViewById(R.id.navigationactivity_image_screenicon);
        mTextViewScreenName = (TextView) findViewById(R.id.navigationactivity_text_screenname);

//        mImageViewLogout = (ImageView) findViewById(R.id.navigationactivity_image_logout);

        mLinearLayoutCart = (RelativeLayout) findViewById(R.id.navigationactivity_layout_cart);
        mImageViewCart = (ImageView) findViewById(R.id.navigationactivity_image_cart);
        mTextViewCartItemNo = (TextView) findViewById(R.id.navigationactivity_text_cartitemno);
        mImageViewFloatingCart = (ImageView) findViewById(R.id.navigationactivity_image_floating_cart);

        mRelativeLayoutProfile.setOnClickListener(this);

        mImageViewNavigate.setOnClickListener(this);
        mImageViewBack.setOnClickListener(this);

        mImageViewSearch.setOnClickListener(this);
        mImageViewModeList.setOnClickListener(this);
        mImageViewModeMap.setOnClickListener(this);

//        mImageViewLogout.setOnClickListener(this);
        mLinearLayoutCart.setOnClickListener(this);
        mImageViewFloatingCart.setOnClickListener(this);

        mEditTextSearchPizza.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchPizza(s.toString());
            }
        });

        mAutoCompleteTextViewSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    mAutoCompleteTextViewSearch.showDropDown();
                    Utils.hide_keyboard(NavigationActivity.this);

//                /*
//                to remove the Adapter from the AutoComplete because if Brazil zip not found then
//                i use google place api to search with given pin after that if user click on the address
//                 */
//                    mAutoCompleteTextViewSearch.setAdapter(null);
//                    mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                    return true;
                }
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    mAutoCompleteTextViewSearch.showDropDown();
                    Utils.hide_keyboard(NavigationActivity.this);
                    mapFragment.CallBrazilAddress(mAutoCompleteTextViewSearch.getText().toString().replaceAll(" ", ""));
                    return true;
                }
                return false;
            }
        });

        mAutoCompleteTextViewSearch.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                Tags.ADDRESS_NUMBER = 2;
                String str = (String) arg0.getItemAtPosition(arg2);
                Toast.makeText(NavigationActivity.this, str, Toast.LENGTH_SHORT).show();
                mapFragment.getLocationPoints(mAutoCompleteTextViewSearch.getText().toString());
                mAutoCompleteTextViewSearch.setVisibility(View.GONE);
                mImageViewSearch.setVisibility(View.VISIBLE);
//                mAddressFragment.setPostalAddress(mAutoCompleteTextViewSearch.getText().toString());
                Utils.HideSoftKeyBoard(getApplicationContext(), mAutoCompleteTextViewSearch);
//                /*
//                to remove the Adapter from the AutoComplete because if Brazil zip not found then
//                i use google place api to search with given pin after that if user click on the address
//                 */
//                mAutoCompleteTextViewSearch.setAdapter(null);
//                mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
            }
        });

//        mAutoCompleteTextViewSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                /*
//                to remove the Adapter from the AutoComplete because if Brazil zip not found then
//                i use google place api to search with given pin after that if user click on the address
//                 */
//                if (mAutoCompleteTextViewSearch.getAdapter() != null) {
//                    mAutoCompleteTextViewSearch.setAdapter(null);
//                    mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
//                }
//            }
//        });
        addItemstoList();
        initializeFragments();

        if (savedInstanceState == null) {
            showFragment(mapFragment, false);
            backstacklist.clear();
            backstacklist = new ArrayList<>();
            backstacklist.add(mapFragment);
            showFragments();
        }

        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                displayView(position);
//                draweritem = position;
                mNavigationListAdapter.notifyDataSetChanged();

                for (int i = 0; i < mArrayListNavigationItem.size(); i++) {
                    mArrayListNavigationItem.get(i).setFlag(false);
                    if (i == position)
                        mArrayListNavigationItem.get(i).setFlag(true);
                }

                /*for (int i = 0; i < mArrayListNavigationItem.size(); i++) {
                    System.out.println("Flag: " + mArrayListNavigationItem.get(i).isFlag());
                }*/

                Utils.hide_keyboard(NavigationActivity.this);
            }
        });

        mArrayListOrderedItem = new ArrayList<>();

        mStore_pref = new Store_pref(this);
        if (mStore_pref.getOrderedData() != null)
            if (mStore_pref.getOrderedData().size() > 0) {
                Log.e("MainActivity.java", "Order data is present in prefrences.");
                mArrayListOrderedItem = new ArrayList<>();
                mArrayListOrderedItem = mStore_pref.getOrderedData();
                Log.e("MainActivity.java", "Order data array size: " + mArrayListOrderedItem.size());
            }

        setProfile();
//        InitSharingDialog();
    }

    boolean isDrawerOpen = false;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.navigation_layout_profile:
                showFragments();
                showFragment(myOptionsFragment, true);
                backstacklist.add(mapFragment);
                Log.e("BackstackList", "Map Fragment is added (from MainActivity Profile).");
                break;
            case R.id.navigationactivity_image_navigation:
                System.out.println("navigation Screen.java: isDrawerOpen: " + isDrawerOpen);
                if (!isDrawerOpen) {
                    isDrawerOpen = true;
//                    System.out.println("navigation Screen.java: Drawer is opened.");
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                } else {
                    isDrawerOpen = false;
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
//                    System.out.println("navigation Screen.java: Drawer is Closed.");
                }
                System.out.println("navigation Screen.java: after click isDrawerOpen: " + isDrawerOpen);
                Utils.hide_keyboard(NavigationActivity.this);
                hideAutocompleteTextview();
                break;
            case R.id.navigationactivity_image_back:
                GoBack();
                Utils.hide_keyboard(NavigationActivity.this);
                hideAutocompleteTextview();
                break;
            case R.id.navigationactivity_image_search:

                if (isDrawerOpen) {
                    isDrawerOpen = false;
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }

                if (mFragmentCurrentVisible == mapFragment) {
                    showAutocompleteTextview();
                    mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

                } else if (mFragmentCurrentVisible == mPizzaListFragment) {

//                    mPizzaListFragment.setSearchToppingsAdapter();
                    mEditTextSearchPizza.setText("");
                    mEditTextSearchPizza.setVisibility(View.VISIBLE);
                    mLinearLayoutScreenNameLogo.setVisibility(View.INVISIBLE);
                    mImageViewSearch.setVisibility(View.INVISIBLE);
                    mEditTextSearchPizza.requestFocus();
                    Utils.ShowSoftKeyBoard(getApplicationContext(), mEditTextSearchPizza);
                    Log.e("navigationactivity_image_search click: ", "Pizza frag");
                }

                break;

            case R.id.navigationactivity_image_mode_list:

                if (isDrawerOpen) {
                    isDrawerOpen = false;
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }

//                mImageViewModeMap.setVisibility(View.VISIBLE);
//                mImageViewModeList.setVisibility(View.INVISIBLE);
                mapFragment.ShowRestroListinNavigationScreen();
                Utils.hide_keyboard(NavigationActivity.this);
                hideAutocompleteTextview();
                break;
            case R.id.navigationactivity_image_mode_map:
                if (isDrawerOpen) {
                    isDrawerOpen = false;
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }

//                mImageViewModeMap.setVisibility(View.INVISIBLE);
//                mImageViewModeList.setVisibility(View.VISIBLE);
                mapFragment.ShowMapinNavigationScreen();
                Utils.hide_keyboard(NavigationActivity.this);
                hideAutocompleteTextview();
                break;

            case R.id.navigationactivity_layout_cart:
                mBasketFragment.lastVisibleLayout = 1;
                showFragments();
                showFragment(mBasketFragment, true);
                backstacklist.add(mBasketFragment);
                Log.e("BackstackList", "Basket Fragment is added (from MainActivity).");
                if (mArrayListOrderedItem.size() > 0) {
                    mBasketFragment.mListViewOrderedItems.setVisibility(View.VISIBLE);
                    Log.e("MainActivity.java", "Order data array size: " + mArrayListOrderedItem.size());
                }
                if (mFragmentCurrentVisible == mPizzaListFragment) {
                    mPizzaListFragment.callBasketRestroData();
                } else if (mFragmentCurrentVisible == mRestaurantFullDetailFragment) {
                    mRestaurantFullDetailFragment.callBasketRestroData();
                } else if (mFragmentCurrentVisible == mFoodMenuFragment) {
                    mFoodMenuFragment.callBasketRestroData();
                }
                showFragments();
                Utils.hide_keyboard(NavigationActivity.this);
                hideAutocompleteTextview();
                break;

            case R.id.navigationactivity_image_floating_cart:

                mBasketFragment.lastVisibleLayout = 1;
                showFragments();
                showFragment(mBasketFragment, true);
                backstacklist.add(mBasketFragment);
                Log.e("BackstackList", "Basket Fragment is added (from MainActivity).");

                if (mArrayListOrderedItem.size() > 0) {
                    mBasketFragment.mListViewOrderedItems.setVisibility(View.VISIBLE);
                    Log.e("MainActivity.java", "Order data array size: " + mArrayListOrderedItem.size());
                } else {
                    Log.e("MainActivity.java", "list is not visible Order data array size: " + mArrayListOrderedItem.size());
                }
                showFragments();
                break;
//            case R.id.navigationactivity_image_floating_edit_profile:
//                myOptionsFragment.setEnableEditables();
//
//                break;

            /*
            case R.id.navigationactivity_image_logout:
                Store_pref mStore_pref = new Store_pref(this);
                mStore_pref.removeAll();
                Intent mIntentLogin = new Intent(NavigationActivity.this, LoginActivity.class);
                mIntentLogin.putExtra("email", mStore_pref.getRememberMe());
                startActivity(mIntentLogin);
                Log.e("NavigationActivity.java", "Logout Successfully." + mStore_pref.getRememberMe());
                finish();
                break;
                */
        }

    }

    public void showAutocompleteTextview() {
        mAutoCompleteTextViewSearch.setText("");
        mAutoCompleteTextViewSearch.setVisibility(View.VISIBLE);
        mLinearLayoutScreenNameLogo.setVisibility(View.INVISIBLE);
        mImageViewSearch.setVisibility(View.INVISIBLE);
        mAutoCompleteTextViewSearch.requestFocus();
        Utils.ShowSoftKeyBoard(getApplicationContext(), mAutoCompleteTextViewSearch);
        Log.e("navigationactivity_image_search click: ", "Map frag");
//        mapFragment.setAutocompleteTextAdapter();
        mAutoCompleteTextViewSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    public void hideAutocompleteTextview() {
        mAutoCompleteTextViewSearch.setVisibility(View.GONE);
        mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
        mImageViewSearch.setVisibility(View.VISIBLE);
    }

    public void displayView(int position) {

        isDrawerOpen = false;

        switch (position) {
            case 0:
                showFragment(mapFragment, false);
                mapFragment.ShowMapinNavigationScreen();
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 1:
                showFragment(mRestaurentFragment, false);
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mRestaurentFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 2:
                Constants.isHistoryServiceCalled = false;
                showFragment(mFavoriteFragment, false);
                mFavoriteFragment.callFavoriteService();
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mFavoriteFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 3:
                showFragment(mOrderStatusFragment, false);
                mOrderStatusFragment.callOrderStatusService();
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mOrderStatusFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 4:
                showFragment(mAddressFragment, false);
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mAddressFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 5:
                showFragment(myOptionsFragment, false);
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(myOptionsFragment);
                mDrawerLayout.closeDrawers();
                myOptionsFragment.setDisableEditables();
                break;
            case 6:
                showFragment(mTermsConditionsFragment, false);
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mTermsConditionsFragment);
                mDrawerLayout.closeDrawers();
                break;
            case 7:
                showFragment(mShareAppFragment, false);
                backstacklist.clear();
                backstacklist = new ArrayList<>();
                backstacklist.add(mapFragment);
                backstacklist.add(mShareAppFragment);
                mShareAppFragment.startBottomanim();
//                mRelativeLayoutSharing.setVisibility(View.VISIBLE);
//                mRelativeLayoutSharing.startAnimation(animSlideDown);
                mDrawerLayout.closeDrawers();
                break;
            case 8:
//                showFragment(mRecommendAppFragment, false);
//                backstacklist.clear();
//                backstacklist = new ArrayList<>();
//                backstacklist.add(mapFragment);
//                backstacklist.add(mRecommendAppFragment);
                mDrawerLayout.closeDrawers();
                mShareAppFragment.startRecommendApp();
                break;
            case 9:
                Store_pref mStore_pref = new Store_pref(this);
                mStore_pref.removeAll();
                mStore_pref.removeOrderedData();
                mStore_pref.removeAdditionsIDS();
                mStore_pref.removeToppingsIDs();
                finish();
                Intent mIntentLogin = new Intent(NavigationActivity.this, LoginActivity.class);
                mIntentLogin.putExtra("email", mStore_pref.getRememberMe());
                startActivity(mIntentLogin);
                Log.e("NavigationActivity.java", "Logout Successfully." + mStore_pref.getRememberMe());

                break;
        }

//        mListView.setSelection(position);

    }

    public void showFragment(Fragment fragment, boolean islocked) {
        Utils.freeMemory();
        if (islocked) {
            mDrawerLayout
                    .setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }

        ft = fm.beginTransaction();

//        if (fragment == mShareAppFragment) {
//            Log.e("showFragment()", "if (fragment == mShareAppFragment)");
//            ft.show(fragment);
////        ft.commit();
//            ft.commitAllowingStateLoss();
//        } else
        {
            Log.e("showFragment()", "if (fragment == mShareAppFragment) else");
            ft.hide(mapFragment);
            ft.hide(mRestaurentFragment);
            ft.hide(mFoodMenuFragment);
            ft.hide(mPizzaListFragment);
            ft.hide(mOrderFragment);
            ft.hide(mBasketFragment);
            ft.hide(mAddressFragment);
            ft.hide(mRestaurantFullDetailFragment);
            ft.hide(mFavoriteFragment);
            ft.hide(myOptionsFragment);
            ft.hide(mTermsConditionsFragment);
            ft.hide(mShareAppFragment);
            ft.hide(mRecommendAppFragment);
            ft.hide(mOrderStatusFragment);

            ft.show(fragment);
//        ft.commit();
            ft.commitAllowingStateLoss();
        }

        mFragmentCurrentVisible = fragment;
        isNavigationLocked = islocked;
        ShowHideIcons();
    }

    private void initializeFragments() {

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();

        mapFragment = new MapFragment();
        mRestaurentFragment = new RestaurentFragment();
        mPizzaListFragment = new PizzaListFragment();
        mFoodMenuFragment = new FoodMenuFragment();
        mOrderFragment = new OrderFragment();
        mBasketFragment = new BasketFragment();
        mAddressFragment = new AddressFragment();
        mRestaurantFullDetailFragment = new RestaurantFullDetailFragment();
        mFavoriteFragment = new FavoriteFragment();
        myOptionsFragment = new MyOptionsFragment();
        mRecommendAppFragment = new RecommendAppFragment();
        mShareAppFragment = new ShareAppFragment();
        mTermsConditionsFragment = new TermsConditionsFragment();
        mOrderStatusFragment = new OrderStatusFragment();


        ft.add(R.id.navigationactivity_framelayout, mapFragment, "Mapfragment");
        ft.add(R.id.navigationactivity_framelayout, mRestaurentFragment,
                "Restaurentfragment");
        ft.add(R.id.navigationactivity_framelayout, mFoodMenuFragment,
                "Foodmenufragment");
        ft.add(R.id.navigationactivity_framelayout, mPizzaListFragment,
                "Pizzalistfragment");
        ft.add(R.id.navigationactivity_framelayout, mOrderFragment,
                "Orderfragment");
        ft.add(R.id.navigationactivity_framelayout, mBasketFragment, "Basketfragment");
        ft.add(R.id.navigationactivity_framelayout, mAddressFragment, "AddressFragment");
        ft.add(R.id.navigationactivity_framelayout, mRestaurantFullDetailFragment, "RestaurantFullDetailfragment");
        ft.add(R.id.navigationactivity_framelayout, mFavoriteFragment, "FavoriteFragment");
        ft.add(R.id.navigationactivity_framelayout, myOptionsFragment, "MyOptionsFragment");
        ft.add(R.id.navigationactivity_framelayout, mRecommendAppFragment, "RecommendAppFragment");
        ft.add(R.id.navigationactivity_framelayout, mShareAppFragment, "ShareAppFragment");
        ft.add(R.id.navigationactivity_framelayout, mTermsConditionsFragment, "TermsConditionsFragment");
        ft.add(R.id.navigationactivity_framelayout, mOrderStatusFragment, "OrderStatusFragment");

        ft.commit();

        //  mapFragment.initmap();
    }

    private void addItemstoList() {

        mArrayListNavigationItem = new ArrayList<>();
//        title = "ThinkPizza";

        for (int i = 0; i < menuitems.length; i++) {
            mArrayListNavigationItem.add(new NavigationListItemModel(menuitems[i], menuicons[i], menuicons_selected[i]));
        }

        mNavigationListAdapter = new NavigationListAdapter(NavigationActivity.this,
                mArrayListNavigationItem);
        mNavigationListAdapter.navDrawerItems.get(0).setFlag(true);
        mListView.setAdapter(mNavigationListAdapter);

    }

    /*
     * @Override public void setTitle(CharSequence title) {
     *
     * this.title = title.toString();
     * getSupportActionBar().setTitle(this.title); }
     *
     * @Override protected void onPostCreate(Bundle savedInstanceState) {
     * super.onPostCreate(savedInstanceState); // Sync the toggle state after
     * onRestoreInstanceState has occurred. mActionBarDrawerToggle.syncState();
     * }
     *
     * @Override public void onConfigurationChanged(Configuration newConfig) {
     * super.onConfigurationChanged(newConfig); // Pass any configuration change
     * to the drawer toggls
     * mActionBarDrawerToggle.onConfigurationChanged(newConfig); }
     */

    /*
     * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * action bar item clicks here. The action bar will // automatically handle
	 * clicks on the Home/Up button, so long // as you specify a parent activity
	 * in AndroidManifest.xml. int id = item.getItemId();
	 * 
	 * // noinspection SimplifiableIfStatement if (id == R.id.action_settings) {
	 * return true; }
	 * 
	 * // Activate the navigation drawer toggle if
	 * (mActionBarDrawerToggle.onOptionsItemSelected(item)) { return true; }
	 * 
	 * return super.onOptionsItemSelected(item); }
	 */

    @Override
    public void onBackPressed() {
        if (isDrawerOpen) {
            System.out.println("Navigation.GoBack(): Drawer is visible will be closed.");
            isDrawerOpen = false;
            mDrawerLayout.closeDrawer(Gravity.LEFT);
//                }
        } else if (backstacklist.size() > 1) {

           /* System.out.println("Navigation_drawer.java: inside on back: "
                    + backstacklist.size());

            System.out.println("Last Visible Fragment Tag: " + mFragmentCurrentVisible.getTag());
            System.out.println("Current Visible Fragment Tag: "
                    + (backstacklist.get(backstacklist.size() - 1).getTag()));


            *//*
            to hide the addition and pizza crust layouts.
             *//*

            System.out.println("onBack: isAdditionVisible:" + mOrderFragment.mLinearLayoutAdditionScreen.isShown());
            System.out.println("onBack: isCrust:" + mOrderFragment.mLinearLayoutPizzaBaseScreen.isShown());

            if (mFragmentCurrentVisible == mOrderFragment) {
                System.out.println("Gone in mFragmentCurrentVisible condition: " + mFragmentCurrentVisible.getTag());
//                if (mOrderFragment.mLinearLayoutAdditionScreen.getVisibility() == View.VISIBLE) {
                if (mOrderFragment.mLinearLayoutAdditionScreen.isShown()) {
                    System.out.println("Addition Screen will be hidden");
                    mOrderFragment.mLinearLayoutAdditionScreen.setVisibility(View.GONE);
                    mOrderFragment.mLinearLayoutAdditionScreen.startAnimation(Utils.bottom);
                    Utils.HideLayout(mOrderFragment.mLinearLayoutAdditionScreen);
                }
//            if (mOrderFragment.mLinearLayoutPizzaBaseScreen.getVisibility() == View.VISIBLE) {
                if (mOrderFragment.mLinearLayoutPizzaBaseScreen.isShown()) {
                    System.out.println("Pizza Crust Screen will be hidden");
                    mOrderFragment.mLinearLayoutPizzaBaseScreen.setVisibility(View.GONE);
                }
            } else {
                showFragment(backstacklist.get(backstacklist.size() - 1), false);
                backstacklist.remove(backstacklist.size() - 1);

                Log.e("BackstackList Remove", "Fragment " + (backstacklist.size() - 1) + " remove.");
            }
*/
            GoBack();

        } else {

//            if (mFragmentCurrentVisible == mapFragment && mapFragment.mRelativeLayoutListMode.isShown()) {
            if (mFragmentCurrentVisible == mapFragment && mapFragment.isModeList) {
                System.out.println("Navigation.GoBack(): Map Fragment is visible.");
                System.out.println("Navigation.GoBack(): Map Fragment is visible. and Restaurant List mode is shown");
                mapFragment.ShowMapinNavigationScreen();
//                mImageViewModeMap.setVisibility(View.VISIBLE);
//                mImageViewModeList.setVisibility(View.INVISIBLE);
            } else if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show();
                System.out.println("Navigation_drawer.java: else on back: " + backstacklist.size());

                // Execute some code after 2 seconds have passed
                Handler handler = new Handler();
                System.out.println("Thread will be started");
                handler.postDelayed(new Runnable() {
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                        System.out.println("Thread is stopped.");
                    }
                }, 3000);

            }
        }

    }

    public void GoBack() {
        try {
            if (backstacklist.size() > 0) {
                System.out.println("Navigation_drawer.java: inside on back: Size"
                        + backstacklist.size());

                System.out.println("Last Visible Fragment Tag: " + (backstacklist.get(backstacklist.size() - 1).getTag()));
                System.out.println("Current Visible Fragment Tag: "
                        + (backstacklist.get(backstacklist.size() - 2).getTag()));

            /*
            to remove the orderes placed from arraylist
             */

                if ((mFragmentCurrentVisible == mFoodMenuFragment && mArrayListOrderedItem.size() > 0)
                        /*|| (mFragmentCurrentVisible == mBasketFragment && mArrayListOrderedItem.size() > 0)
                        && Constants.isOrderHistoryFavorite*/) {

                    String msg = "";
                    /*if (Constants.isOrderHistoryFavorite) {
                        System.out.println("Navigation.GoBack(): Basket Fragment  is visible.");
                        msg = "Are you sure you want to change Order? if Yes then your current order will be Canceled.";
                    } else {*/
                    System.out.println("Navigation.GoBack(): FoodMenu Fragment  is visible.");
                    msg = "Are you sure you want to change Pizzaria? if Yes then your current order will be lost.";
                    /*}*/


//                if (mBasketFragment.mArrayListOrderedItem.size() > 0) {
                    System.out.println("Navigation.GoBack(): Order size:" + mArrayListOrderedItem.size());
                    new AlertDialog.Builder(NavigationActivity.this)
                            .setTitle("Please Confirm")
                            .setMessage(msg)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    mArrayListOrderedItem.clear();
                                    mStore_pref.removeOrderedData();
                                    mBasketFragment.mListViewOrderedItems.setVisibility(View.GONE);
                                    mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
                                    Log.e("BackstackList Remove", "Fragment " + backstacklist.get(backstacklist.size() - 1).getTag() + " removed.");
                                    backstacklist.remove(backstacklist.size() - 1);
                                    ShowHideIcons();
                                    showFragments();
                                    showFragment(backstacklist.get(backstacklist.size() - 1), false);

                                    /*
                                    Static value for canceling order set to defaults so that
                                    pressing ok. and Default Address as Current location is set.
                                     */
                                    Constants.isOrderHistoryFavorite = false;
                                    mapFragment.getFullAddress_CurrentLocation();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
//                }
                } /*else if (mFragmentCurrentVisible == mapFragment) {
                    System.out.println("Navigation.GoBack(): Map Fragment is visible.");
                    if (mapFragment.mRelativeLayoutListMode.isShown()) {
                        System.out.println("Navigation.GoBack(): Map Fragment is visible. and Restaurant List mode is shown");
                        mapFragment.mRelativeLayoutListMode.setVisibility(View.GONE);
                        mapFragment.mRelativeLayoutMapMode.setVisibility(View.VISIBLE);
                        mapFragment.ShowMapinNavigationScreen();

                    }
                } */ else if (mFragmentCurrentVisible == mBasketFragment) {
                    System.out.println("Navigation.GoBack(): Basket Fragment is visible.");

                    if (mBasketFragment.mRelativeLayoutOrderedItems.isShown() && mArrayListOrderedItem.size() > 0 && Constants.isOrderHistoryFavorite) {

                        System.out.println("Navigation.GoBack(): Basket Fragment is visible. and currenlly Orderedlist layout is shown");

                        String msg = "";

                        System.out.println("Navigation.GoBack(): Basket Fragment  is visible.");
                        msg = "Are you sure you want to change Order? if Yes then your current order will be Canceled.";

                        System.out.println("Navigation.GoBack(): Order size:" + mArrayListOrderedItem.size());
                        new AlertDialog.Builder(NavigationActivity.this)
                                .setTitle("Please Confirm")
                                .setMessage(msg)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        mArrayListOrderedItem.clear();
                                        mStore_pref.removeOrderedData();
                                        mBasketFragment.mListViewOrderedItems.setVisibility(View.GONE);
                                        mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
                                        Log.e("BackstackList Remove", "Fragment " + backstacklist.get(backstacklist.size() - 1).getTag() + " removed.");
                                        backstacklist.remove(backstacklist.size() - 1);
                                        ShowHideIcons();
                                        showFragments();
                                        showFragment(backstacklist.get(backstacklist.size() - 1), false);

                                    /*
                                    Static value for canceling order set to defaults so that
                                    pressing ok. and Default Address as Current location is set.
                                     */
                                        Constants.isOrderHistoryFavorite = false;
                                        mapFragment.getFullAddress_CurrentLocation();

                                        /*
                                        Code to remove from backstack and show hide layouts
                                         */
                                        mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                                        Log.e("BackstackList Remove", "Fragment " + backstacklist.get(backstacklist.size() - 1).getTag() + " removed.");
                                        backstacklist.remove(backstacklist.size() - 1);
                                        showFragment(backstacklist.get(backstacklist.size() - 1), false);
                                        ShowHideIcons();
                                        showFragments();
                                        mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_not_finished);
                                        mBasketFragment.mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_not_finished);
                                        mBasketFragment.mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_not_finished);
                                        mBasketFragment.lastVisibleLayout = 1;
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    } else if (mBasketFragment.mRelativeLayoutAddressLayout.isShown()) {
                        System.out.println("Navigation.GoBack(): Basket Fragment is visible. and currenlly Address layout is shown");
                        mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                        mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
                        mBasketFragment.mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.lastVisibleLayout = 1;
                    } else if (mBasketFragment.mRelativeLayoutPayment.isShown()) {
                        System.out.println("Navigation.GoBack(): Basket Fragment is visible. and currenlly Payment layout is shown");
                        mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);
                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.VISIBLE);
                        mBasketFragment.mRelativeLayoutPayment.setVisibility(View.GONE);
                        mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_finished_red);
                        mBasketFragment.mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_finished_red);
                        mBasketFragment.mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.lastVisibleLayout = 2;
                    } else {

                        System.out.println("Navigation.GoBack(): Basket Fragment is visible. and gone in else part");
                        mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                        mBasketFragment.mRelativeLayoutAddressLayout.setVisibility(View.GONE);
                        Log.e("BackstackList Remove", "Fragment " + backstacklist.get(backstacklist.size() - 1).getTag() + " removed.");
                        backstacklist.remove(backstacklist.size() - 1);
                        showFragment(backstacklist.get(backstacklist.size() - 1), false);
                        ShowHideIcons();
                        showFragments();
                        mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.mImageViewProcess2.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.mImageViewProcess3.setImageResource(R.drawable.icon_basket_processs_not_finished);
                        mBasketFragment.lastVisibleLayout = 1;
                    }
                } else {
                    Log.e("BackstackList Remove", "Fragment " + backstacklist.get(backstacklist.size() - 1).getTag() + " removed.");
                    backstacklist.remove(backstacklist.size() - 1);
                    showFragment(backstacklist.get(backstacklist.size() - 1), false);
                    ShowHideIcons();
                    showFragments();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void ShowHideIcons() {

        if (mFragmentCurrentVisible == mapFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.VISIBLE);
//            mImageViewModeMap.setVisibility(View.INVISIBLE);
//            mImageViewModeList.setVisibility(View.VISIBLE);
//            mEditTextSearch.setVisibility(View.GONE);
            mEditTextSearchPizza.setVisibility(View.GONE);
            mAutoCompleteTextViewSearch.setVisibility(View.GONE);
            mAutoCompleteTextViewSearch.setHint(R.string.search_zipcode);
            mImageViewSearch.setVisibility(View.VISIBLE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(location_map_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_select_location));

            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.VISIBLE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);


            for (int i = 0; i < mNavigationListAdapter.getCount(); i++) {
                mNavigationListAdapter.navDrawerItems.get(i).setFlag(false);
            }
            mNavigationListAdapter.navDrawerItems.get(0).setFlag(true);
            mNavigationListAdapter.notifyDataSetChanged();

            if (mapFragment.isModeList) {
                mapFragment.ShowRestroListinNavigationScreen();
            } else if (mapFragment.isModeMap) {
                mapFragment.ShowMapinNavigationScreen();
            } else {
                mapFragment.ShowMapinNavigationScreen();
            }
        }
        if (mFragmentCurrentVisible == mRestaurentFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(restaurant_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_select_restaurant));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.VISIBLE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);
        }

        if (mFragmentCurrentVisible == mAddressFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.VISIBLE);
            mImageViewModeMap.setVisibility(View.INVISIBLE);
            mImageViewModeList.setVisibility(View.INVISIBLE);
            mEditTextSearchPizza.setVisibility(View.GONE);
            mAutoCompleteTextViewSearch.setVisibility(View.GONE);
            mImageViewSearch.setVisibility(View.VISIBLE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(delivery_icon_100);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_select_delivery_address));

            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);
        }

        if (mFragmentCurrentVisible == mFoodMenuFragment) {

            mImageViewBack.setVisibility(View.VISIBLE);
            mImageViewNavigate.setVisibility(View.INVISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(restaurant_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_select_menu));

//            mImageViewLogout.setVisibility(View.GONE);
            mLinearLayoutCart.setVisibility(View.VISIBLE);


            if (mArrayListOrderedItem != null && mArrayListOrderedItem.size() > 0) {
                System.out.println("NavigationActivity.ShowHideIcons(): No of Ordered Pizzas:" + mArrayListOrderedItem.size());
                mImageViewCart.setImageResource(R.drawable.cart_selected_60);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            } else {
                mImageViewCart.setImageResource(R.drawable.cart_60_2);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            }
            mImageViewFloatingCart.setVisibility(View.VISIBLE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);
        }
        if (mFragmentCurrentVisible == mPizzaListFragment) {

            mImageViewBack.setVisibility(View.VISIBLE);
            mImageViewNavigate.setVisibility(View.INVISIBLE);

            mRelativeLayoutMap.setVisibility(View.VISIBLE);
            mImageViewModeList.setVisibility(View.INVISIBLE);
            mEditTextSearchPizza.setVisibility(View.GONE);
            mAutoCompleteTextViewSearch.setHint(R.string.search_ingredient);
            mImageViewSearch.setVisibility(View.VISIBLE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(restaurant_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_select_pizza));

//            mImageViewLogout.setVisibility(View.GONE);
            mLinearLayoutCart.setVisibility(View.VISIBLE);

            if (mArrayListOrderedItem != null && mArrayListOrderedItem.size() > 0) {
                System.out.println("NavigationActivity.ShowHideIcons(): No of Ordered Pizzas:" + mArrayListOrderedItem.size());
                mImageViewCart.setImageResource(R.drawable.cart_selected_60);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            } else {
                mImageViewCart.setImageResource(R.drawable.cart_60_2);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            }
            mImageViewFloatingCart.setVisibility(View.VISIBLE);
        }

        if (mFragmentCurrentVisible == mOrderFragment) {

            mImageViewBack.setVisibility(View.VISIBLE);
            mImageViewNavigate.setVisibility(View.INVISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(restaurant_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_order_pizza));

//            mImageViewLogout.setVisibility(View.GONE);
            mLinearLayoutCart.setVisibility(View.GONE);

            if (mArrayListOrderedItem != null && mArrayListOrderedItem.size() > 0) {
                System.out.println("No of Ordered Pizzas:" + mArrayListOrderedItem.size());
                mImageViewCart.setImageResource(R.drawable.cart_selected_60);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            } else {
                mImageViewCart.setImageResource(R.drawable.cart_60_2);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            }
            mImageViewFloatingCart.setVisibility(View.GONE);
        }

        if (mFragmentCurrentVisible == mRestaurantFullDetailFragment) {

            mImageViewBack.setVisibility(View.VISIBLE);
            mImageViewNavigate.setVisibility(View.INVISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);

            mLinearLayoutScreenNameLogo.setVisibility(View.VISIBLE);
            mImageViewScreenIcon.setImageResource(restaurant_icon_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_restaurant_detail));

//            mImageViewLogout.setVisibility(View.GONE);
            mLinearLayoutCart.setVisibility(View.GONE);

            if (mArrayListOrderedItem != null && mArrayListOrderedItem.size() > 0) {
                System.out.println("No of Ordered Pizzas:" + mArrayListOrderedItem.size());
                mImageViewCart.setImageResource(R.drawable.cart_selected_60);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            } else {
                mImageViewCart.setImageResource(R.drawable.cart_60_2);
                mTextViewCartItemNo.setText("" + mArrayListOrderedItem.size());
            }
            mImageViewFloatingCart.setVisibility(View.VISIBLE);
        }
        if (mFragmentCurrentVisible == mBasketFragment) {

            mImageViewBack.setVisibility(View.VISIBLE);
            mImageViewNavigate.setVisibility(View.INVISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(basket_icon_white_60);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_basket));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.VISIBLE);
            mImageViewFloatingCart.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);

        }
        if (mFragmentCurrentVisible == mFavoriteFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            if (Constants.isHistoryServiceCalled) {
                mImageViewScreenIcon.setImageResource(history_pizza_60);
                mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_history));
            } else {
                mImageViewScreenIcon.setImageResource(favourite_pizza_100);
                mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_favorite));
            }
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);

        }

        if (mFragmentCurrentVisible == myOptionsFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(favourite_pizza_100);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_myoptions));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);

        }

        if (mFragmentCurrentVisible == mRecommendAppFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(favourite_pizza_100);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_recommend_apps));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);
        }
        if (mFragmentCurrentVisible == mShareAppFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(favourite_pizza_100);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_shareapp));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);
        }
        if (mFragmentCurrentVisible == mTermsConditionsFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(favourite_pizza_100);
            mTextViewScreenName.setText(getResources().getString(R.string.act_navigation_tv_screenname_terms_conditions));
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);
//            mImageViewFloatingEditProfile.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);
        }

        if (mFragmentCurrentVisible == mOrderStatusFragment) {

            mImageViewBack.setVisibility(View.GONE);
            mImageViewNavigate.setVisibility(View.VISIBLE);

            mRelativeLayoutMap.setVisibility(View.GONE);
            mImageViewScreenIcon.setImageResource(order_status_white);
            mTextViewScreenName.setText("Order Status");
            mLinearLayoutCart.setVisibility(View.GONE);
//            mImageViewLogout.setVisibility(View.GONE);
            mImageViewFloatingCart.setVisibility(View.GONE);

            mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.GONE);

        }

    }

    public void showFragments() {
        for (int i = 0; i < backstacklist.size(); i++)
            System.out.println("Fragment " + i + " " + backstacklist.get(i).getTag());
    }

//    public void InitSharingDialog() {
//        mRelativeLayoutSharing = (RelativeLayout) findViewById(R.id.navigationactivity_layout_sharing);
//        mTextViewShareFB = (TextView) findViewById(R.id.navigation_activity_share_fb);
//        mTextViewShareGmail = (TextView) findViewById(R.id.navigation_activity_share_gmail);
//        mTextViewShareWhatsApp = (TextView) findViewById(R.id.navigation_activity_share_whatsapp);
//        mTextViewShareCancel = (TextView) findViewById(R.id.navigation_activity_share_cancel);
//
//        mTextViewShareFB.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
////                selectApp("facebook");
//                        Toast.makeText(NavigationActivity.this, "Feature not implement.", Toast.LENGTH_SHORT).show();
//                    }
//                }, 1000);
//                mRelativeLayoutSharing.startAnimation(animSlideUP);
//                mRelativeLayoutSharing.setVisibility(View.GONE);
//                mRelativeLayoutSharing.getAnimation().cancel();
//            }
//        });
//        mTextViewShareGmail.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        // Actions to do after 10 seconds
////                selectApp("facebook");
//                        Toast.makeText(NavigationActivity.this, "Feature not implement.", Toast.LENGTH_SHORT).show();
////                Animation animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
////                mRelativeLayoutSharing.startAnimation(animSlideUp);
//                        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
//                        intent.setType("*/*");
//                        intent.putExtra(Intent.EXTRA_SUBJECT, "Share the ThinkPizza App.");
//                        intent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now !");
//                        try {
//                            intent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        final PackageManager pm = getPackageManager();
//                        final List<ResolveInfo> matches = pm.queryIntentActivities(intent, 0);
//                        ResolveInfo best = null;
//                        for (final ResolveInfo info : matches)
//                            if (info.activityInfo.packageName.endsWith(".gm") ||
//                                    info.activityInfo.name.toLowerCase().contains("gmail"))
//                                best = info;
//                        if (best != null)
//                            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
//                        startActivity(intent);
//
//                    }
//                }, 1000);
//                mRelativeLayoutSharing.startAnimation(animSlideUP);
//                mRelativeLayoutSharing.setVisibility(View.GONE);
//                mRelativeLayoutSharing.getAnimation().cancel();
//            }
//        });
//        mTextViewShareWhatsApp.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//                        selectApp("whatsapp");
//                    }
//                }, 1000);
//                mRelativeLayoutSharing.startAnimation(animSlideUP);
//                mRelativeLayoutSharing.setVisibility(View.GONE);
//                mRelativeLayoutSharing.getAnimation().cancel();
//
//            }
//        });
//        mTextViewShareCancel.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mRelativeLayoutSharing.startAnimation(animSlideUP);
//                mRelativeLayoutSharing.setVisibility(View.GONE);
//                mRelativeLayoutSharing.getAnimation().cancel();
////                mRelativeLayoutSharing.setAnimation(null);
//            }
//        });
//        // load the animation
//        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
//        animSlideUP = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
//
//        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                animSlideDown.cancel();
//                animSlideDown.reset();
//                mRelativeLayoutSharing.setAnimation(null);
////                mRelativeLayoutSharing.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//        animSlideUP.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                animSlideUP.cancel();
//                animSlideUP.reset();
//                mRelativeLayoutSharing.setAnimation(null);
////                mRelativeLayoutSharing.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//    }

//    void selectApp(String appnamne) {
//        try {
//
//            Intent shareIntent = new Intent(Intent.ACTION_SEND);
//            shareIntent.setType("*/*");
//            if (!appnamne.equalsIgnoreCase("facebook"))
//                shareIntent.putExtra(Intent.EXTRA_TEXT, "Hungry ?? Thinking about having pizza ? Then Download and order your favorite pizza using ThinkPizza App now ! ");
//            shareIntent.putExtra(Intent.EXTRA_STREAM, getImagefileUri());
//
//                    /*ShareLinkContent content = new ShareLinkContent.Builder()
//                            .setContentUrl(Uri.parse("https://developers.facebook.com"))
//                            .build();
//                    SharePhoto photo = new SharePhoto.Builder().setBitmap(bitmap).build();
//                    SharePhotoContent mSharePhotoContent= new SharePhotoContent.Builder().addPhoto(photo).build();*/
//
//
//            PackageManager pm = getPackageManager();
//
//            List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
//            boolean found = false;
//            for (final ResolveInfo app : activityList) {
//                if ((app.activityInfo.name).contains(appnamne)) {
//
//                    final ActivityInfo activity = app.activityInfo;
//
//                    final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
//
//                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//
//                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//
//                    shareIntent.setComponent(name);
//
//                    startActivity(shareIntent);
//                    found = true;
//                    break;
//                }
//            }
//            if (!found) {
//                Toast.makeText(NavigationActivity.this, "Application is not installed.", Toast.LENGTH_SHORT).show();
//            }
//            startActivity(Intent.createChooser(shareIntent, "Share ThinkPizza App"));
//            Animation animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
//            mRelativeLayoutSharing.startAnimation(animSlideUp);
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//    }
//
//    Uri getImagefileUri() throws IOException {
//        String photoPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Share ThinkPizza/";
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
//        File dir = new File(photoPath);
//        dir.mkdirs();
//        // Create a name for the saved image
//        File file = new File(dir, "thinkpizza.png");
//        OutputStream output = new FileOutputStream(file);
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//        output.flush();
//        output.close();
//        Uri uri = Uri.fromFile(file);
//        return uri;
//    }

    public void setProfile() {
        if (mStore_pref.getUserId().length() > 0) {
            mImageLoader = new ImageLoader(this);
            mImageLoader.DisplayImage(mStore_pref.getUser().getImg_url(), mImageViewProfilePic);
            mTextViewUserName.setText(mStore_pref.getUser().getName());
            mTextViewLocation.setText(mStore_pref.getUser().getCity());
        }
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        if (mDrawerToggle != null)
//            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void searchPizza(String charText) {
        String upperSearch = charText.toUpperCase(Locale.getDefault());
        mPizzaListFragment.mArrayListPizzaListDatas = new ArrayList<>();

        System.out.println("in searchPizza " + charText);

        if (charText.length() == 0) {
            System.out.println("length ===0");
            mPizzaListFragment.mArrayListPizzaListDatas.addAll(mPizzaListFragment.mGetPizzalist.getMenus());
        } else {
            for (int i = 0; i < mPizzaListFragment.mGetPizzalist.getMenus().size(); i++) {
                //System.out.println("in loop");

                PizzaListData mPizzaListData = mPizzaListFragment.mGetPizzalist.getMenus().get(i);

                if (
                        (mPizzaListData.getName().contains(charText) || mPizzaListData.getName().startsWith(charText) || mPizzaListData.getName().toLowerCase().contains(charText))
                                ||
                                (mPizzaListData.getDescription().contains(charText) || mPizzaListData.getDescription().startsWith(charText) || mPizzaListData.getDescription().toLowerCase().contains(charText))
                        ) {
                    System.out.println("in loop inside");
                    mPizzaListFragment.mArrayListPizzaListDatas.add(mPizzaListData);
                } else {
                    for (int j = 0; j < mPizzaListData.getToppings().size(); j++) {
                        Toppings mToppings = mPizzaListData.getToppings().get(j);
                        if (
                                (mToppings.getName().contains(charText) || mToppings.getName().startsWith(charText) || mToppings.getName().toLowerCase().contains(charText))
                                ) {
                            System.out.println("in loop inside");
                            mPizzaListFragment.mArrayListPizzaListDatas.add(mPizzaListData);
                        }
                    }
                }
            }
        }
        mPizzaListFragment.mListView.setAdapter(new PizzaListItemAdapter(mPizzaListFragment.getActivity(), mPizzaListFragment.mArrayListPizzaListDatas));

    }

//    @Override
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        System.out.println("dispatchKeyEvent called.");
//        if (event.getAction() == KeyEvent.KEYCODE_BACK) {
//            System.out.println("dispatchKeyEvent back called.");
//        }
//        return super.dispatchKeyEvent(event);
//    }
//
//    @Override
//    public boolean dispatchKeyShortcutEvent(KeyEvent event) {
//        System.out.println("dispatchKeyShortcutEvent called.");
//        if (event.getAction() == KeyEvent.KEYCODE_BACK) {
//            System.out.println("dispatchKeyShortcutEvent called.");
//        }
//        return super.dispatchKeyShortcutEvent(event);
//    }
//
//    @Override
//    public boolean dispatchGenericMotionEvent(MotionEvent ev) {
//        System.out.println("dispatchGenericMotionEvent called.");
//        return super.dispatchGenericMotionEvent(ev);
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        System.out.println("dispatchTouchEvent called.");
//        return super.dispatchTouchEvent(ev);
//    }

    public void showBackImage() {
        mImageViewBack.setVisibility(View.VISIBLE);
        mImageViewNavigate.setVisibility(View.INVISIBLE);
        mImageViewSearch.setVisibility(View.GONE);
    }
}
