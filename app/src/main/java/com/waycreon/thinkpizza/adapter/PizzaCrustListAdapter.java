package com.waycreon.thinkpizza.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;
import com.waycreon.waycreon.utils.Constants;

import java.util.ArrayList;

public class PizzaCrustListAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<PizzaCrustData> mArrayListPizzaCrustDatas;

    public PizzaCrustData mPizzaCrustDataSelected;
//    public int TotalofSelectedCurst = 0;

    NavigationActivity mNavigationActivity;

    RadioButton mCurrentlyCheckedRB;
//    public boolean isCrustselectedinAdapter = false;

    public PizzaCrustListAdapter(Activity mActivity,
                                 ArrayList<PizzaCrustData> models) {
        this.mActivity = mActivity;
        this.mArrayListPizzaCrustDatas = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mArrayListPizzaCrustDatas.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return mArrayListPizzaCrustDatas.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder mViewHolder;

        final PizzaCrustData mCurrentData = mArrayListPizzaCrustDatas
                .get(position);

//        if (convertView == null) {
        mViewHolder = new ViewHolder();
        convertView = mActivity.getLayoutInflater().inflate(R.layout.pizzacrust_listitems, null);

        mViewHolder.mLayoutOfList = (LinearLayout) convertView
                .findViewById(R.id.pizzacrust_listitem_layoutoflist);

        mViewHolder.mRadioButtonPizzaCrust = (RadioButton) convertView
                .findViewById(R.id.pizzacrust_listitem_radiobutton_item);
        mViewHolder.mTextViewTextPizzaCrustName = (TextView) convertView
                .findViewById(R.id.pizzacrust_listitem_text_pizzacrustname);
        mViewHolder.mTextViewPizzaCrustPrice = (TextView) convertView
                .findViewById(R.id.pizzacrust_listitem_text_pizzacrustprice);


        mViewHolder.mRadioButtonPizzaCrust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (int) v.getTag();

                System.out.println("Gone in setOnClickListener");


                // checks if we already have a checked radiobutton. If we don't, we can assume that
                // the user clicked the current one to check it, so we can remember it.
                if (mCurrentlyCheckedRB == null) {
                    Log.e("PizzaCrustAdapter().mRadioButtonPizzaCrust.setOnClickListener", "Gone in the mCurrentlyCheckedRB=null");
//                    isCrustselectedinAdapter = true;
                    mCurrentlyCheckedRB = (RadioButton) v;
                    mCurrentlyCheckedRB.setChecked(true);
                }
                // If the user clicks on a RadioButton that we've already stored as being checked, we
                // don't want to do anything.
                if (mCurrentlyCheckedRB == v) {
                    Log.e("PizzaCrustAdapter().mRadioButtonPizzaCrust.setOnClickListener", "Gone in the mCurrentlyCheckedRB=v");
                    mArrayListPizzaCrustDatas.get(pos).setIsPizzaCrustSelected(true);
                    mPizzaCrustDataSelected = mArrayListPizzaCrustDatas.get(pos);
//                        TotalofSelectedCurst = mArrayListPizzaCrustDatas.get(pos).getPizzaCrustPrice();
                    mNavigationActivity.mOrderFragment.mTextViewTotalPizzaCrust_dialog.setText("" + mPizzaCrustDataSelected.getPrice());
//                        v.setBackground(mActivity.getResources().getDrawable(R.drawable.icon_addition_selected_dark_60));

                    return;
                } else {
                    Log.e("PizzaCrustAdapter().mRadioButtonPizzaCrust.setOnClickListener", "Gone in the else");
                    // Otherwise, uncheck the currently checked RadioButton, check the newly checked
                    // RadioButton, and store it for future reference.
//                    isCrustselectedinAdapter = true;
                    mCurrentlyCheckedRB.setChecked(false);

                    for (int i = 0; i < mArrayListPizzaCrustDatas.size(); i++) {
                        mArrayListPizzaCrustDatas.get(i).setIsPizzaCrustSelected(false);
                    }
                    ((RadioButton) v).setChecked(true);
                    mCurrentlyCheckedRB = (RadioButton) v;
                    mArrayListPizzaCrustDatas.get(pos).setIsPizzaCrustSelected(true);
                    mPizzaCrustDataSelected = mArrayListPizzaCrustDatas.get(pos);
                    mNavigationActivity.mOrderFragment.mTextViewTotalPizzaCrust_dialog.setText("" + mPizzaCrustDataSelected.getPrice());
                    System.out.println("PizzaCrustListAdapter: PizzaCrust selected: " + mArrayListPizzaCrustDatas.get(position)
                            .getName() + " at Position: " + pos);
                }


            }
        });

           /* mViewHolder.mRadioButtonPizzaCrust.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    System.out.println("Gone in onCheckedChanged");
                    System.out.println("PizzaCrustListAdapter: PizzaCrust selected: " + mArrayListPizzaCrustDatas.get(position)
                            .getPizzaCrustName() + " at Position: " + position);

                    if (mPizzaCrustDataSelected != null) {
                        System.out.println("Gone in notnull");
                        for (int i = 0; i < mArrayListPizzaCrustDatas.size(); i++) {
                            if (mArrayListPizzaCrustDatas.get(position).getPizzaCrustName()
                                    .equalsIgnoreCase(mPizzaCrustDataSelected.getPizzaCrustName())) {

                                System.out.println("PizzaCrust Adapter: Deselected: " + mArrayListPizzaCrustDatas.get(i).
                                        getPizzaCrustName() + " at Position: " + i);
                                mViewHolder.mRadioButtonPizzaCrust.setChecked(false);
                                mArrayListPizzaCrustDatas.get(position).setIsPizzaCrustSelected(false);

//                                TotalofSelectedCurst = mArrayListPizzaCrustDatas.get(position).getPizzaCrustPrice();
//                                mNavigationActivity.mOrderFragment.mTextViewTotalAdditoin.setText("" + TotalofSelectedCurst);
                                notifyDataSetChanged();
//                                break;
                            }
                        }
                    }

                    notifyDataSetChanged();

                    if (b) {

//                        compoundButton.setBackground(mActivity.getResources().getDrawable(R.drawable.icon_addition_selected_dark_60));
                        mArrayListPizzaCrustDatas.get(position).setIsPizzaCrustSelected(true);
                        mPizzaCrustDataSelected = mArrayListPizzaCrustDatas.get(position);
                        TotalofSelectedCurst = mArrayListPizzaCrustDatas.get(position).getPizzaCrustPrice();
                        mNavigationActivity.mOrderFragment.mTextViewTotalPizzaCrust.setText("" + TotalofSelectedCurst);


                    } else {
//                        compoundButton.setBackground(mActivity.getResources().getDrawable(R.drawable.icon_addition_not_selected_dark_60));
//                        mArrayListPizzaCrustDatas.get(position).setIsPizzaCrustSelected(false);

                        *//*for (int i = 0; i < mArrayListPizzaCrustDatas.size(); i++) {
                            if (mArrayListPizzaCrustDatas.get(position).getPizzaCrustName()
                                    .equalsIgnoreCase(mPizzaCrustDataSelected.getPizzaCrustName())) {

                                System.out.print("PizzaCrustListAdapter: Crust Deselected: " + mArrayListPizzaCrustDatas.get(i).
                                        getPizzaCrustName() + " at Position: " + i);
                                mViewHolder.mRadioButtonPizzaCrust.setChecked(false);
                                TotalofSelectedCurst -= mArrayListPizzaCrustDatas.get(position).getPizzaCrustPrice();
                                mNavigationActivity.mOrderFragment.mTextViewTotalAdditoin.setText("" + TotalofSelectedCurst);
                            }
                        }*//*


                    }
                    notifyDataSetChanged();
                }
            });*/
        convertView.setTag(mViewHolder);

//        } else
//
//        {
//            mViewHolder = (ViewHolder) convertView.getTag();
//        }

        mViewHolder.mRadioButtonPizzaCrust.setTag(position);
        mViewHolder.mTextViewTextPizzaCrustName.setText(mCurrentData
                .getName());

        mViewHolder.mTextViewPizzaCrustPrice.setText("" + mCurrentData
                .getPrice());

        if (mCurrentData.isPizzaCrustSelected()) {
//            mCurrentlyCheckedRB.setChecked(false);
            mViewHolder.mRadioButtonPizzaCrust.setChecked(true);
//            isCrustselectedinAdapter = true;
            mCurrentlyCheckedRB = mViewHolder.mRadioButtonPizzaCrust;
        } else
            mViewHolder.mRadioButtonPizzaCrust.setChecked(false);

        mArrayListPizzaCrustDatas.get(position).printAllData();

        if (Constants.isOrderStatusShown) {
            mViewHolder.mRadioButtonPizzaCrust.setEnabled(false);
        } else {
            mViewHolder.mRadioButtonPizzaCrust.setEnabled(true);
        }

        return convertView;
    }

    public class ViewHolder {
        RadioButton mRadioButtonPizzaCrust;
        TextView mTextViewTextPizzaCrustName;
        TextView mTextViewPizzaCrustPrice;
        LinearLayout mLayoutOfList;
    }

}
 /*
                    new code for showing the items selected
                    this code is pasted from the EditOrderedItems() method
                     */

/*
selectedCrustIds = "";
        for (int i = 0; i < mPizzaCrustDatas_dialog.size(); i++) {

        if (mPizzaCrustDatas_dialog.get(i).getName().
        equalsIgnoreCase(mOrderedItemEdit.getPizzacrust().getName())) {

        System.out.println("In Loop Pizza Crust Name:" + mOrderedItemEdit.getPizzacrust().getName());
        mPizzaCrustListAdapter_dialog.mPizzaCrustDataSelected = mOrderedItemEdit.getPizzacrust();
        mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(true);

        selectedCrustIds = mOrderedItemEdit.getPizzacrust().getId();
        totalpizzacrust_selected = mOrderedItemEdit.getPizzacrust().getPrice();
        } else {
        mPizzaCrustDatas_dialog.get(i).setIsPizzaCrustSelected(false);
        }

        }

        Log.e("EditOrderedItem().PizzaCrust-->", "Crust ID:" + selectedCrustIds);
        Log.e("EditOrderedItem().PizzaCrust-->", "Crust Name:" + mOrderedItemEdit.getPizzacrust().getName());
        Log.e("EditOrderedItem().PizzaCrust-->", "Crust Price:" + totalpizzacrust_selected);

        mTextViewTotalPizzaCrust_dialog.setText("" + totalpizzacrust_selected);

        mTextViewPizzabaseSelected.setText(mOrderedItemEdit.getPizzacrust().getName());
        mTextViewPizzaBasePrice.setText("" + mOrderedItemEdit.getPizzacrust().getPrice());
        mImageViewPizzabase.setBackgroundResource(R.drawable.checked);


        ispizzacrustselected = true;
        mPizzaCrustListAdapter_dialog.notifyDataSetChanged();
*/
