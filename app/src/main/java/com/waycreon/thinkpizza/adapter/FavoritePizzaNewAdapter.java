package com.waycreon.thinkpizza.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.OrderedItemsDataNew;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FavoritePizzaNewAdapter extends ArrayAdapter<OrderedItemsDataNew> {

    Activity mActivity;

    List<OrderedItemsDataNew> mListOrderedItemsDatas;
    ImageLoader mImageLoader;
    NavigationActivity mNavigationActivity;
    String DialogTag = "";
    ProgressDialog mProgressDialog;
    /*
    Tags set are
    ----------------
    FavoritePizza
    HistoryPizza
    FragmentFavorite
    -----------------
     */

    Dialog mDialogFavoriteRemove;

    public FavoritePizzaNewAdapter(Activity mActivity, List<OrderedItemsDataNew> mOrderedItemsDatas, String forDialog) {
        super(mActivity, R.layout.dialog_pizza_new, mOrderedItemsDatas);

        this.mActivity = mActivity;
        this.mListOrderedItemsDatas = mOrderedItemsDatas;
        mImageLoader = new ImageLoader(mActivity);
        Log.e("  FavoritePizzaAdapter()-->", "Size of Order list: " + mOrderedItemsDatas.size());
        mNavigationActivity = (NavigationActivity) mActivity;
        DialogTag = forDialog;
        System.out.println("FavoritePizzaAdapter()-->DialogTag: " + forDialog);
    }

    @Override
    public int getCount() {
        return mListOrderedItemsDatas.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;


        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.dialog_pizza_new, null);

            mViewHolder.mRelativeLayoutParent = (RelativeLayout) convertView.findViewById(R.id.dialog_pizza_parent_layout);

//            mViewHolder.mImageViewPizzaImage = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_pizza);
            mViewHolder.mTextViewRestaurantName = (TextView) convertView.findViewById(R.id.dialog_pizza_new_restaurant_name);
            mViewHolder.mTextViewOrderID = (TextView) convertView.findViewById(R.id.dialog_pizza_new_orderid);
            mViewHolder.mTextViewOrderDate = (TextView) convertView.findViewById(R.id.dialog_pizza_text_pizzacrust);
            mViewHolder.mTextViewTag = (TextView) convertView.findViewById(R.id.dialog_pizza_new_tag);
            mViewHolder.mTextViewDeliveryAddress = (TextView) convertView.findViewById(R.id.dialog_pizza_new_deliveryaddress);

//            mViewHolder.mRelativeLayoutCustoms = (LinearLayout) convertView.findViewById(R.id.dialog_pizza_layout_custom_ingredients);
//            mViewHolder.mTextViewCustomIngredients = (TextView) convertView.findViewById(R.id.dialog_pizza_text_custom_selected);
//            mViewHolder.mTextViewCustomPrice = (TextView) convertView.findViewById(R.id.dialog_pizza_text_custom_selecte_price);
            mViewHolder.mImageViewAddtoCart = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_addtocart);
//            mViewHolder.mImageViewAddress = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_address);
            mViewHolder.mImageViewRemoveFavorite = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_remove_favorite);

//            if (DialogTag.equalsIgnoreCase("FavoritePizza") || DialogTag.equalsIgnoreCase("HistoryPizza")) {
//
//                mViewHolder.mImageViewAddtoCart.setVisibility(View.VISIBLE);
////                mViewHolder.mImageViewAddress.setVisibility(View.VISIBLE);
//                if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
//                    mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);
//                } else if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
//                    mViewHolder.mImageViewRemoveFavorite.setVisibility(View.VISIBLE);
//                }
//
//            } else if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
////                mViewHolder.mImageViewAddtoCart.setVisibility(View.GONE);
////                mViewHolder.mImageViewAddress.setVisibility(View.GONE);
//                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.VISIBLE);
//            } else if (DialogTag.equalsIgnoreCase("AddToFavorite")) {
//                mViewHolder.mImageViewAddtoCart.setVisibility(View.GONE);
////                mViewHolder.mImageViewAddress.setVisibility(View.GONE);
//                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);
//            }

            if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {

                mViewHolder.mImageViewAddtoCart.setVisibility(View.VISIBLE);
                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.VISIBLE);

            } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                mViewHolder.mImageViewAddtoCart.setVisibility(View.VISIBLE);
                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);

            } else {
                mViewHolder.mImageViewAddtoCart.setVisibility(View.VISIBLE);
                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);
            }

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mTextViewRestaurantName.setText(mListOrderedItemsDatas.get(position).getName());
        mViewHolder.mTextViewOrderID.setText("Order ID: " + mListOrderedItemsDatas.get(position).getOrder_id());
        mViewHolder.mTextViewOrderDate.setText("Order Date: " + mListOrderedItemsDatas.get(position).getOrder_date());
        mViewHolder.mTextViewTag.setText(mListOrderedItemsDatas.get(position).getDeliveryAddress().getTag_name());

        String add = "";
        add += mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_name();
        if (mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_no().length() > 0)
            add += ", " + mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_no();
        if (mListOrderedItemsDatas.get(position).getDeliveryAddress().getApt_no().length() > 0)
            add += ",\n" + mListOrderedItemsDatas.get(position).getDeliveryAddress().getApt_no();
        mViewHolder.mTextViewDeliveryAddress.setText(add);

        mViewHolder.mRelativeLayoutParent.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); Position Selected is: " + position);
                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); DialogTag Selected is: " + DialogTag);
//
//                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
//                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
//                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
//                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
//                        } else if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
                        mNavigationActivity.mapFragment.setFavoriteAddressFromFragmentFavorite(mListOrderedItemsDatas.get(position).getDeliveryAddress(), position);

                            /*
                            to add the item to the cart
                             */

                    /*
                        below code added to GetOrderedData Service so that
                        after getting response screen will be shown
                    */
                       /* mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                        Log.e("BackstackList", "BasketFragment Fragment is added.");
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
                        // mNavigationActivity.onBackPressed();
                        mNavigationActivity.ShowHideIcons();
                        mNavigationActivity.showFragments();
//                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));*/
                        callGetOrderData(mListOrderedItemsDatas.get(position).getOrder_id());
                    }

//                    }
                }

        );

        mViewHolder.mImageViewAddtoCart.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        System.out.println("FavoritePizzaAdapter.mImageViewAddtoCart.setOnClickListener(); Position Selected is: " + position);
                        System.out.println("FavoritePizzaAdapter.mImageViewAddtoCart.setOnClickListener(); DialogTag Selected is: " + DialogTag);

                        mNavigationActivity.mapFragment.setFavoriteAddressFromFragmentFavorite(mListOrderedItemsDatas.get(position).getDeliveryAddress(), position);

                        callGetOrderData(mListOrderedItemsDatas.get(position).getOrder_id());


//                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
//                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
//                            mNavigationActivity.mapFragment.mDialogPizzaFavorite.dismiss();
//                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
//                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
//                            mNavigationActivity.mapFragment.mDialogPizzaHistory.dismiss();
//                        }
//                        mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
//                        Log.e("BackstackList", "BasketFragment Fragment is added.");
//                        mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
////                mNavigationActivity.onBackPressed();
//                        mNavigationActivity.ShowHideIcons();
//                        mNavigationActivity.showFragments();
//                        mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));

                    }
                }

        );

       /* mViewHolder.mImageViewAddress.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); Position Selected is: " + position);
                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); DialogTag Selected is: " + DialogTag);

                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaFavorite.dismiss();
                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaHistory.dismiss();

                        }
                    }
                }
        );*/

        mViewHolder.mImageViewRemoveFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (DialogTag.equalsIgnoreCase("FavoritePizza")
                        || DialogTag.equalsIgnoreCase("FragmentFavorite")) {

                    initDialogRemoveFavoritePizza(position);
//                    new DeleteFavoritePizzaService(mListOrderedItemsDatas.get(position).getId(), position).execute();
                }

            }
        });

        return convertView;
    }


    public class ViewHolder {

        RelativeLayout mRelativeLayoutParent;
        //        ImageView mImageViewPizzaImage;
//        TextView mTextViewPizzaname;
//        TextView mTextViewPizzaIngredients;
//        TextView mTextViewPizzaCrust;
//        TextView mTextViewCustomIngredients;
//
        ImageView mImageViewAddtoCart;
        //        ImageView mImageViewAddress;
        ImageView mImageViewRemoveFavorite;

        //        RelativeLayout mRelativeLayoutCustoms;
//        LinearLayout mRelativeLayoutCustoms;
//        TextView mTextViewCustomPrice;

        TextView mTextViewRestaurantName;
        TextView mTextViewOrderID;
        TextView mTextViewOrderDate;
        TextView mTextViewTag;
        TextView mTextViewDeliveryAddress;
    }

    private void callGetOrderData(String order_id) {
        new OrderDataSerivce(order_id).execute();
    }

    public class OrderDataSerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        Store_pref mStore_pref;
        Gson mGson;
        ArrayList<OrderedItemsData> mOrderedItemsDatasFavorite;
        String orderid;
        ProgressDialog mProgressDialogOrder;

        public OrderDataSerivce(String order_id) {
            orderid = order_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialogOrder = new ProgressDialog(mActivity);
            mProgressDialogOrder.setMessage("Loading Order...");
            mProgressDialogOrder.setCancelable(false);
            mProgressDialogOrder.show();
            mGson = new Gson();
            mStore_pref = new Store_pref(mActivity);
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(Constants.GET_ORDER_DATA + "&user_id=" + mStore_pref.getUser().getId()
                    + "&order_id=" + orderid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("OrderDataSerivce().DoinBackground()", "Could not reach to server.");
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialogOrder.dismiss();
            System.out.println("Response of OrderDataService:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

                        JSONArray mJsonArray = mJsonObject.getJSONArray("order");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasFavorite = new ArrayList<>();
                        mNavigationActivity.mArrayListOrderedItem.clear();

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            float total_tmp = 0;
                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setId(temp.getString("id"));
                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
                            mOrderedItemsData.setQty(Double.parseDouble(temp.getString("qty")));

                            System.out.println("Orderdata : " + temp);

                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
                            System.out.println("pizza_details from Gson: " + pizza_details);
                            mOrderedItemsData.setPizza_details(pizza_details);

                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
                            System.out.println("pizzaria from Gson: " + pizzaria);
                            mOrderedItemsData.setPizzaria(pizzaria);

                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            if (temp.getString("pizzacrust").length() > 2) {
                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
                                System.out.println("pizzacrust from Gson: " + pizzacrust);
                                mOrderedItemsData.setPizzacrust(pizzacrust);
                                total_tmp += pizzacrust.getPrice();
                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
                            }
                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            if (temp.getString("extra_ingredients").length() > 2) {

                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
                                float total_additions = 0;
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
                                    System.out.println("AdditionData: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListAdditionsListDatas.add(mAdditionsListData);

                                    total_additions += mAdditionsListData.getPrice();
                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                total_tmp += total_additions;
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
                                mArrayListAdditionsListDatas = null;
                            }
                            /*
                            6-10-2015 code for Toppings selected is added
                             */

                            if (temp.getString("toppings").length() > 2) {

                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();


                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {

                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
                                    System.out.println("Toppings: " + temp_addition);
                                    Toppings mAdditionsListData = new Toppings();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));

                                    mArrayListToppingses.add(mAdditionsListData);

                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setToppings(mArrayListToppingses);


                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListToppingses = null;
                            }

                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
                            System.out.println("address from Gson: " + pizzaria);
                            mOrderedItemsData.setAddress(address);

                            Log.e("OrderDataSerivce()", "-----------------Favorite Pizzas----------------");

//                            if (temp.getString("pizza_size").equalsIgnoreCase("small")) {
//                                System.out.println("Favorite pizza's total set with small size");
//                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                            } else if (temp.getString("pizza_size").equalsIgnoreCase("big")) {
//                                System.out.println("Favorite pizza's total set with Big size");
//                                Log.e("Favorite Service.onPostExecute()", "Big Price: " + mOrderedItemsData.getPizza_details().getPrice_large());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            } else {
//                                System.out.println("Item is not pizza");
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            }

//                            mOrderedItemsData.setTotal(total_tmp);
                            mOrderedItemsData.setTotal(Float.parseFloat(temp.getString("total")));
                            mOrderedItemsData.setIsFromFavorite_History(true);
                            /*
                                Added on 10-10-2015
                            below line is added to set the favoorite id to pizza so that
                            when user add a pizza to basket at that time while orders the pizza
                            its address can be updated.

                             */
                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));

                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
                            mOrderedItemsData.printAllData();
                            /*
                                all order data items has been added to Main Order Array list to place order.
                            */
                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mOrderedItemsDatasFavorite.get(i));
                        }
                        //LOOP ENDS


                        /*
                        below Static variable is set to Cancel the order while pressing back.
                         */
                        Constants.isOrderHistoryFavorite = true;
//                        mAdapterFavoritePizza = new FavoritePizzaNewAdapter(getActivity(), mOrderedItemsDatasFavorite, "FragmentFavorite");
//                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
//                        mAdapterFavoritePizza.notifyDataSetChanged();

                        /*
                        show the Basket screen
                         */
                        mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                        Log.e("BackstackList", "BasketFragment Fragment is added.");
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
                        // mNavigationActivity.onBackPressed();
                        mNavigationActivity.ShowHideIcons();
                        mNavigationActivity.showFragments();
//                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));
                        mNavigationActivity.mBasketFragment.lastVisibleLayout = 1;
                        mNavigationActivity.mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                        mNavigationActivity.mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_not_finished);


                    } else {
                        Log.e("OrderDataSerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(mActivity, "Response Success", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void initDialogRemoveFavoritePizza(final int position) {

        mDialogFavoriteRemove = new Dialog(mActivity);
        mDialogFavoriteRemove.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavoriteRemove.setContentView(R.layout.dialog_favorite_address);

        TextView mTextViewDialogHeading;
        ImageView mImageViewDialogImage;

        mTextViewDialogHeading = (TextView) mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_list_pizzas_text_dialog_title);
        mImageViewDialogImage = (ImageView) mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_list_pizzas_image_dialog_title);

        mTextViewDialogHeading.setText("Do you want to remove this order from Favorites ?");
        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
        mDialogFavoriteRemove.show();

        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DeleteFavoritePizzaService(mListOrderedItemsDatas.get(position).getOrder_id(), position).execute();

            }
        });

        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavoriteRemove.dismiss();
            }
        });
    }

    public class DeleteFavoritePizzaService extends AsyncTask<Void, Void, Void> {

        String response = "";
        String url = "";
        int Pos;

        DeleteFavoritePizzaService(String id, int pos) {
            url = Constants.DELETE_FAVORITE_ORDER + "&id=" + id;
            this.Pos = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                HttpClient mHttpClient = new DefaultHttpClient();
                System.out.println("DeleteFavoritePizzaService Url: " + url);
                HttpGet mHttpGet = new HttpGet(url);
                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("DeleteFavoritePizzaService().DoinBackground()", "Could not reach to server.");
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mProgressDialog.dismiss();
            Log.e("DeleteFavoritePizzaService Response", "Response: " + response);
            Toast.makeText(mActivity, response + "", Toast.LENGTH_SHORT).show();

            try {
                JSONObject mJsonObject = new JSONObject(response);
                if (mJsonObject.length() > 0) {
                    Log.e("result: ", "" + response);
                    mDialogFavoriteRemove.dismiss();
                    if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {

                        mListOrderedItemsDatas.remove(Pos);

                        if (mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza != null)
                            if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                                mNavigationActivity.mapFragment.mAdapterFavoritePizza.notifyDataSetChanged();
                                Toast.makeText(mActivity, "Your favorite Order is removed...", Toast.LENGTH_SHORT).show();
                            }

                        if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
                            mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza.notifyDataSetChanged();
                            Toast.makeText(mActivity, "Your favorite Order is removed.", Toast.LENGTH_SHORT).show();
                        }


                    } else if (mJsonObject.getString("responseCode").equalsIgnoreCase("2")) {
                        Toast.makeText(mActivity, "Sorry, your favorite order can not be removed now.Please try after some time.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mActivity, "Your favorite Order is not removed please try again.", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();

                }// main if Json object ends

            } catch (JSONException ej) {
                ej.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }
    }
}
