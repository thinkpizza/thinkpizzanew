package com.waycreon.thinkpizza.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.imageloader.ImageLoader;

import java.util.List;

public class RestaurentListAdapter extends ArrayAdapter<PizzaRestro> {

    Activity mActivity;
    ViewHolder mViewHolder;
    List<PizzaRestro> mPizzaRestro;
    ImageLoader mImageLoader;
    NavigationActivity mNavigationActivity;

    public RestaurentListAdapter(Activity mActivity, List<PizzaRestro> mRestros) {
        super(mActivity, R.layout.fragment_restaurent_list, mRestros);
        this.mActivity = mActivity;
        mPizzaRestro = mRestros;
        mImageLoader = new ImageLoader(mActivity);
        mNavigationActivity = (NavigationActivity) mActivity;
    }


    @Override
    public int getCount() {
        return mPizzaRestro.size();
    }

    @Override
    public PizzaRestro getItem(int arg0) {
        // TODO Auto-generated method stub
        return mPizzaRestro.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        PizzaRestro mCurrentData = getItem(position);

        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.fragment_restaurent_list, null);

            mViewHolder.mRelativeLayoutParent = (RelativeLayout) convertView.findViewById(R.id.frag_fragrestaurant_parentlayout);
            mViewHolder.mImageViewUserImage = (ImageView) convertView.findViewById(R.id.fragrestaurant_image_restaurent);
            mViewHolder.mTextViewRestaurantName = (TextView) convertView.findViewById(R.id.frag_restaurant_text_restaurentname);
            mViewHolder.mTextViewRestaurantAddress = (TextView) convertView.findViewById(R.id.frag_restaurant_text_restaurentaddress);
            mViewHolder.mTextViewDeliveryTime = (TextView) convertView.findViewById(R.id.frag_restaurant_text_delivery);
            mViewHolder.mTextViewMinimumOrder = (TextView) convertView.findViewById(R.id.frag_restaurant_text_minorder);
            mViewHolder.mRatingBar = (RatingBar) convertView.findViewById(R.id.frag_restaurant_ratingbar_res);
            mViewHolder.mTextViewOpenClose = (TextView) convertView.findViewById(R.id.frag_restaurant_image_open_close_res);

            mViewHolder.mRatingBar.setClickable(false);
            mViewHolder.mRatingBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

            mViewHolder.mRelativeLayoutParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (mPizzaRestro.get(pos).isOpen()) {
                        mNavigationActivity.showFragment(
                                mNavigationActivity.mFoodMenuFragment, true);
                        mNavigationActivity.mFoodMenuFragment.SetRetaurent(getItem(pos));
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mFoodMenuFragment);
                        mNavigationActivity.showFragments();
                        Log.e("BackstackList", "FoodMenu Fragment is added.");
                    }


                }
            });

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mRelativeLayoutParent.setTag(position);
//        Picasso.with(getContext()).load(mCurrentData.getImg_url()).placeholder(R.drawable.res1).into(mViewHolder.mImageViewUserImage);
        mImageLoader.DisplayImage(mCurrentData.getImg_url(), mViewHolder.mImageViewUserImage);
        mViewHolder.mTextViewRestaurantName.setText(mCurrentData
                .getName());
        String add = mCurrentData.getStreet_name();
        add += ", " + mCurrentData.getStreet_no();
        add += "\n" + mCurrentData.getApt_no();
        mViewHolder.mTextViewRestaurantAddress.setText(add);
        mViewHolder.mTextViewDeliveryTime.setText(mCurrentData
                .getDelivery_time());
        mViewHolder.mTextViewMinimumOrder.setText("" + mCurrentData
                .getMin_order());
        if (mCurrentData.isOpen()) {
            mViewHolder.mTextViewOpenClose.setText("  Open  ");
            mViewHolder.mTextViewOpenClose.setBackgroundResource(R.drawable.bk_round_border_gray_red_fill);
            mViewHolder.mTextViewOpenClose.setPadding(6, 6, 6, 6);
        } else {
            mViewHolder.mTextViewOpenClose.setText("  Closed  ");
            mViewHolder.mTextViewOpenClose.setBackgroundResource(R.drawable.bk_round_border_fill_app_dark);
            mViewHolder.mTextViewOpenClose.setPadding(6, 6, 6, 6);
        }
        int total_rating = (int) (mCurrentData.getOverall_ratings() + mCurrentData.getCrust_ratings() + mCurrentData.getToppings_quality_ratings() + mCurrentData.getDelivery_time_ratings());
        int total_rating_count = (int) (mCurrentData.getOverall_count() + mCurrentData.getCrust_count() + mCurrentData.getToppings_quality_count() + mCurrentData.getDelivery_time_count());
        int rating = total_rating / total_rating_count;
        mViewHolder.mRatingBar.setRating(rating);
        System.out.println("Calculated Ratings: " + rating);

        return convertView;
    }

    public class ViewHolder {
        RelativeLayout mRelativeLayoutParent;
        ImageView mImageViewUserImage;
        TextView mTextViewRestaurantName;
        TextView mTextViewRestaurantAddress;
        TextView mTextViewDeliveryTime;
        TextView mTextViewMinimumOrder;
        RatingBar mRatingBar;
        TextView mTextViewOpenClose;
    }

}
/*
public class RestaurentListAdapter extends BaseAdapter {

	Activity mActivity;
	ArrayList<RestaurentData> mListRestaurentListItemDataModels;
	ViewHolder mViewHolder;

	public RestaurentListAdapter(Activity mActivity,
			ArrayList<RestaurentData> models) {
		this.mActivity = mActivity;
		this.mListRestaurentListItemDataModels = models;

	}

	@Override
	public int getCount() {
		return mListRestaurentListItemDataModels.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return mListRestaurentListItemDataModels.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		RestaurentData mCurrentData = mListRestaurentListItemDataModels
				.get(position);

		if (convertView == null) {
			mViewHolder = new ViewHolder();
			convertView = mActivity.getLayoutInflater().inflate(
					R.layout.fragment_restaurent_list, null);

			mViewHolder.mImageViewUserImage = (ImageView) convertView
					.findViewById(R.id.fragrestaurant_image_restaurent);
			mViewHolder.mTextViewRestaurantName = (TextView) convertView
					.findViewById(R.id.frag_restaurant_text_restaurentname);
			mViewHolder.mTextViewRestaurantAddress = (TextView) convertView
					.findViewById(R.id.frag_restaurant_text_restaurentaddress);
			mViewHolder.mTextViewDeliveryTime = (TextView) convertView
					.findViewById(R.id.frag_restaurant_text_delivery);
			mViewHolder.mTextViewMinimumOrder = (TextView) convertView
					.findViewById(R.id.frag_restaurant_text_minorder);
			convertView.setTag(mViewHolder);

		} else {
			mViewHolder = (ViewHolder) convertView.getTag();
		}

		mViewHolder.mImageViewUserImage.setImageResource(mCurrentData
				.getImageID());
		mViewHolder.mTextViewRestaurantName.setText(mCurrentData
				.getRestaurentName());
		mViewHolder.mTextViewRestaurantAddress.setText(mCurrentData
				.getRestaurantsAddress());
		mViewHolder.mTextViewDeliveryTime.setText(mCurrentData
				.getDeliveryTime());
		mViewHolder.mTextViewMinimumOrder.setText(""+mCurrentData
				.getMinimumOrderPrice());

		return convertView;
	}

	public class ViewHolder {
		ImageView mImageViewUserImage;
		TextView mTextViewRestaurantName;
		TextView mTextViewRestaurantAddress;
		TextView mTextViewDeliveryTime;
		TextView mTextViewMinimumOrder;
	}

}
*/
