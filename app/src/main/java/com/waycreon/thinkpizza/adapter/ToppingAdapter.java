package com.waycreon.thinkpizza.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.waycreon.utils.Constants;

import java.util.ArrayList;

public class ToppingAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<Toppings> mArrayListToppings;

    public double TotalofSelectedToppings = 0;

    NavigationActivity mNavigationActivity;

    public ToppingAdapter(Activity mActivity,
                          ArrayList<Toppings> models) {
        this.mActivity = mActivity;
        this.mArrayListToppings = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mArrayListToppings.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return mArrayListToppings.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder mViewHolder;

        Toppings mCurrentData = mArrayListToppings.get(position);

//        if (convertView == null) {
        mViewHolder = new ViewHolder();
        convertView = mActivity.getLayoutInflater().inflate(R.layout.additions_listitems, null);

        mViewHolder.mLayoutOfList = (LinearLayout) convertView
                .findViewById(R.id.addition_listitem_layoutoflist);

        mViewHolder.mCheckBoxTopping = (CheckBox) convertView
                .findViewById(R.id.addition_listitem_checkbox_item);
        mViewHolder.mTextViewTextToppingName = (TextView) convertView
                .findViewById(R.id.addition_listitem_text_additionname);
        mViewHolder.mTextViewToppingPrice = (TextView) convertView
                .findViewById(R.id.addition_listitem_text_additionprice);

        mViewHolder.mTextViewToppingPrice.setVisibility(View.GONE);
        convertView.setTag(mViewHolder);

//        } else {
//            mViewHolder = (ViewHolder) convertView.getTag();
//        }

        mViewHolder.mCheckBoxTopping.setTag(position);

        mViewHolder.mTextViewTextToppingName.setText(mCurrentData
                .getName());
        mViewHolder.mTextViewToppingPrice.setText("" + mCurrentData
                .getPrice());

        if (mCurrentData.isToppingSelected())
            mViewHolder.mCheckBoxTopping.setChecked(true);
        else
            mViewHolder.mCheckBoxTopping.setChecked(false);


        mViewHolder.mCheckBoxTopping.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                float tot = 0;
                System.out.println("Check box clicked position: " + compoundButton.getTag());
                int pos = (int) compoundButton.getTag();

                if (b) {
                    mArrayListToppings.get(position).setIsToppingSelected(true);

                } else {
                    mArrayListToppings.get(pos).setIsToppingSelected(false);

                }
                tot = 0;
                for (int i = 0; i < mArrayListToppings.size(); i++) {
                    //System.out.println("AdditoinListAdapter: Total:(B)-->" + TotalofSelectedAdditions);
                    if (mArrayListToppings.get(i).isToppingSelected()) {
                        System.out.println("ToppingAdapter: Checkbox Selected:-->" + mArrayListToppings.get(i).
                                getName() + " at Position: " + i);
                        System.out.print(" with Price:-->" + mArrayListToppings.get(i).getPrice());
                        tot += mArrayListToppings.get(i).getPrice();
                        System.out.println("ToppingAdapter: Total:(A)-->" + tot);

                    }
                }
                mNavigationActivity.mOrderFragment.mTextViewTotal_Topping_dialog.setText("" + tot);
                TotalofSelectedToppings = tot;

            }
        });


        if (Constants.isOrderStatusShown) {
            mViewHolder.mCheckBoxTopping.setEnabled(false);
        } else {
            mViewHolder.mCheckBoxTopping.setEnabled(true);
        }
        return convertView;
    }

    public class ViewHolder {
        CheckBox mCheckBoxTopping;
        TextView mTextViewTextToppingName;
        TextView mTextViewToppingPrice;
        LinearLayout mLayoutOfList;
    }

}
