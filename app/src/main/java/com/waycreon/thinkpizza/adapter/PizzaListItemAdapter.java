package com.waycreon.thinkpizza.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.waycreon.utils.Utils;

import java.util.List;

public class PizzaListItemAdapter extends ArrayAdapter<PizzaListData> {

    Activity mActivity;
    ViewHolder mViewHolder;
    List<PizzaListData> mArrayListPizzaListDatas;
    ImageLoader mImageLoader;


    public PizzaListItemAdapter(Activity mActivity, List<PizzaListData> mPizzaListDatas) {
        super(mActivity, R.layout.fragment_pizzalist_item, mPizzaListDatas);

        this.mActivity = mActivity;
        this.mArrayListPizzaListDatas = mPizzaListDatas;
        mImageLoader = new ImageLoader(mActivity);

    }

    @Override
    public int getCount() {
        return mArrayListPizzaListDatas.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        PizzaListData mCurrentData = mArrayListPizzaListDatas.get(position);
        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.fragment_pizzalist_item, null);


            mViewHolder.mImageViewPizzaImage = (ImageView) convertView.findViewById(R.id.frag_pizzalist_image_pizza);
            mViewHolder.mTextViewPizzaname = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_pizzaname);
            mViewHolder.mTextViewPizzaDescription = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_pizzadescription);
            mViewHolder.mTextViewRegularPricePizza = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_priceregular);
//            mViewHolder.mTextViewMediumPricePizza = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_pricemedium);
            mViewHolder.mTextViewLargePricePizza = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_pricelarge);
            mViewHolder.mRelativeLayoutPrice = (RelativeLayout) convertView.findViewById(R.id.frag_pizzalist_layout_price);
            mViewHolder.mRelativeLayoutOrderDetail = (RelativeLayout) convertView.findViewById(R.id.frag_pizzalist_layout_orderdetail_price);
            mViewHolder.mTextViewOrderDetail = (TextView) convertView.findViewById(R.id.frag_pizzalist_text_orderdetail_price);
            convertView.setTag(mViewHolder);

            if (mCurrentData.getShow_options().equalsIgnoreCase("0")) {
//                mViewHolder.mTextViewMediumPricePizza.setVisibility(View.GONE);
                mViewHolder.mTextViewLargePricePizza.setVisibility(View.GONE);
                convertView.findViewById(R.id.frag_order_image_regular).setVisibility(View.GONE);
//                convertView.findViewById(R.id.frag_order_image_medium).setVisibility(View.GONE);
                convertView.findViewById(R.id.frag_order_image_large).setVisibility(View.GONE);
            } else {
//                mViewHolder.mTextViewMediumPricePizza.setVisibility(View.VISIBLE);
                mViewHolder.mTextViewLargePricePizza.setVisibility(View.VISIBLE);
                convertView.findViewById(R.id.frag_order_image_regular).setVisibility(View.VISIBLE);
//                convertView.findViewById(R.id.frag_order_image_medium).setVisibility(View.VISIBLE);
                convertView.findViewById(R.id.frag_order_image_large).setVisibility(View.VISIBLE);
            }

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

//            Picasso.with(getActivity()).load(mCurrentData.getImg_url()).placeholder(R.drawable.res1).into(mViewHolder.mImageViewPizzaImage);
        mImageLoader.DisplayImage(mCurrentData.getImg_url(), mViewHolder.mImageViewPizzaImage);

        mViewHolder.mTextViewPizzaname.setText(mCurrentData.getName());
        if (mCurrentData.getShow_options().equalsIgnoreCase("1")) {
            String toppings = "";
            for (int i = 0; i < mArrayListPizzaListDatas.get(position).getToppings().size(); i++) {
                toppings += mArrayListPizzaListDatas.get(position).getToppings().get(i).getName() + ", ";
            }

            toppings = Utils.RemoveLastComma(toppings);

            mViewHolder.mTextViewPizzaDescription.setText(toppings);
        } else {
            mViewHolder.mTextViewPizzaDescription.setText(mCurrentData.getDescription());
        }

        mViewHolder.mTextViewRegularPricePizza.setText("" + mCurrentData.getPrice_small());
//        mViewHolder.mTextViewMediumPricePizza.setText("" + mCurrentData.getPrice_medium());
        mViewHolder.mTextViewLargePricePizza.setText("" + mCurrentData.getPrice_large());

        return convertView;
    }

    public class ViewHolder {

        ImageView mImageViewPizzaImage;
        TextView mTextViewPizzaname;
        TextView mTextViewPizzaDescription;
        TextView mTextViewRegularPricePizza;
        //        TextView mTextViewMediumPricePizza;
        TextView mTextViewLargePricePizza;
        RelativeLayout mRelativeLayoutPrice;
        RelativeLayout mRelativeLayoutOrderDetail;
        TextView mTextViewOrderDetail;
    }


}
