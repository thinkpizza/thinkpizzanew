package com.waycreon.thinkpizza.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Utils;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class BasketListAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<OrderedItemsData> mArrayListOrderedItemsDatas;
    ViewHolder mViewHolder;

    NavigationActivity mNavigationActivity;

    public BasketListAdapter(Activity mActivity,
                             ArrayList<OrderedItemsData> models) {

        this.mActivity = mActivity;
        this.mArrayListOrderedItemsDatas = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mArrayListOrderedItemsDatas.size();
    }

    @Override
    public Object getItem(int arg0) {
        return mArrayListOrderedItemsDatas.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        mViewHolder = null;
        OrderedItemsData mCurrentData = mArrayListOrderedItemsDatas
                .get(position);

        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.basketlist_ordered, null);

            mViewHolder.mRelativeLayoutParent = (RelativeLayout) convertView.findViewById(R.id.basketlist_layout_mainparent);
            mViewHolder.mTextViewPizzeriaName = (TextView) convertView.findViewById(R.id.basketlist_ordered_text_pizzeria_name);
            mViewHolder.mTextViewPizzaNameSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_pizzaname);
            mViewHolder.mTextViewTextAdditionsSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_additionsname);
            mViewHolder.mTextViewPizzaCrustSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_pizzacrust);

            mViewHolder.mTextViewQuantity = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_quantity);
            mViewHolder.mTextViewTotalOrder = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_totalorder);

            mViewHolder.mImageViewRemoveFromCart = (ImageView) convertView
                    .findViewById(R.id.basketlist_ordered_image_removefrombasket);


            /*
-------------------------------------------------------------------------------------
                                Remove item from cart
-------------------------------------------------------------------------------------
 */
            mViewHolder.mImageViewRemoveFromCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Store_pref mStore_pref = new Store_pref(mActivity);

                    if (mArrayListOrderedItemsDatas.size() > 0) {

                        String rid = mArrayListOrderedItemsDatas.get(position).getPizzaria().getId();
                        Log.e("mImageViewRemoveFromCart.setOnClickListener()", "Item will be removed.");
                        System.out.println("Removing Pid:" + mArrayListOrderedItemsDatas.get(position).getPizzaria().getId());
                        mNavigationActivity.mBasketFragment.subtotal -=
                                mArrayListOrderedItemsDatas.get(position).getTotal();
                        mNavigationActivity.mBasketFragment.CalculateTotal();

                        if (mArrayListOrderedItemsDatas.size() == 1) {
                            mNavigationActivity.mBasketFragment.mListViewOrderedItems.setVisibility(View.GONE);
                            mStore_pref.removeOrderedData();
                            mArrayListOrderedItemsDatas.remove(position);
                            mNavigationActivity.mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
                            mNavigationActivity.mBasketFragment.CalculateTotal();
                            return;
                        }
                        mArrayListOrderedItemsDatas.remove(position);
                        mNavigationActivity.mBasketFragment.CalculateTotal();
                        mNavigationActivity.mBasketFragment.mBasketListAdapter.notifyDataSetChanged();

                    }
//                Utils.setListViewHeightBasedOnChildren(mNavigationActivity.mBasketFragment.mListViewOrderedItems);
                    mNavigationActivity.mBasketFragment.setListViewHeightBasedOnChildren();
                    mStore_pref.setOrderedData(mNavigationActivity.mArrayListOrderedItem, "from mImageViewRemoveFromCart.setOnClickListener() of basket adapter");
                }
            });

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mTextViewPizzeriaName.setText(mCurrentData.getPizzaria().getName());
        mViewHolder.mTextViewPizzaNameSelected.setText(mCurrentData.getPizza_details().getName());

        if (mCurrentData.getExtra_ingredients() != null) {
            Log.v("getExtra_ingredients() !=null BasketListAdapter.java", "Lets show Extra Ingredients.");
            String additions = "";
            for (int i = 0; i < mCurrentData.getExtra_ingredients().size(); i++)
                additions += mCurrentData.getExtra_ingredients().get(i).getName() + ", ";

            additions = Utils.RemoveLastComma(additions);

            if (additions.length() > 0) {
                mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.VISIBLE);
                mViewHolder.mTextViewTextAdditionsSelected.setText(additions);
            } else
                mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.INVISIBLE);
        } else {
            mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.GONE);
        }

        if (mCurrentData.getPizzacrust() != null) {
            Log.v("getPizzacrust() !=null BasketListAdapter.java", "Lets show PizzaCrust.");
            if (mCurrentData.getPizzacrust().isPizzaCrustSelected()) {
                mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.VISIBLE);
                mViewHolder.mTextViewPizzaCrustSelected.setText(mCurrentData.getPizzacrust().getName());
            } else
                mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.INVISIBLE);
        } else {
            mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.GONE);
        }


        if ((mCurrentData.getPizzacrust() == null) && (mCurrentData.getExtra_ingredients() == null))
            mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.GONE);

        mViewHolder.mTextViewQuantity.setText("" + mCurrentData.getQty());
        mViewHolder.mTextViewTotalOrder.setText("" + mCurrentData.getTotal());

        System.out.println("BasketAdapter mRelativeLayout.Height[" + position + "]:  " + mViewHolder.mRelativeLayoutParent.getHeight());
//        final View finalConvertView = convertView;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("mRelativeLayout.Height[" + position + "]:  " + mViewHolder.mRelativeLayoutParent.getHeight());
//                        System.out.println("mRelativeLayout.Bottom[" + position + "]:  " + finalConvertView.getBottom());
//                        System.out.println("View.Height[" + position + "]:  " + finalConvertView.getHeight());
//                        System.out.println("View.MeasuredHeight[" + position + "]:  " + finalConvertView.getMeasuredHeight());
//                        if (finalConvertView.getBottom() == 0)
//                            BottomParameter = finalConvertView.getHeight();
//                        else
//                            BottomParameter = finalConvertView.getBottom();

                    }
                });
            }
        }, 200);

        return convertView;
    }


    public class ViewHolder {

        RelativeLayout mRelativeLayoutParent;
        TextView mTextViewPizzeriaName;
        TextView mTextViewPizzaNameSelected;
        TextView mTextViewTextAdditionsSelected;
        TextView mTextViewPizzaCrustSelected;
        TextView mTextViewQuantity;
        TextView mTextViewTotalOrder;
        ImageView mImageViewRemoveFromCart;

    }

}
