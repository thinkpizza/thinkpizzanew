package com.waycreon.thinkpizza.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.NavigationListItemModel;

import java.util.ArrayList;

public class NavigationListAdapter extends BaseAdapter {
    Activity mActivity;
    public ArrayList<NavigationListItemModel> navDrawerItems;
    public ViewHolder mViewHolder;

    public NavigationListAdapter(Activity c, ArrayList<NavigationListItemModel> listdata) {
        this.mActivity = c;
        navDrawerItems = listdata;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return navDrawerItems.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if (arg1 == null) {
            mViewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mActivity
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.navigation_list_item, null);

            mViewHolder.imgicon = (ImageView) arg1
                    .findViewById(R.id.navigation_listitem_image);
            mViewHolder.txtlistname = (TextView) arg1
                    .findViewById(R.id.navigation_listitem_itemtext);

            arg1.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) arg1.getTag();
        }


//            if(selectedpos==arg0){
//
//            }
        if (navDrawerItems.get(position).isFlag()) {
            mViewHolder.imgicon.setImageResource(navDrawerItems.get(position).getSelectedItemIcon());
            mViewHolder.txtlistname.setTextColor(mActivity.getApplicationContext().getResources().getColor(R.color.t_light_red));
        } else {
            mViewHolder.imgicon.setImageResource(navDrawerItems.get(position).getItemIcon());
            mViewHolder.txtlistname.setTextColor(mActivity.getApplicationContext().getResources().getColor(R.color.t_text_dark));
        }
        mViewHolder.txtlistname.setText(navDrawerItems.get(position).getListItemName());
        return arg1;
    }

    public class ViewHolder {

        ImageView imgicon;
        public TextView txtlistname;

    }
}
