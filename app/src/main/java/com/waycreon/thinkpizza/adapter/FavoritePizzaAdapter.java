package com.waycreon.thinkpizza.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Utils;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class FavoritePizzaAdapter extends ArrayAdapter<OrderedItemsData> {

    Activity mActivity;

    List<OrderedItemsData> mListOrderedItemsDatas;
    ImageLoader mImageLoader;
    NavigationActivity mNavigationActivity;
    String DialogTag = "";
    ProgressDialog mProgressDialog;
    /*
    Tags set are
    ----------------
    FavoritePizza
    HistoryPizza
    FragmentFavorite
    -----------------
     */

    Dialog mDialogFavoriteRemove;

    public FavoritePizzaAdapter(Activity mActivity, List<OrderedItemsData> mOrderedItemsDatas, String forDialog) {
        super(mActivity, R.layout.dialog_pizza, mOrderedItemsDatas);

        this.mActivity = mActivity;
        this.mListOrderedItemsDatas = mOrderedItemsDatas;
        mImageLoader = new ImageLoader(mActivity);
        Log.e("  FavoritePizzaAdapter()-->", "Size of Order list: " + mOrderedItemsDatas.size());
        mNavigationActivity = (NavigationActivity) mActivity;
        DialogTag = forDialog;
    }

    @Override
    public int getCount() {
        return mListOrderedItemsDatas.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;


        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.dialog_pizza, null);

            mViewHolder.mRelativeLayoutParent = (RelativeLayout) convertView.findViewById(R.id.dialog_pizza_parent_layout);

            mViewHolder.mImageViewPizzaImage = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_pizza);
            mViewHolder.mTextViewPizzaname = (TextView) convertView.findViewById(R.id.dialog_pizza_text_pizzaname);
            mViewHolder.mTextViewPizzaIngredients = (TextView) convertView.findViewById(R.id.dialog_pizza_text_pizzaingredients);
            mViewHolder.mTextViewPizzaCrust = (TextView) convertView.findViewById(R.id.dialog_pizza_text_pizzacrust);

            mViewHolder.mRelativeLayoutCustoms = (LinearLayout) convertView.findViewById(R.id.dialog_pizza_layout_custom_ingredients);
//            mViewHolder.mCheckBoxCustomCheck = (CheckBox) convertView.findViewById(R.id.dialog_pizza_checkbox_custom_check);
            mViewHolder.mTextViewCustomIngredients = (TextView) convertView.findViewById(R.id.dialog_pizza_text_custom_selected);
            mViewHolder.mTextViewCustomPrice = (TextView) convertView.findViewById(R.id.dialog_pizza_text_custom_selecte_price);
            mViewHolder.mImageViewAddtoCart = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_addtocart);
            mViewHolder.mImageViewAddress = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_address);
            mViewHolder.mImageViewRemoveFavorite = (ImageView) convertView.findViewById(R.id.dialog_pizza_image_remove_favorite);

            if (DialogTag.equalsIgnoreCase("FavoritePizza") || DialogTag.equalsIgnoreCase("HistoryPizza")) {

                mViewHolder.mImageViewAddtoCart.setVisibility(View.VISIBLE);
                mViewHolder.mImageViewAddress.setVisibility(View.VISIBLE);
                if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                    mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);
                } else if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                    mViewHolder.mImageViewRemoveFavorite.setVisibility(View.VISIBLE);
                }

            } else if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
                mViewHolder.mImageViewAddtoCart.setVisibility(View.GONE);
                mViewHolder.mImageViewAddress.setVisibility(View.GONE);
                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.VISIBLE);
            } else if (DialogTag.equalsIgnoreCase("AddToFavorite")) {
                mViewHolder.mImageViewAddtoCart.setVisibility(View.GONE);
                mViewHolder.mImageViewAddress.setVisibility(View.GONE);
                mViewHolder.mImageViewRemoveFavorite.setVisibility(View.GONE);
            }

            convertView.setTag(mViewHolder);

        } else

        {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

//        mViewHolder.mRelativeLayoutParent.setTag(position);
//        mViewHolder.mCheckBoxCustomCheck.setTag(position);

        if (mListOrderedItemsDatas.get(position).getPizza_details() != null) {
//            System.out.println("Imageurl:" + mListOrderedItemsDatas.get(position).getPizza_details().getImg_url());
            mImageLoader.DisplayImage(mListOrderedItemsDatas.get(position).getPizza_details().getImg_url(), mViewHolder.mImageViewPizzaImage);
            mViewHolder.mTextViewPizzaname.setText(mListOrderedItemsDatas.get(position).getPizza_details().getName());
        }

        if (mListOrderedItemsDatas.get(position).getExtra_ingredients() != null
                && mListOrderedItemsDatas.get(position).getExtra_ingredients().size() > 0) {
//            Log.e("FavoritePizzaAdapter().getView()", "Additions is not null");
            String selectedAdditions = "";
            for (int i = 0; i < mListOrderedItemsDatas.get(position).getExtra_ingredients().size(); i++) {
                selectedAdditions += mListOrderedItemsDatas.get(position).getExtra_ingredients().get(i).getName() + ", ";
            }

            selectedAdditions = Utils.RemoveLastComma(selectedAdditions);

            mViewHolder.mTextViewPizzaIngredients.setText("Extra Ingredients: " + selectedAdditions);
        } else {
//            Log.e("FavoritePizzaAdapter().getView()", "Additions is null");
            mViewHolder.mTextViewPizzaIngredients.setText("Extra Ingredients not selected. ");
        }

        if (mListOrderedItemsDatas.get(position).getPizzacrust() != null) {
//            Log.e("FavoritePizzaAdapter().getView()", "PizzaCrust is not null PizzaCrust: " + mListOrderedItemsDatas.get(position).getPizzacrust().getName());
            mViewHolder.mTextViewPizzaCrust.setText("Pizza Crust: " + mListOrderedItemsDatas.get(position).getPizzacrust().getName());
        } else {
            mViewHolder.mTextViewPizzaCrust.setText("Pizza Crust not selected. ");
//            Log.e("FavoritePizzaAdapter().getView()", "PizzaCrust is null");
        }


        if (mListOrderedItemsDatas.get(position).getToppings() != null
                && mListOrderedItemsDatas.get(position).getToppings().size() > 0) {

//            Log.e("FavoritePizzaAdapter().getView()", "Custom Ingredients  is not null");
            String selectedCustoms = "";
            float price = 0;
            for (int i = 0; i < mListOrderedItemsDatas.get(position).getToppings().size(); i++) {
                selectedCustoms += mListOrderedItemsDatas.get(position).getToppings().get(i).getName() + ", ";
                price += mListOrderedItemsDatas.get(position).getToppings().get(i).getPrice();
            }

            selectedCustoms = Utils.RemoveLastComma(selectedCustoms);

            mViewHolder.mRelativeLayoutCustoms.setVisibility(View.VISIBLE);

//            System.out.println("Selected Customs(A): " + selectedCustoms);
            mViewHolder.mTextViewCustomIngredients.setText(selectedCustoms);
            mViewHolder.mTextViewCustomPrice.setText("" + price);

        } else {
//            Log.e("FavoritePizzaAdapter().getView()", "Custom pizza Toping is null");
            mViewHolder.mRelativeLayoutCustoms.setVisibility(View.GONE);
            mViewHolder.mTextViewCustomIngredients.setText("Custom Ingredients not selected.");
            mViewHolder.mTextViewCustomPrice.setText("0.0");
        }

        mViewHolder.mRelativeLayoutParent.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                int pos = (int) mViewHolder.mRelativeLayoutParent.getTag();

                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); Position Selected is: " + position);
                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); DialogTag Selected is: " + DialogTag);

                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaFavorite.dismiss();
                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaHistory.dismiss();
                        } else if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
                            mNavigationActivity.mapFragment.setFavoriteAddressFromFragmentFavorite(mListOrderedItemsDatas.get(position).getAddress(), position);

                            /*
                            to add the item to the cart
                             */

                            mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                            Log.e("BackstackList", "BasketFragment Fragment is added.");
                            mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
                            // mNavigationActivity.onBackPressed();
                            mNavigationActivity.ShowHideIcons();
                            mNavigationActivity.showFragments();
                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));

                        }

                    }
                }

        );

        mViewHolder.mImageViewAddtoCart.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
                            mNavigationActivity.mapFragment.mDialogPizzaFavorite.dismiss();
                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
                            mNavigationActivity.mapFragment.mDialogPizzaHistory.dismiss();
                        }
                        mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                        Log.e("BackstackList", "BasketFragment Fragment is added.");
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
//                mNavigationActivity.onBackPressed();
                        mNavigationActivity.ShowHideIcons();
                        mNavigationActivity.showFragments();
                        mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));

                    }
                }

        );

        mViewHolder.mImageViewAddress.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); Position Selected is: " + position);
                        System.out.println("FavoritePizzaAdapter.setOnClickListener(); DialogTag Selected is: " + DialogTag);

                        if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                            mNavigationActivity.mapFragment.setFavoriteAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaFavorite.dismiss();
                        } else if (DialogTag.equalsIgnoreCase("HistoryPizza")) {
                            mNavigationActivity.mapFragment.setHistoryAddressInDialog(position);
//                    mNavigationActivity.mapFragment.mDialogPizzaHistory.dismiss();

                        }
                    }
                }

        );

        mViewHolder.mImageViewRemoveFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (DialogTag.equalsIgnoreCase("FavoritePizza")
                        || DialogTag.equalsIgnoreCase("FragmentFavorite")) {

                    initDialogRemoveFavoritePizza(position);
//                    new DeleteFavoritePizzaService(mListOrderedItemsDatas.get(position).getId(), position).execute();
                }

            }
        });

        return convertView;
    }

    public class ViewHolder {

        RelativeLayout mRelativeLayoutParent;
        ImageView mImageViewPizzaImage;
        TextView mTextViewPizzaname;
        TextView mTextViewPizzaIngredients;
        TextView mTextViewPizzaCrust;
        TextView mTextViewCustomIngredients;

        ImageView mImageViewAddtoCart;
        ImageView mImageViewAddress;
        ImageView mImageViewRemoveFavorite;

        //        RelativeLayout mRelativeLayoutCustoms;
        LinearLayout mRelativeLayoutCustoms;
        TextView mTextViewCustomPrice;


    }

    void initDialogRemoveFavoritePizza(final int position) {

        mDialogFavoriteRemove = new Dialog(mActivity);
        mDialogFavoriteRemove.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogFavoriteRemove.setContentView(R.layout.dialog_favorite_address);

//        mTextViewDialogHeading = (TextView) mDialogFavorite.findViewById(R.id.address_list_pizzas_text_dialog_title);
//        mImageViewDialogImage = (ImageView) mDialogFavorite.findViewById(R.id.address_list_pizzas_image_dialog_title);


//        mRelativeLayoutAddtoFavorite_dialog = (LinearLayout) mDialogFavorite.findViewById(R.id.layout_button_add_favorite);
       /*mTextViewYes = (TextView) */
        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DeleteFavoritePizzaService(mListOrderedItemsDatas.get(position).getId(), position).execute();

            }
        });
       /*mTextViewNo = (TextView)*/
        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavoriteRemove.dismiss();
            }
        });

        /*mTextViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavorite.dismiss();
                new UpdateFavoriteAddressService(addressid).execute();

            }
        });*/

/*
        mTextViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialogFavorite.dismiss();
            }
        });
*/

        mDialogFavoriteRemove.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Toast.makeText(mActivity, "Dialog Dismissed.", Toast.LENGTH_SHORT).show();
            }
        });
        TextView mTextView = (TextView) mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_list_pizzas_text_dialog_title);
        mTextView.setText(mActivity.getResources().getString(R.string.dialog_list_pizza_remove_favorite));
        mDialogFavoriteRemove.show();
    }

    public class DeleteFavoritePizzaService extends AsyncTask<Void, Void, Void> {

        String response = "";
        String url = "";
        int Pos;

        DeleteFavoritePizzaService(String id, int pos) {
            url = Constants.DELETE_FAVORITE_PIZZA + "&id=" + id;
            this.Pos = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                HttpClient mHttpClient = new DefaultHttpClient();
                System.out.println("DeleteFavoritePizzaService Url: " + url);
                HttpGet mHttpGet = new HttpGet(url);
                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("DeleteFavoritePizzaService().DoinBackground()", "Could not reach to server.");
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mProgressDialog.dismiss();
            Log.e("DeleteFavoritePizzaService Response", "Response: " + response);
            Toast.makeText(mActivity, response + "", Toast.LENGTH_SHORT).show();

            try {
                JSONObject mJsonObject = new JSONObject(response);
                if (mJsonObject.length() > 0) {
                    Log.e("result: ", "" + response);

                    if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {

                        mListOrderedItemsDatas.remove(Pos);


                        if (mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza != null)
                            if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
                                mNavigationActivity.mapFragment.mAdapterFavoritePizza.notifyDataSetChanged();
                                Toast.makeText(mActivity, "Your favorite pizza is removed...", Toast.LENGTH_SHORT).show();
                            }

                        if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
                            mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza.notifyDataSetChanged();
                            Toast.makeText(mActivity, "Your favorite pizza is removed.", Toast.LENGTH_SHORT).show();
                        }



                        mDialogFavoriteRemove.dismiss();
                    } else {
                        Toast.makeText(mActivity, "Your favorite pizza is not removed please try again.", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();

                }// main if Json object ends

            } catch (JSONException ej) {
                ej.printStackTrace();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }
    }
}
