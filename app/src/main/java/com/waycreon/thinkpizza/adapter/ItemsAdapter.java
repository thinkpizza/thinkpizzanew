package com.waycreon.thinkpizza.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.Items;

import java.util.ArrayList;

public class ItemsAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<Items> mArrayListOrderedItemsDatas;
    ViewHolder mViewHolder;

    NavigationActivity mNavigationActivity;

    public ItemsAdapter(Activity mActivity,
                        ArrayList<Items> models) {

        this.mActivity = mActivity;
        this.mArrayListOrderedItemsDatas = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mArrayListOrderedItemsDatas.size();
    }

    @Override
    public Object getItem(int arg0) {
        return mArrayListOrderedItemsDatas.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        mViewHolder = null;
        Items mCurrentData = mArrayListOrderedItemsDatas
                .get(position);

        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.basketlist_ordered, null);

            mViewHolder.mTextViewPizzeriaName = (TextView)
                    convertView.findViewById(R.id.basketlist_ordered_text_pizzeria_name);
            mViewHolder.mTextViewPizzaNameSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_pizzaname);
            mViewHolder.mTextViewTextAdditionsSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_additionsname);
            mViewHolder.mTextViewPizzaCrustSelected = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_pizzacrust);

            mViewHolder.mTextViewQuantity = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_quantity);
            mViewHolder.mTextViewTotalOrder = (TextView) convertView
                    .findViewById(R.id.basketlist_ordered_text_totalorder);

            mViewHolder.mImageViewRemoveFromCart = (ImageView) convertView
                    .findViewById(R.id.basketlist_ordered_image_removefrombasket);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.mImageViewRemoveFromCart.setVisibility(View.GONE);
        mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.GONE);
        mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.GONE);


        mViewHolder.mTextViewPizzeriaName.setText(mCurrentData.getRes_name());
        mViewHolder.mTextViewPizzaNameSelected.setText(mCurrentData.getName());

//        if (mCurrentData.getExtra_ingredients() != null) {
//            Log.v("getExtra_ingredients() !=null BasketListAdapter.java", "Lets show Extra Ingredients.");
//            String additions = "";
//            for (int i = 0; i < mCurrentData.getExtra_ingredients().size(); i++)
//                additions += mCurrentData.getExtra_ingredients().get(i).getName() + ", ";
//
//            additions = Utils.RemoveLastComma(additions);
//
//            if (additions.length() > 0) {
//                mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.VISIBLE);
//                mViewHolder.mTextViewTextAdditionsSelected.setText(additions);
//            } else
//                mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.GONE);
//        } else {
//            mViewHolder.mTextViewTextAdditionsSelected.setVisibility(View.GONE);
//        }
//
//        if (mCurrentData.getPizzacrust() != null) {
//            Log.v("getPizzacrust() !=null BasketListAdapter.java", "Lets show PizzaCrust.");
//            if (mCurrentData.getPizzacrust().isPizzaCrustSelected()) {
//                mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.VISIBLE);
//                mViewHolder.mTextViewPizzaCrustSelected.setText(mCurrentData.getPizzacrust().getName());
//            } else
//                mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.GONE);
//        } else {
//            mViewHolder.mTextViewPizzaCrustSelected.setVisibility(View.GONE);
//        }


        mViewHolder.mTextViewQuantity.setText("" + mCurrentData.getQty());
        mViewHolder.mTextViewTotalOrder.setText("" + mCurrentData.getTotal());

/*
-------------------------------------------------------------------------------------
                                Remove item from cart
-------------------------------------------------------------------------------------
 */
//        mViewHolder.mImageViewRemoveFromCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Store_pref mStore_pref = new Store_pref(mActivity);
//
//                if (mArrayListOrderedItemsDatas.size() > 0) {
//
//                    String rid = mArrayListOrderedItemsDatas.get(position).getPizzaria().getId();
//                    Log.e("mImageViewRemoveFromCart.setOnClickListener()", "Item will be removed.");
//                    System.out.println("Removing Pid:" + mArrayListOrderedItemsDatas.get(position).getPizzaria().getId());
//                    mNavigationActivity.mBasketFragment.subtotal -=
//                            mArrayListOrderedItemsDatas.get(position).getTotal();
//                    mNavigationActivity.mBasketFragment.CalculateTotal();
//
//                    if (mArrayListOrderedItemsDatas.size() == 1) {
//                        mNavigationActivity.mBasketFragment.mListViewOrderedItems.setVisibility(View.GONE);
//                        mStore_pref.removeOrderedData();
//                        mArrayListOrderedItemsDatas.remove(position);
//                        mNavigationActivity.mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
//                        mNavigationActivity.mBasketFragment.CalculateTotal();
//                        return;
//
//
//                    }
//
//                    mArrayListOrderedItemsDatas.remove(position);
//                    mNavigationActivity.mBasketFragment.mBasketListAdapter.notifyDataSetChanged();
//
//                }
////                Utils.setListViewHeightBasedOnChildren(mNavigationActivity.mBasketFragment.mListViewOrderedItems);
//                mStore_pref.setOrderedData(mNavigationActivity.mArrayListOrderedItem, "from mImageViewRemoveFromCart.setOnClickListener() of basket adapter");
//            }
//        });
        return convertView;
    }

    public class ViewHolder {

        TextView mTextViewPizzeriaName;
        TextView mTextViewPizzaNameSelected;
        TextView mTextViewTextAdditionsSelected;
        TextView mTextViewPizzaCrustSelected;
        TextView mTextViewQuantity;
        TextView mTextViewTotalOrder;
        ImageView mImageViewRemoveFromCart;

    }

}
