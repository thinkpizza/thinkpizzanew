package com.waycreon.thinkpizza.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.waycreon.utils.Constants;

import java.util.ArrayList;

public class AdditionsListAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<AdditionsListData> mArrayListAdditionsListDatas;

    //    public ArrayList<AdditionsListData> mArrayListAdditionsSelected = new ArrayList<>();
    public double TotalofSelectedAdditions = 0;

    NavigationActivity mNavigationActivity;

    public AdditionsListAdapter(Activity mActivity,
                                ArrayList<AdditionsListData> models) {
        this.mActivity = mActivity;
        this.mArrayListAdditionsListDatas = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mArrayListAdditionsListDatas.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return mArrayListAdditionsListDatas.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder mViewHolder;

        AdditionsListData mCurrentData = mArrayListAdditionsListDatas
                .get(position);

//        if (convertView == null) {
        mViewHolder = new ViewHolder();
        convertView = mActivity.getLayoutInflater().inflate(R.layout.additions_listitems, null);

        mViewHolder.mLayoutOfList = (LinearLayout) convertView
                .findViewById(R.id.addition_listitem_layoutoflist);

        mViewHolder.mCheckBoxAdditionItem = (CheckBox) convertView
                .findViewById(R.id.addition_listitem_checkbox_item);
        mViewHolder.mTextViewTextAdditionName = (TextView) convertView
                .findViewById(R.id.addition_listitem_text_additionname);
        mViewHolder.mTextViewAdditionPrice = (TextView) convertView
                .findViewById(R.id.addition_listitem_text_additionprice);


        convertView.setTag(mViewHolder);

//        } else {
//            mViewHolder = (ViewHolder) convertView.getTag();
//        }

        mViewHolder.mCheckBoxAdditionItem.setTag(position);

        mViewHolder.mTextViewTextAdditionName.setText(mCurrentData
                .getName());
        mViewHolder.mTextViewAdditionPrice.setText("" + mCurrentData
                .getPrice());

        if (mCurrentData.isAdditionSelected())
            mViewHolder.mCheckBoxAdditionItem.setChecked(true);
        else
            mViewHolder.mCheckBoxAdditionItem.setChecked(false);


        mViewHolder.mCheckBoxAdditionItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                float tot = 0;
                System.out.println("Check box clicked position: " + compoundButton.getTag());
                int pos = (int) compoundButton.getTag();

                if (b) {
                    mArrayListAdditionsListDatas.get(position).setIsAdditionSelected(true);
                 /*   System.out.println("AdditoinListAdapter: Addition Added: " + mArrayListAdditionsListDatas.get(pos).getName() + " at Position: " + pos);
                    mArrayListAdditionsSelected.add(mArrayListAdditionsListDatas.get(pos));
                    TotalofSelectedAdditions += mArrayListAdditionsListDatas.get(pos).getPrice();
                    mNavigationActivity.mOrderFragment.mTextViewTotalAdditoin_dialog.setText("" + TotalofSelectedAdditions);*/

                } else {
                    mArrayListAdditionsListDatas.get(pos).setIsAdditionSelected(false);

                    /*for (int i = 0; i < mArrayListAdditionsSelected.size(); i++) {
                        if (mArrayListAdditionsListDatas.get(pos).getName()
                                .equalsIgnoreCase(mArrayListAdditionsSelected.get(i).getName())) {

                            System.out.println("AdditoinListAdapter: Addition Removed: " + mArrayListAdditionsSelected.get(i).
                                    getName() + " at Position: " + i);
                            mArrayListAdditionsSelected.remove(i);
                            TotalofSelectedAdditions -= mArrayListAdditionsListDatas.get(pos).getPrice();
                            mNavigationActivity.mOrderFragment.mTextViewTotalAdditoin_dialog.setText("" + TotalofSelectedAdditions);
                        }
                    }*/
                }
                tot = 0;
                for (int i = 0; i < mArrayListAdditionsListDatas.size(); i++) {
                    //System.out.println("AdditoinListAdapter: Total:(B)-->" + TotalofSelectedAdditions);
                    if (mArrayListAdditionsListDatas.get(i).isAdditionSelected()) {
                        System.out.println("AdditoinListAdapter: Checkbox Selected:-->" + mArrayListAdditionsListDatas.get(i).
                                getName() + " at Position: " + i);
                        System.out.print(" with Price:-->" + mArrayListAdditionsListDatas.get(i).getPrice());
                        tot += mArrayListAdditionsListDatas.get(i).getPrice();
                        System.out.println("AdditoinListAdapter: Total:(A)-->" + tot);

                        /*
                        old code for additions
                         */
                        /*
                        System.out.println("AdditoinListAdapter: Addition Added: " + mArrayListAdditionsListDatas.get(pos).getName() + " at Position: " + pos);
                        mArrayListAdditionsSelected.add(mArrayListAdditionsListDatas.get(pos));
                        */
                    }
                }
                mNavigationActivity.mOrderFragment.mTextViewTotalAdditoin_dialog.setText("" + tot);
                TotalofSelectedAdditions = tot;

            }
        });

        if (Constants.isOrderStatusShown) {
            mViewHolder.mCheckBoxAdditionItem.setEnabled(false);
        } else {
            mViewHolder.mCheckBoxAdditionItem.setEnabled(true);
        }

        return convertView;
    }

    public class ViewHolder {
        CheckBox mCheckBoxAdditionItem;
        TextView mTextViewTextAdditionName;
        TextView mTextViewAdditionPrice;
        LinearLayout mLayoutOfList;
    }

}
