package com.waycreon.thinkpizza.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.AdditionsListData;
import com.waycreon.thinkpizza.datamodels.Items;
import com.waycreon.thinkpizza.datamodels.OrderedItemsData;
import com.waycreon.thinkpizza.datamodels.OrderedItemsDataNew;
import com.waycreon.thinkpizza.datamodels.PizzaCrustData;
import com.waycreon.thinkpizza.datamodels.PizzaListData;
import com.waycreon.thinkpizza.datamodels.PizzaRestro;
import com.waycreon.thinkpizza.datamodels.Toppings;
import com.waycreon.thinkpizza.imageloader.ImageLoader;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrderStatusListAdapter extends ArrayAdapter<OrderedItemsDataNew> {

    Activity mActivity;

    List<OrderedItemsDataNew> mListOrderedItemsDatas;
    ArrayList<Items> mItemses;
    ItemsAdapter mItemsAdapter;

    NavigationActivity mNavigationActivity;
    ProgressDialog mProgressDialog;
    Dialog mDialogFavoriteRemove;
    ArrayList<OrderedItemsData> mOrderedItemsDataArrayList;
    String orderid = "";
    String menuid = "";
    Gson mGson;

    Dialog mDialogRatings;

    public OrderStatusListAdapter(Activity mActivity, List<OrderedItemsDataNew> mOrderedItemsDatas) {
        super(mActivity, R.layout.order_status_list, mOrderedItemsDatas);

        this.mActivity = mActivity;
        this.mListOrderedItemsDatas = mOrderedItemsDatas;
        Log.e("  OrderStatusListAdapter()-->", "Size of Order list: " + mOrderedItemsDatas.size());
        mNavigationActivity = (NavigationActivity) mActivity;
        mGson = new Gson();
    }

    @Override
    public int getCount() {
        return mListOrderedItemsDatas.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;


        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.order_status_list, null);

//            mViewHolder.mRelativeLayoutParent = (RelativeLayout) convertView.findViewById(R.id.dialog_pizza_parent_layout);

            mViewHolder.mImageViewRestaurant = (ImageView) convertView.findViewById(R.id.order_status_image_restaurantimage);
            mViewHolder.mTextViewRestaurantName = (TextView) convertView.findViewById(R.id.order_status_text_restaurant_name);
            mViewHolder.mTextViewOrderID = (TextView) convertView.findViewById(R.id.order_status_text_orderid);
            mViewHolder.mTextViewOrderDate = (TextView) convertView.findViewById(R.id.order_status_text_orderdate);
            mViewHolder.mTextViewOrderStatus = (TextView) convertView.findViewById(R.id.order_status_text_orderstatus);
            mViewHolder.mTextViewTag = (TextView) convertView.findViewById(R.id.order_status_text_tag);
            mViewHolder.mTextViewDeliveryAddress = (TextView) convertView.findViewById(R.id.order_status_text_address);
            mViewHolder.mListViewItems = (ListView) convertView.findViewById(R.id.order_status_list_ordereditems);

            mViewHolder.mImageViewRating = (ImageView) convertView.findViewById(R.id.order_status_image_rating);

//            mViewHolder.mListViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
//
//                    int pos = (int) view.getTag();
//                    Intent mIntent = new Intent(mActivity, OrderStatusDetail.class);
//                    mIntent.putExtra("order_id", mListOrderedItemsDatas.get(pos).getOrder_id());
//                    mIntent.putExtra("menuid", mListOrderedItemsDatas.get(pos).getItems().get(i).getMenu_id());
//                    mActivity.startActivity(mIntent);
//                }
//            });

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mListViewItems.setTag(position);
        mViewHolder.mImageViewRating.setTag(position);

        mViewHolder.mListViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

//                Intent mIntent = new Intent(mActivity, OrderStatusDetail.class);
//                mIntent.putExtra("order_id", mListOrderedItemsDatas.get(position).getOrder_id());
//                mIntent.putExtra("menu_id", mListOrderedItemsDatas.get(position).getItems().get(i).getMenu_id());
//                mActivity.startActivity(mIntent);

                orderid = mListOrderedItemsDatas.get(position).getOrder_id();
                menuid = mListOrderedItemsDatas.get(position).getItems().get(i).getMenu_id();
                new OrderDetailService().execute();


            }
        });
        mViewHolder.mImageViewRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                System.out.println("Pos: " + pos);

                showRatingsDialog(mListOrderedItemsDatas.get(pos).getItems().get(0).getPid(), pos);
                System.out.println("Pid: " + mListOrderedItemsDatas.get(position).getItems().get(0).getPid());
            }
        });

        System.out.println("Status of order: " + mListOrderedItemsDatas.get(position).getOrder_status());
        if (mListOrderedItemsDatas.get(position).getOrder_status().equalsIgnoreCase("Delivered")) {
            mViewHolder.mImageViewRating.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.mImageViewRating.setVisibility(View.GONE);
        }

        ImageLoader mImageLoader = new ImageLoader(mActivity);
        mImageLoader.DisplayImage(mListOrderedItemsDatas.get(position).getImg_url(), mViewHolder.mImageViewRestaurant);
        mViewHolder.mTextViewRestaurantName.setText(mListOrderedItemsDatas.get(position).getName());
        mViewHolder.mTextViewOrderID.setText("Order ID: " + mListOrderedItemsDatas.get(position).getOrder_id());
        mViewHolder.mTextViewOrderDate.setText("Order Date: " + mListOrderedItemsDatas.get(position).getOrder_date());
        mViewHolder.mTextViewOrderStatus.setText("Order Status: " + mListOrderedItemsDatas.get(position).getOrder_status());
        mViewHolder.mTextViewTag.setText(mListOrderedItemsDatas.get(position).getDeliveryAddress().getTag_name());

        String add = "";
        add += mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_name();
        if (mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_no().length() > 0)
            add += ", " + mListOrderedItemsDatas.get(position).getDeliveryAddress().getStreet_no();
        if (mListOrderedItemsDatas.get(position).getDeliveryAddress().getApt_no().length() > 0)
            add += ",\n" + mListOrderedItemsDatas.get(position).getDeliveryAddress().getApt_no();

        mViewHolder.mTextViewDeliveryAddress.setText(add);

        mItemses = mListOrderedItemsDatas.get(position).getItems();
        mItemsAdapter = new ItemsAdapter(mActivity, mItemses);
        mViewHolder.mListViewItems.setAdapter(mItemsAdapter);
        mViewHolder.mListViewItems.setVisibility(View.VISIBLE);
        setListViewHeightBasedOnChildren(mViewHolder);


        return convertView;
    }


    public class ViewHolder {

        RelativeLayout mRelativeLayoutParent;
        ImageView mImageViewAddtoCart;
        ImageView mImageViewRemoveFavorite;

        ImageView mImageViewRestaurant;
        TextView mTextViewRestaurantName;
        TextView mTextViewOrderID;
        TextView mTextViewOrderDate;
        TextView mTextViewOrderStatus;
        TextView mTextViewTag;
        TextView mTextViewDeliveryAddress;

        ListView mListViewItems;

        ImageView mImageViewCancelOrder;
        ImageView mImageViewRating;
    }

    private void callGetOrderData(String order_id) {
        new OrderDataSerivce(order_id).execute();
    }

    public class OrderDataSerivce extends AsyncTask<Void, Void, Void> {

        String response = "";
        Store_pref mStore_pref;
        Gson mGson;
        ArrayList<OrderedItemsData> mOrderedItemsDatasFavorite;
        String orderid;
        ProgressDialog mProgressDialogOrder;

        public OrderDataSerivce(String order_id) {
            orderid = order_id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialogOrder = new ProgressDialog(mActivity);
            mProgressDialogOrder.setMessage("Loading Order...");
            mProgressDialogOrder.setCancelable(false);
            mProgressDialogOrder.show();
            mGson = new Gson();
            mStore_pref = new Store_pref(mActivity);
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(Constants.GET_ORDER_DATA + "&user_id=" + mStore_pref.getUser().getId()
                    + "&order_id=" + orderid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("OrderDataSerivce().DoinBackground()", "Could not reach to server.");
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialogOrder.dismiss();
            System.out.println("Response of OrderDataService:" + response);
            if (response != null && response.length() > 0) {
                JSONObject mJsonObject;
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {

                        JSONArray mJsonArray = mJsonObject.getJSONArray("order");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDatasFavorite = new ArrayList<>();
                        mNavigationActivity.mArrayListOrderedItem.clear();

                        for (int i = 0; i < mJsonArray.length(); i++) {
                            float total_tmp = 0;
                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setId(temp.getString("id"));
                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
                            mOrderedItemsData.setQty(Integer.parseInt(temp.getString("qty")));

                            System.out.println("Orderdata : " + temp);

                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
                            System.out.println("pizza_details from Gson: " + pizza_details);
                            mOrderedItemsData.setPizza_details(pizza_details);

                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
                            System.out.println("pizzaria from Gson: " + pizzaria);
                            mOrderedItemsData.setPizzaria(pizzaria);

                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            if (temp.getString("pizzacrust").length() > 2) {
                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
                                System.out.println("pizzacrust from Gson: " + pizzacrust);
                                mOrderedItemsData.setPizzacrust(pizzacrust);
                                total_tmp += pizzacrust.getPrice();
                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
                            }
                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            if (temp.getString("extra_ingredients").length() > 2) {

                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
                                float total_additions = 0;
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
                                    System.out.println("AdditionData: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListAdditionsListDatas.add(mAdditionsListData);

                                    total_additions += mAdditionsListData.getPrice();
                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                total_tmp += total_additions;
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
                                mArrayListAdditionsListDatas = null;
                            }
                            /*
                            6-10-2015 code for Toppings selected is added
                             */

                            if (temp.getString("toppings").length() > 2) {

                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();


                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {

                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
                                    System.out.println("Toppings: " + temp_addition);
                                    Toppings mAdditionsListData = new Toppings();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));

                                    mArrayListToppingses.add(mAdditionsListData);

                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setToppings(mArrayListToppingses);


                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListToppingses = null;
                            }

                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
                            System.out.println("address from Gson: " + pizzaria);
                            mOrderedItemsData.setAddress(address);

                            Log.e("OrderDataSerivce()", "-----------------Favorite Pizzas----------------");

//                            if (temp.getString("pizza_size").equalsIgnoreCase("small")) {
//                                System.out.println("Favorite pizza's total set with small size");
//                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//
//                            } else if (temp.getString("pizza_size").equalsIgnoreCase("big")) {
//                                System.out.println("Favorite pizza's total set with Big size");
//                                Log.e("Favorite Service.onPostExecute()", "Big Price: " + mOrderedItemsData.getPizza_details().getPrice_large());
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            } else {
//                                System.out.println("Item is not pizza");
//                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
//                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
//                            }

//                            mOrderedItemsData.setTotal(total_tmp);
                            mOrderedItemsData.setTotal(Float.parseFloat(temp.getString("total")));
                            mOrderedItemsData.setIsFromFavorite_History(true);
                            /*
                                Added on 10-10-2015
                            below line is added to set the favoorite id to pizza so that
                            when user add a pizza to basket at that time while orders the pizza
                            its address can be updated.

                             */
                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));

                            mOrderedItemsDatasFavorite.add(mOrderedItemsData);
                            mOrderedItemsData.printAllData();
                            /*
                                all order data items has been added to Main Order Array list to place order.
                            */
                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mOrderedItemsDatasFavorite.get(i));
                        }
                        //LOOP ENDS


                        /*
                        below Static variable is set to Cancel the order while pressing back.
                         */
                        Constants.isOrderHistoryFavorite = true;
//                        mAdapterFavoritePizza = new FavoritePizzaNewAdapter(getActivity(), mOrderedItemsDatasFavorite, "FragmentFavorite");
//                        mListViewFavorite.setAdapter(mAdapterFavoritePizza);
//                        mAdapterFavoritePizza.notifyDataSetChanged();

                        /*
                        show the Basket screen
                         */
                        mNavigationActivity.showFragment(mNavigationActivity.mBasketFragment, true);
                        Log.e("BackstackList", "BasketFragment Fragment is added.");
                        mNavigationActivity.backstacklist.add(mNavigationActivity.mBasketFragment);
                        // mNavigationActivity.onBackPressed();
                        mNavigationActivity.ShowHideIcons();
                        mNavigationActivity.showFragments();
//                            mNavigationActivity.mBasketFragment.AddItemtoListOrder(mListOrderedItemsDatas.get(position));
                        mNavigationActivity.mBasketFragment.lastVisibleLayout = 1;
                        mNavigationActivity.mBasketFragment.mRelativeLayoutOrderedItems.setVisibility(View.VISIBLE);
                        mNavigationActivity.mBasketFragment.mImageViewProcess1.setImageResource(R.drawable.icon_basket_processs_not_finished);


                    } else {
                        Log.e("OrderDataSerivce().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(mActivity, "Response Success", Toast.LENGTH_SHORT).show();
            }
        }
    }

//    void initDialogRemoveFavoritePizza(final int position) {
//
//        mDialogFavoriteRemove = new Dialog(mActivity);
//        mDialogFavoriteRemove.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mDialogFavoriteRemove.setContentView(R.layout.dialog_favorite_address);
//
//        TextView mTextViewDialogHeading;
//        ImageView mImageViewDialogImage;
//
//        mTextViewDialogHeading = (TextView) mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_list_pizzas_text_dialog_title);
//        mImageViewDialogImage = (ImageView) mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_list_pizzas_image_dialog_title);
//
//        mTextViewDialogHeading.setText("Do you want to remove this order from Favorites ?");
//        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);
//        mDialogFavoriteRemove.show();
//
//        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_yes).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                new DeleteFavoritePizzaService(mListOrderedItemsDatas.get(position).getOrder_id(), position).execute();
//
//            }
//        });
//
//        mDialogFavoriteRemove.findViewById(R.id.dialog_favorite_address_text_no).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mDialogFavoriteRemove.dismiss();
//            }
//        });
//    }

//    public class DeleteFavoritePizzaService extends AsyncTask<Void, Void, Void> {
//
//        String response = "";
//        String url = "";
//        int Pos;
//
//        DeleteFavoritePizzaService(String id, int pos) {
//            url = Constants.DELETE_FAVORITE_ORDER + "&id=" + id;
//            this.Pos = pos;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            mProgressDialog = new ProgressDialog(mActivity);
//            mProgressDialog.setMessage("Please wait...");
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.show();
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            try {
//
//                HttpClient mHttpClient = new DefaultHttpClient();
//                System.out.println("DeleteFavoritePizzaService Url: " + url);
//                HttpGet mHttpGet = new HttpGet(url);
//                ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
//                response = mHttpClient.execute(mHttpGet, mResponseHandler);
//            } catch (IOException e) {
//                e.printStackTrace();
//                Log.e("DeleteFavoritePizzaService().DoinBackground()", "Could not reach to server.");
//            }
//            return null;
//        }
//
//        @SuppressLint("NewApi")
//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            mProgressDialog.dismiss();
//            Log.e("DeleteFavoritePizzaService Response", "Response: " + response);
//            Toast.makeText(mActivity, response + "", Toast.LENGTH_SHORT).show();
//
//            try {
//                JSONObject mJsonObject = new JSONObject(response);
//                if (mJsonObject.length() > 0) {
//                    Log.e("result: ", "" + response);
//
//                    if (mJsonObject.getString("responseCode").equalsIgnoreCase("1")) {
//
//                        mListOrderedItemsDatas.remove(Pos);
//
//
//                        if (mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza != null)
//                            if (DialogTag.equalsIgnoreCase("FavoritePizza")) {
//                                mNavigationActivity.mapFragment.mAdapterFavoritePizza.notifyDataSetChanged();
//                                Toast.makeText(mActivity, "Your favorite Order is removed...", Toast.LENGTH_SHORT).show();
//                            }
//
//                        if (DialogTag.equalsIgnoreCase("FragmentFavorite")) {
//                            mNavigationActivity.mFavoriteFragment.mAdapterFavoritePizza.notifyDataSetChanged();
//                            Toast.makeText(mActivity, "Your favorite Order is removed.", Toast.LENGTH_SHORT).show();
//                        }
//                        mDialogFavoriteRemove.dismiss();
//
//                    } else {
//                        Toast.makeText(mActivity, "Your favorite Order is not removed please try again.", Toast.LENGTH_SHORT).show();
//                    }
//                    Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
//
//                }// main if Json object ends
//
//            } catch (JSONException ej) {
//                ej.printStackTrace();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//
//            }
//
//
//        }
//    }


    public class OrderDetailService extends AsyncTask<Void, Void, Void> {

        String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(Constants.GET_ORDER_MENU_ITEM + "&order_id=" + orderid + "&id=" + menuid);
            ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
            try {
                response = mHttpClient.execute(mHttpGet, mResponseHandler);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("OrderDetailService().DoinBackground()", "Could not reach to server.");
            }
            return null;
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            mProgressDialog.dismiss();
            System.out.println("Response of OrderDetailService:" + response);
            JSONObject mJsonObject;
            if (response != null && response.length() > 0) {
                try {
                    mJsonObject = new JSONObject(response);
                    System.out.println("Json Object: " + mJsonObject);

                    if (mJsonObject.getString("responseCode").equals("1")) {
                        JSONArray mJsonArray = mJsonObject.getJSONArray("orders");
                        System.out.println("Json Array Object: " + mJsonArray);

                        mOrderedItemsDataArrayList = new ArrayList<>();


                        for (int i = 0; i < mJsonArray.length(); i++) {
                            float total_tmp = 0;
                            OrderedItemsData mOrderedItemsData = new OrderedItemsData();
                            JSONObject temp = mJsonArray.getJSONObject(i);

                            mOrderedItemsData.setId(temp.getString("id"));
                            mOrderedItemsData.setUser_id(temp.getString("user_id"));
                            mOrderedItemsData.setMenu_id(temp.getString("menu_id"));
                            mOrderedItemsData.setAddress_id(temp.getString("address_id"));
//                            mOrderedItemsData.setAdditions(temp.getString("additions"));
//                            mOrderedItemsData.setId(temp.getString("custom_ingredients"));
//                            "custom_ingredients": [],
                            mOrderedItemsData.setPizza_crust_id(temp.getString("pizza_crust_id"));
                            mOrderedItemsData.setPizza_size(temp.getString("pizza_size"));
                            mOrderedItemsData.setQty(Double.parseDouble(temp.getString("qty")));

                            System.out.println("Orderdata : " + temp);

                            PizzaListData pizza_details = mGson.fromJson(temp.getString("pizza_details"), PizzaListData.class);
                            System.out.println("pizza_details from Gson: " + pizza_details);
                            mOrderedItemsData.setPizza_details(pizza_details);

                            PizzaRestro pizzaria = mGson.fromJson(temp.getString("pizzaria"), PizzaRestro.class);
                            System.out.println("pizzaria from Gson: " + pizzaria);
                            mOrderedItemsData.setPizzaria(pizzaria);

                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            if (temp.getString("pizzacrust").length() > 2) {
                                PizzaCrustData pizzacrust = mGson.fromJson(temp.getString("pizzacrust"), PizzaCrustData.class);
                                System.out.println("pizzacrust from Gson: " + pizzacrust);
                                mOrderedItemsData.setPizzacrust(pizzacrust);
                                total_tmp += pizzacrust.getPrice();
                                Log.e("Favorite Service.onPostExecute()-->", "Added PizzaCrust Price: " + pizzacrust.getPrice());
                            }
                            Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            if (temp.getString("extra_ingredients").length() > 2) {

                                JSONArray mJsonArrayextra_ingredients = temp.getJSONArray("extra_ingredients");
                                ArrayList<AdditionsListData> mArrayListAdditionsListDatas = new ArrayList<>();
                                float total_additions = 0;
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                for (int j = 0; j < mJsonArrayextra_ingredients.length(); j++) {

                                    JSONObject temp_addition = mJsonArrayextra_ingredients.getJSONObject(j);
                                    System.out.println("AdditionData: " + temp_addition);
                                    AdditionsListData mAdditionsListData = new AdditionsListData();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setPid(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));
                                    mAdditionsListData.setIsAdditionSelected(true);
                                    mArrayListAdditionsListDatas.add(mAdditionsListData);

                                    total_additions += mAdditionsListData.getPrice();
                                    Log.e("Favorite Service.onPostExecute()-->", "Added Additions Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }
                                Log.e("Favorite Service.onPostExecute()", "Total Additions: " + total_additions);
                                total_tmp += total_additions;
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                                mOrderedItemsData.setExtra_ingredients(mArrayListAdditionsListDatas);
                                System.out.println("extra_ingredients from Gson: " + mJsonArrayextra_ingredients);
                                mArrayListAdditionsListDatas = null;
                            }

                            /*
                            6-10-2015 code for Toppings selected is added
                             */

                            if (temp.getString("toppings").length() > 2) {

                                JSONArray mJsonArray_toppings = temp.getJSONArray("toppings");
                                ArrayList<Toppings> mArrayListToppingses = new ArrayList<>();


                                for (int j = 0; j < mJsonArray_toppings.length(); j++) {

                                    JSONObject temp_addition = mJsonArray_toppings.getJSONObject(j);
                                    System.out.println("Toppings: " + temp_addition);
                                    Toppings mAdditionsListData = new Toppings();
                                    mAdditionsListData.setId(temp_addition.getString("id"));
//                                    mAdditionsListData.setMenus_id(temp_addition.getString("menus_id"));
                                    mAdditionsListData.setMenus_id(temp_addition.getString("pid"));
                                    mAdditionsListData.setName(temp_addition.getString("name"));
                                    mAdditionsListData.setPrice(Float.parseFloat(temp_addition.getString("price")));

                                    mArrayListToppingses.add(mAdditionsListData);

                                    Log.e("Favorite Service.onPostExecute()-->", "Added Selected Pizza Topping's Price: " + mAdditionsListData.getPrice());
                                    mAdditionsListData = null;
                                }

                                mOrderedItemsData.setToppings(mArrayListToppingses);


                                System.out.println("toppings from Gson: " + mJsonArray_toppings);
                                //mOrderedItemsData.setExtra_ingredients(extra_ingredients);
                                mArrayListToppingses = null;
                            }

                            com.waycreon.thinkpizza.datamodels.Address address = mGson.fromJson(temp.getString("address"), com.waycreon.thinkpizza.datamodels.Address.class);
                            System.out.println("address from Gson: " + pizzaria);
                            mOrderedItemsData.setAddress(address);

                            Log.e("OrderDetailService()", "-----------------Favorite Pizzas----------------");
                            mOrderedItemsDataArrayList.add(mOrderedItemsData);
                            mOrderedItemsData.printAllData();

                            if (temp.getString("pizza_size").equalsIgnoreCase("regular")) {
                                System.out.println("Favorite pizza's total set with regular size");
                                Log.e("Favorite Service.onPostExecute()", "Small Price: " + mOrderedItemsData.getPizza_details().getPrice_small());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_small());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);

                            } else if (temp.getString("pizza_size").equalsIgnoreCase("medium")) {
                                System.out.println("Favorite pizza's total set with meduim size");
                                Log.e("Favorite Service.onPostExecute()", "Small Medium: " + mOrderedItemsData.getPizza_details().getPrice_medium());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_medium());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            } else if (temp.getString("pizza_size").equalsIgnoreCase("large")) {
                                System.out.println("Favorite pizza's total set with large size");
                                Log.e("Favorite Service.onPostExecute()", "Small Large: " + mOrderedItemsData.getPizza_details().getPrice_large());
                                total_tmp += Float.parseFloat(mOrderedItemsData.getPizza_details().getPrice_large());
                                Log.e("Favorite Service.onPostExecute()", "Total: " + total_tmp);
                            }

                            mOrderedItemsData.setTotal(total_tmp);
                            mOrderedItemsData.setIsFromFavorite_History(true);
                            /*
                                Added on 10-10-2015
                            below line is added to set the favoorite id to pizza so that
                            when user add a pizza to basket at that time while orders the pizza
                            its address can be updated.

                             */
                            mOrderedItemsData.getPizza_details().setFavourite_id(temp.getString("id"));

                        }
                        //LOOP ENDS
                        Constants.isOrderStatusShown = true;
                        mNavigationActivity.showFragment(mNavigationActivity.mOrderFragment, true);
//                        mNavigationActivity.mOrderFragment.SetPizzaData(mData, mPizzaRestro, position);
                        mNavigationActivity.mOrderFragment.EditOrderedItem(mOrderedItemsDataArrayList.get(0), 0);

                        System.out.println("Last Fragment:" + (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1).getTag()));
                        if (mNavigationActivity.mOrderFragment != (mNavigationActivity.backstacklist.get(mNavigationActivity.backstacklist.size() - 1))) {
                            mNavigationActivity.backstacklist.add(mNavigationActivity.mOrderFragment);
                            Log.e("BackstackList", "mOrderFragment Fragment is added.");
                        }
                        mNavigationActivity.showFragments();

//                        mTextViewDialogHeading.setText(R.string.list_address_only_dialog_favorite_pizza);
//                        mImageViewDialogImage.setImageResource(R.drawable.favourite_pizza_100);

                    } else {
                        Log.e("OrderDetailService().onPostExecut()", mJsonObject.getString("responseMsg"));
                        Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(mActivity, "Response Success", Toast.LENGTH_SHORT).show();
            }
//            mImageViewFavorite.setEnabled(true);
        }
    }

    public void setListViewHeightBasedOnChildren(ViewHolder mViewHolder) {
        ListAdapter listAdapter = mViewHolder.mListViewItems.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(mViewHolder.mListViewItems.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        Log.e("setListViewHeightBasedOnChildren()", "List Size: " + listAdapter.getCount());
        int gap = 8;
        for (int i = 0; i < listAdapter.getCount(); i++) {

            Items mItems = (Items) listAdapter.getItem(i);

            if (i > 0 && i < 3)
                gap = 8;
            if (i >= 3)
                gap = 10;
            else if (i >= 6)
                gap = 12;
            else if (i >= 10)
                gap = 13;

            if (mItems.getName().length() > 15)
                gap += 10;
            else if (mItems.getName().length() > 20)
                gap += 12;
            else if (mItems.getName().length() > 25 && mItems.getName().length() < 30)
                gap += 15;
            if (mItems.getName().length() > 35)
                gap += 20;


            view = listAdapter.getView(i, view, mViewHolder.mListViewItems);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));
                System.out.println("Height of view:" + view.getLayoutParams().height);
            }
            System.out.println("measured Height of view(B):" + view.getMeasuredHeight());
            System.out.println("Height of view(B):" + view.getLayoutParams().height);
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

//            totalHeight += view.getMeasuredHeight();
            totalHeight += (view.getMeasuredHeight() + gap);
            System.out.println("Height of view(A):" + view.getLayoutParams().height);
            System.out.println("totalHeight(in loop):" + totalHeight);
        }
        Log.e("setListViewHeightBasedOnChildren()", "Total Hieght (out of loop): " + totalHeight);
        ViewGroup.LayoutParams params = mViewHolder.mListViewItems.getLayoutParams();
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        params.height = totalHeight + (mViewHolder.mListViewItems.getDividerHeight() * (listAdapter.getCount() - 1));
        Log.e("setListViewHeightBasedOnChildren()", "(out of loop)ViewGroup Height: " + params.height);
        mViewHolder.mListViewItems.setLayoutParams(params);
        mViewHolder.mListViewItems.requestLayout();
    }

    private void showRatingsDialog(final String pid, final int position) {

        final RatingBar mRatingBarOverall;
        final RatingBar mRatingBarCrust;
        final RatingBar mRatingBarToppings;
        final RatingBar mRatingBarDeliveryTime;

        Button mButtonSubmit;

        mDialogRatings = new Dialog(mActivity);
        mDialogRatings.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogRatings.setContentView(R.layout.dialog_ratings);


        mButtonSubmit = (Button) mDialogRatings.findViewById(R.id.ratings_button_submit);
        mRatingBarOverall = (RatingBar) mDialogRatings.findViewById(R.id.ratings_overall);
        mRatingBarCrust = (RatingBar) mDialogRatings.findViewById(R.id.ratings_crust);
        mRatingBarToppings = (RatingBar) mDialogRatings.findViewById(R.id.ratings_toppings);
        mRatingBarDeliveryTime = (RatingBar) mDialogRatings.findViewById(R.id.ratings_deliverytime);

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("mRatingBarOverall: " + mRatingBarOverall.getRating());
                System.out.println("mRatingBarCrust: " + mRatingBarCrust.getRating());
                System.out.println("mRatingBarToppings: " + mRatingBarToppings.getRating());
                System.out.println("mRatingBarDeliveryTime: " + mRatingBarDeliveryTime.getRating());

                CallPizzeriaRatingsService(pid, mRatingBarOverall.getRating(), mRatingBarCrust.getRating()
                        , mRatingBarToppings.getRating(), mRatingBarDeliveryTime.getRating(), position);
            }
        });

        mDialogRatings.show();
    }

    /*
    Pizza rating service start
     */
    public void CallPizzeriaRatingsService(String pid, float overall, float crust, float toppings, float deliverytime, int pos) {

        mProgressDialog = new ProgressDialog(mActivity);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String url = Constants.PIZZERIA_RATINGS + "&pid=" + pid + "&overall_ratings=" + (int) overall + "&crust_ratings="
                + (int) crust + "&toppings_quality_ratings=" + (int) toppings + "&delivery_time_ratings=" + (int) deliverytime;

        System.out.println("Pizzeira rating URL: " + url);
        RequestQueue queue = Volley.newRequestQueue(mActivity);
        StringRequest myReq = new StringRequest(Request.Method.GET, url,
                PizzeriaRatingsSuccess(pos), PizzeriaRatingsFail());
        queue.add(myReq);
    }

    private Response.Listener<String> PizzeriaRatingsSuccess(final int pos) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                System.out.println("Response of PizzeriaRatingsService:" + response);

                if (response != null && response.length() > 0) {
                    try {
                        JSONObject mJsonObject;
                        mJsonObject = new JSONObject(response);
                        System.out.println("Json Object: " + mJsonObject);
                        mDialogRatings.dismiss();
                        Log.e("PizzeriaRatingsSuccess().onPostExecut()", mJsonObject.getString("responseCode"));
                        Log.e("PizzeriaRatingsSuccess().onPostExecut()", mJsonObject.getString("responseMsg"));
                        if (mJsonObject.getString("responseCode").equals("1")
                                || mJsonObject.getString("responseCode").equals("2")) {
                            Toast.makeText(mActivity, "Ratings done successfully.", Toast.LENGTH_SHORT).show();

                            mListOrderedItemsDatas.remove(pos);
                            notifyDataSetChanged();
                        } else {
                            Log.e("PizzeriaRatingsSuccess().onPostExecut()", mJsonObject.getString("responseMsg"));
                            Toast.makeText(mActivity, mJsonObject.getString("responseMsg"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                Toast.makeText(mActivity, "Response Success", Toast.LENGTH_SHORT).show();
                }

            }
        };
    }

    private Response.ErrorListener PizzeriaRatingsFail() {
        return new Response.ErrorListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("PizzeriaRatingsFail().OrderStatuslistadapter", "Error: " + error.getMessage());
                Toast.makeText(mActivity, "Sorry, you can not give ratings now please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    /*
    Pizza rating service ends
     */
}
