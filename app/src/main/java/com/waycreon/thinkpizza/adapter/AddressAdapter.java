package com.waycreon.thinkpizza.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.waycreon.thinkpizza.NavigationActivity;
import com.waycreon.thinkpizza.R;
import com.waycreon.thinkpizza.datamodels.Address;

import java.util.ArrayList;

public class AddressAdapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<Address> mAddressArrayListAddress;
    ViewHolder mViewHolder;

    NavigationActivity mNavigationActivity;


    public AddressAdapter(Activity mActivity,
                          ArrayList<Address> models) {
        this.mActivity = mActivity;
        this.mAddressArrayListAddress = models;
        mNavigationActivity = (NavigationActivity) mActivity;

    }

    @Override
    public int getCount() {
        return mAddressArrayListAddress.size();
    }

    @Override
    public Object getItem(int arg0) {
        return mAddressArrayListAddress.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        Address mCurrentData = mAddressArrayListAddress
                .get(position);

        if (convertView == null) {
            mViewHolder = new ViewHolder();
            convertView = mActivity.getLayoutInflater().inflate(
                    R.layout.dialog_address, null);

            mViewHolder.mTextViewTagName = (TextView) convertView
                    .findViewById(R.id.address_tagname);
            mViewHolder.mTextViewFullAddress = (TextView) convertView
                    .findViewById(R.id.address_full_address);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        String full_address = "";
        mViewHolder.mTextViewTagName.setText(mCurrentData.getTag_name());
        full_address = mCurrentData.getStreet_name() + ", " + mCurrentData.getStreet_no() + ", " + mCurrentData.getApt_no()
                + "\n" + mCurrentData.getCity() + " - " + mCurrentData.getZip_code() +
                "\n" + mCurrentData.getState();
        mViewHolder.mTextViewFullAddress.setText(full_address);


        return convertView;
    }

    public class ViewHolder {

        TextView mTextViewTagName;
        TextView mTextViewFullAddress;
    }

}
