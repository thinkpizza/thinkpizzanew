package com.waycreon.thinkpizza;

import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.waycreon.thinkpizza.Response.Register;
import com.waycreon.thinkpizza.datamodels.User;
import com.waycreon.waycreon.utils.Constants;
import com.waycreon.waycreon.utils.Store_pref;
import com.waycreon.waycreon.utils.Tags;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<People.LoadPeopleResult>, OnClickListener {

    // google login
    private static final int DIALOG_PLAY_SERVICES_ERROR = 0;

    private static final int STATE_DEFAULT = 0;
    private static final int STATE_SIGN_IN = 1;
    private static final int STATE_IN_PROGRESS = 2;

    private static final int RC_SIGN_IN = 0;

    private static final String SAVED_PROGRESS = "sign_in_progress";
    private GoogleApiClient mGoogleApiClient;
    private int mSignInProgress;
    private PendingIntent mSignInIntent;

    private int mSignInError;

/*
for new google plus code.
 */


    //    ImageView mImageViewEmail;
    TextView mTextViewLogin;
    TextView mTextViewSignup;
    ImageView mImageViewFB;
    ImageView mImageViewGmail;

    SimpleFacebook mSimpleFacebook;

    Store_pref mStore_pref;

    ProgressDialog Dialog;
    User mUser;
    Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        if (savedInstanceState != null) {
            mSignInProgress = savedInstanceState.getInt(SAVED_PROGRESS,
                    STATE_DEFAULT);
        }

        mStore_pref = new Store_pref(this);
        if (mStore_pref.getUserId().length() > 0) {
            Intent logedin = new Intent(MainActivity.this, NavigationActivity.class);
            startActivity(logedin);
            finish();
        }

        Dialog = new ProgressDialog(this);
        Dialog.setMessage("Loading...");
        Dialog.setCancelable(false);

        mUser = new User();
        mGson = new Gson();
       /* mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();*/
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
//        .addScope(Plus.SCOPE_PLUS_PROFILE)

        revokeGplusAccess();
        mTextViewLogin = (TextView) findViewById(R.id.main_text_login);
        mTextViewSignup = (TextView) findViewById(R.id.main_text_signup);
        mImageViewFB = (ImageView) findViewById(R.id.login_image_fb);
        mImageViewGmail = (ImageView) findViewById(R.id.login_image_gmail);

        mTextViewLogin.setOnClickListener(this);
        mTextViewSignup.setOnClickListener(this);
        mImageViewFB.setOnClickListener(this);
        mImageViewGmail.setOnClickListener(this);

        if (Tags.BUTTON_SELECTED == 1) {
            mTextViewLogin.setBackgroundResource(R.drawable.btn_login_hover);
            mTextViewSignup.setBackgroundResource(R.drawable.btn_register);
        } else {
            mTextViewLogin.setBackgroundResource(R.drawable.btn_login);
            mTextViewSignup.setBackgroundResource(R.drawable.btn_register_hover);
        }

    }


    @Override
    public void onClick(View v) {
        Intent mIntent;
        switch (v.getId()) {
            case R.id.main_text_login:
                mTextViewLogin.setBackgroundResource(R.drawable.btn_login_hover);
                mTextViewSignup.setBackgroundResource(R.drawable.btn_register);

                mIntent = new Intent(getApplicationContext(), LoginActivity.class);
                mIntent.putExtra("email", mStore_pref.getRememberMe());
                startActivity(mIntent);
                finish();
                Tags.BUTTON_SELECTED = 1;
                break;

            case R.id.main_text_signup:
                mTextViewLogin.setBackgroundResource(R.drawable.btn_login);
                mTextViewSignup.setBackgroundResource(R.drawable.btn_register_hover);
                mIntent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(mIntent);
                finish();
                Tags.BUTTON_SELECTED = 2;
                break;

            case R.id.login_image_fb:
                mSimpleFacebook.login(onLoginListener);
                break;

            case R.id.login_image_gmail:
                mGoogleApiClient.connect();
                resolveSignInError();
                break;

        }

    }

    /*private GoogleApiClient buildGoogleApiClient() {
        return new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }*/


    @Override
    public void onConnected(Bundle connectionHint) {
        // Reaching onConnected means we consider the user signed in.

        System.out.println("onConnected() is called.");
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
                this);
        // Update the user interface to reflect that the user is signed in.
        // mSignInButton.setEnabled(false);
        // Retrieve some profile information to personalize our app for the
        // user.
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
//                    this);

            String personName = currentUser.getDisplayName();
            String personPhoto = currentUser.getImage().getUrl();
            System.out.println("personName: " + personName);
            System.out.println("personPhoto: " + personName);
            //We can adjust size of image according to our requirements: here 200 is the size of image
            String personImage = personPhoto.substring(0, personPhoto.lastIndexOf("=") + 1) + "200";
            System.out.println("personImage: " + personImage);
            String personGooglePlusProfile = currentUser.getUrl();

            Log.i("name", "" + currentUser.getName());
            try {
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                mUser.setEmail(email);
                mUser.setGoogle_id(currentUser.getId());
                Log.i("AccountName ", "" + email);
            } catch (Exception e) {
                Log.e("execption gg", "" + e);
            }

            try {
                Log.i("DisplayName ", "" + currentUser.getName());
                mUser.setName(currentUser.getDisplayName());
            } catch (Exception e) {
                Log.e("execption gg", "" + e);
            }
            if (mUser.getEmail().length() > 0) {
//                Log.e("onConnected.MainActivity.java", "Data is passed");
                callLogin();
                // Indicate that the sign in process is complete.
                mSignInProgress = STATE_DEFAULT;
                Toast.makeText(getApplicationContext(), "Email :" + mUser.getEmail(), Toast.LENGTH_SHORT).show();
                /*Intent mIntent = new Intent(getApplicationContext(), SignupActivity.class);
//                mIntent.putExtra("Name", mUser.getName());
//                mIntent.putExtra("Email", mUser.getEmail());
                mIntent.putExtra("UserData", mUser);
                startActivity(mIntent);
                finish();*/
            }

            revokeGplusAccess();
//            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
//            Log.i("email ", "" + email);


        } else {
            System.out.println("Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) is null");
            Log.e("onConnected() else", "Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) is null");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

        // be returned in onConnectionFailed.
        System.out.println("onConnectionFailed() is called.");
        Log.i("fail", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
        System.out.println("result: " + result);
        if (result.getErrorCode() == ConnectionResult.API_UNAVAILABLE) {

        } else if (mSignInProgress != STATE_IN_PROGRESS) {

            mSignInIntent = result.getResolution();
            mSignInError = result.getErrorCode();

            if (mSignInProgress == STATE_SIGN_IN) {

                resolveSignInError();
            }
        }

        // In this sample we consider the user signed out whenever they do not
        // have
        // a connection to Google Play services.
        onSignedOut();
    }

    @Override
    public void onResult(People.LoadPeopleResult peopleData) {

        System.out.println("onResult() is called.");
        if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
        } else {
            Log.e("result g+",
                    "Error requesting visible circles: "
                            + peopleData.getStatus());
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason.
        // We call connect() to attempt to re-establish the connection or get a
        // ConnectionResult that we can attempt to resolve.
        System.out.println("onConnectionSuspended() is called.");
        mGoogleApiClient.connect();
    }

    private void signOutFromGplus() {
        System.out.println("signOutFromGplus() is called.");
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    private void revokeGplusAccess() {
        System.out.println("revokeGplusAccess() is called.");
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e("revoked", "User access revoked!");
                            mGoogleApiClient.connect();
                        }

                    });
        }
    }

    private void resolveSignInError() {
        System.out.println("resolveSignInError() is called.");
        if (mSignInIntent != null) {

            try {

                mSignInProgress = STATE_IN_PROGRESS;
                startIntentSenderForResult(mSignInIntent.getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                Log.i("error",
                        "Sign in intent could not be sent: "
                                + e.getLocalizedMessage());

                mSignInProgress = STATE_SIGN_IN;
                mGoogleApiClient.connect();
            }
        } else {
            showDialog(DIALOG_PLAY_SERVICES_ERROR);
        }
    }

    private void onSignedOut() {
        System.out.println("onSignedOut() is called.");
        mGoogleApiClient.disconnect();
        // Update the UI to reflect that the user is signed out.
        // mSignInButton.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("onActivityResult() is called.");
        if (requestCode == RC_SIGN_IN) {
            System.out.println("Gone inside onActivityResult() in RC_SIGN_IN");
            if (resultCode == RESULT_OK) {
                // If the error resolution was successful we should continue
                // processing errors.
                mSignInProgress = STATE_SIGN_IN;
                System.out.println("success gplus: Result OK");
            } else {
                // If the error resolution was not successful or the user
                // canceled, we should stop processing errors.
                mSignInProgress = STATE_DEFAULT;
            }

            if (!mGoogleApiClient.isConnecting()) {
                // If Google Play services resolved the issue with a dialog then
                // onStart is not called so we need to re-attempt connection
                // here.
                mGoogleApiClient.connect();
            }

        } else {
            System.out.println("Gone inside onActivityResult() in RC_SIGN_IN else part");
            mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
        }


    }

    /*
    Facebook login code starts from here
     */
    OnLoginListener onLoginListener = new OnLoginListener() {

        @Override
        public void onLogin(String s, List<Permission> list, List<Permission> list1) {

            System.out.println("onLogin() is called.");

            Profile.Properties properties = new Profile.Properties.Builder()
                    .add(Profile.Properties.NAME)
                    .add(Profile.Properties.EMAIL)
                    .add(Profile.Properties.ID)
                    .add(Profile.Properties.LOCATION)
                    .add(Profile.Properties.HOMETOWN)
                    .build();
            mSimpleFacebook.getProfile(properties, onProfileListener);
        }

        @Override
        public void onCancel() {
            // user canceled the dialog
            System.out.println("onCancel() is called.");
        }

        @Override
        public void onFail(String reason) {
            // failed to login
            System.out.println("onFail() is called.");
        }

        @Override
        public void onException(Throwable throwable) {
            // exception from facebook
            System.out.println("onException() is called.");
        }

    };

    OnProfileListener onProfileListener = new OnProfileListener() {
        @Override
        public void onComplete(Profile profile) {
            System.out.println("onProfileListener() is called.");

            mUser.setEmail(profile.getEmail());
            mUser.setFacebook_id(profile.getId());
            mUser.setName(profile.getName());
            System.out.println("FAcebook ID: " + profile.getId());
            System.out.println("FAcebook Name: " + profile.getName());
            System.out.println("FAcebook Email: " + profile.getEmail());
            System.out.println("FAcebook HomeTown: " + profile.getHometown());
            System.out.println("FAcebook Location: " + profile.getLocation());

            String fbimageurl = "http://graph.facebook.com/" + profile.getId() + "/picture?type=large";
            System.out.println("FAcebook Picture Url: " + fbimageurl);
            if (mUser.getEmail() != null) {
                if (mUser.getEmail().length() > 0) {
                    callLogin();
                }
            } else {
                Toast.makeText(MainActivity.this, "Sorry, we can not reach to your registered Email ID," +
                        " Please try other login service or register manually.", Toast.LENGTH_LONG).show();
            }

        }

/*
     * You can override other methods here:
     * onThinking(), onFail(String reason), onException(Throwable throwable)
     */

    };

    public void callLogin() {

        System.out.println("callLogin() is called.");

        RequestQueue queue = Volley.newRequestQueue(this);
        Dialog.show();
        StringRequest myReq = new StringRequest(Request.Method.POST,
                Constants.LOGIN,
                createMyReqSuccessListener(),
                createMyReqErrorListener()) {


            protected Map<String, String> getParams() throws com.android.volley.AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String userdata = mGson.toJson(mUser);
                params.put("data", userdata);
                System.out.println("Parameteres passed: " + params);
                return params;
            }

            ;
        };
        queue.add(myReq);

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                System.out.println("createMyReqSuccessListener() is called.");
                Dialog.dismiss();

                Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
                Log.e("result", "" + response);
                Register register = mGson.fromJson(response, Register.class);

                if (register.getResponseCode() == 1) {
                    try {
                        Store_pref mStore_pref = new Store_pref(getApplicationContext());
                        mStore_pref.setUser(register.getUser());
                        mStore_pref.setUserId(register.getUser().getId());

                        Intent i = new Intent(MainActivity.this, NavigationActivity.class);
                        i.putExtra("email", mUser.getEmail());
                        startActivity(i);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (register.getResponseCode() == 2) {
                    //                Log.e("onProfileListener.MainActivity.java", "Data is passed");
                    Intent mIntent = new Intent(getApplicationContext(), SignupActivity.class);
//                mIntent.putExtra("Name", mUser.getName());
//                mIntent.putExtra("Email", mUser.getEmail());
                    mIntent.putExtra("UserData", mUser);
                    startActivity(mIntent);
                    finish();
                }
                Toast.makeText(MainActivity.this, register.getResponseMsg(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("createMyReqErrorListener() is called.");
                Dialog.dismiss();
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }


        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        // Showing status
        if (status == ConnectionResult.SUCCESS) {

        } else {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_PROGRESS, mSignInProgress);
    }
}
